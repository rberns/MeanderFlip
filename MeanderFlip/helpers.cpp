#include "helpers.hpp"

/*
 * The accept function should be provided a probability between 0 and 1.
 */
// bool accept(double probability) {
//     #warning this function should be checked on accuracy. (I think it is)
//     long max_rand_num = RAND_MAX;
//     long discrete_prob = (long)round((double)(max_rand_num+1) * probability);
//     long rand = std::rand();
//     //std::cout << rand << "\n";
//     return rand <= (discrete_prob - 1);
// }
// bool accept(double probability) { 
//     double rand = RandomGenerator::get()->random_uniform(0.0, 1.0);

//     //std::cout << "accept when: " << (probability <= rand)  << " "  << probability << " " << rand << "\n";
//     return (rand <= probability);
// }

// bool accept(int n_true, int n_total) {
//     size_t index = std::rand() % (n_total);
//     return index <= n_true - 1;
// }

Edge * mutable_edge(Edge edge, std::vector<Edge> & edges) {
    return (&edges[edge.id-1]);
}

Edge next(Edge edge, const std::vector<Edge> & edges) {
    return edges[edge.next-1];
}

Edge previous(Edge edge, const std::vector<Edge> & edges) {
    return edges[edge.previous-1];
}

Edge adjacent(Edge edge, const std::vector<Edge> & edges) {
    return edges[edge.adjacent-1];
}

Edge cw(Edge edge, const std::vector<Edge> & edges) {
    return adjacent(next(edge, edges), edges);
}

int number_of_edges_of_vertex(Edge edge, const std::vector<Edge> & edges) {
    int num = 1;
    
    Edge current_edge = cw(edge, edges);

    while (current_edge.id != edge.id)
    {
        num++;
        current_edge = cw(current_edge, edges);
    }
    return num;
}

#warning this function can be simplified by number_of_shore_edges_of_vertex_between
int number_of_shore_edges_of_vertex(Edge edge, const std::vector<Edge> & edges) {
    int num = 1;
    
    Edge current_edge = cw(edge, edges);

    while (current_edge.id != edge.id)
    {
        if(current_edge.is_shore) {
            num++;
        }
        current_edge = cw(current_edge, edges);
    }
    return num;
}

EdgeCount number_of_shore_edges_of_vertex_between(Edge from_edge, Edge to_edge, const std::vector<Edge> & edges) {
    int num = 0;
    int num_end = 0;
    
    Edge current_edge = cw(from_edge, edges);

    while (current_edge.id != to_edge.id)
    {
        if(current_edge.is_shore) {
            num++;
        }
        if(current_edge.is_end) {
            num_end++;
        }
        current_edge = cw(current_edge, edges);
    }
    return {num, num_end};
}

// Edge random_edge(const std::vector<Edge> & edges) {
//     size_t size = edges.size();

//     //size_t index = std::rand() % size;

//     int index = RandomGenerator::get()->random_int(0, size-1);

//     //int index = RandomGenerator::main()->random_int(0, edges.size()-1);

//     return edges[index];
// }

// the next edge of non-shore edge should ALWAYS be a shore edge.
// Edge random_shore_edge(const std::vector<Edge> & edges) {
//     Edge edge = random_edge(edges);
//     if(edge.is_shore == false) {
//         edge = next(edge, edges);
//     }
//     assert(edge.is_shore == true);
//     return edge;
// }

std::vector<std::vector<int> > get_all_paths(std::vector<Edge> & edges) {
    std::vector<bool> visited_faces(edges.size()/4, false);
    
    std::vector<std::vector<int> > paths;

    Edge current_edge;
    
    int path_id = 0;

    for (int i = 0, endi = edges.size(); i < endi; ++i) {
        current_edge = edges[i];
        if (!visited_faces[current_edge.face_id-1]) {
            paths.push_back(std::vector<int>());
            while (!visited_faces[current_edge.face_id-1]) {
                visited_faces[current_edge.face_id-1] = true;
                paths[path_id].push_back(current_edge.face_id);
                while (current_edge.is_shore == false)
                {
                    current_edge = next(current_edge, edges);
                }
                current_edge = next(adjacent(current_edge, edges), edges);                
            }
            path_id++;
        }
    }
    return paths;
}





// RandomGenerator* RandomGenerator::instance = nullptr;

// RandomGenerator * RandomGenerator::get() {
//     if (!instance)
//     instance = new RandomGenerator();
//     return instance;
// }

// RandomGenerator * RandomGenerator::get(unsigned int seed) {
//     if (!instance)
//     instance = new RandomGenerator(seed);
//     return instance;
// }

bool RandomGenerator::accept() {
    return g() % 2;
}

int RandomGenerator::rand_int(int min, int max) {
    std::uniform_int_distribution<int> distribution(min, max);
    return distribution(g);
}

double RandomGenerator::uniform(double min, double max) {
    std::uniform_real_distribution<double> distribution(min, max);
    return distribution(g);
}



// old code


int count_zero(int a, int b) {
    return (int)(a == 0) + (int)(b == 0);
}



std::string paint(std::string str, int color) {
    switch(color) {
    case 0:
        return "\033[0;31m" + str + "\033[0m";
        break;
    case 1:
        return "\033[0;32m" + str + "\033[0m";
        break;
    case 2:
        return "\033[0;33m" + str + "\033[0m";
        break;
    case 3:
        return "\033[0;34m" + str + "\033[0m";
        break;
    case 4:
        return "\033[0;35m" + str + "\033[0m";
        break;
    default:
        return "\033[0;30m" + str + "\033[0m";
    }
    
}

/*
 * Printing a meander system in a human-readable way
 * 
 * By 1. Di Francesco, P., Golinelli, O. & Guitter, E. Meander, folding, and arch statistics. Mathematical and Computer Modelling vol. 26 (1997).
 * a meander is defined as closed self-avoiding connected loop (road) which intersects the line (river) through 2n bridges.
 * The following notation is introduced: M_n^(k) in which n corresponds to the order of the meander
 * and k corresponds to the number of closed connected non-intersecting but possibly interlocking loops which
 * cross the river through a total of 2n bridges.
 */
void debug_pretty_print_paths(const std::vector<std::vector<int> > & paths, int n_faces) {
    std::vector<std::string> upper(n_faces, " ");
    std::vector<std::string> lower(n_faces, " ");

    bool is_upper = false;
    
    int meander_order = n_faces/2; // n_faces corresponds in this implementation to the number of bridges.

    int path_id = 1;

    std::cout << "M_n^(k) = M_" << meander_order  << "^(" << paths.size() << ")\n";
    for (auto& path : paths) {
            
            int path_len = path.size();
            if(DEBUG) {
                std::cout << "path of size: " << path_len << "\n";
            }
            for(int i = 0; i < path_len; i++) {
                std::string next_character;
                std::string previous_character;
                if (is_upper) {
                    next_character = (path[i] > path[(i+1) % path_len]) ? "\\" : "/";
                    previous_character = (path[(i-1+path_len) % path_len] > path[i % path_len]) ? "\\" : "/";
                    upper[path[i]-1] = paint(next_character, path_id);
                    lower[path[i]-1] = paint(previous_character, path_id);
                } else {
                    next_character = (path[i] > path[(i+1) % path_len]) ? "/" : "\\" ;
                    previous_character = (path[(i-1+path_len) % path_len] > path[i % path_len]) ? "/" : "\\" ;
                    lower[path[i]-1] = paint(next_character, path_id);
                    upper[path[i]-1] = paint(previous_character, path_id);

                }
                //std::string last_character = path[(i - 1 + path_len) % path_len] > path[i] ?  "\\" : "/" ;
                
                

                is_upper = !is_upper;

                //std::cout << i << " : " << next_character << "\n";

                

                // if (is_upper)
                // {
                    
                // } else {

                // }
                
            }

            //std::cout << "\n";
            path_id++;
    }
    std::cout << "\n";
    for (auto& e : upper) {
        std::cout << e;
    }
    std::cout << "\n";
    for (int i = 0; i < upper.size(); i++) {
        std::cout << i+1;
    }
    std::cout << "\n";
    for (auto& e : lower) {
        std::cout << e;
    }
    std::cout << "\n\n";
}