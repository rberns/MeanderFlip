import numpy as np
import pandas as pd

from uncertainties import ufloat
import sigfig
# [x, y, z, r, q, s, t, ....]
def gewogen_gemiddelde2(x):
    return np.array(gewogen_gemiddelde(x.T[0], x.T[1]))


def gewogen_gemiddelde(x, Sx):
    gewichten = np.power(Sx, -2)
    gewogen_gem = np.sum(x*gewichten)/np.sum(gewichten)
    fouten_op_gemiddelde = np.power(np.sum(gewichten), -1/2)
    return [gewogen_gem, fouten_op_gemiddelde]


def round(num, on):
    return (np.round(num / on)*on)


def output_latex(df, name):
    with open("{}.txt".format(name), "w") as file:
        print(df.to_latex(na_rep='', column_format='c'), file=file)


def errornumtostr(number):
    return ('{:L}'.format(ufloat(number[0], number[1])))


def formatted_errornumstostrs(numbers, formatting="${:g} \pm {:g}$"):
    out = []
    for number in numbers:
        #out.append('${:L}$'.format(ufloat(number[0], number[1])))
        val, err = sigfig.round(float(number[0]), uncertainty=float(number[1]), cutoff=29, sep=tuple)
        out.append(formatting.format(val, err))
        

    return np.array(out)
#def generate_latex():
#    print(latextable.draw_latex(table, caption="A comparison of rocket features."))
