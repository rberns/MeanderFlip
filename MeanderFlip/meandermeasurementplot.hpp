//
//  meandermeasurementplot.hpp
//  meandermeasurement
//
//  Created by Rutger Berns on 09/06/2022.
//

#ifndef meandermeasurement_hpp
#define meandermeasurement_hpp

#include "meanderrunner.hpp"
#include "histogram.hpp"
#include "json.hpp"
#include "stopwatch.hpp"


class MeanderStringSusceptibility: public MeanderMeasurement
{
private:
    // sample rate
    int n_samples = 1;
    uint64_t batching_size = 1;
    int histogram_size;
    //int index_in_batch = 0;
    int batch_index = 0;

    // susceptibility data
    std::vector<Histogram> hists;
    //Histogram hist;

public:
    MeanderStringSusceptibility(MeanderRunner * runner);

    // setup
    void setup_(nlohmann::json setup_data);

    // string susceptibility
    void measure_();
    //void measure_string_susceptibility();
    void measure_string_susceptibility_full();
    //void measure_string_susceptibility2();
    // data
    std::vector<std::tuple<std::vector<uint64_t>, std::vector<uint64_t> > > data();
    nlohmann::json json_();

    void empty();

    std::vector<uint64_t> N();

    void reset();
};


#endif