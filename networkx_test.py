# %%
import networkx as nx


# %%
G = nx.DiGraph()

# %%

G.add_node(1)
G.add_node(2)
G.add_node(3)

# %%

G.add_edge(1, 2)
G.add_edge(2, 3)
G.add_edge(3, 2)

# %%

G.add_edge(1, 2)
G.add_edge(1, 2)
G.add_edge(1, 2)

#%%

nx.draw(G)

# %%
for u, v, d in G.edges(data=True):
    d['weight'] = c[u, v]