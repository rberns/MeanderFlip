#include "stopwatch.hpp"

void Stopwatch::start() {
    start_time = std::chrono::steady_clock::now();
    elapsed_time = 0;
}

void Stopwatch::pause() {
    auto end_time = std::chrono::steady_clock::now();
    elapsed_time += std::chrono::duration_cast<std::chrono::nanoseconds> (end_time-start_time).count();
    start_time = end_time;
}

void Stopwatch::reset() {
    elapsed_time = 0;
}

void Stopwatch::resume() {
    start_time = std::chrono::steady_clock::now();
}

uint64_t Stopwatch::nanoseconds() {
    return elapsed_time;
}

Stopwatch::Stopwatch(/* args */)
{
}

Stopwatch::~Stopwatch()
{
}
