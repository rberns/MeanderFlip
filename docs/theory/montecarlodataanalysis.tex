% !TEX root = ../thesis.tex

\clearpage

% In Monte Carlo simulations we want to measure the properties in the Markov
% Chain. In this section several observables $f: \Gamma \rightarrow \mathbb{R}$
% will be discussed. The Markov-chain and observable was first mentioned in
% section \ref{section:theory:montecarlo}. 

\subsection{Practical Markov-Chain Monte Carlo}
\label{theory:montecarlodataanalysis}

One can only perform measurements when the probability distribution is stable,
and the samples should be independent of one another to get accurate error
estimates. The analysis of Markov chain Monte Carlo data involves several steps:
finding the equilibration time, finding the correlation time and estimating the
error. These practicalities will be described in this section.

It is best to start with small system sizes on which many flips can be performed
in little time. This way, one can check that the Markov chain is discovering the
entire state space and thus ergodicity holds \cite{Joseph2019}. 


% The samples of a Markov Chain Monte Carlo simulations are correlated since each
% new configurations is generated from the previous configuration.

% There are a few ways of finding this autocorrelation time and we will go into
% two methods in the following section.

% The following section will be based on a few papers and two books, Krauth
% Algorithms and Computations and Monte Carlo Methods in Statistical Physics by
% Newman/Barkema.




%     \item Measuring the autocorrelation time, where does the formula of autocorrelation come from
%     \item What is the error on the data that is measured an autocorrelation time apart.
%     \item Relation between autocorrelation and thinning aka blocking method aka batching -> chopping experiment up.



% the correlation time is sampled with a sample density of 32 and looking at 1024
% sweeps
 
% \mdfsetup{skipabove=\topskip,skipbelow=\topskip} \newrobustcmd\ExampleText{%
% An \textit{inhomogeneous linear} differential equation has the form \begin{align}
% L[v ] = f, \end{align}
% where $L$ is a linear differential operator, $v$ is the dependent variable, and $f$ is a given non−zero function of the independent variables alone.
% }

% \subsection{Measuring string susceptibility}

% Measuring interval 128 sweeps, and take 10000 measurements per file.

% \subsection{Measuring Hausdorff dimension}

% The autocorrelation for the distance profiles at $q = 1$ turns out to be about
% $512 - 1024$, an estimated meausurement interval is taken of $1024$ meausurements.
\subsubsection{Convention and notation}

\begin{figure}[htb!]
    \centering
    \begin{tikzpicture}[node distance = 2cm]
    %\node (denscalc) [lblock, align=center, minimum height=3cm] {Sample \\ density \\ calculator};
    \node (mcmcsampler) [lblock, left color=rbprimarycolor, right color=rbrbluee, align=center] {{\sffamily \color{white} \textbf{MCMC}} \\ {\sffamily \color{white} \textbf{sampler}}};

    \node (observable) [block, align=center, right of=mcmcsampler, xshift=3cm] {Observable \\ $f: \Gamma \rightarrow \mathbb{R}$};

    \node (collector) [block, align=center, right of=observable, xshift=3cm] {Collector \\ $(x_1, x_2, \dots, x_N) \in \mathcal{D}$};

    %\node (result) [block, align=center, right of=collector, xshift=2cm] {Result \\ ($\bar{x} \pm S_{\bar{x}}$)};


    \draw [arrow] (mcmcsampler) -- node[anchor=south] {$X_i \in \Gamma$} (observable);

    \draw [arrow] (observable) -- node[anchor=south] {$x_i \in \mathbb{R}$} (collector);

    %\draw [arrow] (collector) -- node[anchor=south] {} (result);

    %\draw [arrow, yshift=-0.5cm] (-3,0) -- node[anchor=south] {$k$ (rate)} ([yshift=-0.5cm]mcmcsampler.west);
    %\draw [arrow, yshift=0.5cm] (-3,0) -- node[anchor=south] {} ([yshift=0.5cm]mcmcsampler.west);
    %\draw [arrow] (gfpl) -- node[anchor=south] {$q_1 \rightarrow 0$} (multim);
    %\draw [arrow] (multim) -- node[anchor=west] {$q_2 \rightarrow 0$} (mea);
    
    
    \end{tikzpicture}
    \caption{Performing a Markov chain Monte Carlo Experiment}
    \label{fig:theory:conventions}
    \end{figure}

As defined in section \ref{section:theory:montecarlo}, the Markov chain can be
represented by $X_1,\ X_2,\ X_3, \dots \in \Gamma$ where $X_i$ is a state in the
Markov chain. One can perform measurements on these states $X_i$ using an
observable $f: \Gamma \rightarrow \mathbb{R}$, for example $f(X_i) =
k_{\text{components}}(X_i)$. For simplicity from here on, $x_i$ shall refer to a
sample of an observable in the dataset $\mathcal{D}$ of $N$ measurements. The
idea is sketched in Figure \ref{fig:theory:conventions} where $x_i = f(X_i) \in
\mathcal{D}$.

The values of a variable $x_i$ will be distributed according to a certain
probability distribution $P(x)$. One property to characterise this probability
distribution is the standard deviation, $\sigma$, which is the square root of
the variance. $\overline{\cdot \cdot \cdot}$ indicates an average over $N$ data
points and $\langle\cdot\cdot\cdot\rangle$ indicates the average over a
distribution. The mean is then defined as $\mu \equiv \langle x \rangle$ and the
standard deviation is $\sigma^2  \equiv \langle (x - \langle x \rangle)^2
\rangle $ \cite{Young2012}. This notation is based on a summary by Peter Young
about data analysis \cite{Young2012}.

The goal is estimate $\mu$ and $\sigma$ using the data points. For that the
sample mean, $\bar{x}$, and sample variance, $s^2$, are defined by:

\begin{equation}
    \bar{x} = \frac{1}{N} \sum_{i = 1}^N x_i
\end{equation}

\begin{equation}
    s^2 = \frac{1}{N} \sum_{i = 1}^N (x_i - \bar{x})^2
    \label{results:eq:standarddeviationfromdata}
\end{equation}
where $N$ is the number of data points \cite{Young2012}.

Performing many repetitions of the experiment gives the best estimate of the
mean $\mu$ as

\begin{equation}
    \mu = \bar{x}
\end{equation}

and the best estimate for the variance, $\sigma^2$, by

\begin{equation}
    \sigma^2 = \frac{N}{N-1}s^2 = \frac{1}{N-1} \sum_{i = 1}^N (x_i - \bar{x})^2
    \label{theory:eq:bestestimateofsigma}
\end{equation}
which follows from equation \eqref{results:eq:standarddeviationfromdata}.

One can now define the standard error, $S_{\bar{x}}$ which is the standard
deviation on the mean, $\bar{x}$ given by
\begin{equation}
    \mu = \bar{x} \pm S_{\bar{x}}
\end{equation}
where, if all $x_i$ and $x_j$ are statistically independent, $S_{\bar{x}}$ is
given by 

\begin{align}
    S_{\bar{x}} &= \frac{\sigma}{\sqrt{N}} = \frac{s}{\sqrt{N-1}} \label{theory:eq:standarderrorfirst} \\
    S_{\bar{x}} &= \sqrt{\frac{1}{N(N-1)} \sum_{i = 1}^N (x_i - \bar{x})^2} \label{theory:eq:standarderror}
\end{align}
where $\sigma$ is the standard deviation and $N$ is the number of data points
\cite{Young2012}. Equation \eqref{theory:eq:standarderror} follows from equation
\eqref{theory:eq:standarderrorfirst} by substituting equation
\eqref{theory:eq:bestestimateofsigma}. The standard error given by equation
\eqref{theory:eq:standarderror} is unbiased \cite{Young2012}. The notation
$S_{\bar{x}}$ is chosen to really emphasize that it is a statistical error or
standard deviation of the mean and NOT the standard deviation. For a detailed
and clear derivation of the quantities described have a look at \cite{Young2012}
by \citeauthor{Young2012}.

\subsubsection{Equilibration time}
Since a Markov chain depends on its history and the initial
conditions, the chain has to equilibrate. The unequilibrated part of the
Markov chain must be removed in order to only work with a properly equilibrated
chain. This means that the equation to calculate the expectation value for the
observable $f$ must be slightly changed to

\begin{equation}
    \overline{f} =  \frac{1}{n-b} \sum_{i=b+1}^{n} f(X_i)
\end{equation}
where $X_i$ is a state in the Markov chain and where $b$ is the cut-off.

The first variable to find is the equilibration time $\tau_{\mathrm{eq}}$ and
choose $b$ well above the equilibration time $\tau_{\mathrm{eq}}$. Time in these
Markov chains are the number of performed flips. For simplicity, this can be
expressed in the number of sweeps which is the average number of flips per
element of the system. In the case of meanders, the number of sweeps will be
defined as the average number of flips per bridge. So a $\mathrm{sweep}$ will be
defined as $2n$ flips. 

\begin{equation}
    1 \ \mathrm{sweep} = 2n \ \text{flips}
\end{equation}
where $n$ refers to the order of the meander. A sweep corresponds performing on
average one flip per bridge or face. 


%\subsubsection{}
\subsubsection{Correlation time}

The Markov chain is statistical method that uses randomness to approximate a
certain distribution. The Markov-chain changes when one performs flips.
Performing $x$ flips can be interpreted as a time-axis and samples on this
time-axis are correlated. The correlation means that the state $X_i$ and $X_j$
in the Markov chain are correlated, and thus not statistically independent. The
standard method of estimating the statistical error given by equation
\eqref{theory:eq:standarderror} is invalid for a Markov chain.

For an observable $f$, one can quantify this correlation by the
autocorrelation function. The autocorrelation function is the normalized
autocovariance given by 

\begin{equation}
    \Gamma_a = \frac{1}{N-a} \sum_{k = 1}^{(N-a)} (f_k - \langle f \rangle)(f_{k+a} - \langle f \rangle)
    \label{theory:eq:montecarlo:autocovariance}
\end{equation}
% \begin{equation}
%     \Gamma_a = \frac{1}{N-a} \sum_{k = 1}^{(N-a)} (f_k - \overline{x})(f_{k+a} - \overline{x})
%     \label{theory:eq:montecarlo:autocovariance}
% \end{equation}
and the autocorrelation function is given by
\begin{equation}
    \rho_a = \frac{\Gamma_a}{\Gamma_0}
    \label{theory:eq:montecarlo:autocorrelationfunction}
\end{equation}

The normalized sample autocovariance that estimates the normalized
autocovariance is given by

\begin{equation}
    \gamma_a = \frac{1}{N-a} \sum_{k = 1}^{(N-a)} (x_k - \bar{x})(x_{k+a} - \bar{x})
\end{equation}
where $x_i \in \mathcal{D}$ and the sample autocorrelation function is then
given by \cite{BuddCourse2021, Joseph2019}
\begin{equation}
    \bar{\rho}_a = \frac{\gamma_a}{\gamma_0}
\end{equation}

The autocorrelation function can be approximated by an exponential
$\exp{(-t/\tau)}$ where $\tau$ is the so-called autocorrelation time. One
method of determining the autocorrelation time is therefore by fitting an
exponential to the autocorrelation function \cite{BuddCourse2021}. The other
method is using $\tau_{\text{int}}$ which is the integrated autocorrelation
time given by
\cite{Joseph2019}

\begin{equation}
    \tau_{\text{int}} = \frac{1}{2} + \sum_{a = 1}^{M} \rho_a
    \label{theory:eq:montecarlo:tauint}
\end{equation}

Taking $M \rightarrow \infty$ would give a high uncertainty since we would
consider the autocorrelation over a small section of the Markov chain (as can be
seen from equation \eqref{theory:eq:montecarlo:autocovariance}) which causes a
higher variance. To get around this problem a cut-off should be chosen
\cite{Joseph2019}. One can choose $M$ as the first $M$ that satisfies

\begin{equation}
    M \geq 4 \tau_{\text{int}} + 1
\end{equation}
for which $\tau_{\text{int}}$ is the autocorrelation time, so $\tau_{\text{int}}
= \tau$. 

Fitting an exponential to the autocorrelation function is demonstrated in Figure
\ref{fig:theory:autocorrexp}. In Figure \ref{fig:theory:autocorrexp}, the
easiest method is for fitting an exponential is used: namely by checking where
the exponential intersects with $1/e$. This works because at $t = \tau$,
$e^{-t/\tau} = 1/e$. The other method is using least-mean squares/regression and
transform to a logarithmic scale for that, however a cut-off should be
implemented since the tail of the autocorrelation function will be noisy
\cite{Newman1999}. In Figure \ref{fig:theory:tintmethod}, fitting based on the
$\tau_\mathrm{int}$-method is demonstrated by finding the first $M$ for which $M
\geq 4 \tau_\mathrm{int} + 1$.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/autocorrelationtimefit.pdf}
    \begin{subfigure}[b]{0.55\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents1.pdf}
    \caption{}\label{fig:theory:autocorrexp}
    \end{subfigure}
    \begin{subfigure}[b]{0.43\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents2.pdf}
    \caption{}\label{fig:theory:tintmethod}
    \end{subfigure}
    \caption{Measurements of the autocorrelation time $\tau$ in
    $\mathrm{sweeps}$. (a) The autocorrelation time is measured by finding the
    intersection with $\gamma/\gamma_0 = 1/e$. (b) The autocorrelation time is
    fitted using the $\tau_\mathrm{int}$-method. The measured autocorrelation
    times by averaging $24$ measurements are (a) $\tau = 0.498 \pm 0.006$ and
    (b) $\tau = 0.516 \pm 0.008$. The different measurements are indicated with
    the dotted lines.}
    \label{results:plot:autocorrelationtimefit}
\end{figure}

Using the autocorrelation of an observable of the Markov chain, the statistical
error of the Markov chain can be studied. The promise of this method is to be
more effective than binning strategies which will be later introduced
\cite{Joseph2019}.



The statistical error $S_{\bar{x}}$ for an observable $f$ with a correlated
dataset $(x_1, x_2, \dots, x_N) \in \mathcal{D}$ is given by

\begin{equation}
    S_{\bar{x}} = \sqrt{\frac{1 + 2\tau/\Delta t}{n-1} \left(\frac{1}{N} \sum_{i = 1}^N (x_i - \bar{x})^2\right)}
    \label{theory:eq:montecarlo:erroranalysiswithautocorr}
\end{equation}
where $\tau$ is the autocorrelation time that was defined earlier and $\Delta t$
is the measurement interval. The measurement interval is introduced such that
the information saved is less ambiguous because the measurements will be less
correlated. Equation \eqref{theory:eq:montecarlo:erroranalysiswithautocorr} has
some properties, if the autocorrelation time $\tau$ is very small, one finds the
usual definition for the statistical error, like in equation
\eqref{theory:eq:standarderror}. When $\tau \gg \Delta t$ and $n \gg 1$,
equation \eqref{theory:eq:montecarlo:erroranalysiswithautocorr} can be
simplified. First, use $n \gg 1$ and $n = \frac{t_\text{max}}{\Delta t}$ which
gives

\begin{equation}
    S_{\bar{x}} = \sqrt{\frac{\Delta t(1 + 2\tau/\Delta t)}{t_\text{max}}s^2}
\end{equation}

and now use $\tau/\Delta t \gg 1$ resulting in

\begin{equation}
    S_{\bar{x}} = \sqrt{\frac{2\tau}{t_\text{max}}s^2}, S_{\bar{x}} = \sqrt{\frac{2\tau}{n \Delta t}s^2}
    \label{theory:eq:montecarlo:errorconclusion}
\end{equation}
where it must be emphasized that this formula is \textbf{only valid under the
assumptions}: $n \gg 1$ and $\tau \gg \Delta t$, i.e. many samples per
autocorrelation time $\tau$ and the measurement interval must be way smaller
than the autocorrelation time. Equation
\eqref{theory:eq:montecarlo:errorconclusion} shows that one can choose the most
convenient measurement interval $\Delta t$. We should however keep the
\textbf{assumptions} in mind \cite{Newman1999}.



Taking the measurement interval $\Delta t$ to be equal to $2\tau$ is the most
natural definition of statistical independence \cite{Newman1999}. This follows
from taking $\Delta t = 1$ in equation
\eqref{theory:eq:montecarlo:errorconclusion} which results in 

\begin{equation}
    S_{\bar{x}} = \sqrt{\frac{2\tau}{n}s^2}
    \label{theory:eq:montecarlo:errordeltatone}
\end{equation}
Taking $\Delta t = 2\tau$ would have resulted in the standard formula for the
statistical error of the observable $f$. This is not true, since $\Delta t =
2\tau$ does not satisfy $\tau/\Delta t \gg 1$. The correct error for samples
taken at $\Delta t = 2\tau$ can be derived from equation
\eqref{theory:eq:montecarlo:erroranalysiswithautocorr}, which yields

\begin{equation}
    S_{\bar{x}} = \sqrt{\frac{2\sigma^2}{n}} = \sqrt{2 \left(S_{\bar{x}}^{\text{statistically independent}}\right)^2}
\end{equation}



\clearpage

\subsection{Error analysis}
\label{section:sub:erroranalysis}

In this section binning, bootstrap, stationary bootstrap and jackknife will be
introduced as another way to estimate statistical errors.

\subsubsection{Blocking, binning, bunching or batching}
\label{subsection:blockingbinning}
The binning method, also known as blocking method or batching method, splits the
Markov chain into $N_b$ equal sized bins of length $m$ \cite{Newman1999,
BuddCourse2021, Krauth2006, Janke2002}. By taking the average of each bin, one
can interpret the $N_b$ averages as performing the experiment $N_b$ times. The
opinion about binning for estimating errors is mixed. Although, with the right
procedures and checks, binning can give good results. As described by Krauth in
\cite{Krauth2006}, it is useful to do binning for different bin sizes. In the
case of working below the correlation time of the Markov chain the error will be
underestimated since the averages of the bins will then also be correlated. For
bin sizes larger than the autocorrelation time, one expects to find the same and
correct error. This can be seen in Figure \ref{results:plot:componentsbinning}
where the error flattens when the bin size $m$ is sufficiently large. For very
large bin sizes the error of the error will increase because the total number of
bins $N_b$ will be low. The batching method will thus give good results for bin
sizes way larger than the autocorrelation time (while still having sufficient
bins to get an accurate statistical error). 

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/binning.pdf}
    \caption{Demonstration of the binning method on a Markov chain. The
    observable is the number of components, $k$. The statistical error versus
    the bin sizes is plotted in the diagram. We can clearly identify the
    statistical error increasing for larger bin sizes and thus fewer bins. The
    region where bins are independent can also be identified. The properties of
    the system: a measurement interval $\Delta t = 1$, size $n = 128$ and number
    of samples $N = 2^{27} = 1,34218 \cdot 10^{18}$. Smallest bin size $m = 2$
    and smallest number of bins $N_b = 32$.}
    \label{results:plot:componentsbinning}
\end{figure}



\subsubsection{Bootstrap method}
\label{subsection:bootstrap}

The bootstrap method is a resampling method of a dataset $\mathcal{D}$ of size
$n$, $x_1, x_2, x_3, \dots, x_n$, one samples randomly $n$ data points resulting
in a new dataset $x_{\mathrm{RNG}(1)}, x_{\mathrm{RNG}(2)}, \dots,
x_{\mathrm{RNG}(n)}$. In the new dataset, there can be \textbf{duplicate}
elements, on average $63\%$ will be duplicates. From this new resampled dataset,
one can again calculate the average denoted by $c_i$ \cite{Newman1999}.
Typically, one resamples $m$ times, so $m$ new datasets and $m$ averages, $c_1,
c_2, \dots, c_m$. Lastly, the final statistical error is calculated by:

\begin{equation}
    S_{\bar{x}} =  \sqrt{\sum_{i = 1}^{n}(c_i - c)^2} = \sqrt{\overline{c^2} - \bar{c}^2}
\end{equation}

The Bootstrap method promises to also handle correlated data
\cite{Newman1999}.

A slightly modified version of the Bootstrap method is Stationary bootstrap.
Stationary Bootstrap adds a resampling probability $p$. The resampling
probability results in resampling subsequences of typical lengths $1/p$ of the
original data \cite{BuddCourse2021}. Here insert.....\cite{Politis1994}. 

% TODO


\subsubsection{Jackknife method}
\label{subsection:jackknife}

The jackknife method, somewhat similar to leave-one-out cross-validation,
creates $n$ datasets from the original data set, but the $i^{\mathrm{th}}$
element of the original dataset is left out. The averages of these datasets can
be called $c_i$. The error estimate is then given by \cite{Newman1999}

\begin{equation}
    S_{\bar{x}} = \sqrt{\sum_{i = 1}^{n}(c_i - c)^2} = \sqrt{\overline{c^2} - \bar{c}^2}
    \label{erroranalysis:eq:jackknife}
\end{equation}

Jackknife is best fit for smaller datasets since it creates $n$ new datasets
from one dataset of length $n$. Jackknife only works with uncorrelated data
\cite{Newman1999}. Jackknife is typically used to calculate the statistical
error of the final results of an experiment conducted $10$ times.

\clearpage
\subsection{Performing  Monte Carlo simulations}

In the previous section, it was discussed that Markov chains are correlated.
Storing information about every state in the Markov chain is inefficient. It is
best to measure at a rate $r$, as described in Figure
\ref{fig:thermalizationtime}.

Two good variables to control are the number of sweeps and the number of
measurements per sweep. These two variables have a natural relation with the
autocorrelation and statistical error and thus one can intuitively change the
accuracy. Here these quantities are related to each other.

% source https://tex.stackexchange.com/questions/429451/k-fold-cross-validation-figure-using-tikz-or-table
\begin{figure}[htb!]
    \centering
\begin{tikzpicture}
    \matrix (M) [matrix of nodes,
        nodes={minimum height = 7mm, minimum width = 2cm, outer sep=0, anchor=center, draw, fill=rbrblueb!50},
        column 1/.style={nodes={draw=none, fill=none}, minimum width = 4cm}, column 2/.style={nodes={minimum width = 3cm, fill=rbprimarycolor!30}}, column 6/.style={nodes={minimum width = 2.5cm}},
        row sep=1mm, column sep=-\pgflinewidth, nodes in empty cells,
        e/.style={fill=rbrblueb!50}
      ]
      {
        Markov chain &  &  &  &  & \\
      };
        \draw ([yshift=2mm]M-1-2.north west) coordinate (LT) edge[|<->|, >= latex] node[above]{Total number of $\mathrm{sweeps}$} ([yshift=2mm]M-1-6.north east);
        \draw ([yshift=-2mm]M-1-2.south west) coordinate (LT) edge[|<->|, >= latex] node[below]{$b$} ([yshift=-2mm]M-1-2.south east);
        \draw ([yshift=-2mm]M-1-3.south west) coordinate (LT) edge[|<->|, >= latex] node[below]{$r$} ([yshift=-2mm]M-1-3.south east);
        \draw ([yshift=-2mm]M-1-4.south west) coordinate (LT) edge[|<->|, >= latex] node[below]{$r$} ([yshift=-2mm]M-1-4.south east);
        \draw ([yshift=-2mm]M-1-5.south west) coordinate (LT) edge[|<->|, >= latex] node[below]{$r$} ([yshift=-2mm]M-1-5.south east);
        \draw ([yshift=-2mm]M-1-6.south west) coordinate (LT) edge[|<->|, >= latex] node[below]{$\ldots$} ([yshift=-2mm]M-1-6.south east);
        \draw ([yshift=-0.4cm]M-1-3.south west) edge[-, >= latex] node[anchor=south, yshift=-1cm]{$X_1$} ([yshift=-1cm]M-1-3.south west);
        \draw ([yshift=-0.4cm]M-1-4.south west) edge[-, >= latex] node[anchor=south, yshift=-1cm]{$X_2$} ([yshift=-1cm]M-1-4.south west);
        \draw ([yshift=-0.4cm]M-1-5.south west) edge[-, >= latex] node[anchor=south, yshift=-1cm]{$X_3$} ([yshift=-1cm]M-1-5.south west);
        \draw ([yshift=-0.4cm]M-1-6.south west) edge[-, >= latex] node[anchor=south, yshift=-1cm]{$X_4$} ([yshift=-1cm]M-1-6.south west);
        \draw ([yshift=-0.4cm]M-1-6.south east) edge[-, >= latex] node[anchor=south, yshift=-1cm]{$X_n$} ([yshift=-1cm]M-1-6.south east);
  \end{tikzpicture}
  \caption{Markov chain Monte Carlo simulation setup, where $b \geq \tau_{\mathrm{eq}}$}
  \label{fig:thermalizationtime}
\end{figure}

$\rho_\mathrm{samples}$ is the number of measurements per sweep (density).
This translates to a rate $r = \frac{2n}{\rho_\mathrm{samples}}$ and a total
number of measurements $n_\mathrm{measurements} =
\frac{n_\mathrm{sweeps}2n}{r}$.
