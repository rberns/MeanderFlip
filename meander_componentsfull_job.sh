#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=200M
#SBATCH --time=23:59:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 1

cd ~/MeanderFlip/
python3 MeanderFlipRunnerComponentsFull.py -q 2.0 -s 512 -c 32 -p 1 -o componentsfull -f 262144