import numpy as np
import matplotlib.pyplot as plt

ballA = np.array([1, 0, 0])
ballB = np.array([3, 0, 0])
vA = np.array([0,0,0])
vB = np.array([0,0,0])

def force(k, r):
    """func to calculate force

    Args:
        k (float): force constant
        r (3-vector): x vector

    Returns:
        3-vector: force
    """
    return -k*(r-1.5*r/np.linalg.norm(r))

dt = 0.01

ballAcoords = []
ballBcoords = []

for i in range(400):
    F_a = force(1, ballA-ballB)
    vA = vA + dt * F_a
    ballA = ballA + dt * vA

    F_b = force(1, ballB-ballA)
    vB = vB + dt * F_b
    ballB = ballB + dt * vB

    ballAcoords.append(ballA)
    ballBcoords.append(ballB)


print(ballAcoords)
print(ballBcoords)

plt.plot(ballAcoords)
plt.plot(ballBcoords)
plt.show()