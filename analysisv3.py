import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import json
from scipy.interpolate import interp1d
from scipy.optimize import minimize
from numpy.linalg import eig
from numpy.linalg import matrix_power

avg = dict()

Nbins = 1000
Nmax = 10000

bin_edges = None

def analyser(array, N):
    
    print(N)
    global bin_edges
    pNarr = []

    

    for row in array:
        matrix = np.array(row["matrix"])
        print(matrix)
        w, v = eig(matrix_power(matrix, 10))
        print(w)
        plt.scatter(np.arange(N), np.abs(w))
        plt.show()
        t = np.arange(2, 4000, 10)
        #print(matrix_power(matrix, 100))
        p = []
        
        for t_ in t:
            p.append(np.trace(matrix_power(matrix, t_))/N)
        print(np.array(p))
        logp = np.nan_to_num(np.log10(np.array(p)))
        indices = np.nonzero(np.log10(t) < 2) # < 3 for B
        print(indices)
        #ax = np.where(logp < 4, p.log10(t))
        graph = np.polyfit(np.log10(t[indices]), logp[indices], 1)
        print(graph)

        plt.plot(np.log10(t), logp)
        plt.plot(np.log10(t), np.poly1d(graph)(np.log10(t)))
        #plt.yscale("log")
        plt.title("N: {} and $d_s$ = {}".format(N, graph[0]*-2))
        #plt.xscale("log")
        plt.show()

        

        # plt.plot(np.log10(t), np.gradient(np.nan_to_num(np.log10(np.array(p))), 10))
        # plt.show()
        #hist, bin_edges = np.histogram(row, bins=(10 ** np.logspace(0, np.log10(Nmax), Nbins)))
        row_ = np.array(row["steps"])
        hist, bin_edges = np.histogram(row_, bins=Nbins, range=(0, Nmax)) 
        bin_edges = bin_edges
        pN = hist/len(row)
        pNarr.append(pN)

    pNarrNum = np.array(pNarr)
    pNavg = np.mean(pNarrNum, axis=0)
    
    avg[N] = pNavg




with open('data_spectral_D.json') as json_file:
    data = json.load(json_file)

    print(data.keys())

    for (key, value) in data.items():
        analyser(value, int(key))


for (key, value) in avg.items():
    #x = np.linspace(0, Nmax, Nbins-1)
    x = np.linspace(0, Nmax, Nbins)
    #x = np.logspace(0, np.log10(Nmax), Nbins-1)
    plt.plot(np.log10(x), np.log10(value), label="N: {}".format(key))
    #plt.yscale("log")
    #plt.xscale("log")
    f = interp1d(x, value)    

plt.legend()
plt.show()



# for (key, value) in avg.items():
#     x = np.linspace(0, 38, 200)
#     f = interp1d(np.arange(39), value, kind='cubic')
#     plt.plot(x, f(x), label="Interpolated N: {}".format(key))

# plt.legend()
# plt.show()


# dH = 3.56
# for (key, value) in avg.items():
#     N = key
#     x = np.linspace(0, 38, 200)
#     f = interp1d(np.arange(39), value, kind='cubic')
#     plt.plot(x*N**(-1/dH), f(x)*N**(1/dH), label="Interpolated N: {}".format(key))

# plt.legend()
# plt.show()

# # Fitting

# def fitfunc(dH, curves):

#     yvals = []

#     x = np.linspace(0, 38, 200)

#     for (key, value) in curves.items():
#         N = key
#         f = interp1d(np.arange(39), value, kind='cubic', bounds_error=False, fill_value=0)
#         yvals.append(f(x*N**(1/dH))*N**(1/dH))

#     yvals = np.array(yvals)

#     #print(yvals)

#     #print(np.std(yvals, axis=0))

#     #plt.plot(x,np.std(yvals, axis=0))
#     return np.max(np.std(yvals, axis=0))

# #fitfunc(3.56)



# res = minimize(fitfunc, 3, args=(avg))

# print(res)

# dH = res.x[0]
# for (key, value) in avg.items():
#     N = key
#     x = np.linspace(0, 38, 200)
#     f = interp1d(np.arange(39), value, kind='cubic')
#     plt.plot(x*N**(-1/dH), f(x)*N**(1/dH), label="Interpolated N: {}".format(key))

# plt.legend()
# plt.show()