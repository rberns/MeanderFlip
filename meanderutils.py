import pandas as pd
import numpy as np
import json
from os.path import getctime
import glob
from regex import L
import scipy.special
from practicum import *

from pathlib2 import Path



class SimFile(object):
    def __init__(self, file_name):
        self.file_name = file_name
      
    def __enter__(self):
        self.file = open(self.file_name)
        return json.load(self.file)
  
    def __exit__(self, exc_type, exc_value, exc_tb):
        self.file.close()

def getsims(q, n, latest=False, pre="default"):
    #print("simdata/q{:.4f}/n{}/sim_*".format(q, n))
    if not latest:
        return glob.glob("simdata/{}/q{:.4f}/n{}/sim_data_*".format(pre, q, n))
    else:
        return max(glob.glob("simdata/{}/q{:.4f}/n{}/sim_data_*".format(pre, q, n)), key=getctime)

def getanalysis(q, n):
    return glob.glob("simdata/q{:.4f}/n{}/dist.pkl".format(q, n))


# class SimData(object):
#     def __init__(self, q, n, susceptibility=False):
#         self.q = q
#         self.n = n
#         self.susceptibility = susceptibility
      
#     def __enter__(self):
#         self.files = []
#         data = []
#         for sim_path in getsims(self.q, self.n, pre="sim_datastring" if self.susceptibility else "sim_data"):
#             file = open(sim_path)
#             self.files.append(file)
#             data.append(json.load(file))

#         return data
  
#     def __exit__(self, exc_type, exc_value, exc_tb):
#         for file in self.files:
#             file.close()

class SimDataIterator(object):
    def __init__(self, q, n, name="default"):
        self.q = q
        self.n = n
        self.name = name
        self.paths = getsims(self.q, self.n, pre=self.name)

    def __iter__(self):
        return self

    def __next__(self):
        if len(self.paths) == 0:
            raise StopIteration
        path = self.paths.pop(0)
        #print("left =", len(path))
        with open(path) as file:
            data = json.load(file)

        return data


class SimDataIterator(object):
    def __init__(self, q, n, name="default"):
        self.q = q
        self.n = n
        self.name = name
        self.paths = getsims(self.q, self.n, pre=self.name)

    def __iter__(self):
        return self

    def __next__(self):
        if len(self.paths) == 0:
            raise StopIteration
        path = self.paths.pop(0)
        # print("left =", len(path))
        with open(path, "r") as file:
            data = json.load(file)

        return data
# class SimDiagnostics(object):
#     def __init__(self, q, n):
#         self.q = q
#         self.n = n
      
#     def __enter__(self):
#         self.files = []
#         data = []
#         for sim_path in getsims(self.q, self.n, pre="sim_diagnostics"):
#             file = open(sim_path)
#             self.files.append(file)
#             data.append(json.load(file))

#         return data
  
#     def __exit__(self, exc_type, exc_value, exc_tb):
#         for file in self.files:
#             file.close()

# class SimAnalysis(object):
#     def __init__(self, q, n):
#         self.q = q
#         self.n = n
#         self.file_name = getanalysis(q, n)[0]
      
#     def __enter__(self):
#         return pd.read_pickle(self.file_name)
  
#     def __exit__(self, exc_type, exc_value, exc_tb):
#         pass

def string_susceptibility_(c):
    if c <= 1:
        return (c-1-np.sqrt((c-1)*(c-25)))/12
    else:
        return 1/2

def central_charge(q):
    e = (1.0/np.pi)*np.arccos(q/2.0)
    
    return -1.0-(6.0*(e**2.0))/(1.0-e)

string_susceptibility = np.vectorize(string_susceptibility_)


def conjectured_hausdorff(c):
    if c <= 1:
        return 2*(np.sqrt(25-c)+np.sqrt(49-c)) / (np.sqrt(25-c)+np.sqrt(1-c))
    else:
        return 2

def gamma_to_central_charge(gamma):
    return 25 - 6 * (2/gamma + gamma/2)**2

def central_charge_to_gamma(c):
    g_s = string_susceptibility(c)
    gamma = np.sqrt(4/(1-g_s))

    return gamma

def conjectured_hausdorff_DG(c):
    g = central_charge_to_gamma(c)
    if c > 1:
        return 2
    return 2 + (g**2)/2 + g/np.sqrt(6)

conjectured_hausdorff_DG = np.vectorize(conjectured_hausdorff_DG)


def catalan(n):
    #return (1/(n+1))*scipy.special.binom(2*n, n)
    return (1/(n+1))*scipy.special.comb(2*n, n, exact=False)

from sympy import catalan

def p_analytic(x, N):
    #b = np.arange(n+1)
    return (2*(catalan(x)*catalan(N-x)/catalan(N))**2).evalf()
    #return #2*(catalan(x)*catalan(N-x)/catalan(N))**2

p_analytic = np.vectorize(p_analytic)

def histogram_to_coordinates(hist, hist_sq):
    vals = []
    for i, (val, val_sq) in enumerate(zip(hist, hist_sq)):
        if val != 0:
            vals.append(np.array([(i+1), val, val_sq]))
    return np.array(vals)

def string_hist(q, n, name):
    print("STRINGHIST v2")
    N = 2*n
    arr = np.zeros(n)
    arr_sq = np.zeros(n)
    n_indep = 0
    n_accepted = 0
    n_iterations = 0
    n_avg_autocorrelationtime_ = 0
    t_flipping = 0
    t_measuring = 0

    n_files = 0

    for measurement in SimDataIterator(q=q, n=n, name=name):
        n_files += 1
        
        n_accepted += measurement["accepted"]
        n_iterations += measurement["iter"]
        t_flipping += measurement["time_flipping"]
        version = measurement["version"]
        
        print("version:", version)
        
        for measure in measurement["measurements"]:
            if measure["measurement_type"] != 2:
                continue
            
            
            n_avg_autocorrelationtime_ += measure["rate"]
            t_measuring += measure["t_measuring"]
            
            if version >= 8:
                # warning we here assume that we only take the first histogram, so only batching_size = 1 support
                n_indep += measure["metadata"]["n_independent"][0]
                #print("VERSION 8")
                data = np.array(measure["data"])
                #print(data[0,0,:].shape)
                #print(data[0,0,:].shape, data[0,1,:].shape)
                arr += data[0,0,:]
                arr_sq += data[0,1,:]
            else:
                n_indep += measure["metadata"]["n_independent"]
                for [x, y, y_sq] in measure["data"]:
                    arr[int((x-1)/2)] += y
                    arr_sq[int((x-1)/2)] += y_sq

    n_avg_autocorrelationtime = n_avg_autocorrelationtime_ / n_files
    n_avg_autocorrelationtime_sweeps = n_avg_autocorrelationtime / (2*n)

    hist = histogram_to_coordinates(arr, arr_sq).T

    x = hist[0]
    y = hist[1]
    y_sq = hist[2]

    d = {'x': hist[0], 'y': hist[1], 'y_sq': hist[2]}
    df = pd.DataFrame(data=d)

    y_err, y_std = calc_error(y, y_sq, n_indep)

    df["n"] = y / n_indep
    df["n_err"] = y_err
    df["n_std"] = y_std

    total_time_hr = (t_flipping + t_measuring) / (1_000_000_000 * 3600)

    measuring_percentage = (t_measuring / t_flipping) * 100

    time_per_sweep = total_time_hr / n_indep * 3600

    acceptance_rate = n_accepted / n_iterations * 100

    time_per_flip_us = ((t_flipping) / n_iterations) / 1000

    #print("STRINGHIST v2 END")

    return [df, {"n_indep": n_indep, "n_accepted": n_accepted, "n_iterations": n_iterations, "n_avg_autocorr_sweeps": n_avg_autocorrelationtime_sweeps, "t_flipping": t_flipping, "t_measuring": t_measuring, "n": n, "q": q, "n_faces": 2*n, "total_time (hr)": total_time_hr, "measuring_percentage (%)": measuring_percentage, "time_per_sweep (s)": time_per_sweep, "time_per_flip (us)": time_per_flip_us, "acceptance_rate (%)": acceptance_rate}]

def calc_error(arr, arr_sq, N):
    return [1/(np.sqrt(N))*np.sqrt(arr_sq/N - ((1/N)*arr)**2), np.sqrt(arr_sq/N-((1/N)*arr)**2)]

def string_hist_full_data(q, n):
    print("STRINGHIST v2")
    N = 2*n
    
    n_indep = 0
    n_accepted = 0
    n_iterations = 0
    n_avg_autocorrelationtime_ = 0
    t_flipping = 0
    t_measuring = 0

    n_files = 0

    cols = np.arange(n-1)+1

    dfdata  = pd.DataFrame(data=None, index=cols)

    for i, measurement in enumerate(SimDataIterator(q=q, n=n, susceptibility=True)):
        n_files += 1
        
        n_accepted += measurement["accepted"]
        n_iterations += measurement["iter"]
        t_flipping += measurement["time_flipping"]
        
        for measure in measurement["measurements"]:
            if measure["measurement_type"] != 2:
                continue

            n_indep += measure["metadata"]["n_independent"]
            n_avg_autocorrelationtime_ += measure["rate"]
            t_measuring += measure["t_measuring"]

            arr = np.zeros(n-1)
            arr_sq = np.zeros(n-1)

            for [x, y, y_sq] in measure["data"]:
                arr[int((x-1)/2)] += y
                arr_sq[int((x-1)/2)] += y_sq

            dfdatasub  = pd.DataFrame(data=None, index=cols)

            dfdatasub["y_{}".format(i)] = arr
            dfdatasub["y_sq_{}".format(i)] = arr_sq

            dfdata = pd.concat([dfdata, dfdatasub], axis=1)

            

    n_avg_autocorrelationtime = n_avg_autocorrelationtime_ / n_files
    n_avg_autocorrelationtime_sweeps = n_avg_autocorrelationtime / (2*n)

    #hist = histogram_to_coordinates(arr, arr_sq).T

    #x = hist[0]
    #y = hist[1]
    #y_sq = hist[2]

    #d = {'x': hist[0], 'y': hist[1], 'y_sq': hist[2]}
    #df = pd.DataFrame(data=d)

    #y_err, y_std = calc_error(y, y_sq, n_indep)

    #df["n"] = y / n_indep
    #df["n_err"] = y_err
    #df["n_std"] = y_std

    total_time_hr = (t_flipping + t_measuring) / (1_000_000_000 * 3600)

    measuring_percentage = (t_measuring / t_flipping) * 100

    time_per_sweep = total_time_hr / n_indep * 3600

    acceptance_rate = n_accepted / n_iterations * 100

    time_per_flip_us = ((t_flipping) / n_iterations) / 1000

    print("STRINGHIST v2 END")

    return [dfdata, {"n_indep": n_indep, "n_accepted": n_accepted, "n_iterations": n_iterations, "n_avg_autocorr_sweeps": n_avg_autocorrelationtime_sweeps, "t_flipping": t_flipping, "t_measuring": t_measuring, "n": n, "q": q, "n_faces": 2*n, "total_time (hr)": total_time_hr, "measuring_percentage (%)": measuring_percentage, "time_per_sweep (s)": time_per_sweep, "time_per_flip (us)": time_per_flip_us, "acceptance_rate (%)": acceptance_rate}]


def autocorrelationmeasurement(q, n):
    print("STRINGHIST v2")
    N = 2*n
    
    #n_indep = 0
    n_accepted = 0
    n_iterations = 0
    n_avg_autocorrelationtime_ = 0
    t_flipping = 0
    t_measuring = 0

    n_files = 0

    dfdata = pd.DataFrame()

    for i, measurement in enumerate(SimDataIterator(q=q, n=n, susceptibility=True)):
        

        #n_accepted += measurement["accepted"]
        #n_iterations += measurement["iter"]
        #t_flipping += measurement["time_flipping"]
        
        for measure in measurement["measurements"]:
            if measure["measurement_type"] != 3:
                continue
        
            n_accepted += measurement["accepted"]
            n_iterations += measurement["iter"]
            t_flipping += measurement["time_flipping"]
            n_files += 1
            
            #n_indep += measure["metadata"]["n_independent"]
            n_avg_autocorrelationtime_ += measure["rate"]
            t_measuring += measure["t_measuring"]

            dfdata["{}".format(i)] = measure["data"]



            

    n_avg_autocorrelationtime = n_avg_autocorrelationtime_ / n_files
    n_avg_autocorrelationtime_sweeps = n_avg_autocorrelationtime / (2*n)

    #hist = histogram_to_coordinates(arr, arr_sq).T

    #x = hist[0]
    #y = hist[1]
    #y_sq = hist[2]

    #d = {'x': hist[0], 'y': hist[1], 'y_sq': hist[2]}
    #df = pd.DataFrame(data=d)

    #y_err, y_std = calc_error(y, y_sq, n_indep)

    #df["n"] = y / n_indep
    #df["n_err"] = y_err
    #df["n_std"] = y_std

    total_time_hr = (t_flipping + t_measuring) / (1_000_000_000 * 3600)

    measuring_percentage = (t_measuring / t_flipping) * 100

    #time_per_sweep = total_time_hr / n_indep * 3600

    acceptance_rate = n_accepted / n_iterations * 100

    time_per_flip_us = ((t_flipping) / n_iterations) / 1000

    print("STRINGHIST v2 END")

    return [dfdata, {"n_accepted": n_accepted, "n_iterations": n_iterations, "n_avg_autocorr_sweeps": n_avg_autocorrelationtime_sweeps, "t_flipping": t_flipping, "t_measuring": t_measuring, "n": n, "q": q, "n_faces": 2*n, "total_time (hr)": total_time_hr, "measuring_percentage (%)": measuring_percentage, "time_per_flip (us)": time_per_flip_us, "acceptance_rate (%)": acceptance_rate}]

def metadata_creator(measurement):
    t_flipping = measurement["time_flipping"]
    t_measuring = measurement["measurements"][0]["t_measuring"]
    n_iterations = measurement["iter"]
    n_accepted = measurement["accepted"]
    q = measurement["q"]
    seed = measurement["seed"]
    n = measurement["n"]
    rate = measurement["measurements"][0]["rate"]

    total_time_hr = (t_flipping + t_measuring) / (1_000_000_000 * 3600)

    measuring_percentage = (t_measuring / t_flipping) * 100

    acceptance_rate = n_accepted / n_iterations * 100

    time_per_flip_us = ((t_flipping) / n_iterations) / 1000

    out = {"seed": seed, "n_accepted": n_accepted, "n_iterations": n_iterations, "rate": rate, "t_flipping": t_flipping, "t_measuring": t_measuring, "n": n, "q": q, "n_faces": 2*n, "total_time (hr)": total_time_hr, "measuring_percentage (%)": measuring_percentage, "time_per_flip (us)": time_per_flip_us, "acceptance_rate (%)": acceptance_rate}

    return pd.DataFrame.from_dict(out, orient='index')



def split_array_in_batches(arr, batch_size):
    remainder = arr.shape[0] % batch_size
    #print(remainder, arr.shape[0])
    #print("remainder", remainder)
    if remainder != 0:
        arr = arr[:-remainder]
        #print("cutted")
    #print(len(arr), batch_size)
    # back of array will be cut if batch size does not fit the length.
    return arr.reshape(int(len(arr)/batch_size), batch_size)

def error(sequence, k, batch_size):
    c = np.where(sequence == k, 1, 0)
    
    #print(k, sequence)
    #print(c)
    #length = len(c)

    



    #n_batches = int(length/batch_size)

    #print("batches:", n_batches, "size of batch:", batch_size)

    #c = c.reshape((int(length/batch_size), batch_size))
    c = split_array_in_batches(c, batch_size)

    n_batches = c.shape[1]

    means = np.mean(c, axis=1)

    mean = np.mean(means)

    squared_sum = np.sum(np.square(means))/n_batches
    sum_squared = np.square(np.sum(means/n_batches))

    err = (1/(np.sqrt(n_batches)))*np.sqrt(squared_sum-sum_squared) 
    
    return [mean, err]

# Error plot
def errordata(k):
    k = np.array(k)
    max_iter_log2 = int(np.log2(len(k)))

    x = np.power(2, np.arange(5, max_iter_log2))
    y = np.zeros(x.shape)
    yerr = np.zeros(x.shape)

    avg = int(np.mean(k))

    for i, batches in enumerate(x):
        
        mean, std = error(k, avg, batches)

        y[i] = mean
        yerr[i] = std

    return [x, yerr, y]

def sample_autocovariance(x, tmax, steps, rate):
    '''Compute the autocorrelation of the time series x for t = 0,1,...,tmax-1.'''
    x_shifted = x - np.mean(x)
    t = np.linspace(0, tmax/rate, num=steps, dtype=int)
    t_out = np.linspace(0, tmax, num=steps)
    return t_out, np.array([np.dot(x_shifted[:len(x)-t_], x_shifted[t_:])/len(x) for t_ in t])

#@jit(nopython=True)
def find_correlation_time(t, autocov):
    '''Return the index of the first entry that is smaller than autocov[0]/e.'''
    return t[autocov < np.exp(-1)*autocov[0]][0]


def spliterate(buf, chunk):
    for start in range(0, len(buf), chunk):
        yield buf[start:start + chunk]

#@jit(nopython=True)
def batcher(continuous_data, min_batch_size, max_batch_size, n_batches=100, log_scale=True, base=2):
    mean = np.zeros(n_batches)
    error = np.zeros(n_batches)

    if log_scale:
        x = np.logspace(np.log2(min_batch_size), np.log2(max_batch_size), n_batches, dtype=int, base=2)
        print(x)
    else:
        x = np.linspace(min_batch_size, max_batch_size, n_batches, dtype=int)


    for i, chunk_size in enumerate(x):
        #print(i, chunk_size)
        means = []

        
        batches = spliterate(continuous_data, chunk_size)

        n_batches = 0
        for tmp in batches:
            n_batches += 1
            #print(tmp)
            means.append(np.mean(tmp))


        means = np.array(means)

        
        # mean = np.mean(means)

        squared_sum = np.sum(np.square(means))/n_batches
        sum_squared = np.square(np.sum(means/n_batches))

        err = (1/(np.sqrt(n_batches-1)))*np.sqrt(squared_sum-sum_squared) 

        mean[i] = np.mean(means)
        error[i] = err

    return x, mean, error


def sweeps(val, n):
    return 2*n*val


def find_max_size_array(arrays):
    max_size = 0
    for array in arrays:
        max_size = max(max_size, len(array))
    return max_size

def array_merger(arrays):
    new_size = find_max_size_array(arrays)

    hist = np.zeros(new_size)

    for hist_ in arrays:
        h = hist_.copy()
        h.resize(new_size)
        hist += h
        
    return hist

# https://people.duke.edu/~ccc14/sta-663/ResamplingAndMonteCarloSimulations.html

def jackknife(x, func):
    """Jackknife estimate of the estimator func"""
    n = len(x)
    idx = np.arange(n)
    return np.sum(func(x[idx!=i]) for i in range(n))/float(n)

def jackknife_var(x, func):
    """Jackknife estiamte of the variance of the estimator func."""
    n = len(x)
    idx = np.arange(n)
    j_est = jackknife(x, func)
    return (n-1)/(n + 0.0) * np.sum((func(x[idx!=i]) - j_est)**2.0
                                    for i in range(n))

rng = np.random.default_rng()  

def resample(samples,p=1):
    if p==1:
        return rng.choice(samples,len(samples))
    n = len(samples)
    resamples = []
    index = rng.integers(n)
    #print(index)
    for i in range(n):
        index = rng.integers(n) if rng.random() < p else (index+1)%n
        resamples.append(samples[index])
    return resamples

def bootstrap_estimate(data, observable, num=100,p=1):
    
    measurements = [observable(resample(data,p)) for _ in range(num)]
    
    return np.mean(measurements), np.std(measurements)

def batching_error(data, batch_size):
    c = split_array_in_batches(data, batch_size)

    n_batches = c.shape[0]

    means = np.mean(c, axis=1)

    mean = np.mean(means)

    err = 1/(np.sqrt(n_batches-1))*np.std(means)
    
    return [mean, err, n_batches]


def collector_func(parameters):
    q, n = parameters
    print("fitting:", q, n)
    n_size = n
    N = 2*n_size
    try:
        df, metadata = string_hist(q=q, n=n_size, name="stringsusceptibility")
    except Exception as e:
        #print("{} at n = {}, q = {}".format(e, n, q))
        #global errors
        #errors.append("{} at n = {}, q = {}".format(e, n, q))
        #raise e
        return "{} at n = {}, q = {}".format(e, n, q)

    n_indep = metadata["n_indep"]

    df["total_time (hr)"] = metadata["total_time (hr)"]
    df["measuring_percentage (%)"] = metadata["measuring_percentage (%)"]
    df["time_per_sweep (s)"] = metadata["time_per_sweep (s)"]
    df["time_per_flip (us)"] = metadata["time_per_flip (us)"]
    df["acceptance_rate (%)"] = metadata["acceptance_rate (%)"]



    df["avgn"] = df["y"]/(n_indep)

    df["Savgn"] = df["n_err"]

    df["Slogavgn"] = df["Savgn"] / df["avgn"]

    df["p"] = df["avgn"] / n

    df["n"] = n
    df["q"] = q
    df["n_indep"] = n_indep
    df["transx"] = df["x"]*(1-df["x"]/n)

    return df