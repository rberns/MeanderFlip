
#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=1000M
#SBATCH --time=6-23:59:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 8 -w cn113

cd ~/MeanderFlip/
python3 MeanderFlipRunnerDistanceCorr.py -q 0.1 0.2 0.4 0.6 0.8 1.0 -s 128 512 2048 4096 -p 8 -c 4 -f 512 -o distancecorr3 -d 128
