//
//  meanderpermutation.hpp
//  MeanderPermutation
//
//  Created by Rutger Berns on 21/11/2021.
//

#ifndef meanderpermutation_hpp
#define meanderpermutation_hpp

//#define NAIVE_DTREE

#include "planarmap.hpp"
#include "helpers.hpp"
#include <set>
#include <map>
#include "dtree/type.h"
#include "dtree/seq.h"
#include "dtree/seq-inl.h"
#include "puremeanderflip.hpp"
#include <assert.h>
#include "meander.hpp"

#if(DEEPDEBUGPERM)
#define PRINTDEBUG(x) (this->print());
#else
#define PRINTDEBUG(x);
#endif

typedef dtree::WithAggr<dtree::Count<int>, dtree::Begin<>> C;

typedef dtree::WithReverse<C> R;

typedef dtree::EndSeq<R> Node;



struct DirectionalCutter
{
    int direction;
    bool should_cut;
};

class MeanderPermutation
{
private: 
    //std::vector<Bridge> bridges;
    Node * connections_;
    std::set<int> roots;
    int n_bridges;
    std::map<Node *, int> translator;

    FlipMap last_flip;

    DirectionalCutter direction(Node * from_node, Node * to_node);
    void simple_connect(int left_bridge, int right_bridge);
    void simple_connect(Node * left_node, Node * right_node);
    void reverse(Node * node);
    void add_node(int id);
    void remove_node(int id);
    void add_node(Node * node);
    void remove_node(Node * node);
    void add_root(Node * node);
    void remove_root(Node * node);
    int id(Node * node);
    Node * get(int id);
public:

    Node * bridge_node(int bridge);
    int disconnect(int from_bridge, int to_bridge);
    int disconnect(Node * from_node, Node * to_node);
    int shiftcut(int from_bridge, int to_bridge);
    int shiftcut(Node * from_node, Node * to_node);
    void flip(int first, int cw, int afirst, int acw);
    void undo();

    MeanderPermutation(int n_bridges);
    ~MeanderPermutation();
    void from(const std::vector<Edge> & edges);
    std::vector<int> get_sizes();
    void print();
    void print(Node * node);
    int k_components();
    std::vector<int> get_path(int node);
    std::vector<std::vector<int> > get_paths();
    std::vector<std::vector<int> > get_norm_paths(); // O(n)
};




#endif /* meanderpermutation_hpp */
