% !TEX root = ../thesis.tex
\clearpage

\section{Meanders as a conformal field theory coupled to two-dimensional quantum gravity}

Matter can be introduced to 2DQG by coupling 2DQG to a statistical system, like
meanders. Meanders are conjectured to be a conformal field theory coupled to
two-dimensional quantum gravity (2DQG). Di Francesco et al. introduced this conjecture
in \cite{DiFrancescoExactAsym2000} and later numerical evidence and theoretical
arguments were put forward in references \cite{DiFrancescoNumCheck2000,
DiFrancesco2005}. The basic idea is that the meanders can be seen as two
fully-packed loops on a random geometry. In the continuum limit, these
fully-packed loop models are described by a conformal field theory. In the
following section, the background of meanders as a CFT coupled to 2DQG will be
discussed. The goal is to explain how the central charge of that CFT is derived
by \citeauthor{DiFrancescoNumCheck2000} Using the KPZ formula introduced in
section \ref{section:criticalrelationskpz}, the central charge can be used to
predict the critical exponents of meanders coupled to 2DQG. 

First, fully-packed loop models on a regular lattice will be introduced in
section \ref{theory:fullypackedloopmodel}, then in section
\ref{theory:fullypackedloopmodelgravity}, the two fully-packed loop models will
be coupled to 2DQG, which will then be called $\mathrm{GFPL}^2$ model. The two
fully-packed loop models are denoted by the $\mathrm{FPL}^2$ model. By taking
the limits of parameters of the fully-packed loop model coupled to 2DQG, one can
recover meanders and meanders systems. These steps are also displayed in Figure
\ref{fig:theory:theoreticalstepsfpl}.

The intuitive idea of the following section is that the central charge is an
indication for the number of degrees of freedom. The type of lattice and the
properties of meanders influence the number of degrees of freedom and therefore
the central charge. Coupling to 2DQG is a change of lattice for the CFT that was
first defined on a regular lattice. Studying a theory in its gravity-form is
studying it on a random lattice instead of a regular lattice.

\begin{figure}[htb!]
\centering
\begin{tikzpicture}[node distance = 2cm]
\node (fpl) [block] {$\mathrm{FPL}^2 (q_1, q_2)$-model};
\node (gfpl) [block, right of=fpl, xshift=3cm] {$\mathrm{GFPL}^2 (q_1, q_2)$-model};
\node (multim) [block, right of=gfpl, xshift=3cm, yshift=1cm] {Multimeanders};
\node (mea) [block, right of=gfpl, xshift=3cm, yshift=-1cm] {Meanders};

\draw [arrow] (fpl) -- node[anchor=south] {2DQG} (gfpl);
\draw [arrow] (gfpl) -- node[anchor=south] {$q_1 \rightarrow 0$} (multim);
\draw [arrow] (multim) -- node[anchor=west] {$q_2 \rightarrow 0$} (mea);


\end{tikzpicture}
\caption{Steps to relate $\mathrm{FPL}^2$-model to meanders. The $\mathrm{FPL}^2$-model
can be transformed into the $\mathrm{GFPL}^2$-model by coupling the model to
two-dimensional quantum gravity. The coupling is realised by replacing the
square lattice of the $\mathrm{FPL}^2$-model with random quadrangulations of the
2-sphere. The $\mathrm{GFPL}^2$-model can again be related to meanders and
meander systems by taking the parameter $q_1 \rightarrow 0$ and $q_2 \rightarrow
0$.}
\label{fig:theory:theoreticalstepsfpl}
\end{figure}


\clearpage
\subsection{Fully-packed loop models}
\label{theory:fullypackedloopmodel}

The connection of meanders to two-dimensional quantum gravity starts by discussing
fully-packed loop models. \textbf{Fully-packed} means that every vertex is
visited. In the case of fully-packed loop models, all vertices are visited by a
self-avoiding random walk exactly once \cite{Jacobsen1998, Batchelor1996}. Here
fully packed loop models will be studied on a square lattice which can be seen
in Figure \ref{theory:FPLmodeldrawing:lattice}. The procedure described here is
based on the work of \citeauthor{Jacobsen1998} and
\citeauthor{DiFrancescoNumCheck2000} \cite{Jacobsen1998,
DiFrancescoNumCheck2000}.


\begin{figure}[htb!]
\centering
\begin{subfigure}[b]{.40\linewidth}
\includegraphics[width=\linewidth, page=2]{theory/assets/FPL2ModelLattice.pdf}
\caption{}\label{theory:FPLmodeldrawing:lattice}
\end{subfigure}
\begin{subfigure}[b]{.40\linewidth}
\includegraphics[width=\linewidth, page=1]{theory/assets/FPL2ModelLattice.pdf}
\caption{}\label{theory:FPLmodeldrawing:fpl2}
\end{subfigure}



\caption{(a) Square lattice (b) $\mathrm{FPL}^2$-model short for
two-flavored fully packed loop model. The \textcolor{rbprimarycolor}{red}
loops refer to roads and the \textcolor{rblinkcolor}{blue} loops refer to the rivers.
\textbf{Note:} the lattices have periodic boundary conditions in up/down and
left/right directions.}



\label{theory:fig:FPLmodeldrawing}



\end{figure}

\begin{figure}[htb!]
    \centering
    \begin{tikzpicture}
        \draw[color=rbprimarycolor, ultra thick] (1,1) -- (1,-1) node[below, color=black]{a};
        \draw[color=rbprimarycolor, ultra thick] (4,1) -- (4,-1);
        \draw[color=rbprimarycolor, ultra thick] (4,0) -- (5,0);
        % River
        \draw[color=rblinkcolor, ultra thick] (0,0) -- (2,0);
        \draw[color=rblinkcolor, ultra thick] (3,0) -- (4,0);
        \draw[color=rblinkcolor, ultra thick] (4,0) -- (4,-1) node[below, color=black]{b};
    \end{tikzpicture}
    \caption[]{Vertices of the fully-packed loop model, (a) "crossing" vertex and (b) "avoiding" vertex.}
    \label{theory:pic:fplvertices}
    \end{figure}

Meanders have two distinct types of loops, a river and a road, in other words
two flavors of loops. It is therefore natural to look at the two-flavored
fully-packed loop model ($\mathrm{FPL}^2$-model). The $\mathrm{FPL}^2$-model can
be described on a square lattice in which two loops visit every vertex once, so
two \textbf{fully-packed} loop models. At every vertex the loops can avoid or
cross, see the two vertex types in Figure \ref{theory:pic:fplvertices}.
Moreover, two weights $q_1$ and $q_2$ are assigned to the river and the road
loop respectively \cite{DiFrancescoFoldingColoring2000}. The model is critical
for weights $0 \leq q_1, q_2 \leq 2$, within this area every point $(q_1, q_2)$
has different universality class when coupled to 2DQG. By assigning weights to
the river and road
loops results in the following partition function:

\begin{equation}
    Z_{\mathrm{FPL}^2} (q_1, q_2) = \sum_{\text{fully-packed loop configurations}}{q_1^{k_1} q_2^{k_2}}
    \label{theory:meanders:partitionfunction}
\end{equation}

where $q_1$, $q_2$ are interpreted as the weights assigned to rivers and roads
respectively \cite{DiFrancescoNumCheck2000}.

Equation \eqref{theory:meanders:partitionfunction} describes a more general case
of meanders where the number of roads and rivers are variable. The partition
function for meander systems is recovered by taking $q_1 \rightarrow 0$ which gives:

\begin{equation}
    Z (q) = \sum_{\text{configurations}}{q^{k}}
    \label{theory:meanders:partitionfunctionmultimeanders}
\end{equation}
where $q = q_2$ is the weight assigned to the roads and $k = k_2$ is the number
of roads. From equation \eqref{theory:meanders:partitionfunctionmultimeanders},
the partition function of meanders is recovered by taking the limit $q
\rightarrow 0$. 

\subsection{Finding the CFT describing the continuum limit of the $\mathrm{FPL}^2$-model}

The continuum limit of the $\mathrm{FPL}^2$-model can be described by a
conformally invariant effective field theory. The goal is to find the critical
exponents of this conformal field theory, which can be done by describing the
$\mathrm{FPL}^2$-model using a height model. A height model defines a height $h$
at each lattice site \cite{Cardy2008}. The height can be for example a scalar,
but here the height will be considered a vector. 

Every face in the lattice of Figure \ref{theory:FPLmodeldrawing:fpl2} is given a
height $h$. In other words, the height $h$ is assigned to vertices of the dual map of
Figure \ref{theory:FPLmodeldrawing:fpl2}. Now one needs to come up with a set of
height rules that define how the height $h$ changes when going to a different
faces (or vertices in the dual map). Firstly, the vertices of Figure
\ref{theory:FPLmodeldrawing:lattice} need to be bicolorable and for that the map
needs to be bipartite (see section \ref{section:graphsandplanarmaps}). This can
be easily realized for the square lattice, since a checkerboard pattern easily
allows to divide the graph up into two subsets where only vertices of a
different color will neighbor each other. The bicoloring is visualised in Figure
\ref{theory:FPLmodeldrawing:bicolorablesquarelattice}. 

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{.40\linewidth}
    \includegraphics[width=\linewidth, page=2]{theory/assets/FPL2ModelLatticeWithColor.pdf}
    \caption{}\label{theory:FPLmodeldrawing:bicolorablesquarelattice}
    \end{subfigure}
    \begin{subfigure}[b]{.40\linewidth}
    \includegraphics[width=\linewidth, page=1]{theory/assets/FPL2ModelLatticeWithColor.pdf}
    \caption{}\label{theory:FPLmodeldrawing:addeddirections}
    \end{subfigure}

\caption{Visualisation of (a) bipartite square lattice with two vertex colors: filled
\coloredvertex $\ $ and empty \coloredvertex[white] (b) $\mathrm{FPL}^2$-model
with orientation of the edges indicated by the arrows and bicolored vertices.
The \textcolor{rbprimarycolor}{red} loops refer to roads and the
\textcolor{rbbluec}{blue} loops refer to the rivers. \textbf{Note:} the lattices
have periodic boundary conditions in up/down and left/right directions.}

\label{theory:fig:FPLmodeldrawingwithcolor}
\end{figure}

Secondly, the loops can be assigned an orientation, like in Figure
\ref{theory:FPLmodeldrawing:addeddirections}. The orientation and the bicoloring
allows to define the height rules given in Figure
\ref{theory:fig:heightrules}\footnote{Note that there is a certain ambiguity in
choosing the height rules. The height rules are however chosen in such a way
that one should get $\mathbf{A} + \mathbf{B} + \mathbf{C} + \mathbf{D} =
\mathbf{0}$ when walking around a vertex. This is the same convention as in
references \cite{DiFrancescoExactAsym2000,DiFrancescoNumCheck2000} by Di
Francesco et al.}.

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{.20\linewidth}
    \includegraphics[width=\linewidth, page=1]{theory/assets/Heightrules.pdf}
    \caption{$\mathbf{h}_1 - \mathbf{h}_2 = \mathbf{A}$}\label{theory:FPLmodeldrawing:Aheight}
    \end{subfigure}
    \begin{subfigure}[b]{.20\linewidth}
    \includegraphics[width=\linewidth, page=2]{theory/assets/Heightrules.pdf}
    \caption{$\mathbf{h}_1 - \mathbf{h}_2 = \mathbf{B}$}\label{theory:FPLmodeldrawing:Bheight}
    \end{subfigure}
    \begin{subfigure}[b]{.20\linewidth}
    \includegraphics[width=\linewidth, page=4]{theory/assets/Heightrules.pdf}
    \caption{$\mathbf{h}_1 - \mathbf{h}_2 = \mathbf{C}$}\label{theory:FPLmodeldrawing:Cheight}
    \end{subfigure}
    \begin{subfigure}[b]{.20\linewidth}
    \includegraphics[width=\linewidth, page=3]{theory/assets/Heightrules.pdf}
    \caption{$\mathbf{h}_1 - \mathbf{h}_2 = \mathbf{D}$}\label{theory:FPLmodeldrawing:Dheight}
    \end{subfigure}
    
    \caption{The four height rules that determine how the height changes when
    going to an adjacent face by crossing one of four types of edges. The height
    rules are drawn as crops of a square lattice with one type of edge. $\mathbf{h}_1$ is the vector-height associated with the face
    below the edge and $\mathbf{h}_2$ with the face above the edge.}
    
    \label{theory:fig:heightrules}
\end{figure}

% The edges
% can also be assigned a value. For that one can use the Ampère convention,
% which says that if one passes an oriented edge pointing to the left (right)
% then the height is increased (decreased) by the value of the edge. The
% height rules above result in the following edge values by the Ampère
% covention: $\mathbf{A}$, -$\mathbf{B}$, $\mathbf{C}$, -$\mathbf{D}$
% respectively.

The height rules have to be well-defined. In other words, when one makes a
\textbf{closed} walk, there should be no net height change. Making a simple walk
around a vertex like in Figure \ref{theory:drawing:explanationeheightmodel}
results in the following condition on the height values:
\begin{equation}
    \mathbf{A} + \mathbf{B} + \mathbf{C} + \mathbf{D} = \mathbf{0}
    \label{theory:eq:conditiononheights}
\end{equation}
which can de derived by applying the height rules defined in Figure
\ref{theory:fig:FPLmodeldrawingwithcolor}. The procedure is simple, by starting
at $\mathbf{h}_1$ one gets $\mathbf{h}_2 = \mathbf{h}_1 - \mathbf{D}$,
$\mathbf{h}_3 = \mathbf{h}_1 - \mathbf{D} - \mathbf{A}$, $\mathbf{h}_4 =
\mathbf{h}_1 - \mathbf{D} - \mathbf{A} - \mathbf{B}$, $\mathbf{h}_1 =
\mathbf{h}_1 - \mathbf{D} - \mathbf{A} - \mathbf{B} - \mathbf{C}$ which
ultimately results in equation \ref{theory:eq:conditiononheights}.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.4\textwidth]{theory/assets/RequirementOnHeight.pdf}
    \caption{Walking around a vertex to ensure that the height is well-defined.
    $\mathbf{h}_1$, $\mathbf{h}_2$, $\mathbf{h}_3$ and $\mathbf{h}_4$ indicate
    the height of the face and $-\mathbf{D}$, $-\mathbf{A}$, $-\mathbf{B}$ and
    $-\mathbf{C}$ next to the \textcolor{rbrblued}{blue} dashed line indicate the
    change of height in the direction of the arrow.}
    \label{theory:drawing:explanationeheightmodel}
\end{figure}

From $\mathbf{A} + \mathbf{B} + \mathbf{C} + \mathbf{D} = \mathbf{0}$ can be
concluded that there are 3 degrees of freedom and thus the height, $\mathbf{h}
\in \mathbb{R}^3$, is a 3-vector. In the continuum limit, this height model was
therefore argued to become a 3D scalar field (CFT) with the following central
charge \cite{DiFrancescoNumCheck2000,Jacobsen1998, Jacobsen1999}

\begin{equation}
    c = 3 - 6\left(\frac{e_1^2}{1-e_1}+\frac{e_2^2}{1-e_2}\right)
    \label{theory:eq:centralchargefpl}
\end{equation}
where $e_1$ and $e_2$ are related to $q_1$ and $q_2$ by:

\begin{equation}
    q_i = 2 \cos{(\pi e_i)}
    \label{theory:eq:weightboltz}
\end{equation}
where $q_1$ and $q_2$ are the weights assigned to the river and road
respectively and $e_i$ is constrained by $0 \leq e_i \leq 1/2$. The derivation
of the central charge is rather technical and can be found in detail in
\cite{Jacobsen1998} by \citeauthor{Jacobsen1998}.





\subsection{Coupling the $\mathrm{FPL}^2$-model to gravity}
\label{theory:fullypackedloopmodelgravity}

Taking the limit $q_1 \rightarrow 0$ and $q_2 \rightarrow 0$ of the
$\mathrm{FPL}^2$-model does not give meanders \cite{DiFrancescoNumCheck2000,
DiFrancesco2005}. The correct statistical system can be retrieved by coupling
the $\mathrm{FPL}^2$-model to gravity, which is then called the
$\mathrm{GFPL}^2$-model. Coupling the $\mathrm{FPL}^2$-model to gravity means
that the square lattice is changed to a random planar four-valent graph, i.e.
random quadrangulations. This change is not trivial, since not every planar
four-valent graph is bicolorable and so the height rules must change. When the
bicolorability of vertices is removed, the first two edges and last two edges in
Figure \ref{theory:fig:heightrules} will be the same. The new height rules are
given in Figure \ref{theory:fig:heightrulesnonbi}. 

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{.22\linewidth}
    \includegraphics[width=\linewidth, page=1]{theory/assets/HeightrulesNonBi.pdf}
    \caption{$\mathbf{h}_1 - \mathbf{h}_2 = \mathbf{A}$}\label{theory:FPLmodeldrawingnonbi:Aheight}
    \end{subfigure}
    \begin{subfigure}[b]{.22\linewidth}
    \includegraphics[width=\linewidth, page=2]{theory/assets/HeightrulesNonBi.pdf}
    \caption{$\mathbf{h}_1 - \mathbf{h}_2 =  \mathbf{B} = -\mathbf{A}$}\label{theory:FPLmodeldrawingnonbi:Bheight}
    \end{subfigure}
    \begin{subfigure}[b]{.22\linewidth}
    \includegraphics[width=\linewidth, page=3]{theory/assets/HeightrulesNonBi.pdf}
    \caption{$\mathbf{h}_1 - \mathbf{h}_2 = \mathbf{C}$}\label{theory:FPLmodeldrawingnonbi:Cheight}
    \end{subfigure}
    \begin{subfigure}[b]{.22\linewidth}
    \includegraphics[width=\linewidth, page=3]{theory/assets/HeightrulesNonBi.pdf}
    \caption{$\mathbf{h}_1 - \mathbf{h}_2  = \mathbf{D} = -\mathbf{C}$}\label{theory:FPLmodeldrawingnonbi:Dheight}
    \end{subfigure}
    
    \caption{The four height rules that determine how the height changes when
    going to an adjacent face by crossing one of four types of edges. Note
    however that there are effectively only two edge types because the vertices
    are not bicolored. The height rules are drawn as crops of a square lattice
    with one type of edge highlighted. $\mathbf{h}_1$ is the vector-height
    associated with the face below the edge and $\mathbf{h}_2$ with the face
    above the edge.}
    
    \label{theory:fig:heightrulesnonbi}
\end{figure}

From the height rules one can also see that the new conditions to keep the
height model well-defined are $\mathbf{A}+\mathbf{B} = \mathbf{0}$ and
$\mathbf{C}+\mathbf{D}=\mathbf{0}$. Now the height model has only two degrees of
freedom, resulting in a central charge of
\begin{equation}
    c = 2 - 6\left(\frac{e_1^2}{1-e_1}+\frac{e_2^2}{1-e_2}\right)
    \label{theory:eq:centralchargegravitational}
\end{equation}
where one sees a $c \rightarrow c-1$ shift with respect to the former central
charge given by Equation \eqref{theory:eq:centralchargefpl}
\cite{DiFrancescoNumCheck2000}. 

Losing the bicolorability means that the "avoiding" vertex in Figure
\ref{theory:pic:fplvertices}b, will become irrelevant for the
$\mathrm{GFPL}^2$-model. It is argued in \cite{DiFrancescoNumCheck2000} by
\citeauthor{DiFrancescoNumCheck2000} that the disappearing of this "avoiding"
vertex is not problematic. One of the reasons is that meanders with this
"avoiding" vertex, called tangent meanders, have exactly the same universality
class as meanders. So the "avoiding"/tangency points can just be neglected.

Now one can take $q_1 \rightarrow 0$ in Equation
\eqref{theory:eq:centralchargegravitational} to get to the case of one-infinite
river, i.e. meander systems. This corresponds to $e_1 = \frac{1}{2}$ which gives
\begin{equation}
    c = -1 - 6\left(\frac{e^2}{1-e}\right)
    \label{eq:theory:finalcentralcharge}
\end{equation}
where $e_2 = e$ is substituted and $q = q_2 = 2 \cos{(\pi e)}$ where $q$ is the
weight assigned to roads in the meander systems.

Now several remarks about meanders and multi-meander systems can be made. First,
$0 \leq q \leq 2$ which translates to $c \in [-4, -1]$. A geometry decorated
with a multi-meander system can therefore be tuned such that it corresponds to a
CFT with a central charge $c \in [-4, -1]$ coupled to 2DQG. For the pure meander
limit $q \rightarrow 0$, one finds 2DQG coupled to a CFT with
central charge $c = -4$.

% tangency points are irrelevant for meanders, but that is not the case for the
% $\mathrm{FPL}^2$-model \cite{DiFrancescoNumCheck2000}.
% \citeauthor{DiFrancescoNumCheck2000} also gives several arguments why tangency
% is irrelevant for meanders \cite{DiFrancescoNumCheck2000}. Firstly, by analysing
% the case of multi-components meanders where $q_1 \rightarrow 0$ and $q_2 = 1$ it
% can be derived that the avoiding matrix is irrelevant
% \cite{DiFrancescoNumCheck2000}. Secondly, a more intuitive understanding of why
% the "avoiding" vertex is irrelevant comes from the fact that one can just
% consider it as two loops walking in parallel.

% Firstly, the most straigtforward argument is that for  $q_1 \rightarrow 0$






% \begin{figure}[htb!]
%     \centering
%     \includegraphics[width=\linewidth]{theory/assets/edgeswithoutbicolorability.pdf}
%     \caption{Vertices explaining the change of the height variable across labeled edges when the vertices are not bicolorable.}
%     \label{fig:theory:withoutbicolor}
% \end{figure}



 

\subsection{Critical exponents of meanders}
\label{theory:criticalexponentsofmeanders}

Now that the central charge of the statistical system coupled to 2DQG is known,
the KPZ formula can be used to extract the critical exponents. In Figure
\ref{fig:theory:meandercritical2}, the critical exponents $\gamma_\mathrm{s}$
and $d_H$ are plotted as a function of $q$ using Equation
\eqref{theory:observables:eq:stringcentralcharge2} and Equation
\eqref{eq:theory:finalcentralcharge}. Note however that those conjectured
formulas are only valid up to $q = 2$. For $q > 2$,
\citeauthor{DiFrancescoNumCheck2000} made several predictions. For $0 \leq q
\leq 2$ every point is expected to be in a different universality class. This
can be clearly seen because between $0 \leq q \leq 2$ the string susceptibility
and Hausdorff dimension change continuously as a function of $q$. This
\textcolor{rbprimarycolor}{red} shaded region also corresponds to the
\textcolor{rbprimarycolor}{red} region in Figure
\ref{fig:theory:meandercritical1} with the only difference the x-axis. For $q >
2$ the model is no longer critical and corresponds to the branched polymer phase
shaded in \textcolor{rbbluec}{blue}. At $q = 2$, a phase transition to the
branched polymer phase is predicted.

\begin{figure}[htb!]
    
    \centering
    \includegraphics[]{theory/assets/meanderscriticalexponents.pdf}
    \begin{subfigure}[b]{0.48\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents1.pdf}
    \caption{}\label{fig:theory:meandercritical1}
    \end{subfigure}
    \begin{subfigure}[b]{0.48\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents2.pdf}
    \caption{}\label{fig:theory:meandercritical2}
    \end{subfigure}

    \caption{(a) Central charge of coupled system versus the string
    susceptibility $\gamma_\mathrm{s}$ and the Hausdorff dimension $d_H$. For
    the Hausdorff dimension equation \eqref{eq:theory:dinggwynnehaus} was used.
    Shaded in \textcolor{rbprimarycolor}{red} are the central charges the
    $\mathrm{GFPL}^2$-model can achieve by tuning the weight $q$. (b) The string
    susceptibility $\gamma_\mathrm{s}$ and the Hausdorff dimension $d_H$ are
    plotted versus the weight $q$. The \textcolor{rbprimarycolor}{red} shaded
    area corresponds to the same range of universality classes. The same goes
    for the \textcolor{rbbluec}{blue} area. The \textcolor{rbprimarycolor}{red}
    dots in the \textcolor{rbprimarycolor}{red} shaded region correspond to the
    same universality class (spanning tree) with $c = -2$ in both plots. Note
    that the vertical-axis is the same for both plots.}
    \label{theory:fig:plotofhowqbehavesversuscritical}
\end{figure}



% In order to get the correct model for meanders, tangency points have to be
% forbidden. Several arguments for forbidding tangency points are given in
% reference \cite{DiFrancescoNumCheck2000}. It has been shown by
% \cite{DiFrancescoNumCheck2000} exactly for $q = 1$ that the tangency vertex is
% irrelevant \cite{DiFrancescoNumCheck2000} and for other values of $q$.

% You could say it is 2D free scalar field because we have two types of loops so
% two degrees of freedom, but this might be too simple (Timothy).


% A gravitational version of the fully packed loop model is meant replacement of the lattice by statistical sum over all possible fluctuations of the lattice into tesselations of surfaces of arbitrary genus. -> powerful but not completely rigorous \cite{DiFrancescoFoldingColoring2000}.

% The $\mathrm{FPL}^2$ model can be coupled to eulerian gravity in which case it
% will be expressed as a sum over genus zero tetravalent graphs with only faces of
% even valency \cite{DiFrancescoNumCheck2000}.

% TODO Height model is that you give everything a scalar weight??

% TODO Coulomb gas theory??

% TODO O (n) loop model


% TODO [Nu]



% \textbf{In the description of the above they make a difference between $q > 2$ (which would be non-critical, based on resembling case of O(n) model) and $q_1, q_2$ after the $c = 1$ area (would branched polymer phase). I do not know why they make this difference and what the consequences would be, I would need to have a look at O(n) models and whether they have a branched polymer phase.}
\clearpage

\subsection{Studying the $\mathrm{GFPL}^2$-model}

There are two non-rigorously proven steps used above. Firstly, the KPZ formula
is not rigorously proven. Secondly, the connection between meanders and the
fully packed loop model coupled to meanders is also not rigorously proven but
made based on reasonable arguments made above and more presented in
\cite{DiFrancescoExactAsym2000,DiFrancescoNumCheck2000}. A system that is
similar to the $\mathrm{GFPL}^2 (q_1, q_2)$-model are random quadrangulations.
In the following the connection between the $\mathrm{GFPL}^2 (q_1, q_2)$-model
and random quadrangulations will be made. 

The $\mathrm{GFPL}^2 (q_1, q_2)$-model can be studied using random
quadrangulations \cite{DiFrancesco2005}, in the following section will be argued
why this approach is correct. Firstly, one takes a square lattice and
four-colors ($1,2,3,4$) all the vertices, like in Figure
\ref{theory:fplquadrsquarelattice}. The square lattice here is also face
bicolorable. On this lattice, the height rules of Figure
\ref{theory:heightdescription} can be chosen and for a consistent height model
$\mathbf{A} = -\mathbf{B}$ and $\mathbf{C} = -\mathbf{D}$. This change of height
is equivalent to the $\mathrm{GFPL}^2 (q_1, q_2)$-model \cite{DiFrancesco2005}. 

\begin{figure}[htb!]
    

    \centering
    \begin{subfigure}[b]{.4\linewidth}
    \includegraphics[width=\linewidth, page=1]{theory/assets/ConnectionGFPLtoQuadr.pdf}
    \caption{}\label{theory:fplquadrsquarelattice}
    \end{subfigure}
    \begin{subfigure}[b]{.22\linewidth}
    \includegraphics[width=\linewidth, page=2]{theory/assets/ConnectionGFPLtoQuadr.pdf}
    \caption{}\label{theory:heightdescription}
    \end{subfigure}

    \caption{(a) Quadrangulations of which the vertices are four-colored and the faces bicolored. (b) Height rules}
    \label{theory:drawing:squarelatticefolding}
\end{figure}

As seen in section
\ref{section:graphsandplanarmaps}, maps can be built by glueing polygons. Here a
quadrangle with the "avoiding" vertex in Figure
\ref{theory:fig:meanderbuildingblock} is taken as the polygon/building block.
The horizontal half-edges in Figure \ref{theory:fig:meanderbuildingblock} are
called the shore half-edges because the road indicated with the
\textcolor{rbprimarycolor}{red} dashed line crosses them. 

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.25\textwidth]{theory/assets/MeandersBuildingBlock.pdf}
    
    
    
    \caption{The building block for building a quadrangulations of the 2-sphere.
    Road indicated by the \textcolor{rbprimarycolor}{red} dashed line and the
    river indicated by the \textcolor{rblinkcolor}{blue} dashed line.}
    \label{theory:fig:meanderbuildingblock}
\end{figure}

Glueing rivers to rivers results in a row of quadrangles and then one can glue
the roads based on a certain meander configuration resulting in a
quadrangulation, see Figure \ref{theory:nonfoldedmeander}. Embedding the system
on a flat plane then results in Figure \ref{theory:foldedmeander}.

\begin{figure}[htb!]
    \centering
    
    \centering
    \begin{subfigure}[b]{.4\linewidth}
    \includegraphics[width=\linewidth, page=1]{theory/assets/ConnectionGFPLtoQuadrMeander.pdf}
    \caption{}\label{theory:nonfoldedmeander}
    \end{subfigure}
    \begin{subfigure}[b]{.4\linewidth}
    \includegraphics[width=\linewidth, page=2]{theory/assets/ConnectionGFPLtoQuadrMeander.pdf}
    \caption{}\label{theory:foldedmeander}
    \end{subfigure}

    \caption{(a) Drawing of a quadrangulation based on a meander configuration. (b) Embedding of the quadrangulation of Figure a on the flat plane.}
    \label{theory:drawing:meanderintroduction}
\end{figure}


% \begin{figure}[htb!]
%     \centering
%     \includegraphics[]{theory/assets/analyticstringsusceptibility.pdf}
    
    
    
%     \caption{Expectation for the $\gamma_s$ versus the weight $q$ based on the conjecture of \cite{DiFrancescoNumCheck2000}.}
%     \label{theory:fig:stringsusconjecture}
% \end{figure}


\input{theory/observables_meanders.tex}


% \section{Literature notes for Exact meander asymptotics: a numerical check}

% There is no explicit formula for the number of meanders. These meanders can be
% described by two self-avoiding loops, which can be translated into a matrix
% model $O(n_1, n_2)$. For $n_2 = 1$ it can be solved exactly.

% 'The meander problem is a realization of the coupling of gravity to a certain
% two-flavored loop model.' Meanders are allowed to cross and touch (tangency
% point). 


% The fully packed-loop model can be described on a lattice in which a white and
% black loop avoid/cross every vertex.

% The following partition function is associated with this model.

% \begin{equation}
%     Z_{FPL} (n_1, n_2) = \sum_{\text{fully-packed loop configurations}}{n_1^{L_1} n_2^{L_2}}
% \end{equation}

% $n_i$ can be recast into Boltzmann weights (see paper).

% \begin{equation}
%     n_i = 2 \cos{(\pi e_i)}
% \end{equation}

% 'To finally get to meanders, we must consider the coupling of the FPL2(n1,n2) model to
% two-dimensional quantum gravity, by allowing the square lattice to fluctuate into arbitrary planar four-valent graphs. For each such graph the fully-packed loop model is still defined by coloring the edges black or white and allowing only the vertices shown in Fig. 1. As before, each colored loop is weighted by the appropriate ni factor (i = 1, 2). If we try to go through the steps of the previous section, namely by transforming the
% model into a height model, the issue of bicolorability of the vertices of the lattice becomes crucial on a random four-valent graph. Indeed, not all such graphs are vertex-bicolorable. So the coupling to gravity stricto sensu (sum over arbitrary planar four-valent graphs) will destroy this property'

% More generally, the multi-component meander polynomial belongs to the universality of the $GFPL^2(n_1,n_2)$ model. For $n_1 \rightarrow 0$ and $n_2 = q$ and central charge given in following equation.

% \begin{equation}
%     c(n_1, n_2) = c_{DPL} (n_1, n_2) \equiv 2 - 6\left(\frac{e_1^2}{1-e_1}+\frac{e_2^2}{1-e_2}\right)
% \end{equation}


% 'In particular, there exists a finite value $x_c$ of $x$ at which the (connected) partition function behaves as $Z(x) \sim (x_c - x)^{2-\gamma(c)}$'.

% $x$ is the weight of crossing of white and black loop.

% By equation 2.32 in the article. $c = -1 - 6 \frac{e^2}{1-e}$. $e = \frac{1}{\pi} \arccos {\left(\frac{q}{2}\right)}$.

% For $q = 2$ this corresponds to $c = -1$ (because $\arccos{1} = 0$). 

% For-semi meanders the transition at $q = 2$ is never reached, but another phenomenon occurs at $q_c < 2$. 

% \subsection{On the section: 5.1 Ranges of validity and transitions}

% They take multi-river and multi-road meanders. For $n_i > 2$ the $DPL^2$ model
% is no longer critical. However by matrix model techniques it was shown that you
% get $O(n)$ model with $\alpha = 3/2$ or $\gamma = 1/2$ which corresponds to the
% Branched polymer regime.

% $c = 1$ barrier is reached when $n_1, n_2 < 2$, but $c(n_1, n_2) > 1$, but this
% also corresponds to $\gamma = 1/2$ and $\alpha = 3/2$.


% They also state the following: 'On the ordinate axis of Fig. 24, where $n_1 =
% 0$, we recover for the announced phase transition at $n_2 = q = 2$, beyond which
% the $DPL^2$ model is no longer critical. Numerical results get poorer as we
% approach this point and we could not confidently analyze it numerically so far.'



% They also denote something about the range of validity of the phase figure. 


% 'As discussed before, we may also consider the coupling of the FPL2 model to eulerian gravity, namely by summing over genus zero tetravalent graphs with only faces of even valency. Then all the above predictions are expected to still hold, except that we must take the formula (2.3) for the central charge, that remains one unit above that of the DPL2 model as we have ensured the vertex-bicolorability of the graphs we sum over [22,23]. Note that, the above shifted central charge.

% The range of validity of Fig. 24 is reduced in this case to in this eulerian case, the b-vertex of Fig. 1 is now relevant and we must take $y \neq 0$ to get the zone of the square $[0, 2] \times [0, 2]$ delimited by the curve $c = 1$ joining the point $(2,q^\star)$ to the point $(q_\star, 2)$, with $ q^\star = 2\cos{(\pi(\sqrt{13}-1)/6)} = 0.410135.... $

% One should also be able to check numerically the predictions for eulerian gravity, using a suitable modification.'

% Lastly, they make some remarks about adding matter or consider more loop colors to generate new types of meanders and also different universality classes.


% fully-packed loop model’s configurations defined here differ from those of the so-called densely packed loop model [18] in that each vertex is visited by a black and a white loop, whereas in the dense case, loops of a given (say black) color are not constrained to visit all vertices