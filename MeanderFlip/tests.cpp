#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2/catch.hpp>

#include <iostream>
#include <vector>
#include <array>
#include "planarmap.hpp"
#include "meanderflip.hpp"
#include "meandergenerator.hpp"
#include "meandermeasurement.hpp"
#include "histogram.hpp"


TEST_CASE( "Checking flip_permutation move") {

    // testing the flip function
    std::vector<Edge> test_edges;
    test_edges.push_back({.id = 1, .next = 0, .previous = 0, .adjacent = 3});
    test_edges.push_back({.id = 2, .next = 0, .previous = 0, .adjacent = 4});
    test_edges.push_back({.id = 3, .next = 0, .previous = 0, .adjacent = 1});
    test_edges.push_back({.id = 4, .next = 0, .previous = 0, .adjacent = 2});
    
    // correct flip
    std::vector<Edge> correct_flipped_edges;
    correct_flipped_edges.push_back({.id = 1, .next = 0, .previous = 0, .adjacent = 2});
    correct_flipped_edges.push_back({.id = 2, .next = 0, .previous = 0, .adjacent = 1});
    correct_flipped_edges.push_back({.id = 3, .next = 0, .previous = 0, .adjacent = 4});
    correct_flipped_edges.push_back({.id = 4, .next = 0, .previous = 0, .adjacent = 3});

    std::vector<Edge> mutable_test_edges(test_edges);

    flip_permutation(mutable_test_edges, mutable_test_edges[3], mutable_test_edges[0]);

    std::vector<Edge> test_result(mutable_test_edges);

    CHECK( test_result == correct_flipped_edges );
}

TEST_CASE( "Find all paths") {
    int N = 4; // 2n

    // Generate meander system
    struct MeanderSystem generated_meander_system = generate_glued_map(N);

    // Get the edges
    std::vector<Edge> edges = generated_meander_system.edges;

    // Get the paths
    std::vector<std::vector<int> > paths = get_all_paths(edges);

    std::vector<std::vector<int> > correct_paths = {{1,4, 3,2}};

    CHECK(paths == correct_paths);

}

TEST_CASE( "Find all paths 2") {

    int N = 8; // 2n

    // Generate meander system
     struct MeanderSystem generated_meander_system = generate_glued_map(N);

    // Get the edges
    std::vector<Edge> edges = generated_meander_system.edges;

    // Get the paths
    std::vector<std::vector<int> >  paths = get_all_paths(edges);

    std::vector<std::vector<int> >  correct_paths = {{1, 8, 7, 2}, {3, 6, 5, 4}};

    CHECK(paths == correct_paths);
}

TEST_CASE("Meanderpermutation tests") {
    std::srand(1000);

    int N = 4; // 2n
    
    struct MeanderSystem generated_meander_system = generate_glued_map(N);

    std::vector<Edge> edges = generated_meander_system.edges;

    MeanderPermutation perm = MeanderPermutation(N);

    perm.from(edges);

    /*
    * TO-DO:
    * std::vector implementation speed.
    * Add test based on simple system.
    * 
    * +---+---+---+---+
    * | / | \ | / | \ |
    * +---+---+---+---+
    * | 1 | 2 | 3 | 4 |
    * +---+---+---+---+
    * | \ | \ | / | / |
    * +---+---+---+---+
    * 
    * L 1 -> 4 -> 3 -> 2 R
    * 
    * Flip:
    * 1||4
    * 2||3
    * to:
    * 1||2
    * 4||3
    */

    std::vector<std::vector<int> > paths = perm.get_paths();



    perm.flip(2, 4, 3, 1);
}

TEST_CASE("Quantity test") {
    

    std::vector<uint64_t> hello = {1, 2, 2};

    Histogram hist(hello, 2);

    CHECK(hist.hist == std::vector<uint64_t>({1, 2}));
    CHECK(hist.hist_squared == std::vector<uint64_t>({1, 4}));

    std::vector<uint64_t> hello2 = {1, 1, 2};

    Histogram hist2(hello2, 2);

    hist += hist2;

    CHECK(hist.hist == std::vector<u_int64_t>({3,3}));

    CHECK(hist.N == 2);
}