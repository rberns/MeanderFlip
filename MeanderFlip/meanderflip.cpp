//
//  meanderflip.cpp
//  MeanderFlip
//
//  Created by Rutger Berns on 17/10/2021.
//
#include "meanderflip.hpp"

/*
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * |         |   |   |         |  | flip=> |  | 1 | 3 |  | turn=> |  |   |   |   |   |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * | 1 (2nd) | ∧ | ∨ | 3       |  | flip=> |  | > | > |  | turn=> |  | 2 | ∧ | ∨ | 1 |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * | 2       | ∧ | ∨ | 4 (1st) |  | flip=> |  | < | < |  | turn=> |  | 4 | ∧ | ∨ | 3 |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * |         |   |   |         |  | flip=> |  | 2 | 4 |  | turn=> |  |   |   |   |   |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * 
 * Description:
 * Select a first edge and a second edge which is another edge oriented clockwise to
 * the vertex. The flip is then as follows. First edge (4) gets glued to adjacent of 2nd edge (3).
 * Then the adjacent (3) of the 2nd edge is glued to the 1st edge (4).
 * 
 * After that 
 */
std::vector<Edge> flip(std::vector<Edge> & edges, Edge first_edge, Edge second_edge) {
    Edge adjacent_first_edge = adjacent(first_edge, edges);
    Edge adjacent_second_edge = adjacent(second_edge, edges);
    
    // glueing (3) to (4), and glueing (2) to (1)
    mutable_edge(first_edge, edges)->adjacent = second_edge.adjacent;
    mutable_edge(second_edge, edges)->adjacent = first_edge.adjacent;    

    // glueing (1) to (2), and glueing (4) to (3)
    mutable_edge(adjacent_first_edge, edges)->adjacent = adjacent_second_edge.adjacent;
    mutable_edge(adjacent_second_edge, edges)->adjacent = adjacent_first_edge.adjacent;
    
    //return edges;
}



/*
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * |         |   |   |         |  | flip=> |  | 1 | 3 |  | turn=> |  |   |   |   |   |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * | 1 (2nd) | ∧ | ∨ | 3       |  | flip=> |  | > | > |  | turn=> |  | 2 | ∧ | ∨ | 1 |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * | 2       | ∧ | ∨ | 4 (1st) |  | flip=> |  | < | < |  | turn=> |  | 4 | ∧ | ∨ | 3 |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * |         |   |   |         |  | flip=> |  | 2 | 4 |  | turn=> |  |   |   |   |   |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * 
 * The connection between the first edge (4) and the first adjacent (2) will be broken.
 * 
 */
void flip_permutation(std::vector<Edge> & edges, Edge first_edge, Edge second_edge) {
    GlueMap map = glue_flip(first_edge.id, second_edge.id, first_edge.adjacent, second_edge.adjacent);

    Edge adjacent_first_edge = adjacent(first_edge, edges);
    Edge adjacent_second_edge = adjacent(second_edge, edges);
    
    mutable_edge(first_edge, edges)->adjacent = map.first;
    mutable_edge(second_edge, edges)->adjacent = map.cw;    

    mutable_edge(adjacent_first_edge, edges)->adjacent = map.afirst;
    mutable_edge(adjacent_second_edge, edges)->adjacent = map.acw;
}

struct EdgeAccept {
    bool accepted;
    Edge random_edge_of_vertex;
};

// bool flipmove(std::vector<Edge> & edges, MeanderPermutation *permutation, double q, int * N_ends, int N, FlipInfo * info, void (*preflipcontrol)(FlipInfo*)) {
//     // select a random edge
//     Edge shore_edge = random_shore_edge(edges);

//     // number of vertex edges
//     int n = number_of_edges_of_vertex(shore_edge, edges);
//     //int n_shore = number_of_shore_edges_of_vertex(shore_edge, edges);

    
    

//     // finding a random cw edge
//     Edge current_edge = cw(shore_edge, edges);

//     std::vector<Edge> other_edges_around_vertex;

//     assert(current_edge.id != shore_edge.id);
//     //current_edge.id != shore_edge.id

//     while (current_edge.id != shore_edge.id)
//     {
//         if(current_edge.is_shore) {
//             other_edges_around_vertex.push_back(current_edge);
//         }
//         current_edge = cw(current_edge, edges);
//     }

//     //debug_face_printer(other_edges_around_vertex);
//     #if(DEBUGPERM)
//     std::cout << "other_edges_around_vertex: \n";
//     #endif
//     for(size_t i = 0; i < other_edges_around_vertex.size(); i++) {
//         #if(DEBUGPERM)
//             std::cout << other_edges_around_vertex[i] << "\n";
//         #endif
//     }

//     // reject move when there are no other edges around the vertex
//     // two approaches of solving, rejecting and changing the acceptance probability to account for this
//     // other approach is to 
//     if(other_edges_around_vertex.size() == 0) {
//         return false;
//     }

//     Edge suggestion_edge = select_uniform_random_edge(other_edges_around_vertex);

//     Edge adjacent_shore_edge = adjacent(shore_edge, edges);
//     Edge adjacent_suggestion_edge = adjacent(suggestion_edge, edges);

//     EdgeCount neighbors_initial_edge = number_of_shore_edges_of_vertex_between(adjacent_shore_edge, adjacent_shore_edge, edges);
//     EdgeCount neighbors_suggestion_edge = number_of_shore_edges_of_vertex_between(adjacent_suggestion_edge, adjacent_suggestion_edge, edges); // I changed this to adjacent_suggestion_edge from suggestion_edge

//     EdgeCount n_initial_to_suggestion = number_of_shore_edges_of_vertex_between(shore_edge, suggestion_edge, edges);
//     EdgeCount n_suggestion_to_initial = number_of_shore_edges_of_vertex_between(suggestion_edge, shore_edge, edges);

//     int n_shore = n_initial_to_suggestion.shore_edges + n_suggestion_to_initial.shore_edges + 2;

//     // if n_initial_to_suggestion, n_suggestion_to_initial is zero check for root
//     // int local_N_initial = count_zero(neighbors_initial_edge.shore_edges + neighbors_initial_edge.end_edges, 
//     //                                  neighbors_suggestion_edge.shore_edges + neighbors_suggestion_edge.end_edges);

//     // int local_N_final = count_zero(n_initial_to_suggestion.shore_edges + n_initial_to_suggestion.end_edges, 
//     //                                n_suggestion_to_initial.shore_edges + n_suggestion_to_initial.end_edges);

//     int local_N_initial = count_zero(neighbors_initial_edge.shore_edges, 
//                                      neighbors_suggestion_edge.shore_edges);

//     int local_N_final = count_zero(n_initial_to_suggestion.shore_edges, 
//                                    n_suggestion_to_initial.shore_edges);


//     assert(local_N_initial <= 2);
//     assert(local_N_final <= 2);

//     int suggested_N_change = local_N_final - local_N_initial;

//     if(DEBUG) {
//         //std::cout << "local_N_initial: " << local_N_initial << ", local_N_final: " << local_N_final << ", N_initial" << *N_ends << "\n";
//         //std::cout << "shore edge " << shore_edge << " with n neighbors: " << n << " and n shore neighbors: " << n_shore << "\n";
//     }

//     int N_shore_edges = (N*2);

//     double N_initial = (double)(N_shore_edges - *N_ends);
//     double N_final = (double)(N_shore_edges - (*N_ends + suggested_N_change));

//     double v_initial = (double)n_shore;
//     double v_final = (double)((neighbors_initial_edge.shore_edges + 1) + (neighbors_suggestion_edge.shore_edges + 1)); // +1 are because function returns amount of shore edges between itself so +1;

//     double k_initial = (double)permutation->k_components();
//     permutation->flip(shore_edge.face_id, suggestion_edge.face_id, adjacent_shore_edge.face_id, adjacent_suggestion_edge.face_id);
//     double k_final = (double)permutation->k_components();

//     //double p = pow(q, k_final - k_initial) * (N_initial/N_final) * ((v_initial-1)/(v_final-1));

//     double p = pow(q, k_final - k_initial) * ((v_initial-1)/(v_final-1));

//     double A = std::min(1.0, p);
//     //std::cout << "p: " << A << "\n";
//     #if INFOPRINT
//     std::cout << "p: " << p << "\n";
//     #endif

    
    
//     #if DEBUG 
//     //std::cout << suggestion_edge << " with probability P = 1/N * 1/v, with v = " << n_shore-1 << " (number of shore neighbors)\n";
        
//     std::cout << "acceptance probability: " << A << "\n";
//     //std::cout << "new vertex will have neighbors: " <<  neighbors_initial_edge + neighbors_suggestion_edge + 2 << "\n";
//     //std::cout << "probability = 1/N * 1/v, with v = " <<  neighbors_initial_edge + neighbors_suggestion_edge + 2 - 1 << " (number of shore neighbors after flip)\n";

//         // 1/(max(v_old, v_new)) / (1/(v_old)) = v_old / (max(v_old, v_new))
//     //std::cout << "acceptance probability: " << ((float)old_v)/((float)max_v) << "\n";
    
//     #endif

//     if(accept(A)) {
//         #if(INFOPRINT)
//         std::cout << "AFTER\n";
//         permutation->print();
//         std::cout << "v_initial: " << v_initial << "\n";
//         std::cout << "v_final: " << v_final << "\n";
//         std::cout << "N_initial: " << N_initial << "\n";
//         std::cout << "N_final: " << N_final << "\n";
//         std::cout << "k_initial: " << k_initial << "\n";
//         std::cout << "k_final: " << k_final << "\n";
//         std::cout << "local_N_initial: " << local_N_initial << "\n";
//         std::cout << "local_N_final: " << local_N_final << "\n";
//         std::cout << "n_initial_to_suggestion: " << n_initial_to_suggestion.shore_edges << "\n";
//         std::cout << "n_suggestion_to_initial: " << n_suggestion_to_initial.shore_edges << "\n";
//         std::cout << "n_neighbors_initial_edge: " << neighbors_initial_edge.shore_edges << "\n";
//         std::cout << "n_neighbors_suggestion_edge: " << neighbors_suggestion_edge.shore_edges << "\n";
//         std::cout << "suggested_N_change: " << suggested_N_change << "\n";
//         std::cout << "Edge flip - " << "first: " << shore_edge.id << ", cw: " << suggestion_edge.id << ", afirst: " << adjacent_shore_edge.id << ", acw: " << adjacent_suggestion_edge.id << "\n";
//         std::cout << "Face flip - " << "first: " << shore_edge.face_id << ", cw: " << suggestion_edge.face_id << ", afirst: " << adjacent_shore_edge.face_id << ", acw: " << adjacent_suggestion_edge.face_id << "\n";
//         #endif
//         //callback(shore_edge, suggestion_edge, args);
//         if (info)
//         {
//             info->first = shore_edge;
//             info->cw = suggestion_edge;
//             info->afirst = adjacent_shore_edge;
//             info->acw = adjacent_suggestion_edge;
//             info->v_initial = v_initial;
//             info->v_final = v_final;
//             info->N_intial = N_initial;
//             info->N_final = N_final;
//             info->k_initial = k_initial;
//             info->k_final = k_final;
//             info->local_N_initial = local_N_initial;
//             info->local_N_final = local_N_final;
//             info->n_initial_to_suggestion = n_initial_to_suggestion.shore_edges;
//             info->n_suggestion_to_initial = n_suggestion_to_initial.shore_edges;
//             info->n_neighbors_initial_edge = neighbors_initial_edge.shore_edges;
//             info->n_neighbors_suggestion_edge = neighbors_suggestion_edge.shore_edges;
//             info->suggested_N_change = suggested_N_change;
//             info->acceptance_probability = p;
//             info->N = N;
//             info->edges = &edges;
//             info->accepted = true;
//         }
        

//         #if(DEBUG) 
//         std::cout << "accepted" << shore_edge << " and " << suggestion_edge << "\n";
//         #endif

//         #if(DEBUGPERM)
//         std::cout << "AFTER\n";
//         permutation->print();
//         #endif

//         if(preflipcontrol && info) preflipcontrol(info);

//         flip_permutation(edges, shore_edge, suggestion_edge);

//         *N_ends = *N_ends + suggested_N_change;

//         #if INEFFICIENTINFOPRINT
//         std::vector<std::vector<int> > paths = get_all_paths(edges);

//         //pathss.push_back(paths);

//         //n_paths.push_back(paths.size());

//         //debug_pretty_print_paths(paths, N);
//         #endif
//         //std::cout << "N_ends" << *N_ends <<"\n";
//         //permutation->print();
//         return true;
//     } else {
//         permutation->undo();
//         if(DEBUG) {
//             std::cout << "rejected\n";
//         }
//         return false;
//     }
// }