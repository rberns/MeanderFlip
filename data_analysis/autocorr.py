# %%
from meanderutils import *
import matplotlib.pyplot as plt
import seaborn as sns


palette = sns.color_palette("tab10", as_cmap=True)

sns.set_theme()
sns.set_style("ticks")
sns.set_palette(sns.color_palette("winter_r", 3))

# %%


data_per_size = dict()

for n in [128, 256, 512, 1024, 2048]:
    for data in SimDataIterator(1.0, n=n, name="componentscorrelationtime"):
        data_per_size[n] = data["measurements"][0]["data"]
        #continuous_data = data["measurements"][0]["data"][:n*100]
        #rate = data["accepted"]/data["iter"]
        #sns.lineplot(x=np.arange(len(continuous_data))/n, y=np.array(continuous_data)/continuous_data[0], label="{}".format(rate))

df = pd.DataFrame.from_dict(data_per_size)


df.to_pickle("/simdata/componentscorrelationtime/q{:.4f}/data.pkl")

# plt.grid()
# plt.ylabel("$k$ components")
# plt.xlabel("$n$ sweeps")
#sns.grid()

# %%
q = 1.0
n_size = 128
N = 2*n_size
df, data = autocorrelationmeasurement(q=q, n=n_size)
# %%
def sample_autocovariance(x, tmax):
    '''Compute the autocorrelation of the time series x for t = 0,1,...,tmax-1.'''
    x_shifted = x - np.mean(x)
    return np.array([np.dot(x_shifted[:len(x)-t], x_shifted[t:])/len(x) for t in range(tmax)])

def find_correlation_time(autocov):
    '''Return the index of the first entry that is smaller than autocov[0]/e.'''
    return np.where(autocov < np.exp(-1)*autocov[0])[0][0]
# %%
k = df.iloc[:, 0].values

print(k)

tmax = 5000

cov = sample_autocovariance(k[200000:600000], tmax)

corrtime = find_correlation_time(cov)

print("correlation time:", corrtime)

plt.plot(cov/cov[0])
plt.plot(np.exp(-np.arange(tmax)/corrtime))


# %%
print(df)

df.to_pickle("simdata/q{:.4f}/n{}/autocorr.pkl".format(q, n_size))
# %%
data
dfmetadata = pd.DataFrame(data=[data.values()], columns=data.keys())

dfmetadata.to_pickle("simdata/q{:.4f}/n{}/autocorr_metadata.pkl".format(q, n_size))
#pickle.dump(data,"metadata")

# %%


#%%

def split_array_in_batches(arr, batch_size):
    '''the funcion produces an array with rows as bunches of length batch_size'''
    remainder = arr.shape[0] % batch_size
    
    if remainder != 0:
        arr = arr[:-remainder]

    # every row will be a batch
    # back of array will be cut if batch size does not fit the length.
    return arr.reshape(int(len(arr)/batch_size), batch_size)


def error(sequence, batch_size):
    #c = np.where(sequence == k, 1, 0)


    c = split_array_in_batches(sequence, batch_size)


    n_batches = c.shape[0]

    means = np.mean(c, axis=1)

    mean = np.mean(means)


    

    squared_sum = np.sum(np.square(means))/n_batches
    sum_squared = np.square(np.sum(means)/n_batches)

    std = np.sqrt(squared_sum-sum_squared)
    err = (1/(np.sqrt(n_batches)))*std
    
    return [mean, err, std, n_batches]
#%%

x = np.logspace(2, 22, 50, base=2, dtype=int)
y = np.zeros(len(x))
yerr = np.zeros(len(x))


for i, batches in enumerate(x):
    mean, err, _ = error(k[1000000:], batches)

    y[i] = mean
    yerr[i] = err


plt.plot(x/n_size, y)

plt.show()

print(x)
print(yerr)

plt.plot(x/n_size, yerr)

plt.xscale('log', base=10)
plt.vlines(corrtime/n_size, ymin=np.min(yerr), ymax=np.max(yerr), color='r')

plt.show()



plt.plot(x/n_size, np.gradient(yerr))
plt.xscale('log', base=10)
plt.yscale('log')

plt.show()
# %%
print("correlation time (sweeps): ", corrtime/n_size)

# %%
mean, err, std, n_batches = error(k, corrtime)
err, std

# %%

dO = err*np.sqrt(2*corrtime)
dO
# %%
start = 100000
end = start + corrtime * 5
plt.plot(np.arange(len(k))[start:end],k[start:end])
# %%
start = 100000
end = start + 8192 * 5
plt.plot(np.arange(len(k))[start:end],k[start:end])
# %%
