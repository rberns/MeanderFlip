//
//  planarmap+json.hpp
//  MeanderFlip
//
//  Created by Rutger Berns on 18/12/2021.
//
#ifndef planarmapjson_hpp
#define planarmapjson_hpp

#include <stdio.h>
#include "json.hpp"
#include "planarmap.hpp"

//json serialization
inline void to_json(nlohmann::json &j, const Edge &s)
{
    j["id"] = s.id;
    j["adjacent"] = s.adjacent;
    j["next"] = s.next;
    j["previous"] = s.previous;
    j["is_shore"] = s.is_shore;
    j["face_id"] = s.face_id;
    j["is_end"] = s.is_end;
}

inline void from_json(const nlohmann::json &j, Edge &s)
{
    s.id = j.at("id").get<int>();
    s.next = j.at("next").get<int>();
    s.previous = j.at("previous").get<int>();
    s.adjacent = j.at("adjacent").get<int>();
    s.is_shore = j.at("is_shore").get<bool>();
    s.face_id = j.at("face_id").get<int>();
    s.is_end = j.at("is_end").get<bool>();
}

#endif /* planarmapjson_hpp */