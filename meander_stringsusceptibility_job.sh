#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=200M
#SBATCH --time=1-23:59:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 7 -w cn113

cd ~/MeanderFlip/
python3 MeanderFlipRunnerStringSusceptibility5.py -q 1 2 3 4 5 6 7 8 9 10 -s 262144 -p 10 -c 16 -f 16 -o stringsusceptibility

