//
//  helpers.hpp
//  MeanderFlip
// 
//  Created by Rutger Berns on 16/10/2021.
//

#ifndef helpers_hpp
#define helpers_hpp

#include <stdio.h>   
#include <stdlib.h>
#include <random>
#include "planarmap.hpp"
#include <math.h>
#include "meander.hpp"

bool accept(double probability);

bool accept(int n_true, int n_false);

struct EdgeCount {
    int shore_edges;
    int end_edges;
};

Edge * mutable_edge(Edge edge, std::vector<Edge> & edges);

Edge next(Edge edge, const std::vector<Edge> & edges);

Edge previous(Edge edge, const std::vector<Edge> & edges);

Edge adjacent(Edge edge, const std::vector<Edge> & edges);

Edge cw(Edge edge, const std::vector<Edge> & edges);

int number_of_edges_of_vertex(Edge edge, const std::vector<Edge> & edges);

int number_of_shore_edges_of_vertex(Edge edge, const std::vector<Edge> & edges);

EdgeCount number_of_shore_edges_of_vertex_between(Edge from_edge, Edge to_edge, const std::vector<Edge> & edges);

Edge random_edge(const std::vector<Edge> & edges);

Edge random_shore_edge(const std::vector<Edge> & edges);

std::vector<std::vector<int> > get_all_paths(std::vector<Edge> & edges);

void debug_pretty_print_paths(const std::vector<std::vector<int> > & paths, int n_faces);

int count_zero(int a, int b);

class RandomGenerator {
    protected:
    
    std::mt19937_64 g;

    public:
    // Private constructor so that no objects can be created.
    RandomGenerator(unsigned int seed) {
        g = std::mt19937_64(seed);
    }

    bool accept();
    int rand_int(int min, int max);
    double uniform(double min = 0.0, double max = 1.0);
};


#endif