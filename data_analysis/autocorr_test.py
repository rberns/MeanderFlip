# %%
from meanderutils import *
import matplotlib.pyplot as plt

# %%
q = 3.0
n_size = 512
N = 2*n_size

# %%


df = pd.read_pickle("simdata/q{:.4f}/n{}/stringhist.pkl".format(q, n_size))

result = df.iloc[0, ::2].values

# %%
def sample_autocovariance(x, tmax):
    '''Compute the autocorrelation of the time series x for t = 0,1,...,tmax-1.'''
    x_shifted = x - np.mean(x)
    return np.array([np.dot(x_shifted[:len(x)-t], x_shifted[t:])/len(x) for t in range(tmax)])

# %%

result
# %%

cov = sample_autocovariance(df.iloc[0, ::2].values, 199)

# %%
plt.plot(cov/cov[0])
# %%
