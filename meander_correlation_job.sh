#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=100M
#SBATCH --time=1-23:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 3 -w cn113

cd ~/MeanderFlip/

python3 MeanderFlipRunnerComponentsCorrelation.py -q 1.5 1.6 1.7 1.8 1.9 2.1 2.2 2.3 2.4 2.5 -s 128 -c 8 -p 3 -f 1024 -o compcorrelation -d 32
# python MeanderFlipRunnerComponentsCorrelation.py -q 2 -s 128 -c 8 -p 1 -f 100 -o compcorr

