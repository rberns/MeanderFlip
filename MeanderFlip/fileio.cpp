#include "fileio.hpp"

// std::string file_name_generator(double q, int n, const std::string& id) {
//     std::stringstream stream;
//     stream << "simdata/q";
//     stream << std::fixed << std::setprecision(4) << q;
//     stream << "/n" << n;
//     stream << "/" << "sim_" << id << ".json";
    
//     return stream.str();
// }

std::string curr_date_local() {
    std::stringstream stream;
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    stream << std::put_time(&tm, "%Y%m%d%H%M%S");
    return stream.str();
}

Fileio::Fileio(double q, int n, int seed, int total_runs, const std::string& name)
{
    this->q = q;
    this->n = n;
    this->name = name;
    this->seed = seed;
    this->total_runs = total_runs;
    this->start_date = curr_date_local();
}

Fileio::~Fileio()
{
    
}

bool Fileio::initialize_dir() {
    errno = 0;
    std::cout << "output dir: " << dir_path().c_str() << "\n";

    std::filesystem::create_directories(dir_path());
    return true;
    //int ret = mkdir(dir_path().c_str(), S_IRWXU);
    // if (ret == -1) {
    //     switch (errno) {
    //         case EACCES :
    //             std::cout << "the parent directory does not allow write\n";
    //             exit(EXIT_FAILURE);
    //         case EEXIST:
    //             std::cout << "pathname already exists\n";
    //             exit(EXIT_FAILURE);
    //         case ENAMETOOLONG:
    //             std::cout << "pathname is too long\n";
    //             exit(EXIT_FAILURE);
    //         default:
    //             perror("mkdir");
    //             exit(EXIT_FAILURE);
    //     }
    // }
    // return true;

    // source: https://www.delftstack.com/howto/c/mkdir-in-c/
}

std::stringstream Fileio::pre() {
    std::stringstream stream;
    stream << "simdata/";
    stream << name;
    stream << "/q";
    stream << std::fixed << std::setprecision(4) << q;
    stream << "/n" << n;

    return stream;
}

std::string Fileio::dir_path() {
    return this->pre().str();
}

void Fileio::write(nlohmann::json json, int run) {
    std::stringstream stream = pre();
    stream << "/sim_" << "data_" + start_date + "_" + std::to_string(seed) + "_" + std::to_string(run) + "_" + std::to_string(total_runs) << ".json";
    myfile.open (stream.str());
    myfile << json;
    myfile.close();
}