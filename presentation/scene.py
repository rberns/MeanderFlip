from tokenize import Whitespace
from manim import *
from manim.utils.color import Colors
from more_itertools import one
import numpy as np
import cmasher as cm


def construct_arc(start, end, is_above=True, color=WHITE, scale=1):
    return ArcBetweenPoints(start, end, -180*DEGREES if is_above else 180*DEGREES, stroke_width=4*scale).set_color(color)

def construct_arc_helper(coords, offset, start, end, is_above=True, color=WHITE):
    if is_above:
        coords = coords + offset
    else:
        coords = coords - offset
    if start < end:
        return construct_arc(coords[start], coords[end], is_above, color)
    else:
        return construct_arc(coords[end], coords[start], is_above, color)

def construct_meander(order, *connections, spacing=1):
    coords = np.array([np.array([(float(x)+0.5)*spacing, 0, 0]) for x in range(order)])
    coords = coords - np.array([spacing*order/2,0,0])
    dots = [Dot(color=WHITE).move_to(coord).scale(0.5) for coord in coords]

    line = Line(LEFT*spacing*order/2, RIGHT*spacing*order/2).set_color(Colors.blue_d.value)
    arcgroups = []
    for connect in connections:
        arcgroups.append(VGroup(*[construct_arc_helper(coords, np.array([0,0,0]), *con) for con in connect]))

    return line, VGroup(*dots), tuple(arcgroups)

def construct_meander_with_spacing(order, *connections, spacing=1, vspacing=1, wants_line=True):
    coords = np.array([np.array([(float(x)+0.5)*spacing, 0, 0]) for x in range(order)])
    coords = coords - np.array([spacing*order/2,0,0])
    dots = [Dot(color=WHITE).move_to(coord).scale(0.5) for coord in coords]

    line = Line(LEFT*vspacing*order/2, RIGHT*vspacing*order/2).set_color(Colors.blue_d.value)
    arcgroups = []
    if wants_line:
        extra_lines = VGroup(*[Line(coord+UP*2, coord+DOWN*2) for coord in coords])
    else:
        extra_lines = VGroup()
    for connect in connections:
        arcgroups.append(VGroup(*[construct_arc_helper(coords, vspacing*UP, *con) for con in connect]))

    return line, VGroup(*dots), tuple(arcgroups), extra_lines

class CreateCircle(Scene):
    def construct(self):
        #text = Text("Hello world", font_size=144)

        

        

        #self.add(text)
        image = ImageMobject("../drawings/meanderingriver.jpg")
        width = 10
        image.scale_to_fit_width(width)
        self.add(image)
        #circle = Circle()  # create a circle
        #circle.set_fill(PINK, opacity=0.5)  # set the color and transparency
        self.next_section("Meanders")

        svgobj = SVGMobject("../drawings/river.svg")
        svgobj.scale_to_fit_width(0.8*width)
        svgobj.align_to(image, DOWN)
        svgobj.shift(0.2*RIGHT)
        svgobj.set_color(Colors.blue_d.value)
        self.play(FadeIn(svgobj))

        self.next_section("Meanders modelling")
        #self.play(Create(circle))  # show the circle on screen
        meanderwithroad = SVGMobject("../drawings/meanderwithroad.svg")
        meanderwithroad.scale_to_fit_width(8)
        riversvg = meanderwithroad.submobjects[0]
        riversvg.set_color(Colors.blue_d.value)

        road1svg = meanderwithroad.submobjects[1]
        road2svg = meanderwithroad.submobjects[2]

        road1svg.set_color(Colors.yellow_e.value)
        road2svg.set_color(Colors.gold_e.value)


        self.next_section("Meanders and river")

        self.play(Transform(svgobj, riversvg), FadeOut(image))
        #self.remove(riversvg)
        #self.add(riversvg)
        self.remove(svgobj)
        self.add(riversvg)

        self.next_section("Add the road")

        self.play(FadeIn(road1svg))
        self.play(FadeIn(road2svg))

        self.wait()
        self.next_section("Transform into meander")

        line, dots, (big_arc, small_arc) = construct_meander(6, [(0, 1, False), (0,5, True), (1,4, True), (4,5, False)], [(2,3, False), (2,3, True)])
        big_arc.set_color(Colors.yellow_e.value)
        small_arc.set_color(Colors.gold_e.value)
        #self.add(line)
    
     
        self.play(Transform(road1svg, big_arc), Transform(road2svg, small_arc), Transform(riversvg, line), FadeIn(dots))
        #self.play()
        self.remove(riversvg)
        self.add(line)
        #self.play(FadeOut(riversvg))
        self.next_section("Transform into meander")



class MeanderDrawing(Scene):
    

    def construct(self):
        #n = 6
        line, dots, arcs = construct_meander(6, [(1,2), (3,4), (2,3, False, Colors.gold_b.value)])
        self.add(line)
        self.add(dots)
     
        self.play(Create(arcs))
     
        self.wait()
        
class PolymerChainParallel(Scene):

    def construct(self):
        line, dots, arcs, extra_lines = construct_meander_with_spacing(10, [(0, 1, False), (1,2, True), (2,3, False), (3,4, True), (4,5, False),
                                                  (5, 6, True), (6,7, False), (7,8, True), (8,9, False)])
        self.play(FadeIn(line), FadeIn(dots), FadeIn(arcs[0]), FadeIn(extra_lines))
        #self.add(dots)
        #self.add(arcs[0])
        #self.add(extra_lines)
        #arrleft = Arrow(start=ORIGIN, end=0.5*UP)
        arrow_1 = DashedLine(start=4.5*LEFT, end=4.5*LEFT+0.5*UP, color=WHITE)
        arrow_2 = DashedLine(start=4.5*RIGHT, end=4.5*RIGHT+0.5*UP, color=WHITE)
        #arright = Arrow(start=0, end=-0.5*UP)
        
        line2, dots2, arcs2 = construct_meander(10, [(0, 1, False), (1,2, True), (2,3, False), (3,4, True), (4,5, False),
                                                  (5, 6, True), (6,7, False), (7,8, True), (8,9, False)])
        self.next_section("Polymer")
        self.play(FadeOut(extra_lines))
        self.play(Transform(arcs[0], arcs2[0]), Transform(dots, dots2), Create(arrow_1), Create(arrow_2))
        #self.play()
        #self.play()
        #self.play()
        #self.wait()
        self.next_section("Meander")
        self.play(FadeOut(arcs[0]), FadeOut(dots), FadeOut(line), FadeOut(arrow_1), FadeOut(arrow_2))
        
        #ft = Text("Meanders as a toy Model of 2D quantum gravity", font="Open Sans")
        #self.play(FadeIn(ft))

import json
import networkx as nx

class FractalGeometry(ThreeDScene):
    

    def construct(self):
        with open("../build/testq1n500.json") as file:
            data = json.load(file)

        G = nx.Graph(data["nodes"])

        pos = nx.spring_layout(G, dim=3, iterations=100, weight=1)
        #print(pos)
        faces = np.array(data["faces"])-1
        #print(faces)
        #nx.draw(G, pos=pos)

        

        pos2 = np.array(list(pos.values()))

        #print(pos2)
        #print(faces)
        # vertex_coords = [
        #     [1, 1, 0],
        #     [1, -1, 0],
        #     [-1, -1, 0],
        #     [-1, 1, 0],
        #     [0, 0, 2]
        # ]
        # faces_list = [
        #     [0, 1, 4],
        #     [1, 2, 4],
        #     [2, 3, 4],
        #     [3, 0, 4],
        #     [0, 1, 2, 3]
        # ]
        self.set_camera_orientation(phi=75 * DEGREES, theta=30 * DEGREES)
        #pyramid = Polyhedron(pos2*4, faces.tolist(), graph_config=(vertex_type=None))
        #for i in range(len(pyramid.faces)):
        #    pyramid.faces[i].set_color(random_bright_color())
        #self.add(pyramid)
        face_group = VGroup()
        for face in pos2[faces]:
            face_group.add(Polygon(*(face*4), shade_in_3d=True, stroke_width=0).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))
        self.play(FadeIn(face_group))

        
        #self.play(frame.animate.increment_phi(-90 * DEGREES))
        # self.begin_3dillusion_camera_rotation(rate=2)
        # self.wait(PI/2)
        # self.stop_3dillusion_camera_rotation()
        
        # self.play(Create(pyramid))


def meander_element():
    group = VGroup()
    bottom_arr = Arrow(start=(LEFT+DOWN), end=(RIGHT+DOWN), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
    top_arr = Arrow(start=(RIGHT+UP), end=(LEFT+UP), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
    right_arr = Arrow(start=(RIGHT+DOWN), end=(RIGHT+UP), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
    left_arr = Arrow(start=(LEFT+UP), end=(LEFT+DOWN), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
    
    background = Square(2).set_fill(WHITE, opacity=0.2)
    river = Line(start=LEFT, end=RIGHT, color=Colors.blue_d.value)

    road = Line(start=DOWN, end=UP, color=Colors.gold_d.value)

    group.add(bottom_arr, top_arr, right_arr, left_arr, river, road, background)
    return group

class CreateMeanderGeometry(Scene):
    def construct(self):
        all_initial = VGroup()

        bottom_arr = Arrow(start=(LEFT+DOWN), end=(RIGHT+DOWN), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
        top_arr = Arrow(start=(RIGHT+UP), end=(LEFT+UP), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
        right_arr = Arrow(start=(RIGHT+DOWN), end=(RIGHT+UP), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
        left_arr = Arrow(start=(LEFT+UP), end=(LEFT+DOWN), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
        background = Square(2).set_fill(WHITE, opacity=0.2)

        self.play(FadeIn(background), GrowArrow(bottom_arr), GrowArrow(top_arr), GrowArrow(right_arr), GrowArrow(left_arr))
        self.next_section("Add river")
        river = Line(start=LEFT, end=RIGHT, color=Colors.blue_d.value)

        road = Line(start=DOWN, end=UP, color=Colors.gold_d.value)
        all_initial.add(bottom_arr, top_arr, right_arr, left_arr, river, road, background)
        self.play(Create(river, run_time=0.5))
        self.next_section("Add road")
        self.play(Create(road, run_time=0.5))

        self.next_section("Add more quadrangles")
        self.play(ScaleInPlace(all_initial, 0.47))
        self.play(all_initial.animate.shift(0.5*LEFT))

        order = 6
        coords = np.array([np.array([(float(x)+0.5)*1, 0, 0]) for x in range(order)])
        coords = (coords - np.array([1*order/2,0,0])).tolist()
        del coords[2]
        coords = np.array(coords)
        objs = [GrowFromCenter(meander_element().scale(0.47).shift(coord)) for coord in coords]

        self.play(*objs)

        line, dots, arcs1, extra_lines = construct_meander_with_spacing(6, [(0, 1, True), (1, 2, False), (2, 3, True), (3,4, False), (4,5, True),
                                                  (5, 0, False)], spacing=1, vspacing=0.5, wants_line=False)
        arcs1[0].set_color(Colors.gold_d.value)
        self.next_section("Add glueing of roads")

        self.play(FadeIn(arcs1[0], run_time=0.3))

        self.next_section("New glueing")
        
        self.play(FadeOut(arcs1[0], run_time=0.3))


        line, dots, arcs2, extra_lines = construct_meander_with_spacing(6, [(0, 5, True), (0, 1, False), (1, 4, True), (3, 4, False), (2, 3, True),
                                                  (2, 5, False)], spacing=1, vspacing=0.5, wants_line=False)
        arcs2[0].set_color(Colors.gold_d.value)
        self.play(FadeIn(arcs2[0], run_time=0.3))

class CreateMeanderGeometryStill(Scene):
    def construct(self):
        # all_initial = VGroup()

        # bottom_arr = Arrow(start=(LEFT+DOWN), end=(RIGHT+DOWN), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
        # top_arr = Arrow(start=(RIGHT+UP), end=(LEFT+UP), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
        # right_arr = Arrow(start=(RIGHT+DOWN), end=(RIGHT+UP), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
        # left_arr = Arrow(start=(LEFT+UP), end=(LEFT+DOWN), color=WHITE, buff=0, max_stroke_width_to_length_ratio=2.5, max_tip_length_to_length_ratio=0.1)
        # background = Square(2).set_fill(WHITE, opacity=0.2)

        # self.play(FadeIn(background), GrowArrow(bottom_arr), GrowArrow(top_arr), GrowArrow(right_arr), GrowArrow(left_arr))
        # self.next_section("Add river")
        # river = Line(start=LEFT, end=RIGHT, color=Colors.blue_d.value)

        # road = Line(start=DOWN, end=UP, color=Colors.gold_d.value)
        # all_initial.add(bottom_arr, top_arr, right_arr, left_arr, river, road, background)
        # self.play(Create(river, run_time=0.5))
        # self.next_section("Add road")
        # self.play(Create(road, run_time=0.5))

        # self.next_section("Add more quadrangles")
        # self.play(ScaleInPlace(all_initial, 0.47))
        # self.play(all_initial.animate.shift(0.5*LEFT))

        order = 6
        coords = np.array([np.array([(float(x)+0.5)*1, 0, 0]) for x in range(order)])
        coords = (coords - np.array([1*order/2,0,0])).tolist()
        #del coords[2]
        coords = np.array(coords)
        objs = [meander_element().scale(0.47).shift(coord) for coord in coords]

        self.add(*objs)

        line, dots, arcs1, extra_lines = construct_meander_with_spacing(6, [(0, 3, False), (0,5, True), (1,4, True), (1,2, False), (2,3, True), (4, 5, False)], spacing=1, vspacing=0.5, wants_line=False)

        self.add(arcs1[0])
        # self.add(arcs1[0])
        # arcs1[0].set_color(Colors.gold_d.value)
        # self.next_section("Add glueing of roads")

        # self.play(FadeIn(arcs1[0], run_time=0.3))

        # self.next_section("New glueing")
        
        # self.play(FadeOut(arcs1[0], run_time=0.3))


        # line, dots, arcs2, extra_lines = construct_meander_with_spacing(6, [(0, 5, True), (0, 1, False), (1, 4, True), (3, 4, False), (2, 3, True),
        #                                           (2, 5, False)], spacing=1, vspacing=0.5, wants_line=False)
        # arcs2[0].set_color(Colors.gold_d.value)
        # self.play(FadeIn(arcs2[0], run_time=0.3))
        #self.add(meander_element())
        #element = meander_element()
        
        #self.add(element)

def construct_arc_helperv2(coords, offset, start, end, is_above=True, color=WHITE, scale=1):
    if is_above:
        coords = coords + offset
    else:
        coords = coords - offset
    if start < end:
        return construct_arc(coords[start-1], coords[end-1], is_above, color, scale)
    else:
        return construct_arc(coords[end-1], coords[start-1], is_above, color, scale)

def construct_meander_body(order, spacing=1):
    coords = np.array([np.array([(float(x)+0.5)*spacing, 0, 0]) for x in range(order)])
    coords = coords - np.array([spacing*order/2,0,0])
    dots = [Dot(color=WHITE).move_to(coord).scale(0.5) for coord in coords]

    line = Line(LEFT*spacing*order/2, RIGHT*spacing*order/2).set_color(Colors.blue_d.value)

    return line, VGroup(*dots)

def construct_meanderv2(order, *connections, spacing=1):
    coords = np.array([np.array([(float(x)+0.5)*spacing, 0, 0]) for x in range(order)])
    coords = coords - np.array([spacing*order/2,0,0])
    dots = [Dot(color=WHITE).move_to(coord).scale(0.5) for coord in coords]

    line = Line(LEFT*spacing*order/2, RIGHT*spacing*order/2).set_color(Colors.blue_d.value)
    arcgroups = []
    for connect in connections:
        arcgroups.append(VGroup(*[construct_arc_helperv2(coords, np.array([0,0,0]), *con) for con in connect]))

    return line, VGroup(*dots), tuple(arcgroups)

def transform_array(array):
    transformed = dict()
    for i, arr in enumerate(array):
        for item in arr:
            transformed[item] = i
    return transformed

def construct_meanderv3(order, *connections, paths, index_to_color, spacing=1, scale=1):
    coords = np.array([np.array([(float(x)+0.5)*spacing, 0, 0]) for x in range(order)])
    coords = coords - np.array([spacing*order/2,0,0])
    dots = [Dot(color=WHITE).move_to(coord).scale(0.5*scale) for coord in coords]

    lookuptable = transform_array(paths)

    line = Line(LEFT*spacing*order/2, RIGHT*spacing*order/2, stroke_width=4*scale).set_color(Colors.blue_d.value)
    arcgroups = []
    for connect in connections:
        arcgroups.append(VGroup(*[construct_arc_helperv2(coords, np.array([0,0,0]), *con, color=index_to_color[lookuptable[con[0]]], scale=scale) for con in connect]))

    return line, VGroup(*dots), tuple(arcgroups)

class WhatWeDontKnow1(Scene):
    def construct(self):
        text = MathTex(r"M_n^{(1)} = .....", font_size=120).set_scale(30.0)#.move_to(UP*1.5)#.move_to(UP*1.5)
        text[0][4].set_color(BLUE)
        #text[0][2].set_color(GREEN_E)
        self.add(text)

class WhatWeDontKnow2(Scene):
    def construct(self):
        text = MathTex(r"M_n^{(1)} = .....", font_size=120).set_scale(30.0).move_to(UP*1.5)#.move_to(UP*1.5)
        text[0][4].set_color(BLUE)
        #text[0][2].set_color(GREEN_E)
        self.add(text)

        self.next_section()

        text2 = MathTex(r"M_{22}^{(1)} \approx 7.1 \cdot 10^{18}", font_size=60).set_scale(30.0).move_to(DOWN*1.2)#.move_to(UP*1.5)
        text2[0][4:6].set_color(BLUE)
        #text2[0][2].set_color(GREEN_E)
        self.add(text2)




        # line, dots = construct_meander_body(10, spacing=0.7)

        # line.shift(DOWN*0.5)
        # dots.shift(DOWN*0.5)
        # #first_arc.shift(DOWN*0.5)

        # self.add(line)
        # self.add(dots)

        # br = Brace(line, sharpness=1)

        

        # text = MathTex(r"2n \text{ bridges}").set_scale(10.0).move_to(DOWN*1.5)

        # self.add(br, text)


        #self.play(FadeIn(br, run_time=0.5), Write(text, run_time=0.5))
        #self.add(first_arc)

class TheoryDiagram(Scene):
    def construct(self):
        scale_factor = 0.25
        cftdraw = ImageMobject("../drawings/CFTdrawing.png").scale(scale_factor)
        flatdraw = ImageMobject("../drawings/flatdrawing.png").scale(scale_factor)
        meadraw = ImageMobject("../drawings/meanderdrawing.png").scale(scale_factor)
        quantumgravdraw = ImageMobject("../drawings/quantumgravdrawing.png").scale(scale_factor)

        font_size = 24

        null_element = Dot().set_color(BLACK)

        horarrow = DoubleArrow(0.5*LEFT, 0.5*RIGHT)
        verarrow = DoubleArrow(0.5*UP, 0.5*DOWN)

        qmverarrow = verarrow.copy()
        cftarrow = verarrow.copy()

        hormeaflat = horarrow.copy()
        kpzarrow = horarrow.copy()

        kpztext = Text("KPZ", font_size=18, font="Open Sans", weight=BOLD)

        

        #discrete = Text("Discrete", font_size=font_size, weight=BOLD)
        #continuum = Text("Continuum\nlimit", font_size=font_size, weight=BOLD)

        continuum = MathTex(r"n \rightarrow \infty").scale(0.6)
        continuum2 = continuum.copy()

        quest1 = MathTex(r"?").scale(0.6)
        quest2 = quest1.copy()
        quest3 = quest1.copy()


        meanderstext =  Text("Meanders", font="Open Sans", font_size=font_size, weight=BOLD)
        qmtext =        Text("2D Quantum gravity", font="Open Sans", font_size=font_size, weight=BOLD)
        cfttext =       Text("CFT", font="Open Sans", font_size=font_size, weight=BOLD)
        flattext =      Text("Flat geometry", font="Open Sans", font_size=font_size, weight=BOLD)

        MCMCtext =      Text("MCMC", font="Open Sans", font_size=font_size, weight=BOLD)

        

        cft = Group(cftdraw, cfttext).arrange(DOWN)
        qm = Group(quantumgravdraw, qmtext).arrange(DOWN)
        flat = Group(flatdraw, flattext).arrange(DOWN)
        mea = Group(meadraw, meanderstext).arrange(DOWN)

        imgs = Group(  mea, horarrow, flat,
                     qmverarrow, null_element, cftarrow,
                     qm, kpzarrow, cft)

        imgs.arrange_in_grid(rows=3, cols=3, buff=0.5)
        
        kpztext.next_to(kpzarrow, UP)
        continuum.next_to(qmverarrow, RIGHT)
        continuum2.next_to(cftarrow, RIGHT)

        quest1.next_to(kpzarrow, DOWN)
        quest2.next_to(cftarrow, LEFT)
        quest3.next_to(horarrow, DOWN)

        MCMCtext.next_to(qmverarrow, LEFT)

        #self.add(imgs)
        self.add(mea)

        self.next_section()

        self.play(FadeIn(qmverarrow, qm, continuum, run_time=0.5))

        self.next_section()

        self.play(FadeIn(horarrow, flat, run_time=0.5))

        self.next_section()

        self.play(FadeIn(cftarrow, cft, continuum2, run_time=0.5))

        self.next_section()

        self.play(FadeIn(kpzarrow, kpzarrow, kpztext, run_time=0.5))

        self.next_section()

        self.play(FadeIn(quest1, quest2, quest3, run_time=0.5))

        self.next_section()

        self.play(FadeIn(MCMCtext, run_time=0.5))
        #self.add(kpztext, continuum, continuum2)






        

class WhatWeWantToKnow(Scene):
    def construct(self):
        text = MathTex(r"M_n", font_size=120).set_scale(30.0).move_to(UP*1.5)

        

        self.play(Write(text))

        self.next_section("Mn")

        

        line, dots = construct_meander_body(10, spacing=0.7)

        line.shift(DOWN*0.5)
        dots.shift(DOWN*0.5)

        self.play(Create(line, run_time=0.5), Create(dots, run_time=0.5))

        br = Brace(line, sharpness=1)

        

        text = MathTex(r"2n \text{ bridges}").set_scale(10.0).move_to(DOWN*1.5)

        self.play(FadeIn(br, run_time=0.5), Write(text, run_time=0.5))
        #self.play())

class MeanderPicture(Scene):
    def construct(self):
        line, dots, (first_arc, ) = construct_meander(6, [(0, 3, False), (0,5, True), (1,4, True), (1,2, False), (2,3, True), (4, 5, False)])

        line.shift(DOWN*0.5)
        dots.shift(DOWN*0.5)
        first_arc.shift(DOWN*0.5)

        self.add(line)
        self.add(dots)
        self.add(first_arc)

        

        #self.play())

class WhatWeWantToKnowv2(Scene):
    def construct(self):
        #text = MathTex(r"M_n", font_size=120).set_scale(30.0).move_to(UP*1.5)

        
        #self.play(Write(text))

        #self.next_section("Mn")
        #line, dots = construct_meander_body(4, spacing=0.7)

        line, dots, (first_arc, ) = construct_meander(6, [(0, 1, False), (1,2, True), (2,3, False), (3,4, True), (4,5, False), (0, 5, True)])

        line.shift(DOWN*0.5)
        dots.shift(DOWN*0.5)
        first_arc.shift(DOWN*0.5)

        self.play(Create(line, run_time=0.5), Create(dots, run_time=0.5))

        br = Brace(line, sharpness=1)

        #emoji = Text("🌉")

        #self.play(FadeIn(emoji))
        self.next_section("created")

        text = MathTex(r"2n \text{ bridges}").set_scale(10.0).move_to(DOWN*1.5)

        self.play(FadeIn(br, run_time=0.5), Write(text, run_time=0.5))

        self.next_section("bridges")

        self.play(FadeOut(br, text))

        
        self.play(Create(first_arc))

        self.next_section("arcfirstin")

        self.play(FadeOut(first_arc))
        line, dots, (second_arc, ) = construct_meander(6, [(0, 3, False), (0,5, True), (1,4, True), (1,2, False), (2,3, True), (4, 5, False)])

        second_arc.shift(DOWN*0.5)
        self.play(Create(second_arc))
        
        self.next_section("secondarc")
        #self.play())

class WhatWeWantToKnowStill(Scene):
    def construct(self):
        text = MathTex(r"M_n", font_size=120).set_scale(30.0)
        self.add(text)

class ExactAsymptoticsStill(Scene):
    def construct(self):
        Title = Tex(r"Meanders: exact asymptotics", font_size=80).set_scale(30.0).move_to(UP*2)
        SupTitle = Tex(r"Di Francesco, P. Golinelli, O. and Guitter, E.", font_size=40).set_scale(30.0).move_to(UP*1)

        text = MathTex(r"M_n^{(1)} \sim \frac{R^{2n} }{n^\alpha} \text{ for } n \rightarrow \infty", font_size=60).set_scale(30.0).move_to(DOWN*1.5)
        text[0][4].set_color(BLUE)

        self.play(Write(Title, run_time=0.5), Write(SupTitle, run_time=0.5), Write(text, run_time=0.5))

        framebox1 = SurroundingRectangle(text, buff = .1)
        self.next_section()
        self.play(Create(framebox1, run_time=0.5))

def get_group_of_meanders(file_name, k_components, color, spacing=0.15, scaling=0.5, order=6):
    with open(file_name) as file:
        data = json.load(file)

    #order = order
    
    if k_components != -1:
        one_comp = list(filter(lambda x: x["k_components"] == k_components, data["configurations"]))
    else:
        one_comp = list(data["configurations"])

    group = VGroup()

    meanders = []

    for config in one_comp:

        line, dots, arcs = construct_meanderv3(order, config["arcs"], paths=config["path"], index_to_color={0:color, 1:color, 2:color}, spacing=spacing, scale=scaling)
        
        square = Square(spacing*order).set_fill(BLACK, opacity=0.0).set_stroke(BLACK, 0, 0)

        objects = VGroup(line, *arcs, dots, square)

        meanders.append(objects)

        group.add(objects)

    group.arrange_in_grid(rows=2, buff=0.1)

    return meanders, group

def get_group_of_meanders2(file_name, k_components, color, spacing=0.15, scaling=0.5, order=6, range_start=0, range_end=0):
    with open(file_name) as file:
        data = json.load(file)

    #order = order
    
    if k_components != -1:
        one_comp = list(filter(lambda x: x["k_components"] == k_components, data["configurations"]))
    else:
        one_comp = list(data["configurations"])

    one_comp = one_comp[range_start:range_end]

    group = VGroup()

    meanders = []

    for config in one_comp:

        line, dots, arcs = construct_meanderv3(order, config["arcs"], paths=config["path"], index_to_color={0:BLUE_E, 1:BLUE_C, 2:BLUE_A}, spacing=spacing, scale=scaling)
        
        square = Square(spacing*order).set_fill(BLACK, opacity=0.0).set_stroke(BLACK, 0, 0)

        objects = VGroup(line, *arcs, dots, square)

        meanders.append(objects)

        group.add(objects)

    group.arrange_in_grid(rows=2, buff=0.1)

    return meanders, group


class MnkScene(Scene):
    def construct(self):
        with open("../simdata/all_configurations/q1.0000/n3/sim_data_20220609162011_1_0_0.json") as file:
            data = json.load(file)

        config = list(filter(lambda x: x["k_components"] == 1, data["configurations"]))[0]
        config2 = list(filter(lambda x: x["k_components"] == 1, data["configurations"]))[1]

        line, dots, arcs = construct_meanderv3(6, config["arcs"], paths=config["path"], index_to_color={0:WHITE, 1:YELLOW_E}, spacing=0.8, scale=1)
        line2, dots2, arcs2 = construct_meanderv3(6, config2["arcs"], paths=config2["path"], index_to_color={0:WHITE, 1:YELLOW_E}, spacing=0.8, scale=1)

        #self.add(line, *arcs, dots)

        line.shift(0.5*UP)
        dots.shift(0.5*UP)
        arcs[0].shift(0.5*UP)
        arcs2[0].shift(0.5*UP)

        self.play(FadeIn(line, dots))

        self.next_section()

        br = Brace(line, sharpness=1).shift(DOWN*2)

        self.play(FadeIn(br, run_time=0.5))

        text1 = MathTex(r"2n \text{ bridges}").set_scale(10.0).shift(DOWN*2.5)
        text1[0][1].set_color(BLUE)

        self.play(Write(text1))

        self.next_section()

        self.play(FadeIn(*arcs, run_time=0.5))

        self.next_section()

        self.play(FadeOut(*arcs, run_time=0.5), FadeIn(*arcs2, run_time=0.5))

        #self.next_section()

        #text2 = MathTex(r"\text{meander of order } n").set_scale(10.0).shift(UP*2.5)
        #text2[0][-1].set_color(BLUE)

        #self.play(Write(text2, run_time=0.5))

        self.next_section()

        textMn = MathTex(r"M_n").scale(2.5).shift(RIGHT*4.5)
        textMn[0][-1].set_color(BLUE)

        self.play(Write(textMn, run_time=0.5))

        self.next_section("fadeout")

        self.play(FadeOut(line, *arcs2, dots, textMn, text1, br))
        

        # meanders1, group1 = get_group_of_meanders("../simdata/all_configurations/q1.0000/n3/sim_data_20220609162011_1_0_0.json", 1)

        # for (i, meander) in enumerate(meanders1):
        #     self.play(FadeIn(meander, run_time=0.3, target_position=ORIGIN))

        
        #self.play(Create(group))

class MnkScenePartTwo(Scene):
    def construct(self):
        meanders1, group1 = get_group_of_meanders("../simdata/all_configurations/q1.0000/n3/sim_data_20220609162011_1_0_0.json", 1, GREEN_E)

        

        meanders2, group2 = get_group_of_meanders("../simdata/all_configurations/q1.0000/n3/sim_data_20220609162011_1_0_0.json", 2, GOLD_E)

        

        meanders3, group3 = get_group_of_meanders("../simdata/all_configurations/q1.0000/n3/sim_data_20220609162011_1_0_0.json", 3, RED_E)
        
        group1.scale(1.5)

        for (i, meander) in enumerate(meanders1):
            self.play(FadeIn(meander, run_time=0.3, target_position=ORIGIN))

        textMn = MathTex(r"M_3 = {}".format(len(meanders1))).scale(1.5).shift(UP*2)
        textMn[0][1].set_color(BLUE)

        self.play(Write(textMn))

        self.next_section()

        
        newtextMn = MathTex(r"M_3^{(k)}" + "= {}".format(len(meanders1))).scale(1.5).shift(UP*2)
        newtextMn[0][4].set_color(BLUE)
        newtextMn[0][2].set_color(GREEN_E)

        self.play(Transform(textMn, newtextMn))
        self.remove(textMn,newtextMn)
        self.add(newtextMn)
        

        self.next_section()

        newfilledtextMn = MathTex(r"M_3^{(1)}" + "= {}".format(len(meanders1))).scale(1.5).shift(UP*2)
        newfilledtextMn[0][4].set_color(BLUE)
        newfilledtextMn[0][2].set_color(GREEN_E)

        self.play(Transform(newtextMn, newfilledtextMn))

        self.next_section()

        self.remove(newtextMn,newfilledtextMn)
        self.add(newfilledtextMn)

        self.play(FadeOut(newfilledtextMn), FadeOut(group1))

        self.next_section()


        textMean1 = MathTex(r"M_3^{(1)}" + "= {}".format(len(meanders1))).scale(1)
        textMean1[0][4].set_color(BLUE)
        textMean1[0][2].set_color(GREEN_E)

        textMean2 = MathTex(r"M_3^{(2)}" + "= {}".format(len(meanders2))).scale(1)
        textMean2[0][4].set_color(BLUE)
        textMean2[0][2].set_color(GOLD_E)

        textMean3 = MathTex(r"M_3^{(3)}" + "= {}".format(len(meanders3))).scale(1)
        textMean3[0][4].set_color(BLUE)
        textMean3[0][2].set_color(RED_E)

        #textMeanTot = MathTex(r"M_3 = {}".format(len(meanders3)+len(meanders2)+len(meanders1))).scale(1).shift(UP*3)
        #group1copy = group1.copy()

        group1.scale(1/1.5)

        all_elements = VGroup(group1, textMean1, group2, textMean2, group3, textMean3)

        #self.play(Transform(group1, group1copy))
        #all_elements.shift(DOWN)

        all_elements.arrange_in_grid(cols=2, col_alignments="ll", buff=(1, 0.25))

        self.play(FadeIn(group1, textMean1, run_time=0.5))
        self.play(FadeIn(group2, textMean2, run_time=0.5))
        self.play(FadeIn(group3, textMean3, run_time=0.5))

        #extratext = VGroup(textMean1, textMean2, textMean3)

        self.next_section()

        framebox1 = SurroundingRectangle(textMean1, buff = .2)
        self.next_section()
        self.play(Create(framebox1))

        #self.next_section()

        #self.play(Write(textMeanTot))


        
        # for (i, meander) in enumerate(meanders2):
        #     self.play(FadeIn(meander, run_time=0.3, target_position=ORIGIN))

        # for (i, meander) in enumerate(meanders3):
        #     self.play(FadeIn(meander, run_time=0.3, target_position=ORIGIN))


        #self.play()



class SimulatingMeanders(Scene):
    def construct(self):
        with open("../build/testq0.1n4.json") as file:
            data = json.load(file)
        
        order = 8

        line, dots = construct_meander_body(order, spacing=0.7)

        #self.add(line, dots)

        last_arcs = None
        
        number_ = DecimalNumber(0.0, num_decimal_places=0).set_color(WHITE).set_scale(5.0).move_to(LEFT*4)
        # Add an updater to keep the DecimalNumber centered as its value changes
        #number_.add_updater(lambda number: number.move_to(LEFT*8))

        text = MathTex(r"M_4 =").set_scale(5.0).move_to(LEFT*5)
        self.play(Write(text), Write(number_), FadeIn(line), FadeIn(dots))

        count_ = 0

        for config in data["configurations"]:
            arcsdata = config["arcs"]

            line2, dots2, arcs2 = construct_meanderv2(order, arcsdata, spacing=0.7)
            


            if last_arcs != None:
                self.play(FadeOut(last_arcs, shift=LEFT, run_time=0.5), FadeIn(arcs2[0], shift=LEFT, run_time=0.5))
            else:
                self.play(FadeIn(arcs2[0], shift=LEFT, run_time=0.5))

            count_ += 1

            self.remove(number_)
            number_ = DecimalNumber(count_, num_decimal_places=0).set_color(WHITE).set_scale(5.0).move_to(LEFT*4)
            self.add(number_)

            self.wait(duration=0.5)
            last_arcs = arcs2[0]


def faces_from_data(data, seed=100):
    G = nx.Graph(data["nodes"])

    pos = nx.spring_layout(G, dim=3, iterations=100, weight=1, seed=seed)
    #print(pos)
    faces = np.array(data["faces"])-1

    pos2 = np.array(list(pos.values()))

    return pos2[faces]


def faces_from_data2(data, seed=100, iterations=100):
    #print(data["nodes"])
    G = nx.Graph(data["nodes"])
    #pos = nx.nx_agraph.graphviz_layout(G, prog="sfdp")

    #print(pos)
    pos = nx.spring_layout(G, dim=3, iterations=iterations, weight=1, seed=seed)
    #print(pos)
    faces = np.array(data["faces"])-1

    

    pos2 = np.array(list(pos.values()))

    first = pos2[faces[:,0:2]]
    second = pos2[faces[:,1:3]]

    normals = first-second
    #print(normals)
    #print(normals[:,0,:], normals[:,1,:])
    #print("BEGIN")
    #print()
    #print("END")
    #normals[]
    
    real_normals = np.cross(normals[:,0,:], normals[:,1,:])

    #print(cross_product_of_arr(normals))

    return pos2[faces], pos2, faces, real_normals


def draw_structure_advanced(file_name, index=0, seed=101, scale_factor=1.5, iterations=100):
    with open(file_name) as file:
        data100 = json.load(file)

        n = data100["configurations"][index]["n"]

        configuration = data100["configurations"][index]
        
        face_group100 = VGroup()
        line_width = 2


        max_distance = np.max(configuration["face_distance"])
        colors = cm.take_cmap_colors('cmr.ocean', max_distance, return_fmt='hex')

        colors_roads = cm.take_cmap_colors('tab20', len(configuration["path"]), return_fmt='hex')

        #face_group100.scale(0.8)
        #print("max_distance", max_distance)
        faces, pos, _, _ = faces_from_data2(configuration, seed=seed, iterations=iterations)
        
        for (i, face) in enumerate(faces):
            dist = configuration["face_distance"][i]
            #print("dist", dist)
            face_group100.add(Polygon(*(face*scale_factor), shade_in_3d=True, stroke_width=0.3).set_color(WHITE).set_fill(colors[dist-1], opacity=0.8))

        print("added faces")

        #tris.set_facecolor(["#FFF" for dist in configuration["face_distance"]])
        #tris.set_alpha(0.8)

        scale_factor_ = scale_factor 

        faces_visited = set()

        river_lines = []

        lines_3d = VGroup()
        roads_3d = VGroup()

        for river in configuration["river_connections"]:

            print(river)
            face_id = river[2]
            #print(river)
            if face_id in faces_visited:
                continue

            faces_visited.add(face_id)

            nodes_of_face = configuration["faces"][face_id-1]

            nodes_of_face_set = set(nodes_of_face)

            nodes_of_face_set.discard(river[0])
            nodes_of_face_set.discard(river[1])

            opposite = list(nodes_of_face_set)

            first_coord = (pos[river[0]-1] + pos[river[1]-1]) / 2
            second_coord = (pos[opposite[0]-1] + pos[opposite[1]-1]) / 2

            line_coords = np.array([first_coord, second_coord])

            print(line_coords)

            line = Line3D(first_coord*scale_factor, second_coord*scale_factor).set_color("#C43B3C")
            lines_3d.add(line)

            #print(line_coords)
            #river_lines.append(line_coords)
        

        print("added rest")
        #linecol_river = Line3DCollection(np.array(river_lines)*scale_factor_, color='blue', linewidth=line_width, zorder=4.5)



        #bridge_lines = []
        faces_visited = set()

        lines3D_bridges = []

        for bridge in configuration["bridge_pillars"]:
            face_id = bridge[2]

            if face_id in faces_visited:
                continue

            faces_visited.add(face_id)

            nodes_of_face = configuration["faces"][face_id-1]

            nodes_of_face_set = set(nodes_of_face)

            nodes_of_face_set.discard(bridge[0])
            nodes_of_face_set.discard(bridge[1])

            opposite = list(nodes_of_face_set)

            first_coord = (pos[bridge[0]-1] + pos[bridge[1]-1]) / 2
            second_coord = (pos[opposite[0]-1] + pos[opposite[1]-1]) / 2

            line_coords = np.array([first_coord, second_coord])*scale_factor_
            
            roads_3d.add(Line3D(first_coord*scale_factor, second_coord*scale_factor).set_color("#124984"))
            #print(line_coords)
            #lines3D_bridges.append(Line3D(*(line_coords.T), color=colors_roads[find_path(configuration["path"], face_id)], linewidth=line_width, zorder=4.5))
            #bridge_lines.append(line_coords)
            
        #linecol_bridge = Line3DCollection(np.array(bridge_lines)*scale_factor, color='blue', linewidth=line_width)
        
        return lines_3d, roads_3d, face_group100
        #return tris, pos, n, linecol_river, lines3D_bridges


def draw_structure(file_name, index=0, seed=101):
    with open(file_name) as file:
        data100 = json.load(file)

        configuration = data100["configurations"][index]
        
        face_group100 = VGroup()

        max_distance = np.max(configuration["face_distance"])
        colors = cm.take_cmap_colors('cmr.ocean', max_distance, return_fmt='hex')
        face_group100.scale(0.8)
        #print("max_distance", max_distance)
        for (i, face) in enumerate(faces_from_data(configuration, seed=seed)):
            dist = configuration["face_distance"][i]
            #print("dist", dist)
            face_group100.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.3).set_color(WHITE).set_fill(colors[dist-1], opacity=0.8))
        return face_group100

def draw_structure3(file_name, index=0, seed=101):
    with open(file_name) as file:
        data100 = json.load(file)

        configuration = data100["configurations"][index]
        
        face_group100 = VGroup()

        max_distance = np.max(configuration["face_distance"])
        colors = cm.take_cmap_colors('cmr.ocean', max_distance, return_fmt='hex')
        #face_group100.scale(0.8)
        #print("max_distance", max_distance)
        for (i, face) in enumerate(faces_from_data(configuration, seed=seed)):
            dist = configuration["face_distance"][i]
            #print("dist", dist)
            face_group100.add(Polygon(*(face*4), shade_in_3d=True, stroke_width=0.3).set_color(WHITE).set_fill(colors[dist-1], opacity=0.8))
        return face_group100

class FractalGeometryColorsMeanders(ThreeDScene):
    def construct(self):
        
        face_group = draw_structure("../simdata/meander_structure/q1.0000/n1000/sim_data_20220610131001_1_0_0.json")
        face_group.scale(2)
        self.add(face_group)

class FractalGeometryColorsMeandersv280(ThreeDScene):
    def construct(self):
        
        face_group = draw_structure("../simdata/meander_structure/q1.0000/n1000/sim_data_20220610131001_1_0_0.json", seed=80)
        face_group.scale(2)

        self.add(face_group)

class FractalGeometryColorsMeandersv290(ThreeDScene):
    def construct(self):
        
        face_group = draw_structure("../simdata/meander_structure/q1.0000/n1000/sim_data_20220610131001_1_0_0.json", seed=90)
        face_group.scale(2)

        self.add(face_group)

class FractalGeometryColorsMeandersv2100(ThreeDScene):
    def construct(self):
        
        face_group = draw_structure("../simdata/meander_structure/q1.0000/n1000/sim_data_20220610131001_1_0_0.json", seed=100)
        face_group.scale(2)

        self.add(face_group)

class FractalGeometryColorsMeandersv2200(ThreeDScene):
    def construct(self):
        
        face_group = draw_structure("../simdata/meander_structure/q1.0000/n1000/sim_data_20220610131001_1_0_0.json", seed=200)
        face_group.scale(2)

        self.add(face_group)


class FractalGeometryColorsMeandersOriginal(ThreeDScene):
    def construct(self):
        
        face_group = draw_structure3("../simdata/meander_structure_new/q0.1000/n128/sim_data_20220804133933_1_0_0.json")
        #face_group.scale(4/1.6)
        self.add(face_group)

    
class FractalGeometryColorsMeanders2(ThreeDScene):
    def construct(self):
        
        river, road, faces = draw_structure_advanced("../simdata/meander_structure_new/q0.1000/n128/sim_data_20220804133933_1_0_0.json", scale_factor=4)
        #faces.scale(2)
        self.add(faces)
        self.add(river)
        self.add(road)

class CriticalExponent(ThreeDScene):
    def construct(self):
        face_group = draw_structure("../simdata/meander_chain/q20.0000/n100/sim_data_20220611220055_1_0_0.json", seed=1024)
        face_group.scale(2.5)
        self.add(face_group)

class CriticalExponentLowQ(ThreeDScene):
    def construct(self):
        self.camera.background_color=WHITE
        face_group = draw_structure("../simdata/meander_chain/q0.1000/n100/sim_data_20220611220228_1_0_0.json", seed=1024)
        face_group.scale(2)
        self.add(face_group)


class CriticalExponentMidQ(ThreeDScene):
    def construct(self):
        face_group = draw_structure("../simdata/meander_chain/q1.0000/n100/sim_data_20220611221655_1_0_0.json", seed=90)
        face_group.scale(2)
        self.add(face_group)

class CriticalExponentMidQ1000(ThreeDScene):
    def construct(self):
        face_group = draw_structure("../simdata/meander_structure/q1.0000/n1000/sim_data_20220610131001_1_0_0.json")
        face_group.scale(2)
        self.add(face_group)


class FractalGeometryMeandersq1(ThreeDScene):
    def construct(self):
        face_group100 = draw_structure("../simdata/meander_structure/q1.0000/n100/sim_data_20220610124939_1_0_0.json").scale(1.2)
        face_group500 = draw_structure("../simdata/meander_structure/q1.0000/n500/sim_data_20220610160922_1_0_0.json").scale(1.2)
        face_group1000 = draw_structure("../simdata/meander_structure/q1.0000/n1000/sim_data_20220610131001_1_0_0.json").scale(1.2)

        textobject100 = MathTex(r"n = 100", font_size=40)
        textobject500 = MathTex(r"n = 500", font_size=40)
        textobject1000 = MathTex(r"n = 1000", font_size=40)

        qlabel = MathTex(r"q = 1", font_size=40)

        qlabel.shift(3*UP)

        group = VGroup(face_group100, face_group500, face_group1000, textobject100, textobject500, textobject1000)

        group.arrange_in_grid(rows=2)

        self.add(group)
        self.add(qlabel)

        
class FractalGeometryMeandersq20(ThreeDScene):
    def construct(self):
        face_group100 = draw_structure("../simdata/meander_chain/q20.0000/n100/sim_data_20220611220055_1_0_0.json").scale(1.2)
        face_group500 = draw_structure("../simdata/meander_chain/q20.0000/n500/sim_data_20220611220111_1_0_0.json").scale(1.2)
        face_group1000 = draw_structure("../simdata/meander_chain/q20.0000/n1000/sim_data_20220611220134_1_0_0.json").scale(1.2)

        textobject100 = MathTex(r"n = 100", font_size=40)
        textobject500 = MathTex(r"n = 500", font_size=40)
        textobject1000 = MathTex(r"n = 1000", font_size=40)

        qlabel = MathTex(r"q = 20", font_size=40)

        qlabel.shift(3*UP)

        group = VGroup(face_group100, face_group500, face_group1000, textobject100, textobject500, textobject1000)

        group.arrange_in_grid(rows=2)

        self.add(group)
        self.add(qlabel)


class FractalGeometryMeanders(ThreeDScene):
    

    def construct(self):
        # with open("../build/randq1n100.json") as file:
        #     data100 = json.load(file)
        
        # data100 = data100["configurations"][0]

        # with open("../build/randq1n1000.json") as file:
        #     data1000 = json.load(file)
        
        # data1000 = data1000["configurations"][0]

        # with open("../build/randq1n500.json") as file:
        #     data500 = json.load(file)
        
        # data500 = data500["configurations"][0]

        

        #print(pos2)
        #print(faces)
        # vertex_coords = [
        #     [1, 1, 0],
        #     [1, -1, 0],
        #     [-1, -1, 0],
        #     [-1, 1, 0],
        #     [0, 0, 2]
        # ]
        # faces_list = [
        #     [0, 1, 4],
        #     [1, 2, 4],
        #     [2, 3, 4],
        #     [3, 0, 4],
        #     [0, 1, 2, 3]
        # ]
        #self.set_camera_orientation(phi=75 * DEGREES, theta=30 * DEGREES)
        #pyramid = Polyhedron(pos2*4, faces.tolist(), graph_config=(vertex_type=None))
        #for i in range(len(pyramid.faces)):
        #    pyramid.faces[i].set_color(random_bright_color())
        #self.add(pyramid)
        # face_group100 = VGroup()
        # for face in faces_from_data(data100):
        #     face_group100.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.5).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))

        # face_group1000 = VGroup()
        # for face in faces_from_data(data1000):
        #     face_group1000.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.5).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))

        # face_group500 = VGroup()
        # for face in faces_from_data(data500):
        #     face_group500.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.5).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))
        
        face_group100 = draw_structure("../simdata/meander_structure/q1.0000/n100/sim_data_20220610124939_1_0_0.json")
        face_group500 = draw_structure("../simdata/meander_structure/q1.0000/n500/sim_data_20220610160922_1_0_0.json")
        face_group1000 = draw_structure("../simdata/meander_structure/q1.0000/n1000/sim_data_20220610131001_1_0_0.json")

        # face_group100 = draw_structure("../simdata/meander_structure/q1.0000/n100/sim_data_20220610124939_1_0_0.json")
        # face_group500 = draw_structure("../simdata/meander_structure/q1.0000/n100/sim_data_20220610124939_1_0_0.json")
        # face_group1000 = draw_structure("../simdata/meander_structure/q1.0000/n100/sim_data_20220610124939_1_0_0.json")

        # face_group100.move_to(4*LEFT)
        # face_group1000.move_to(4*RIGHT)



        # self.add(MathTex(r"n = 100", font_size=40).move_to(4*LEFT+3*DOWN))
        # self.add(MathTex(r"n = 500", font_size=40).move_to(3*DOWN))
        # self.add(MathTex(r"n = 1000", font_size=40).move_to(4*RIGHT+3*DOWN))

        textobject100 = MathTex(r"n = 100", font_size=40)
        textobject500 = MathTex(r"n = 500", font_size=40)
        textobject1000 = MathTex(r"n = 1000", font_size=40)
        textobjectinf = MathTex(r"n \rightarrow \infty", font_size=40)

        colors = cm.take_cmap_colors('cmr.ocean', 6, return_fmt='hex')

        quantumgravity = VGroup(
            Text("2D", font="Open Sans", font_size=36, weight=BOLD),
            Text("Quantum", font="Open Sans", font_size=36, weight=BOLD),
            Text("Gravity", font="Open Sans", font_size=36, weight=BOLD)
        ).arrange(DOWN).set_color('#FFF')

        quantumgravity.set_opacity(0)
        textobjectinf.set_opacity(0)

        group = VGroup(face_group100, face_group500, face_group1000, textobject100, textobject500, textobject1000)

        group.arrange_in_grid(rows=2, col_widths=[3, 3, 3])

        self.add(group)

        self.next_section()

        new_group = VGroup(face_group100, face_group500, face_group1000, quantumgravity, textobject100, textobject500, textobject1000, textobjectinf)
        
        self.play(new_group.animate.arrange_in_grid(rows=2, col_widths=[3, 3, 3, 3]))

        quantumgravity.set_opacity(1)
        textobjectinf.set_opacity(1)

        self.play(FadeIn(quantumgravity, run_time=0.5), FadeIn(textobjectinf, run_time=0.5))

        self.next_section()
        #self.next_section()


        #self.begin_ambient_camera_rotation(rate=0.1)
        #self.wait()
        #self.stop_ambient_camera_rotation()
        #self.move_camera(phi=180*DEGREES, run_time=3, rate_func=rate_functions.linear)
        #self.wait()

        
        #self.play(frame.animate.increment_phi(-90 * DEGREES))
        # self.begin_3dillusion_camera_rotation(rate=2)
        # self.wait(PI/2)
        # self.stop_3dillusion_camera_rotation()
        
        # self.play(Create(pyramid))


class ParaSurface(ThreeDScene):
    def func(self, u, v):
        return np.array([v, u, u**2+v**2])

    def construct(self):
        axes = ThreeDAxes(x_range=[-4,4], x_length=8)
        surface = Surface(
            lambda u, v: axes.c2p(*self.func(u, v)),
            u_range=[-PI, PI],
            v_range=[0, TAU]
        )
        self.set_camera_orientation(theta=70 * DEGREES, phi=75 * DEGREES)
        self.add(axes, surface)

class FractalGeometryMeandersNextRegime(ThreeDScene):
    

    def construct(self):
        with open("../build/randq20n100.json") as file:
            data100 = json.load(file)
        
        data100 = data100["configurations"][0]

        with open("../build/randq20n1000.json") as file:
            data1000 = json.load(file)
        
        data1000 = data1000["configurations"][0]

        with open("../build/randq20n500.json") as file:
            data500 = json.load(file)
        
        data500 = data500["configurations"][0]

        

        #print(pos2)
        #print(faces)
        # vertex_coords = [
        #     [1, 1, 0],
        #     [1, -1, 0],
        #     [-1, -1, 0],
        #     [-1, 1, 0],
        #     [0, 0, 2]
        # ]
        # faces_list = [
        #     [0, 1, 4],
        #     [1, 2, 4],
        #     [2, 3, 4],
        #     [3, 0, 4],
        #     [0, 1, 2, 3]
        # ]
        #self.set_camera_orientation(phi=75 * DEGREES, theta=30 * DEGREES)
        #pyramid = Polyhedron(pos2*4, faces.tolist(), graph_config=(vertex_type=None))
        #for i in range(len(pyramid.faces)):
        #    pyramid.faces[i].set_color(random_bright_color())
        #self.add(pyramid)
        face_group100 = VGroup()
        for face in faces_from_data(data100):
            face_group100.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.5).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))

        face_group1000 = VGroup()
        for face in faces_from_data(data1000, seed=200):
            face_group1000.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.5).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))

        face_group500 = VGroup()
        for face in faces_from_data(data500, seed=200):
            face_group500.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.5).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))
        
        face_group100.move_to(4*LEFT)
        face_group1000.move_to(4*RIGHT)

        self.add(MathTex(r"n = 100", font_size=40).move_to(4*LEFT+3*DOWN))
        self.add(MathTex(r"n = 500", font_size=40).move_to(3*DOWN))
        self.add(MathTex(r"n = 1000", font_size=40).move_to(4*RIGHT+3*DOWN))

        self.add(face_group100, face_group500, face_group1000)

        #self.begin_ambient_camera_rotation(rate=0.1)
        #self.wait()
        #self.stop_ambient_camera_rotation()
        #self.move_camera(phi=180*DEGREES, run_time=3, rate_func=rate_functions.linear)
        #self.wait()

        
        #self.play(frame.animate.increment_phi(-90 * DEGREES))
        # self.begin_3dillusion_camera_rotation(rate=2)
        # self.wait(PI/2)
        # self.stop_3dillusion_camera_rotation()
        
        # self.play(Create(pyramid))

class FractalGeometryMeandersLowRegime(ThreeDScene):
    

    def construct(self):
        with open("../build/randq01n100.json") as file:
            data100 = json.load(file)
        
        data100 = data100["configurations"][0]

        with open("../build/randq01n1000.json") as file:
            data1000 = json.load(file)
        
        data1000 = data1000["configurations"][0]

        with open("../build/randq01n500.json") as file:
            data500 = json.load(file)
        
        data500 = data500["configurations"][0]

        

        #print(pos2)
        #print(faces)
        # vertex_coords = [
        #     [1, 1, 0],
        #     [1, -1, 0],
        #     [-1, -1, 0],
        #     [-1, 1, 0],
        #     [0, 0, 2]
        # ]
        # faces_list = [
        #     [0, 1, 4],
        #     [1, 2, 4],
        #     [2, 3, 4],
        #     [3, 0, 4],
        #     [0, 1, 2, 3]
        # ]
        #self.set_camera_orientation(phi=75 * DEGREES, theta=30 * DEGREES)
        #pyramid = Polyhedron(pos2*4, faces.tolist(), graph_config=(vertex_type=None))
        #for i in range(len(pyramid.faces)):
        #    pyramid.faces[i].set_color(random_bright_color())
        #self.add(pyramid)
        face_group100 = VGroup()
        for face in faces_from_data(data100):
            face_group100.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.5).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))

        face_group1000 = VGroup()
        for face in faces_from_data(data1000):
            face_group1000.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.5).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))

        face_group500 = VGroup()
        for face in faces_from_data(data500):
            face_group500.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.5).set_color(WHITE).set_fill(random_bright_color(), opacity=0.8))
        
        face_group100.move_to(4*LEFT)
        face_group1000.move_to(4*RIGHT)

        self.add(MathTex(r"n = 100", font_size=40).move_to(4*LEFT+3*DOWN))
        self.add(MathTex(r"n = 500", font_size=40).move_to(3*DOWN))
        self.add(MathTex(r"n = 1000", font_size=40).move_to(4*RIGHT+3*DOWN))

        self.add(face_group100, face_group500, face_group1000)

        #self.begin_ambient_camera_rotation(rate=0.1)
        #self.wait()
        #self.stop_ambient_camera_rotation()
        #self.move_camera(phi=180*DEGREES, run_time=3, rate_func=rate_functions.linear)
        #self.wait()

        
        #self.play(frame.animate.increment_phi(-90 * DEGREES))
        # self.begin_3dillusion_camera_rotation(rate=2)
        # self.wait(PI/2)
        # self.stop_3dillusion_camera_rotation()
        
        # self.play(Create(pyramid))


class SimulatingMeandersChain(Scene):
    def construct(self):
        with open("../build/randq1n4chain.json") as file:
            data = json.load(file)
        
        order = 8

        line, dots = construct_meander_body(order, spacing=0.7)

        #self.add(line, dots)

        last_arcs = None
        
        #number_ = DecimalNumber(0.0, num_decimal_places=0).set_color(WHITE).set_scale(5.0).move_to(LEFT*4)
        # Add an updater to keep the DecimalNumber centered as its value changes
        #number_.add_updater(lambda number: number.move_to(LEFT*8))

        #text = MathTex(r"M_4 =").set_scale(5.0).move_to(LEFT*5)
        self.play(FadeIn(line), FadeIn(dots))

        #count_ = 0

        for config in data["configurations"]:
            arcsdata = config["arcs"]

            line2, dots2, arcs2 = construct_meanderv2(order, arcsdata, spacing=0.7)
            


            if last_arcs != None:
                self.play(FadeOut(last_arcs, shift=LEFT, run_time=0.5), FadeIn(arcs2[0], shift=LEFT, run_time=0.5))
            else:
                self.play(FadeIn(arcs2[0], shift=LEFT, run_time=0.5))

            #count_ += 1

            #self.remove(number_)
            #number_ = DecimalNumber(count_, num_decimal_places=0).set_color(WHITE).set_scale(5.0).move_to(LEFT*4)
            #self.add(number_)

            self.wait(duration=0.5)
            last_arcs = arcs2[0]

class SimulatingMeandersChain2(Scene):
    def construct(self):
        
        meanders, group = get_group_of_meanders2("../simdata/meander_chain/q1.0000/n4/sim_data_20220611214421_1_0_0.json", -1, WHITE, order=8, range_start=0, range_end=8)
        group.scale(1)
        group.arrange_in_grid(rows=1)

        self.add(group)



class LowqHighq(Scene):
    def construct(self):
        with open("../build/randq1n4chain.json") as file:
            data1 = json.load(file)

        arcsdata1 = data1["configurations"][0]["arcs"]

        with open("../build/randq20n4chain.json") as file:
            data2 = json.load(file)
        
        arcsdata2 = data2["configurations"][0]["arcs"]

        order = 8

        #line, dots = construct_meander_body(order, spacing=0.7)

        line1, dots1, arcs1 = construct_meanderv2(order, arcsdata1, spacing=0.5)

        line2, dots2, arcs2 = construct_meanderv2(order, arcsdata2, spacing=0.5)

        sx = 3.5

        line1.move_to(LEFT*sx)
        line2.move_to(RIGHT*sx)
        dots1.move_to(LEFT*sx)
        dots2.move_to(RIGHT*sx)
        arcs1 = arcs1[0].shift(LEFT*sx)
        arcs2 = arcs2[0].shift(RIGHT*sx)

        self.add(line1, line2, dots1, dots2, arcs1, arcs2)

        self.add(MathTex(r"q = 0.1", font_size=40).move_to(sx*LEFT+3*DOWN))
        self.add(MathTex(r"q = 20", font_size=40).move_to(3*DOWN+RIGHT*sx))

        self.add(MathTex("Z = \sum_{configurations} q^{k}").shift(UP*3))

        #self.add(line, dots)

        #last_arcs = None
        
        #number_ = DecimalNumber(0.0, num_decimal_places=0).set_color(WHITE).set_scale(5.0).move_to(LEFT*4)
        # Add an updater to keep the DecimalNumber centered as its value changes
        #number_.add_updater(lambda number: number.move_to(LEFT*8))

        #text = MathTex(r"M_4 =").set_scale(5.0).move_to(LEFT*5)
        #self.play(FadeIn(line), FadeIn(dots))

        #count_ = 0

        # for config in data["configurations"]:
        #     arcsdata = config["arcs"]

        #     line2, dots2, arcs2 = construct_meanderv2(order, arcsdata, spacing=0.7)
            


        #     if last_arcs != None:
        #         self.play(FadeOut(last_arcs, shift=LEFT, run_time=0.5), FadeIn(arcs2[0], shift=LEFT, run_time=0.5))
        #     else:
        #         self.play(FadeIn(arcs2[0], shift=LEFT, run_time=0.5))

        #     #count_ += 1

        #     #self.remove(number_)
        #     #number_ = DecimalNumber(count_, num_decimal_places=0).set_color(WHITE).set_scale(5.0).move_to(LEFT*4)
        #     #self.add(number_)

        #     self.wait(duration=0.5)
        #     last_arcs = arcs2[0]