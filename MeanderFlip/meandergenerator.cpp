//
//  meandergenerator.cpp
//  MeanderFlip
//
//  Created by Rutger Berns on 17/10/2021.
//
#include "meandergenerator.hpp"

int get_previous(int degree, int i) {
    return ((i-1) + degree) % degree;
}

// Goal is to have:
/*
 * +----------+----------+----------+
 * |          | 3(shore) |          |
 * +----------+----------+----------+
 * | 0(river) |          | 2(river) |
 * +----------+----------+----------+
 * |          | 1(shore) |          |
 * +----------+----------+----------+
 * 
 */
Face generate_bridge_segment(int degree, int start_id, int face_id) {
    Face face;
    for (int i = 0; i < degree; i++) {
        struct Edge edge = {i + start_id, (i+1)%degree + start_id, get_previous(degree, i) + start_id , 0, (bool)(i % 2), face_id, false};
        face.push_back(edge);
    }
    return face;
}




/*
 * 
 * Default generated configuration
 * 
 * +---+---+---+---+
 * | / | \ | / | \ |
 * +---+---+---+---+
 * | 1 | 2 | 3 | 4 |
 * +---+---+---+---+
 * | \ | \ | / | / |
 * +---+---+---+---+
 * 
 */
MeanderSystem generate_glued_map(int n_bridges) {
    // Approach generate n faces (n should be even otherwise)
    int edge_id = 1;
    
    int degree = 4;
        
    std::vector<Edge> edges;

    std::vector<Face> faces;
    
    std::vector<int> face_of_edge;

    // Generating the faces
    for (int i = 0; i < n_bridges; i++) {
        int face_id = i + 1;
        faces.push_back(generate_bridge_segment(degree, edge_id, face_id));
        face_of_edge.push_back(face_id);
        face_of_edge.push_back(face_id);
        face_of_edge.push_back(face_id);
        face_of_edge.push_back(face_id);
        edge_id += 4;
    }

    // Connecting the faces, the river
    for (int i = 0; i < n_bridges; i++) {
        faces[i][2].adjacent = faces[(i+1)%n_bridges][0].id;
        faces[(i+1)%n_bridges][0].adjacent = faces[i][2].id;
    }

    // Glueing
    for (int i = 0; i < n_bridges/2; i++) {
        int j = n_bridges - i - 1;
        int k = 2*i;
        int q = 2*i+1;
        // down side all arcs will be placed \\\\\/////
        faces[i][1].adjacent = faces[j][1].id;
        faces[j][1].adjacent = faces[i][1].id;
        // top side all small arcs /\/\/\/\/\/\/
        faces[k][3].adjacent = faces[q][3].id;
        faces[q][3].adjacent = faces[k][3].id;
    }

    // Because of the top side consists of small arcs. It will have n_bridges ending shore edges.
    // The bottom side has large arcs, which will result in just one shore edge ending.
    int n_shore_ends = n_bridges/2 + 2;

    // the two edges on the beginning and end are labelled is end, because they are always connected to the root nodes of the meander system.
    faces[0][0].is_end = true;
    faces[n_bridges-1][2].is_end = true;

    for (int i = 0; i < n_bridges; i++) {
        edges.insert(edges.end(), faces[i].begin(), faces[i].end());
    }

    
    //#if INFOPRINT
    //debug_face_printer(edges);
    //#endif
    //MeanderPermutation perm = MeanderPermutation(n_bridges);
    //perm.from(edges);
    
    return {edges, face_of_edge, faces, n_shore_ends};
}

