% !TeX root = ../thesis.tex

\subsubsection{String susceptibility}
\label{section:observables:stringsus}
 
The string susceptibility measures the fractal structure of two-dimensional
quantum gravity (2DQG). It quantifies the roughness of a surface
\cite{Ambjorn1993, Jain1992}. The method to find the string susceptibility is by
taking a discrete surface and counting the number of spikes of a certain size,
more formally called the number of baby universes. 

A baby universe is like a balloon on a surface connected through a
minimal neck, like in Figure \ref{theory:sketch:babyuniverse}. The minimal neck
is the smallest possible loop in a geometry. More formally, a baby universe is
defined as a simply connected area whose boundary length is much smaller than
the square root of its area \cite{Jain1992}. The area of the baby universe can
be denoted by $n$ and the area of the total geometry by $N$. The area $n$ of a
baby universe must satisfy $n < N/2$.

In the case of quadrangulations the smallest loop or \textbf{minimum} neck is
equal to $2$ and in the case of triangulations the smallest loop is equal to
$3$. A baby universe connected through a minimum neck is called a \textbf{mimbu}
short for 'minimum neck baby universe' \cite{Jain1992}.

% Baby
% universes are areas of the surface of a random geometry that have a small
% boundary but are way smaller than the square root of its area

\begin{figure}[htb!]
    \centering
    \includegraphics[]{theory/assets/babyuniversedrawing.pdf}
    \caption{Sketch of a geometry with 2 minimal necks indicated by the dashed lines. For the rightmost minimal neck, the baby universe is indicated in \textcolor{rbprimarycolor}{red}.}
    \label{theory:sketch:babyuniverse}
\end{figure}

An interesting measure to look at, is the distribution describing mimbu's of
size $n$. When the genus is set to zero, $g = 0$, the partition function of
random surfaces in two-dimensional quantum gravity generally grows
asymptotically with $N$ as

\begin{equation}
    Z(N) \approx e^{\mu_c N} N^{\gamma_\mathrm{s} -3}
    \label{theory:observables:eq:partitionfunction}
\end{equation}
where $\gamma_\mathrm{s}$ is the string susceptibility. Using this partition function one
can approximate the probability for a \textbf{mimbu} of size $n$ on a geometry
of size $N$, given by

\begin{equation}
    p \approx \frac{n Z(n) (N - n) Z(N - n)}{N Z(N)} = (n \cdot (1- n/N))^{\gamma_\mathrm{s}-2}
    \label{theory:observables:eq:distributionofbabyuniverses}
\end{equation}
For calculating the probability of a \textbf{mimbu} of size $n$ on a geometry of
size $N$, it is used that a geometry of size $N$ will be made out of a geometry
of size $n$ and a geometry of size $N-n$.

From equation \eqref{theory:observables:eq:distributionofbabyuniverses} one can
see that the string susceptibility is easily retrieved by measuring $p$ and then
performing regression on $\ln{p}$ versus $\ln{(n \cdot (1- n/N))}$. The slope
should then measure $\gamma_\mathrm{s} - 2$.

\subsubsection{Hausdorff dimension}
\label{section:hausdorffdimension}

Hausdorff dimension, $d_H$ is a critical exponent which describes the dimension
of a geometry. For flat geometry $\mathbb{R}^d$, the dimension $d$ is equal to
$d_H$ \cite{BuddCourseGeometry2017}. For self-similar, fractal geometry the
Hausdorff dimension is less trivial. One way to define the Hausdorff dimension
is by the scaling of the number of vertices in a geodesic ball of size $r$ where
\begin{equation}
    V \sim r^{d_H}
    \label{eq:theory:volumehaus}
\end{equation}
for the number of edges $n \rightarrow \infty$. The number of vertices on a
surface of a geodesic ball grows then like $r^{d_H-1}$ for $n \rightarrow
\infty$ \cite{VanderFeltz2021}.

A trivial example is the Hausdorff dimension of a circle, see Figure
\ref{fig:theory:hausdorffsketch}. The area of the circle $\pi r^2$ which should
be proportional to the number of dots (vertices) in the circle. Comparing this
to equation \eqref{eq:theory:volumehaus} yields a Hausdorff dimension of $d_H =
2$. The same can be concluded when counting the number of vertices at the border
of the circle which scales like $2\pi r$ and thus also yields a Hausdorff
dimension of $2$.

\begin{figure}
    \centering
    \begin{tikzpicture}
        %\draw[step=0.5 ,gray] (-4,-3) grid[] (4,3);
        \foreach \x in {-4,-3.5,...,4} {
            \foreach \y in {-3,-2.5,...,3} {
                \fill[color=black] (\x,\y) circle (0.2mm);
            }
        }   
        \draw[black, fill=rbrblueb,fill opacity=0.5] (0,0) circle (2.2);
        \draw (0,0) node {$r^{d_H} \sim \pi r^2$};
        \draw (2.9, 1.5) node {$r^{d_H-1} \sim 2 \pi r $};
    \end{tikzpicture}
    \caption{$\mathbb{R}^2$ surface with a circle of area $r^{d_H} \sim \pi r^2$
    and the number of vertices at the distance $r$ is equal to $r^{d_H-1} \sim 2
    \pi r $.}
    \label{fig:theory:hausdorffsketch}
\end{figure}

The Hausdorff dimension can be measured using the relations for the number of
vertices at a distance $r$. It will however be difficult to measure accurately
because it is only valid when $1 \ll r \ll N^{1/{d_H}}$ and would thus require
huge system sizes \cite{BuddCourse2021}. A way to get around that is by using
finite size scaling which allows to measure the Hausdorff dimension accurately
\cite{BuddCourse2021}. The idea of finite size scaling is to rescale the
distance profiles in Figure \ref{results:fig:distanceprofile} such that they
overlap. Finite size scaling will be more practically introduced in section
\ref{section:finitesizescaling}.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/distanceprofilerho.pdf}
    
    \caption{Histogram of $\rho_n(r)$ of meander decorated geometries with $q = 1$.}
    \label{results:fig:distanceprofile}
\end{figure}

