
#%%
import json
import dateparser
from meanderutils import *
import os.path
# %%
getsims(1, 128, pre="sim_datastring")


# %%
for sim_path in getsims(3, 512, pre="sim_datastring"):
    file = open(sim_path)
    

    jsondata = json.load(file)



    jsondata["measurements"] = [{"measurement_type": 2, "t_measuring": jsondata["time_measuring"], "rate": jsondata["iterations"], "data": jsondata["stringhist"], "metadata": {"n_independent": jsondata["N"]}}]

    jsondata["version"] = 6
    del jsondata['stringhist']
    del jsondata['N']
    del jsondata["num_measurements"]
    del jsondata["time_measuring"]
    #print()

    date = dateparser.parse(jsondata['date'])

    del jsondata["type"]
    del jsondata["date"]
    del jsondata["k_by_iterations"]
    del jsondata["dist"]
    del jsondata["iterations"]
    del jsondata["n_meanders_of_k"]


    jsondata["date"] = date.strftime("%Y%m%d%H%M%S")

    #print()

    #out_data = json.dumps(jsondata)

    

    out_path = os.path.join(os.path.dirname(sim_path), "sim_data_{}_{}_{}_{}.json".format(jsondata["date"], jsondata["seed"], jsondata["run"]+1, jsondata["totalrun"]))

    
    with open(out_path, 'w') as f:
        json.dump(jsondata, f)
# %%

# %%
