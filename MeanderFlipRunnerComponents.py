from pathlib import Path
import os
from stringprep import map_table_b2
import subprocess
import sys
import multiprocessing
import argparse
import random
import math
import json
import pandas as pd
import numpy as np

def in_sweeps(num, n):
    return num/(2*n)

def eq_time(n, q):
    #base = pow(2, int(math.ceil(math.log2(q*256))))
    # 512 sweeps * q * n (where 1/q is mapped to q)
    base = 512 * (2 * n) * int(math.exp(abs(math.log(q))))
    return base

def nkcalculator(density, n_sweeps, n):
    """
    The nk calculator calculates the number of 
    total number of measurements and the rate (k)
    from the sample density and the number of 
    sweeps.
    n is the size of the system.
    """
    if(density > 2*n):
        raise ValueError("You cannot have a sample density than the number of flips in a sweep")

    
    k = int(2*n/density)
    n_measurements = n_sweeps*density

    return k, n_measurements
    


def run(conf):
    n, q, n_files, n_measurements, name = conf
    
    df = pd.read_csv("simdata/compcorrelation/report.csv")

    rate = int(np.round(2*df.loc[(df["q"] == q) & (df["n"] == n)]["mean"])) # 2 * autocorr time

    print("rate:", rate)

    #rate, n_measurements = nkcalculator(density, n_sweeps, n=n)
    
    # print("will perform", n_measurements, "with a rate of", rate)
    # print("n_sweeps:", n_sweeps, "density:", density)

    config = [{ "id": 3, 
                "rate": rate, 
                "type": "components",
                "setup_data": dict()}]
    strconfig = json.dumps(config)
    strconfigescaped = json.dumps(strconfig)
    #print(strconfig, strconfigescaped)

    #n_sweeps = n_sweeps * int(math.exp(abs(math.log(q)))) 
    #print("will perform: ", n_sweeps)
    #Path("simdata/q{:.4f}/n{}".format(q, n)).mkdir(parents=True, exist_ok=True)
    cmd = "./build_cluster/meander_main -o {} -q {} -n {} -c {} --seed {} {} -m {} -e {}".format(name, q, n, n_files, random.randint(0, 2**30), "-f {}".format(n_measurements) if n_measurements else "", strconfigescaped, eq_time(n, q))
    return subprocess.call(cmd, shell=True)
    #return run_sim(q=q, n=n, configurations=c, n_measurements_per_file=n_measurements_per_file, name=name) # relaxation time is at least 64 times n

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Generates Meander Sytems')
    parser.add_argument('-q', type=float, help='q weight', nargs="+")
    parser.add_argument('-s', type=int, help='Size (list of integers)', nargs="+")
    parser.add_argument('-c', type=int, default=200, help='Number of files')
    parser.add_argument('-p', type=int, default=4, help='Number of concurrent processes')
    parser.add_argument('-f', type=int, default=1024, help='Number of measurements per file')
    parser.add_argument('-o', type=str, default="default", help='name of file')
    #parser.add_argument('-d', type=int, default=32, help='Density')
    args = parser.parse_args()

    if args.q is None:
        parser.error("No weight q specified!")
    if args.s is None:
        parser.error("No sizes s specified!")
    for size in args.s:
        if size % 2:
            parser.error("Not dividable by two! You sure?")


    pool_obj = multiprocessing.Pool(processes=args.p)
    pool_obj.map(run, [(size, q, args.c, args.f, args.o) for q in args.q for size in args.s])

# python MeanderFlipRunnerComponentsCorrelation.py -q 2 -s 128 -c 8 -p 1 -f 1024 -o compcorrelation -d 32