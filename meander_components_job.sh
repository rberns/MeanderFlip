#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=200M
#SBATCH --time=23:59:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 7 -w cn113

cd ~/MeanderFlip/
python3 MeanderFlipRunnerComponents.py -q 2.0 -s 256 512 1024 2048 4096 8192 16384 -c 16 -p 7 -o components -f 262144 > liveout.txt