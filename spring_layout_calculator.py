import json
import networkx as nx
import numpy as np
import sys
from plyfile import PlyData, PlyElement
import pathlib

# from https://github.com/dranjan/python-plyfile/blob/master/test/test_plyfile.py
def tet_ply(text, byte_order):
    vertex = np.array([(0, 0, 0),
                          (0, 1, 1),
                          (1, 0, 1),
                          (1, 1, 0)],
                         dtype=[('x', 'f4'), ('y', 'f4'), ('z', 'f4')])

    face = np.array([([0, 1, 2], 255, 255, 255),
                        ([0, 2, 3], 255,   0,   0),
                        ([0, 1, 3],   0, 255,   0),
                        ([1, 2, 3],   0,   0, 255)],
                       dtype=[('vertex_indices', 'i4', (3,)),
                              ('red', 'u1'), ('green', 'u1'),
                              ('blue', 'u1')])

    return PlyData(
        [
            PlyElement.describe(
                vertex, 'vertex',
                comments=['tetrahedron vertices']
            ),
            PlyElement.describe(face, 'face')
        ],
        text=text, byte_order=byte_order,
        comments=['single tetrahedron with colored faces']
    )

def faces_from_data(data, seed=100):
    G = nx.Graph(data["nodes"])

    pos = nx.spring_layout(G, dim=3, iterations=100, weight=1, seed=seed)
    #print(pos)
    faces = np.array(data["faces"])-1

    pos2 = np.array(list(pos.values()))

    return pos2[faces]

path = sys.argv[1]

fancy_path = pathlib.Path(path)

print(fancy_path.stem)

print(pathlib.Path(fancy_path.parent, fancy_path.stem + ".ply"))

new_path = pathlib.Path(fancy_path.parent, fancy_path.stem + ".ply")

seed = 1234

with open(path) as file:
    data = json.load(file)

for config in data["configurations"]:
    G = nx.Graph(config["nodes"])
    # pos gives dictionary of vertex: position of vertex
    pos = nx.spring_layout(G, dim=3, iterations=100, weight=1, seed=seed)
    # vertices gives list of coordinates
    #print()
    vertices = np.array([tuple(e) for e in list(pos.values())], dtype=[('x', 'f4'), ('y', 'f4'),('z', 'f4')])

    #vertices = vertices.astype()

    print(vertices)

    print(config["nodes"])

    #faces = 

    faces = np.array([(np.array(e)-1, np.random.random()*255, np.random.random()*255, np.random.random()*255) for e in config["triangles"]], dtype=[('vertex_indices', 'i4', (3,)), ('red', 'u1'), ('green', 'u1'), ('blue', 'u1')])
    
    PlyData(
        [
            PlyElement.describe(
                vertices, 'vertex',
                comments=['tetrahedron vertices']
            ),
            PlyElement.describe(faces, 'face')
        ],
        comments=['single tetrahedron with colored faces']
    ).write(new_path)