# Project colors
from ast import If
import cmasher as cmr
import platform

from matplotlib.colors import to_rgba

def colors_to_latex(colors):
    lst = [color_to_latex(item) + "\n" for item in colors.items()]

    with open("docs/rbcolors.sty", "w") as file:
        file.write("\ProvidesPackage{rbcolors}\n\n")
        file.writelines(lst)

    return lst
        
def color_to_latex(color: tuple):
    return r"\definecolor{" + color[0] + r"}{HTML}{" + color[1][1:] + r"}"


cmap = cmr.get_sub_cmap('RdBu', 0.15, 0.95, N=6)

hex_colors = cmr.take_cmap_colors('RdBu', 6, cmap_range=(0.15, 0.95), return_fmt='hex')
rgba_colors = cmr.take_cmap_colors('RdBu', 6, cmap_range=(0.15, 0.95))




cmap_blues = cmr.get_sub_cmap('cmr.ocean', 0.15, 0.95, N=6)

hex_colors_realblues = cmr.take_cmap_colors('Blues', 6, cmap_range=(0.05, 0.95), return_fmt='hex')
hex_colors_blues = cmr.take_cmap_colors('cmr.ocean', 6, cmap_range=(0.05, 0.95), return_fmt='hex')
rgba_colors_blues = cmr.take_cmap_colors('cmr.ocean', 6, cmap_range=(0.05, 0.95))


colors = dict(  rblinkcolor = hex_colors[-1],
                rbblue = hex_colors[-1],
                rbprimarycolor = hex_colors[0],
                rbbluea = hex_colors_blues[0],
                rbblueb = hex_colors_blues[1],
                rbbluec = hex_colors_blues[2],
                rbblued = hex_colors_blues[3],
                rbbluee = hex_colors_blues[4],
                rbbluef = hex_colors_blues[5],
                rbrbluea = hex_colors_realblues[0],
                rbrblueb = hex_colors_realblues[1],
                rbrbluec = hex_colors_realblues[2],
                rbrblued = hex_colors_realblues[3],
                rbrbluee = hex_colors_realblues[4],
                rbrbluef = hex_colors_realblues[5],
                rblightgray = "#D5D5D5",
                rbdarkgray = "#343434",
                rbsecondarycolor= hex_colors_realblues[2])

print(colors)


colors_rgba = {x[0]: to_rgba(x[1]) for x in list(colors.items())}


if __name__ == "__main__":
    print(colors_to_latex(colors))
    if platform.system() == "Darwin":
        from colorpaletteconverter import Palette

        print(colors_rgba)

        palette = Palette(name="rbcolor")

        for (name, color) in colors_rgba.items():
            palette.add_color(color=color, key=name)
        
        palette.save()
        



