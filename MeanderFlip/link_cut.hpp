//
//  link_cut.hpp
//  
// 
//  https://github.com/saadtaame/link-cut-tree
//

#ifndef link_cut_hpp
#define link_cut_hpp

#include <iostream>

struct Node
{   int sz = 0, label; /* size, label */
    int vir = 0;
    Node *p, *pp, *l, *r; /* parent, path-parent, left, right pointers */
    Node() { p = pp = l = r = 0; }
};

class LinkCut
{   

    public:
    Node *x;
    LinkCut(int n);
    virtual ~LinkCut();
    void link(int u, int v);

    int cut(int u);

    int root(int u);

    int depth(int u);

    int deepest_depth(int u);

    int lca(int u, int v);
};


#endif