#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=200M
#SBATCH --time=2-23:59:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 4

cd ~/MeanderFlip/
python3 MeanderFlipRunnerStringSusceptibilityCorr.py -q 1 2 3 4 5 6 7 8 9 10 -s 128 -p 4 -c 16 -f 1024 -o stringsusceptibilitycorr

