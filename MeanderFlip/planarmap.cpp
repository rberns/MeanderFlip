//
//  planarmap.cpp
//  MeanderFlip
//
//  Created by Rutger Berns on 11/10/2021.
//

#include "planarmap.hpp"
#include "json.hpp"

std::ostream& operator<<(std::ostream& os, const Edge& edge)
{
    return os << "id: " << edge.id << ", next: " << edge.next << ", previous:" << edge.previous << ", adjacent: " << edge.adjacent << ", is_shore: "<< edge.is_shore << ", is_end: " << edge.is_end;
}

std::ostream& operator<<(std::ostream& os, const Face& face)
{
    os << "size: " << face.size() << "\n";
    for (auto& edge : face) {
        os << edge << "\n";
        //std::cout << "id: " << edge.id << ", next: " << edge.next << ", previous:" << edge.previous << ", adjacent: " << edge.adjacent << ", is_shore: "<< edge.is_shore << "\n";
    }
    return os;
}

void debug_face_printer(Face face) {
    std::cout << "size: " << face.size() << "\n";
    
    for (auto& edge : face) {
        std::cout << edge << "\n";
        //std::cout << "id: " << edge.id << ", next: " << edge.next << ", previous:" << edge.previous << ", adjacent: " << edge.adjacent << ", is_shore: "<< edge.is_shore << "\n";
    }
}

struct Connection {
    int from, to;
};

int get_cw(std::vector<Edge> & edges, int edge) {
    return edges[edges[edge-1].next-1].adjacent;
}


/// FROM planarmap.h from Timothy Budd precision measurements of Hausdorff dimensions in two-dimensional quantum gravity

