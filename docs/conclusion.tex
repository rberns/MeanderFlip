% !TeX root = thesis.tex




A discrete surface can be built based on a meander configuration and
\citeauthor{DiFrancescoExactAsym2000} conjectured that in the continuum limit
these discrete surfaces are two-dimensional quantum gravity (2DQG). The main
research question was, is there numerical evidence that verifies or falsifies
this? Predictions about the string susceptibility and Hausdorff dimension conjecture have been checked using numerical evidence. Additionally, the expected phase transition for $q > 2$ has
also been studied.

To check the conjectured relations Markov chain Monte Carlo techniques were used
to sample geometries built by meander configurations. The sampling can be
controlled by the weight $q$ which is the weight assigned to the roads. The
number of roads, string susceptibility and Hausdorff dimension of the sampled
geometries have been measured as a function of the weight $q$.

The number of roads/components $k$ was studied as an order parameter for the
expected phase transition for $q > 2$. The number of roads $k$ as a function of
$q$ was however smooth for all measured sizes. The analysis using Binder
cumulant of the number of roads did not show any signs of a phase transition. A
possible explanation for this might be that the number of roads is not a good
order parameter. In conclusion, no phase transition was measured when using the
number of components as an order parameter.

Measuring the string susceptibility was more fruitful. The measurements matched
the analytic results for the string susceptibility at $q = 1$. The string
susceptibility was fitted using several methods. Fitting based on an
approximation of the analytic result at $q = 1$ gave good results at $q = 1$ for
the string susceptibility but poor results for other values of $q$. Fitting
using the linear fitting methods worked best and uses the least amount of
assumptions which results in more reliable string susceptibility measurements.
Between $1 < q < 2$ the string susceptibility seemed to follow the predicted
curve. When approaching $q = 2$ however, the numerical string susceptibility
seemed to favour a slightly higher string susceptibility than predicted. The
same was reported in \cite{DiFrancescoNumCheck2000} by
\citeauthor{DiFrancescoNumCheck2000} The deviation can be attributed to
discretization effects and/or it could be a flaw of the fitting method. The
conjecture could also be wrong for $q = 2$ but more numerically evidence
at $q = 2$ is required to test that. Extrapolating the string susceptibility for
$q \rightarrow \infty$, the string susceptibility approaches $\gamma_\mathrm{s}
= 1/2$ which corresponds to the predicted branched polymer phase. A more sharp
transition was predicted. The conjecture holds however for the continuum limit,
studying it with discrete geometry can be a possible cause for not seeing a
sharp transition to branched polymers. The smooth instead of sharp transition is
therefore attributed to discretization effects.

The Hausdorff dimension at $q = 1$ was measured and matches well with the
expected Hausdorff dimension for $q = 1$. The data might even seem to favour the
Ding and Gwynne formula for the larger system sizes but no hard conclusions can
be made because the statistical error is too high. At $q = 0.2$, the measured
Hausdorff dimensions seemed to approach the expected Hausdorff dimension, but
larger system sizes are required to verify this claim. It must however be noted
that for larger system sizes the Hausdorff dimension could also approach to a
value just below the predicted Hausdorff dimension which would mean that the
predicted Hausdorff dimension is wrong.

In conclusion, studying quadrangulations decorated with meanders using Markov
Chain Monte Carlo simulations and the developed flip move based on the
Metropolis-Hastings algorithm resulted in new numerical evidence for larger
meander systems than studied in the past. The new numerical data for larger
system sizes largely supports the conjecture for $0.2 \leq q \leq 2$ and no big
differences with results at smaller size systems have been found. Meanders can
thus most likely be used to study statistical systems with a central charge $c
\in [-4, -1]$ coupled to 2DQG. For $q > 2$, no sharp transition is found which
shows that very large geometries ($n \gg 262144$) might be required to see a
sharp transition.

\textit{And the next time you look at a very long meandering river, you might
actually be looking at two-dimensional quantum gravity!}

\section*{Outlook}

Further numerical evidence can be collected in the future by extending the
measurements to more values of $q$, in particular for the Hausdorff dimension
for $q < 1$. Also, larger system sizes should be measured. This is however
computationally expensive. In the future, other Markov chain Monte Carlo moves
like exchanging baby universes could be introduced to reduce the autocorrelation
time and therefore speed up the simulations \cite{Ambjornbabymove}.

The quadrangulations decorated with meanders are an interesting system which
with enough computational resources might even be used to study the conjectured
relations between the Hausdorff dimension and string susceptibility, like the
Ding and Gwynne formula. 

For future research, multi-river systems could be considered. This is
conjectured to allow, studying CFTs coupled to gravity with different central
charges between $c = -4$ and $c = 1$. When the weight for the rivers is set to
$q_1 = 1$ and the weight of the roads set to $q_2 = n$, one can study the $O(n)$
loop model coupled to 2DQG \cite{DiFrancescoNumCheck2000}. The flip move can be
extended easily to multi-river systems, however, keeping the algorithm $O(\log
n)$ might be difficult.

The string susceptibility can be fitted using different fitting methods that are
tailor-made to find just the linear parts of a fit, in section
\ref{section:linearfitting} RANSAC was tried which gave good results in
comparison to linear fitting. In the future, RANSAC-like approaches could be
tried for obtaining the string susceptibility using a fit of the probability of
meander baby universes. Furthermore, a different method for measuring the string
susceptibility could be used based on the method described in \cite{Golinelli2000}
by \citeauthor{Golinelli2000}. For that, the Markov chain Monte Carlo simulation
would need to be changed to a grand canonical ensemble, so different system
sizes can be found in the Markov chain.




