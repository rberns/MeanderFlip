#include "puremeanderflip.hpp"

/*
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * |         |   |   |         |  | flip=> |  | 1 | 3 |  | turn=> |  |   |   |   |   |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * |  1 acw  | ∧ | ∨ |  3 cw   |  | flip=> |  | > | > |  | turn=> |  | 2 | ∧ | ∨ | 1 |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * | 2 first | ∧ | ∨ |4 afirst |  | flip=> |  | < | < |  | turn=> |  | 4 | ∧ | ∨ | 3 |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * |         |   |   |         |  | flip=> |  | 2 | 4 |  | turn=> |  |   |   |   |   |
 * +---------+---+---+---------+--+--------+--+---+---+--+--------+--+---+---+---+---+
 * 
 * Description:
 * 
 * GlueMap {.first = acw, .cw = afirst, .afirst = cw, .acw = first}
 * 
 * The flip is straightforward, (1) and (2) above will be glued. And (3) and (4) will be glued.
 * It returns a glueMap which contains the adjacent of the listed names. So int first will contain the id of the adjacent of first.
 */
GlueMap glue_flip(int first, int cw, int afirst, int acw) {
    return GlueMap {.first = acw, .cw = afirst, .afirst = cw, .acw = first};
}