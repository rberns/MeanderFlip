from pathlib import Path
import os
from stringprep import map_table_b2
import subprocess
import sys
import multiprocessing
import argparse
import random
import math
import json

def iterations_generator(n, q):
    base = pow(2, int(math.ceil(math.log2(q*256))))
    return n*base

def run(conf):
    n, q, sample_density, n_sweeps, name = conf
    rate = int(2*n/sample_density)
    #[{\"id\": 3, \"rate\": 1, \"type\": \"components\", \"setup_data\": {}}]
    config = [{ "id": 3, 
                "rate": rate, 
                "type": "components",
                "setup_data": dict()}]
    strconfig = json.dumps(config)
    strconfigescaped = json.dumps(strconfig)
    print(strconfig, strconfigescaped)

    n_sweeps = n_sweeps * int(math.exp(abs(math.log(q))))
    print("will perform: ", n_sweeps)
    #Path("simdata/q{:.4f}/n{}".format(q, n)).mkdir(parents=True, exist_ok=True)
    cmd = "./build/meander_main -o {} -q {} -n {} -c {} --seed {} {} -m {}".format(name, q, n, 1, random.randint(0, 2**30), "-f {}".format(n_sweeps*sample_density) if n_sweeps else "", strconfigescaped)
    return subprocess.call(cmd, shell=True)
    #return run_sim(q=q, n=n, configurations=c, n_measurements_per_file=n_measurements_per_file, name=name) # relaxation time is at least 64 times n

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Generates Meander Sytems')
    parser.add_argument('-q', type=float, help='q weight')
    parser.add_argument('-s', type=int, help='Size (list of integers)', nargs="+")
    parser.add_argument('-d', type=int, default=64, help='Number of samples per sweep')
    parser.add_argument('-p', type=int, default=4, help='Number of concurrent processes')
    parser.add_argument('-f', type=int, default=1000, help='Number of sweeps')
    parser.add_argument('-o', type=str, default="default", help='name of file')
    args = parser.parse_args()

    if args.q is None:
        parser.error("No weight q specified!")
    if args.s is None:
        parser.error("No sizes s specified!")
    for size in args.s:
        if size % 2:
            parser.error("Not dividable by two! You sure?")


    pool_obj = multiprocessing.Pool(processes=args.p)
    pool_obj.map(run, [(size, args.q, args.d, args.f, args.o) for size in args.s])