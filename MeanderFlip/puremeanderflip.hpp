//
//  puremeanderflip.hpp
//  PureMeanderFlip
//
//  Created by Rutger Berns on 4/12/2021.
//

#ifndef puremeanderflip_hpp
#define puremeanderflip_hpp

struct FlipMap {
    int first, cw, afirst, acw;
};

// This GlueMap contains the adjacent of the listed names. So int first will contain the id of the adjacent of first.
typedef FlipMap GlueMap;

GlueMap glue_flip(int first, int cw, int afirst, int acw);

#endif /* puremeanderflip_hpp */