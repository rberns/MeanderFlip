//
//  multicomponentmeander.cpp
//  MeanderFlip
//
//  Created by Rutger Berns on 14/11/2021.
//

#include <iostream>
#include <fstream>
#include "meanderrunner.hpp"
#include "json.hpp"
#include "meanderplotter.hpp"
#include "meandermeasurement.hpp"

void debug_print_all_paths(const std::vector<std::vector<int> > & paths) {
    for (auto& path : paths) {
            std::cout << "path of size: " << path.size() << " ";
            for (int i: path)
                std::cout << i << "->";
            std::cout << "\n";
        }
}


int main(int argc, const char * argv[]) {
    int n = 3;
    int seed = 1;
    double q = 40;

    MeanderRunner runner(n, seed, q);

    runner.setup();

    

    //runner.simulation.quad_stringsusceptibility_rand();

    

    std::cout << "Iterating:\n";
    
    int iterations = pow(2, 15);

    runner.iterate(iterations);
    //runner.current_state();
    //runner.simulation.map.print_edges();

    //std::vector<Edge> edges = ;
    nlohmann::json j;

    //MeanderStringSusceptibility string_measurement(&runner, 1);

    runner.report();
    runner.current_state();

    //string_measurement.measure_string_susceptibility_full();
    //j["str"] = string_measurement.string_susceptibility_data();

    std::cout << "str " << j << "\n";

    //nlohmann::json j;
    
    // j["configurations"] = nlohmann::json::array();

    // for (size_t i = 0; i < 10; i++)
    // {
    //     // while (runner.simulation.k_components() != 1)
    //     // {
    //     //     runner.iterate(1000);
    //     // 
    //     runner.iterate(1);

    //     runner.current_state();
    //     //runner.iterate(10);
         
    //     std::pair< std::vector<std::vector<int> >, std::vector<std::vector<int> > > nodes = runner.simulation.map.node_graph();

    //     nlohmann::json j_nested;


    //     j_nested["nodes"] = nodes.first;
    //     j_nested["faces"] = nodes.second;

    //     j_nested["arcs"] = runner.simulation.map.convert_to_arcs();

    //     j["configurations"].push_back(j_nested);

    //     //runner.iterate(1000);
    // }
    

    



    //convert_to_node_graph(runner.simulation.map.get_edges());

    
    // for (size_t i = 0; i < 20; i++)
    // {
    //     runner.iterate(1);
    //     plot(runner.simulation.map.get_edges(), 2*n, "test" + std::to_string(i) + ".png");
    // }
    
    
    // auto out = runner.measure_string_susceptibility(100);

    // for(auto o: out) {
    //     std::cout << "x: " << o.first << "y: " << o.second << "\n";
    // }
    // runner.iterate(1);
    // runner.current_state();
    // runner.iterate(1);
    // runner.current_state();
    
    
    // runner.report();
 
    //debug_print_all_paths(runner.simulation.get_paths());
    //debug_print_all_paths(runner.simulation.get_paths_from_permutation());

    // std::cout << "Done iterating " << runner.simulation.k_components() << "\n";

    //j["dist"] = runner.simulation.measure_dist();

    j["k_by_iterations"] = runner.k_components;

    j["n"] = n;
    j["seed"] = seed;

    // j["how"] = "hey";

    //std::cout << j;

    std::ofstream myfile;
    myfile.open ("randq20n4chain.json");
    myfile << j;
    myfile.close();
}
