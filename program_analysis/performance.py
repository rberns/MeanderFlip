import xml.dom.minidom
import os
import os.path
import matplotlib.pyplot as plt

# get_numbers_from_string
def gnfs(string):
    return [int(i) for i in string.split() if i.isdigit()]

if not os.path.isfile("performance_data.xml"):
    print("running command")
    os.system("../build/meanderrunner_tests -r xml -o performance_data.xml")

doc = xml.dom.minidom.parse("performance_data.xml")

testcases = doc.getElementsByTagName("TestCase")

print(testcases)

x = []
y = []
Sy = []

for testcase in testcases:
    if "flip" in testcase.getAttribute("name"):
        datapoints = testcase.getElementsByTagName("BenchmarkResults")
        print(datapoints)
        for point in datapoints:
            x.append(gnfs(point.getAttribute("name"))[0])
            y.append(int(float(point.getElementsByTagName("mean")[0].getAttribute("value"))))
            Sy.append(int(float(point.getElementsByTagName("standardDeviation")[0].getAttribute("value"))))

print(x, y, Sy)

plt.errorbar(x, y, yerr=Sy)

plt.show()