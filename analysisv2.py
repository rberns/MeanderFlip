import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import json
from scipy.interpolate import interp1d
from scipy.optimize import minimize

Nbins = 200

avg = dict()

def analyser(array, N):
    arr = np.array(array)
    print(N, arr)
    
    pNarr = []

    for row in arr:
        hist, bin_edges = np.histogram(row, bins=np.arange(Nbins+1))

        pN = hist/len(row)
        pNarr.append(pN)

    pNarrNum = np.array(pNarr)
    pNavg = np.mean(pNarrNum, axis=0)
    
    avg[N] = pNavg

    


with open('data_A.json') as json_file:
    data = json.load(json_file)

    print(data.keys())

    for (key, value) in data.items():
        analyser(value, int(key))


for (key, value) in avg.items():
    x = np.arange(Nbins)
    plt.plot(x, value, label="N: {}".format(key))
    f = interp1d(x, value)    

plt.legend()
plt.show()



for (key, value) in avg.items():
    x = np.linspace(0, Nbins-1, 200)
    f = interp1d(np.arange(Nbins), value, kind='cubic')
    plt.plot(x, f(x), label="Interpolated N: {}".format(key))

plt.legend()
plt.show()


dH = 4
for (key, value) in avg.items():
    N = key
    x = np.linspace(0, Nbins-1, 200)
    f = interp1d(np.arange(Nbins), value, kind='cubic')
    plt.plot(x*N**(-1/dH), f(x)*N**(1/dH), label="Interpolated N: {}".format(key))

plt.legend()
plt.show()

# Fitting

def fitfunc(dH, curves):

    yvals = []

    x = np.linspace(0, Nbins-1, 200)

    for (key, value) in curves.items():
        N = key
        f = interp1d(np.arange(Nbins), value, kind='cubic', bounds_error=False, fill_value=0)
        yvals.append(f(x*N**(1/dH))*N**(1/dH))

    yvals = np.array(yvals)

    #print(yvals)

    #print(np.std(yvals, axis=0))

    #plt.plot(x,np.std(yvals, axis=0))
    return np.sum(np.std(yvals, axis=0))

#fitfunc(3.56)



res = minimize(fitfunc, 3.5, args=(avg), bounds=[(1,5)])

print(res)

dH = res.x[0]
for (key, value) in avg.items():
    N = key
    x = np.linspace(0, Nbins-1, 200)
    f = interp1d(np.arange(Nbins), value, kind='cubic')
    plt.plot(x*N**(-1/dH), f(x)*N**(1/dH), label="Interpolated N: {}".format(key))

plt.legend()
plt.show()