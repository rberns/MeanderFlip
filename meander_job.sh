#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=100M
#SBATCH --time=4:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 16 -w cn113

cd ~/MeanderFlip/
python3 MeanderFlipRunner.py -q 0.25 -i 50 -s 2048 2048 2048 2048 4096 4096 4096 4096 8192 8192 8192 8192 16384 16384 16384 -p 16