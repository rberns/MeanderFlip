# %%
from meanderutils import *
# %%
q = 3.0
n_size = 512
N = 2*n_size

# %%
df, data = string_hist_full_data(q=q, n=n_size)
# %%
print(df)

df.to_pickle("simdata/q{:.4f}/n{}/stringhist.pkl".format(q, n_size))
# %%
data
dfmetadata = pd.DataFrame(data=[data.values()], columns=data.keys())

dfmetadata.to_pickle("simdata/q{:.4f}/n{}/stringhist_metadata.pkl".format(q, n_size))
#pickle.dump(data,"metadata")

# %%


# %%
