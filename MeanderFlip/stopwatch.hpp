//
//  meanderrunner.hpp
//  MeanderFlip
// 
//  Created by Rutger Berns on 19/04/22
// 
// Based on online stopwatch example
//

#ifndef stopwatch_hpp
#define stopwatch_hpp

#include <iostream>
#include <ctime>
#include <chrono>

class Stopwatch
{
private:
    uint64_t elapsed_time = 0;
    std::chrono::steady_clock::time_point start_time;
    /* data */
public:
    Stopwatch(/* args */);
    ~Stopwatch();
    void start();
    void pause();
    void resume();
    void reset();
    uint64_t nanoseconds();
};


#endif