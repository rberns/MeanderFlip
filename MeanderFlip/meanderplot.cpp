#include <stdio.h>
#include <iostream>
#include <cairo/cairo.h>

int main() {
    printf("Hello world!\n");

    int N = 10;

    cairo_surface_t *surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 250*N, 250);
    cairo_t *cr = cairo_create (surface);

    cairo_select_font_face (cr, "serif", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size (cr, 32.0);
    cairo_set_source_rgb (cr, 0.0, 0.0, 1.0);
    cairo_move_to (cr, 10.0, 50.0);
    cairo_show_text (cr, "Hello, world");

    for (size_t i = 0; i < N; i++)
    {
        cairo_move_to (cr, i*250, 0);
        cairo_rectangle (cr, i*250, 0, 250, 250);
        cairo_set_source_rgb (cr, 1, 0, 0);
        cairo_set_line_width (cr, 2);
        cairo_stroke (cr);
    }
    

   

    cairo_destroy (cr);
    cairo_surface_write_to_png (surface, "hello.png");
    cairo_surface_destroy (surface);
    return 0;

}