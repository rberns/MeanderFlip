#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=100M
#SBATCH --time=2:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 4 -w cn113

cd ~/MeanderFlip/
python3 MeanderFlipRunner.py -q 1 -i 200 -s 1024 1024 1024 1024 --sus