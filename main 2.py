import matplotlib.pyplot as plt
import numpy as np

arr = np.loadtxt("test.txt")

print(arr)

arrcount = len(arr)

pNNarr = []

N = 1200

for row in arr:
    print("len", len(row))
    hist, bin_edges = np.histogram(row, bins=np.arange(40))

    pN = hist/len(row)
    pNNarr.append(pN * N)

pNNarrNum = np.array(pNNarr)
pNNavg = np.mean(pNNarrNum, axis=0)

area = np.trapz(pNNavg, x=np.arange(1, 40))
print("area", area)

for pNN in pNNarr:
    plt.plot(np.arange(1, 40), pNN)

x = np.linspace(0, 40, 200)

#plt.plot(x-1, (0.3*np.power(x, 3.56-1)))
plt.ylim(0, 150)
plt.xlim(0, 40)

plt.show()

y = lambda xx: (0.3*np.power(xx, 3.56-1))

pNNorm = np.poly1d(np.polyfit(np.arange(1, 40), pNNavg/N, 16))

dH = 3.56

plt.plot(x, pNNorm(x))
plt.plot(np.arange(1, 40), pNNavg/N)

plt.show()

from scipy.optimize import curve_fit

#plt.plot(x, N**(1/3.56)*pNNorm(x))
plt.plot(x*N**(1/3.56), N**(1/3.56)*y(x*N**(-1/3.56)))
#plt.plot(np.arange(1, 40), pNNavg/N)

plt.show()

#NN = 1200

# def fitfunc(x, dH):
#     return (NN**(1/dH))*pNNorm((NN**(1/dH))*x)

#xvals = np.arange(1, 40)*(NN**())

# popt, pcov = curve_fit(fitfunc, np.arange(1,40), pNNavg/area)
# print(popt)

# #plt.plot(x, fitfunc(x, *popt), label="fit")
# plt.plot(x, fitfunc(x, 3.56), label="fit2")

# plt.plot(np.arange(1, 40)*(NN**(1.0/3.56)), pNNavg*(NN**(1.0/3.56)), label="Data")
# plt.title("Final plot")
# plt.xlim(0, 5)
# plt.ylim(0, 1)
# plt.legend()
# plt.show()

#from scipy.optimize import curve_fit


# plt.plot(np.arange(1,40), pNN)
# plt.show()

# hist, bin_edges = np.histogram(arr, bins=np.arange(40))

# pN = hist/arrcount
# pNN = pN * np.arange(1,40)

# plt.plot(np.arange(1,40), pNN)
# plt.show()

# rng = np.random.RandomState(10)  # deterministic random data

# plt.hist(arr, bins=np.arange(40))
# plt.title("Histogram")
# plt.show()

