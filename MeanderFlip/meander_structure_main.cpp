//
//  multicomponentmeander.cpp
//  MeanderFlip
//
//  Created by Rutger Berns on 14/11/2021.
//

#include <iostream>
#include <fstream>
#include "meanderrunner.hpp"
#include "json.hpp"
#include "meanderplotter.hpp"
#include "meandermeasurement.hpp"
#include "fileio.hpp"
#include "popl/popl.hpp"
#include <functional>

#define MAX_ITERATIONS 100000

void debug_print_all_paths(const std::vector<std::vector<int> > & paths) {
    for (auto& path : paths) {
            std::cout << "path of size: " << path.size() << " ";
            for (int i: path)
                std::cout << i << "->";
            std::cout << "\n";
        }
}

const uint64_t mnk[12][12] ={{1,0,0,0,0,0,0,0,0,0,0,0},
                            {2,2,0,0,0,0,0,0,0,0,0,0},
                            {8,12,5,0,0,0,0,0,0,0,0,0},
                            {42,84,56,14,0,0,0,0,0,0,0,0},
                            {262,640,580,240,42,0,0,0,0,0,0,0},
                            {1828,5236,5894,3344,990,132,0,0,0,0,0,0},
                            {13820,45164,60312,42840,17472,4004,429,0,0,0,0,0},
                            {110954,406012,624240,529104,271240,85904,16016,1430,0,0,0,0},
                            {933458,3772008,6540510,6413784,3935238,1569984,405552,63648,4862,0,0,0},
                            {8152860,35994184,69323910,76980880,54787208,26200468,8536890,1860480,251940,16796,0,0},
                            {73424650,351173328,742518832,919032664,742366152,412348728,161172704,44346456,8356656,994840,58786,0},
                            {678390116,3490681428,8028001566,10941339452,9871243896,6230748192,2830421952,934582000,222516030,36936988,3922512,208012}};

std::vector<std::vector<int> > triangles(std::vector<std::vector<int> > faces) {
    std::vector<std::vector<int> > triangles_out;
    for (auto face : faces)
    {
        triangles_out.push_back({face[0], face[1], face[2]});
        triangles_out.push_back({face[0], face[2], face[3]});
    }
    return triangles_out;
}

int main(int argc, const char * argv[]) {
    // default command:
    // ./build/meander_finder -o all_configurations
    //
    // generates all configurations of a certain size up to n = 12.
    //  
    //

    int n = 3;
    int seed = 1;
    double q = 1;
    int n_configurations = 2;
    int iterations = 0;
    std::string default_name = "default";

    popl::OptionParser op("Allowed options");

    auto name_option = op.add<popl::Value<std::string> >("o", "name", "output name", default_name);
    auto N_option   = op.add<popl::Value<int>>("n", "meanderoforder", "meander of order n, which results in 2n bridges", n, &n);
    auto q_option   = op.add<popl::Value<double>>("q", "weight", "q parameter (weight)", q, &q);
    auto c_option = op.add<popl::Value<int>>("c", "independentconfigurations", "number of runs", n_configurations, &n_configurations);
    auto i_option = op.add<popl::Value<int>>("i", "interval", "interval", iterations, &iterations);

    op.parse(argc, argv);

    Fileio file = Fileio(q, n, seed, 0, name_option->value());
    file.initialize_dir();



    MeanderRunner runner(n, seed, q);


    runner.setup();

    


    uint64_t therm_time = (2*(uint64_t)n)*1024*(uint64_t)ceil(exp(abs(log(q))));

    std::cout << "therm_time: " << therm_time << "\n";
    
    if (iterations == 0)
    {
        int iterations = therm_time;
    }
    
    

    runner.iterate(therm_time);

    
    
    

    nlohmann::json j;

    j["configurations"] = nlohmann::json::array();

    for (size_t i = 0; i < n_configurations; i++)
    {
        runner.iterate(iterations);

        auto path = runner.simulation.get_paths();
        nlohmann::json j_nested;
        j_nested["n"] = n;
        j_nested["k_components"] = runner.simulation.k_components();
        j_nested["arcs"] = runner.simulation.map.convert_to_arcs();
        j_nested["path"] = path;

        auto nodes = runner.simulation.map.node_graph();

        j_nested["nodes"] = std::get<0>(nodes);
        j_nested["faces"] = std::get<1>(nodes);

        j_nested["triangles"] = triangles(std::get<1>(nodes));
        //j_nested["vertex_distance"] = std::get<3>(nodes);
        j_nested["face_distance"] = std::get<3>(nodes);

        j_nested["bridge_pillars"] = std::get<4>(nodes);
        j_nested["river_connections"] = std::get<5>(nodes);
        j_nested["connection_is_shore"] = std::get<6>(nodes);

        j["configurations"].push_back(j_nested);
    }
    


    // for (size_t i = 0; i < MAX_ITERATIONS; i++)
    // {
    //     //runner.iterate(10);
    //     //auto path = runner.simulation.get_paths();
    //     //if(!sets.contains(path)) {
    //         sets.insert(path);

    //         nlohmann::json j_nested;


    //         // j_nested["nodes"] = nodes.first;
    //         // j_nested["faces"] = nodes.second;
    //         //j_nested[""] = runner.simulation
    //         j_nested["n"] = n;
    //         j_nested["k_components"] = runner.simulation.k_components();
    //         j_nested["arcs"] = runner.simulation.map.convert_to_arcs();
    //         j_nested["path"] = path;

    //         j["configurations"].push_back(j_nested);

    //         hist[runner.simulation.k_components()-1] += 1;
            
    //         //if(runner.simulation.k_components() == 1) {
    //         runner.current_state();
    //         //}

    //     }
    //     if (sets.size() == sum_paper)
    //     {
    //         std::cout << "reached " << sum_paper << " configurations\n";
    //         break;
    //     }
    // }

    // for (auto h : hist)
    // {
    //     std::cout << h << ", ";        
    // }
    
    
    file.write(j, 0);

    //runner.report();
    //runner.current_state();

    //j["arcs"] = runner.simulation.map.convert_to_arcs();

    //std::cout << j << "\n";


    //std::cout << j << "\n";
    //string_measurement.measure_string_susceptibility_full();
    //j["str"] = string_measurement.string_susceptibility_data();

    // std::cout << "str " << j << "\n";

    //nlohmann::json j;
    
    // j["configurations"] = nlohmann::json::array();

    // for (size_t i = 0; i < 10; i++)
    // {
    //     // while (runner.simulation.k_components() != 1)
    //     // {
    //     //     runner.iterate(1000);
    //     // 
    //     runner.iterate(1);

    //     runner.current_state();
    //     //runner.iterate(10);
         
    //     std::pair< std::vector<std::vector<int> >, std::vector<std::vector<int> > > nodes = runner.simulation.map.node_graph();

    //     nlohmann::json j_nested;


    //     j_nested["nodes"] = nodes.first;
    //     j_nested["faces"] = nodes.second;

    //     j_nested["arcs"] = runner.simulation.map.convert_to_arcs();

    //     j["configurations"].push_back(j_nested);

    //     //runner.iterate(1000);
    // }
    

    



    //convert_to_node_graph(runner.simulation.map.get_edges());

    
    // for (size_t i = 0; i < 20; i++)
    // {
    //     runner.iterate(1);
    //     plot(runner.simulation.map.get_edges(), 2*n, "test" + std::to_string(i) + ".png");
    // }
    
    
    // auto out = runner.measure_string_susceptibility(100);

    // for(auto o: out) {
    //     std::cout << "x: " << o.first << "y: " << o.second << "\n";
    // }
    // runner.iterate(1);
    // runner.current_state();
    // runner.iterate(1);
    // runner.current_state();
    
    
    // runner.report();
 
    //debug_print_all_paths(runner.simulation.get_paths());
    //debug_print_all_paths(runner.simulation.get_paths_from_permutation());

    // std::cout << "Done iterating " << runner.simulation.k_components() << "\n";

    //j["dist"] = runner.simulation.measure_dist();

    // j["k_by_iterations"] = runner.k_components;

    // j["n"] = n;
    // j["seed"] = seed;

    // // j["how"] = "hey";

    // //std::cout << j;

    // std::ofstream myfile;
    // myfile.open ("randq20n4chain.json");
    // myfile << j;
    // myfile.close();
}
