# Introduction

Meanders are a quite simple model. It's described by a road passing a river. If the road passes the river $2n$ times we speak of meander of order $n$.

It is a self-avoiding loop.

The common questions for meanders is finding how many configurations can you make for a certain amount of bridges. This is relevant for example for polymer chain folding of long molecules [1]. Another example would be the number of ways to fold a piece of paper [1].



Finding the exact amount of configurations is the challenging part. So far no exact formula for the amount of configurations has been found [2]. 

One approach one can take is enumerating all possible configurations. The total amount of configurations becomes very huge very quick and therefore this has only been done for up to $n = 30$ [3,4]. In other words for up to 60 bridges.

$$
\begin{equation}
Hello
\end{equation}
$$

60 bridges is not a lot.

So how would you get passed this problem?

The first step was to put it into a loop model.

meander numbers scale asymptotically as Mn ∼ CR2n/nα,where R is a connectivity constant and α a configuration exponent.

First of all you need a model.

You give every closed segment (loop) a weight for example $n_1$ [2]

## 2D quantum gravity
Why would you want to study 2D quantum gravity? 2D quantum gravity can be seen as a laboratory, where conceptual questions about quantum gravity can be studied [5].

[1. Di Francesco, P., Golinelli, O. & Guitter, E. Meander, folding, and arch statistics. Mathematical and Computer Modelling vol. 26 (1997).]

[2. Di Francesco, P., Golinelli, O. & Guitter, E. Meanders: exact asymptotics. Nucl. Phys. B 570, 699–712 (2000).]

[3. Fukuda, M. & Nechita, I. Enumerating meandric systems with large number of loops. Ann. l’Institut Henri Poincare Comb. Phys. their Interact. 6, 607–640 (2016).]

[4. [BS10] Bruce Bobier and Joe Sawada. A fast algorithm to generate open meandric systems and meanders. ACM
Transactions on Algorithms (TALG), 6(2):42, 2010] (from reference 3)

[5. https://www.mv.helsinki.fi/home/hoyer/symp13/Presentations/Ambjorn.pdf]

