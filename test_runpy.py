from default_analysis_funcs import *
import matplotlib.pyplot as plt
import sys
import os
#import cPickle as pickle
import pickle
# sizes = [size for size in ]

# plot_deviation("data_N22q1s1000000gen.json")
# plot_deviation("data_N22q1s100000gen.json")
# plot_deviation("data_N24q1s1000000gen.json")
# plot_deviation("data_N24q1s100000gen.json")

max_iter = int(sys.argv[1])

seed = int(sys.argv[2])

q = float(sys.argv[3])

id_ = sys.argv[4]

sizes = [int(size) for size in sys.argv[5:]]

print(sizes)

def run_sim(size, n_iter, q, seed):
    name = "data_N22q1s{}gen2".format(str(size))
    print(os.system("./build/meander_main -n {} -s {} -p 1 -f {}{}.json -q {} -y {}".format(str(size), str(n_iter), name, id_, q, str(seed))))
    return name

sims = []

for size in sizes:
    sims.append(run_sim(size, max_iter, q, seed))
    
pickle.dump([sims, max_iter], open('latestsim.pkl', 'wb'))
# plot_deviation("data_N22q1s1000000gen.json")
# plot_deviation("data_N22q1s100000gen.json")
# plot_deviation("data_N24
# 
# q1s1000000gen.json")
# plot_deviation("data_N24q1s100000gen.json")

for sim in sims:
    sim += id_ + ".json"
    print("sim at iter: {}".format(max_iter))
    plot_deviation(sim, False)

plt.show()
# plt.show()