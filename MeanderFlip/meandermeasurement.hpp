//
//  meandermeasurement.hpp
//  meandermeasurement
//
//  Created by Rutger Berns on 12/02/2022.
//

#ifndef meandermeasurement_hpp
#define meandermeasurement_hpp

#include "meanderrunner.hpp"
#include "histogram.hpp"
#include "json.hpp"
#include "stopwatch.hpp"

class MeanderMeasurement
{
protected:
    MeanderRunner * runner;
    Stopwatch measure_stopwatch;

    
    

public:
    int rate = 1;
    int id = 0;

    //virtual void measure(MeanderMonteCarlo * simulation, const FlipInfo& info, int iteration) = 0;
    MeanderMeasurement(MeanderRunner * runner);

    void measure();
    void setup(nlohmann::json jsondata);

    nlohmann::json json();

    virtual void measure_() = 0;
    virtual nlohmann::json json_() = 0;
    virtual void setup_(nlohmann::json setup_data) = 0;
    virtual void reset() = 0;

    virtual ~MeanderMeasurement() = default;
};

class MeanderStringSusceptibility: public MeanderMeasurement
{
private:
    // sample rate
    int n_samples = 1;
    uint64_t batching_size = 1;
    int histogram_size;
    //int index_in_batch = 0;
    int batch_index = 0;

    // susceptibility data
    std::vector<Histogram> hists;
    //Histogram hist;

public:
    MeanderStringSusceptibility(MeanderRunner * runner);

    // setup
    void setup_(nlohmann::json setup_data);

    // string susceptibility
    void measure_();
    //void measure_string_susceptibility();
    void measure_string_susceptibility_full();
    //void measure_string_susceptibility2();
    // data
    std::vector<std::tuple<std::vector<uint64_t>, std::vector<uint64_t> > > data();
    nlohmann::json json_();

    void empty();

    std::vector<uint64_t> N();

    void reset();
};

class MeanderDistance: public MeanderMeasurement
{
private:

    // sample rate
    uint64_t n_samples = 1;
    uint64_t batching_size = 1;
    size_t histogram_size = 8;
    int batch_index = 0;
    bool is_dual = true;

    // distance data
    std::vector<Histogram> hists;

public:
    MeanderDistance(MeanderRunner * runner);

    // setup
    void setup_(nlohmann::json setup_data);

    // string susceptibility
    void measure_();
    void measure_distance();

    // data
    
    std::vector<std::tuple<std::vector<uint64_t>, std::vector<uint64_t> > > data();
    nlohmann::json json_();

    void empty();

    std::vector<uint64_t> N();

    void reset();
};

class MeanderComponents: public MeanderMeasurement
{
private:
    // number of components through time
    std::vector<int> k_components = std::vector<int>();
    

public:
    MeanderComponents(MeanderRunner * runner);

    // setup
    void setup_(nlohmann::json setup_data);

    // string susceptibility
    void measure_();

    // data
    nlohmann::json json_();

    void reset();
};


#endif