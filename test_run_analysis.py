from default_analysis_funcs import *
import matplotlib.pyplot as plt
import sys


plot_deviation("data_N22q1s1000000gen.json")
plot_deviation("data_N22q1s100000gen.json")
plot_deviation("data_N24q1s1000000gen.json")
plot_deviation("data_N24q1s100000gen.json")

plt.show()