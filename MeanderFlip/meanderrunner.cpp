#include "meanderrunner.hpp"


MeanderRunner::MeanderRunner(int n, int seed, double q): simulation(n, seed, q), n_meanders_of_k(n, 0)
{
    this->simulation.delegate = this;
}

void MeanderRunner::setup() {
    this->simulation.setup();
}

void MeanderRunner::iterate() {
    accepted += simulation.flip();
    iter += 1;
}

void MeanderRunner::iterate(uint64_t iterations) {
    for(uint64_t i = 0; i < iterations; i++) {
        accepted += simulation.flip();
        iter += 1;
    }
}

void MeanderRunner::iterate_and_mnk(uint64_t iterations) {
    for(uint64_t i = 0; i < iterations; i++) {
        accepted += simulation.flip();
        iter += 1;
        int k = simulation.k_components();
        n_meanders_of_k[k-1] += 1;
        k_components.push_back(k);
    }
}


void MeanderRunner::afterflip(MeanderMonteCarlo * simulation, const FlipInfo& info, uint64_t iteration) {
    std::cout << "afterflip called\n";
}

void MeanderRunner::report() {
    std::cout << "iterations: " << iter << ", accepted: " << accepted << ", average: " << ((double)accepted)/((double)iter) << "\n";
}

void MeanderRunner::reset() {
    std::fill(n_meanders_of_k.begin(), n_meanders_of_k.end(), 0);
    k_components.clear();
    iter = 0;
    accepted = 0;
}

std::vector<uint64_t> MeanderRunner::measure_dist(int n_samples, bool is_dual) {
    std::vector<uint64_t> hist;

    for (size_t k = 0; k < n_samples; k++)
    {
        std::vector<uint64_t> result;
        if(is_dual) {
            result = simulation.measure_dist();
        } else {
            result = simulation.measure_dist_edges();
        }
        
        if (result.size() > hist.size())
        {
            hist.resize(result.size(), 0);
        }
        for (size_t i = 0; i < result.size(); i++)
        {
            hist[i] += result[i];
        }
    }
    return hist;
}


void MeanderRunner::current_state() {
    this->simulation.current_state();
}

nlohmann::json MeanderRunner::json_report() {
    nlohmann::json json;
    json["n"] = simulation.meander_of_order_n(); 
    json["q"] = simulation.get_q();
    json["seed"] = simulation.get_seed();
    json["version"] = 8;
    //json["n_meanders_of_k"] = n_meanders_of_k;
    //json["k_by_iterations"] = k_components;
    json["iter"] = iter;
    json["accepted"] = accepted;

    return json;
}