# %%

from matplotlib import scale
import matplotlib.pyplot as plt
import numpy as np
import json
import pandas as pd

from default_analysis_funcs import *

# %%

# %%

# %%

plot_conv("dataN20.json")
plot_conv("dataN20v2.json")
plot_conv("dataN20v3.json")
plot_conv("dataN100.json")
plot_conv("dataN100v2.json")
plot_conv("dataN100v3.json")

# %%
#[y1, x] = get_time("dataN10000.json")
#y2, _ = get_time("dataN100000.json")
y1, _ = get_time("dataN100v1.json")
y2, _ = get_time("dataN1000v1.json")
[y3, x] = get_time("dataN10000v1.json")
y4, _ = get_time("dataN100000v1.json")
y5, _ = get_time("dataN1000000v1.json")
print(y2/y1)
print(y3/y2)
print(y4/y3)
print(y5/y4)

plt.plot(x, y1, label="N100v1")
plt.plot(x, y2, label="N1000v1")
plt.plot(x, y3, label="N10000v1")
plt.plot(x, y4, label="N100000v1")
plt.plot(x, y5, label="N1000000v1")
plt.legend()
# %%
y1, _ = get_time("dataN100v1t.json")
y2, _ = get_time("dataN1000v1t.json")
[y3, x] = get_time("dataN10000v1t.json")
y4, _ = get_time("dataN100000v1t.json")
y5, _ = get_time("dataN1000000v1t.json")
print(y2/y1)
print(y3/y2)
print(y4/y3)
print(y5/y4)

plt.plot(x, y1, label="N100v1t")
plt.plot(x, y2, label="N1000v1t")
plt.plot(x, y3, label="N10000v1t")
plt.plot(x, y4, label="N100000v1t")
plt.plot(x, y5, label="N1000000v1t")
# %%
plot_conv("data_N100p1.json")
plot_conv("data_N100p10.json")
plot_conv("data_N100q005.json")
plot_conv("data_N100q1000.json")
# %%
plot_conv("data_N100q1000.json")
plot_conv("data_N100q2.json")
plot_conv("data_N100q1.json")

#%%


# %%
plot_deviation("data_N24q1s1000000.json")
plot_deviation("data_N24q1s100000000v2.json")
plot_deviation("data_N24q1s1000000v2.json")
plot_deviation("data_N24q1s100000v2.json")
plot_deviation("data_N22q1s100000v2.json", N=11)

# %%
