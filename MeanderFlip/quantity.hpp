//
//  quantity.hpp
//  quantity
//
//  Created by Rutger Berns on 12/02/2021.
//

#ifndef quantity_hpp
#define quantity_hpp

template<typename T>
struct Quantity
{
    T value;
    T value_squared;

    Quantity add(const Quantity& other) {
        Quantity new_quantity {
            value + other.value, value_squared + other.value_squared
        };
        return new_quantity;
    }

    Quantity& operator +=(const Quantity& other)
    {
        add(other);
        return *this;
    }
};


#endif