//
//  fileio.hpp
//  MeanderFlip
// 
//  Created by Rutger Berns on 24/04/2022.
//

#ifndef fileio_hpp
#define fileio_hpp

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <errno.h>
#include <ctime>
#include "json.hpp"
#include <filesystem>

class Fileio
{
private:
    double q;
    int n;
    bool initialized = false;
    int total_runs;
    int seed;
    std::stringstream pre();
    std::string name;
    std::ofstream myfile;
    std::string start_date;

public:
    Fileio(double q, int n, int seed, int total_runs, const std::string& name);
    bool initialize_dir();
    
    void write(nlohmann::json json, int run);
    std::string dir_path();
    ~Fileio();
};

#endif