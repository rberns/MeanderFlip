import open3d as o3d
import numpy as np


# print("Testing mesh in Open3D...")
# armadillo_mesh = o3d.data.ArmadilloMesh()
# mesh = o3d.io.read_triangle_mesh(armadillo_mesh.path)

# knot_mesh = o3d.data.KnotMesh()
# mesh = o3d.io.read_triangle_mesh(knot_mesh.path)
# print(mesh)
# print('Vertices:')
# print(np.asarray(mesh.vertices))
# print('Triangles:')
# print(np.asarray(mesh.triangles))



# #print(mesh.triangles)
# print('Vertex colors:')
# print(np.asarray(mesh.vertex_colors))
# #print(mesh.vertex["colors"])

# num_vertices = np.asarray(mesh.vertices).shape[0]

# arr = np.array([1-np.linspace(0, 1, num_vertices), np.linspace(0, 1, num_vertices)**2, np.linspace(0, 1, num_vertices)])

# mesh.vertex_colors = o3d.utility.Vector3dVector(arr.T)

# print("Computing normal and rendering it.")
# mesh.compute_vertex_normals()
# print(np.asarray(mesh.triangle_normals))
# o3d.visualization.draw_geometries([mesh])

mesh = o3d.io.read_triangle_mesh("simdata/meander_structure/q1.0000/n100/sim_data_20220610124541_1_0_0.ply")
print(mesh)
mesh.compute_vertex_normals()

#mesh.paint_uniform_color([1, 0.706, 0])

o3d.visualization.draw_geometries([mesh])

