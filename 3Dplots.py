#!/usr/bin/env python
# coding: utf-8

# In[1]:


from meanderutils import *
from styles import *
import networkx as nx
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection


# In[14]:


def faces_from_data(data, seed=100):
    G = nx.Graph(data["nodes"])
    #pos = nx.nx_agraph.graphviz_layout(G, prog="sfdp")

    #print(pos)
    pos = nx.spring_layout(G, dim=3, iterations=100, weight=1, seed=seed)
    #print(pos)
    faces = np.array(data["faces"])-1

    pos2 = np.array(list(pos.values()))

    return pos2[faces], pos2

def draw_structure(file_name, index=0, seed=101):
    with open(file_name) as file:
        data100 = json.load(file)

        configuration = data100["configurations"][index]
        
        #face_group100 = VGroup()

        max_distance = np.max(configuration["face_distance"])
        colors = cmr.take_cmap_colors('cmr.ocean', max_distance, return_fmt='hex')
        #face_group100.scale(0.8)
        #print("max_distance", max_distance)
        faces, pos = faces_from_data(configuration, seed=seed)
        tris = Poly3DCollection(faces*1.5, linewidth=0.2)
        tris.set_facecolor([colors[dist-1] for dist in configuration["face_distance"]])
        tris.set_alpha(0.8)

        return tris, pos
        # for (i, face) in enumerate():
        #     dist = configuration["face_distance"][i]
        #     #print("dist", dist)
        #     face_group100.add(Polygon(*(face*2), shade_in_3d=True, stroke_width=0.3).set_color(WHITE).set_fill(colors[dist-1], opacity=0.8))
        # return face_group100


# In[17]:


fig = plt.figure()
ax = Axes3D(fig, auto_add_to_figure=False)
fig.add_axes(ax)

ax.set_axis_off()

ax.set_title("(a)")

# simdata/meander_chain/q0.1000/n100/sim_data_20220611220228_1_0_0.json
# simdata/meander_chain/q20.0000/n100/sim_data_20220611220055_1_0_0.json
#tris, pos = draw_structure("simdata/meander_chain/q0.1000/n100/sim_data_20220611220228_1_0_0.json")

tris, pos = draw_structure("simdata/test/q1.0000/n10000/sim_data_20220707163337_1_0_0.json")
ax.add_collection3d(tris)

ax.set_xlim3d(np.amin(pos[:,0]),np.amax(pos[:,0]))
ax.set_ylim3d(np.amin(pos[:,1]),np.amax(pos[:,1]))
ax.set_zlim3d(np.amin(pos[:,2]),np.amax(pos[:,2]))

#ax.set_xticks([])
#ax.set_yticks([])
#ax.set_zticks([])

ax.view_init(0, 0)

#plt.show()
plt.savefig("docs/results/assets/meanderchainplot.pdf", bbox_inches='tight')


# In[ ]:




