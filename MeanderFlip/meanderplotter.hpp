//
//  meanderplotter.hpp
//  MeanderFlip
//
//  Created by Rutger Berns on 18/12/2021.
//

#ifndef meanderplotter_hpp
#define meanderplotter_hpp

#include <stdio.h>
#include <iostream>
#include <cairo/cairo.h>
#include <cairo/cairo-svg.h>
#include "json.hpp"
#include "planarmap+json.hpp"
#include "meandergenerator.hpp"

void plot(const std::vector<Edge> & edges, int N, std::string filename, std::map<int, std::string> * highlight_edges = nullptr);

#endif /* meanderplotter_hpp
 */