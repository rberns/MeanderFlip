#include "meandermap.hpp"

MeanderMap::MeanderMap(int n) {
    this->n = n;
    this->edges = std::vector<Edge>();
}

/*
 * 
 * Default generated configuration
 * 
 * +---+---+---+---+
 * | / | \ | / | \ |
 * +---+---+---+---+
 * | 1 | 2 | 3 | 4 |
 * +---+---+---+---+
 * | \ | \ | / | / |
 * +---+---+---+---+
 * 
 */
void MeanderMap::generate() {
    // Approach generate 2*n faces, so 8n edges
    int edge_id = 1;
    
    int degree = 4;
        
    std::vector<Face> faces;
    
    std::vector<int> face_of_edge;

    // Generating the faces
    for (int i = 0; i < 2*n; i++) {
        int face_id = i + 1;
        faces.push_back(generate_bridge_segment(degree, edge_id, face_id));
        face_of_edge.push_back(face_id);
        face_of_edge.push_back(face_id);
        face_of_edge.push_back(face_id);
        face_of_edge.push_back(face_id);
        edge_id += 4;
    }

    // Connecting the faces, the river
    for (int i = 0; i < 2*n; i++) {
        faces[i][2].adjacent = faces[(i+1)%(2*n)][0].id;
        faces[(i+1)%(2*n)][0].adjacent = faces[i][2].id;
    }

    // Glueing
    for (int i = 0; i < n; i++) {
        int j = 2*n - i - 1;
        int k = 2*i;
        int q = 2*i+1;
        // down side all arcs will be placed \\\\\/////
        faces[i][1].adjacent = faces[j][1].id;
        faces[j][1].adjacent = faces[i][1].id;
        // top side all small arcs /\/\/\/\/\/\/
        faces[k][3].adjacent = faces[q][3].id;
        faces[q][3].adjacent = faces[k][3].id;
    }

    // the two edges on the beginning and end are labelled is end, because they are always connected to the root nodes of the meander system.
    faces[0][0].is_end = true;
    faces[2*n-1][2].is_end = true;

    for (int i = 0; i < 2*n; i++) {
        this->edges.insert(edges.end(), faces[i].begin(), faces[i].end());
    }
}

void MeanderMap::print_edges() {
    for(auto edge: edges) {
        std::cout << edge << "\n";
    }
}

void MeanderMonteCarlo::current_state() {
    auto paths = this->get_paths();
    debug_pretty_print_paths(paths, this->num_faces());
}

MeanderMonteCarlo::MeanderMonteCarlo(int n, int seed, double q): map(n), random(seed), permutation(2*n) {
    this->seed = seed;
    this->q = q;
}

int MeanderMonteCarlo::meander_of_order_n() {
    return map.n;
}

double MeanderMonteCarlo::get_q() {
    return q;
}

bool MeanderMonteCarlo::flip(bool dump, uint64_t iteration) {
    // select a random edge
    Edge shore_edge = random_shore_edge();

    // number of vertex edges
    int n = number_of_edges_of_vertex(shore_edge, map.edges);

    // finding a random cw edge
    Edge current_edge = cw(shore_edge, map.edges);

    std::vector<Edge> other_edges_around_vertex;

    assert(current_edge.id != shore_edge.id);

    while (current_edge.id != shore_edge.id)
    {
        if(current_edge.is_shore) {
            other_edges_around_vertex.push_back(current_edge);
        }
        current_edge = cw(current_edge, map.edges);
    }

    #if(DEBUGPERM)
    std::cout << "other_edges_around_vertex: \n";
    #endif
    for(size_t i = 0; i < other_edges_around_vertex.size(); i++) {
        #if(DEBUGPERM)
            std::cout << other_edges_around_vertex[i] << "\n";
        #endif
    }

    // reject move when there are no other edges around the vertex
    // two approaches of solving, rejecting and changing the acceptance probability to account for this
    // other approach is to 
    if(other_edges_around_vertex.size() == 0) {
        return false;
    }

    Edge suggestion_edge = random_edge_from(other_edges_around_vertex);

    Edge adjacent_shore_edge = adjacent(shore_edge, map.edges);
    Edge adjacent_suggestion_edge = adjacent(suggestion_edge, map.edges);

    EdgeCount neighbors_initial_edge = number_of_shore_edges_of_vertex_between(adjacent_shore_edge, adjacent_shore_edge, map.edges);
    EdgeCount neighbors_suggestion_edge = number_of_shore_edges_of_vertex_between(adjacent_suggestion_edge, adjacent_suggestion_edge, map.edges); // I changed this to adjacent_suggestion_edge from suggestion_edge

    EdgeCount n_initial_to_suggestion = number_of_shore_edges_of_vertex_between(shore_edge, suggestion_edge, map.edges);
    EdgeCount n_suggestion_to_initial = number_of_shore_edges_of_vertex_between(suggestion_edge, shore_edge, map.edges);

    int n_shore = n_initial_to_suggestion.shore_edges + n_suggestion_to_initial.shore_edges + 2;

    double v_initial = (double)n_shore;
    double v_final = (double)((neighbors_initial_edge.shore_edges + 1) + (neighbors_suggestion_edge.shore_edges + 1)); // +1 are because function returns amount of shore edges between itself so +1;

    double k_initial = (double)permutation.k_components();
    permutation.flip(shore_edge.face_id, suggestion_edge.face_id, adjacent_shore_edge.face_id, adjacent_suggestion_edge.face_id);
    double k_final = (double)permutation.k_components();

    //double p = pow(q, k_final - k_initial) * (N_initial/N_final) * ((v_initial-1)/(v_final-1));

    double p = pow(q, k_final - k_initial) * ((v_initial-1)/(v_final-1));

    double A = std::min(1.0, p);
    //std::cout << "p: " << A << "\n";
    #if INFOPRINT
    std::cout << "p: " << p << "\n";
    #endif

    
    
    #if DEBUG 
    //std::cout << suggestion_edge << " with probability P = 1/N * 1/v, with v = " << n_shore-1 << " (number of shore neighbors)\n";
        
    std::cout << "acceptance probability: " << A << "\n";
    //std::cout << "new vertex will have neighbors: " <<  neighbors_initial_edge + neighbors_suggestion_edge + 2 << "\n";
    //std::cout << "probability = 1/N * 1/v, with v = " <<  neighbors_initial_edge + neighbors_suggestion_edge + 2 - 1 << " (number of shore neighbors after flip)\n";

        // 1/(max(v_old, v_new)) / (1/(v_old)) = v_old / (max(v_old, v_new))
    //std::cout << "acceptance probability: " << ((float)old_v)/((float)max_v) << "\n";
    
    #endif

    if(accept(A)) {
        #if(INFOPRINT)
        std::cout << "AFTER\n";
        permutation.print();
        std::cout << "v_initial: " << v_initial << "\n";
        std::cout << "v_final: " << v_final << "\n";
        //std::cout << "N_initial: " << N_initial << "\n";
        //std::cout << "N_final: " << N_final << "\n";
        std::cout << "k_initial: " << k_initial << "\n";
        std::cout << "k_final: " << k_final << "\n";
        //std::cout << "local_N_initial: " << local_N_initial << "\n";
        //std::cout << "local_N_final: " << local_N_final << "\n";
        std::cout << "n_initial_to_suggestion: " << n_initial_to_suggestion.shore_edges << "\n";
        std::cout << "n_suggestion_to_initial: " << n_suggestion_to_initial.shore_edges << "\n";
        std::cout << "n_neighbors_initial_edge: " << neighbors_initial_edge.shore_edges << "\n";
        std::cout << "n_neighbors_suggestion_edge: " << neighbors_suggestion_edge.shore_edges << "\n";
        //std::cout << "suggested_N_change: " << suggested_N_change << "\n";
        std::cout << "Edge flip - " << "first: " << shore_edge.id << ", cw: " << suggestion_edge.id << ", afirst: " << adjacent_shore_edge.id << ", acw: " << adjacent_suggestion_edge.id << "\n";
        std::cout << "Face flip - " << "first: " << shore_edge.face_id << ", cw: " << suggestion_edge.face_id << ", afirst: " << adjacent_shore_edge.face_id << ", acw: " << adjacent_suggestion_edge.face_id << "\n";
        #endif
        
        
        

        #if(DEBUG) 
        std::cout << "accepted" << shore_edge << " and " << suggestion_edge << "\n";
        #endif

        #if(DEBUGPERM)
        std::cout << "AFTER\n";
        permutation.print();
        #endif

        flip_permutation(map.edges, shore_edge, suggestion_edge);

        if (dump)
        {
            FlipInfo info;
            info.first = shore_edge;
            info.cw = suggestion_edge;
            info.afirst = adjacent_shore_edge;
            info.acw = adjacent_suggestion_edge;
            info.v_initial = v_initial;
            info.v_final = v_final;
            info.k_initial = k_initial;
            info.k_final = k_final;
            info.n_initial_to_suggestion = n_initial_to_suggestion.shore_edges;
            info.n_suggestion_to_initial = n_suggestion_to_initial.shore_edges;
            info.n_neighbors_initial_edge = neighbors_initial_edge.shore_edges;
            info.n_neighbors_suggestion_edge = neighbors_suggestion_edge.shore_edges;
            info.acceptance_probability = p;
            info.accepted = true;

            this->delegate->afterflip(this, info, iteration);
        }

        #if INEFFICIENTINFOPRINT
        std::vector<std::vector<int> > paths = get_all_paths(map.edges);
        debug_pretty_print_paths(paths, 2*n);
        #endif


        
        return true;
    } else {
        permutation.undo();
        if(DEBUG) {
            std::cout << "rejected\n";
        }
        return false;
    }
}


Edge MeanderMap::get_edge(int id) {
    return edges[id-1];
}

int MeanderMap::num_edges() {
    assert(8*n == edges.size());
    return 8*n;
}

Edge MeanderMap::adjacent(Edge edge) {
    return get_edge(edge.adjacent);
}

Edge MeanderMap::next(Edge edge) {
    return get_edge(edge.next);
}

Edge MeanderMap::cw(Edge edge) {
    return next(adjacent(edge));
}

Edge MeanderMap::acw(Edge edge) {
    return adjacent(next(edge));
}

// function for finding other edges around a vertex
std::vector<int> MeanderMap::edges_around_vertex_excluding_initial_cw(Edge initial) {
	Edge edge = this->cw(initial);
	std::vector<int> edges_around_vertex;
	while (edge.id != initial.id)
	{
        //std::cout << "test " << edge <<"\n";
		edges_around_vertex.push_back(edge.id);
		edge = this->cw(edge);
	}
	return edges_around_vertex;
}

const std::vector<Edge> & MeanderMap::get_edges() {
    #warning(Poor function)
    return edges;
}

std::vector<int> MeanderMap::edges_around_vertex_excluding_initial_acw(Edge initial) {
	Edge edge = this->acw(initial);
	std::vector<int> edges_around_vertex;
	while (edge.id != initial.id)
	{
        //std::cout << "test2\n";
		edges_around_vertex.push_back(edge.id);
		edge = this->acw(edge);
	}
	return edges_around_vertex;
}

// Edges in area
int MeanderMap::find_faces_in_area(std::vector<int> boundary_) {
	int boundary_size = boundary_.size();

	std::set<int> boundary {std::begin(boundary_), std::end(boundary_)};

	std::vector<int> edgeList;
	std::vector<int> tmpEdgeList;
	std::vector<bool> visited(edges.size(), false);

	edgeList.push_back(boundary_.front());

	int faces = 0;

	for (unsigned dist = 0; !edgeList.empty(); ++dist) {
		tmpEdgeList.clear();

		for (auto edge_id: edgeList) {
			if (!visited[edge_id-1])
			{
				faces++;
			}
			
			while (!visited[edge_id-1]) {
                auto edge = get_edge(edge_id);
				if (!boundary.count(edge_id))
				{
					tmpEdgeList.push_back(adjacent(edge).id);
				}
				visited[edge_id-1] = true;
				edge_id = next(edge).id;
			}
			
		}
		std::swap(edgeList, tmpEdgeList);
	}

	return faces;
}


std::tuple<std::vector<std::vector<int> >, std::vector<std::vector<int> >, std::vector<int>, std::vector<int>, std::vector<std::vector<int> >, std::vector<std::vector<int> >, std::vector<bool> > MeanderMap::node_graph() {
    std::vector<bool> visited(edges.size(), false);
    std::vector<std::vector<Edge>> vertex_edges;

    int current_edge = 1;
    
    int num = 1;
    
    std::vector<int> node_of_edge(edges.size(), 0);
    
    std::vector<std::vector<int> > edges_per_node;
    std::vector<int> distance_of_node;
    std::vector<std::vector<int> > vertex_per_face(this->n*2, std::vector<int>());
    std::vector<int > distance_of_face(this->n*2, 0);
    
    std::vector<int> edgeList_;
	std::vector<int> tmpEdgeList_;

    for (int i = 0, endi = edges.size(); i < endi; ++i) {
        current_edge = i+1;
        if (!visited[current_edge-1]) {
            ++num;
            edges_per_node.push_back(std::vector<int>());
            while (!visited[current_edge-1]) {
                visited[current_edge-1] = true;
                current_edge = this->cw(this->get_edge(current_edge)).id;
                edges_per_node[num-2].push_back(current_edge);
                node_of_edge[current_edge-1] = num-1;
                //vertex_per_face[this->get_edge(current_edge).face_id-1].push_back(num-1);
            }
        }
    }

    // edgeList_.push_back(1);

    // int vertex_distance = 1;

    // for (unsigned dist = 0; !edgeList_.empty(); ++dist) {
	// 	tmpEdgeList_.clear();
	// 	uint64_t vertsAtDist = 0;
    //     for (auto edge_id : edgeList_) {

    //         if (!visited[edge_id-1])
    //         {
    //             ++num;
    //             edges_per_node.push_back(std::vector<int>());
    //             distance_of_node.push_back(vertex_distance);
    //         }
            
            
    //         while (!visited[edge_id-1]) {
    //             auto edge = get_edge(edge_id);
	// 			tmpEdgeList_.push_back(next(edge).id);
    //             visited[edge_id-1] = true;
    //             edge_id = this->cw(this->get_edge(edge_id)).id;
    //             edges_per_node[num-2].push_back(edge_id);
    //             node_of_edge[edge_id-1] = num-1;
    //         }
    //     }
    //     vertex_distance += 1;
    //     //hist.data.push_back(vertsAtDist);
    //     std::swap(edgeList_, tmpEdgeList_);
    // }

    visited = std::vector<bool>(edges.size(), false);

    std::vector<int> edgeList;
	std::vector<int> tmpEdgeList;
	//std::vector<bool> visited(edges.size(), false);

	//edgeList.push_back(boundary_.front());

	//int faces = 0;
    edgeList.push_back(1);

    int face_dist = 1;

	for (unsigned dist = 0; !edgeList.empty(); ++dist) {
		tmpEdgeList.clear();

		for (auto edge_id: edgeList) {
			if (!visited[edge_id-1])
			{
                auto edge = get_edge(edge_id);
				distance_of_face[edge.face_id-1] = face_dist;
			}

			
			while (!visited[edge_id-1]) {
                auto edge = get_edge(edge_id);
				tmpEdgeList.push_back(adjacent(edge).id);
				visited[edge_id-1] = true;
                vertex_per_face[edge.face_id-1].push_back(node_of_edge[edge.id-1]);
				edge_id = next(edge).id;
                
			}
			
		}

        face_dist += 1;

		std::swap(edgeList, tmpEdgeList);
	}
    
    
    
    for(int i = 0; i < edges_per_node.size(); i++) {
        std::cout << "node: " << i + 1 << ", edges: ";
        for (auto& edge : edges_per_node[i]) {
            std::cout << edge << ", ";
        }
        std::cout << "\n";
    }
    
    std::vector<std::vector<int> > node_connections;
    
    for(int i = 0; i < node_of_edge.size(); i++) {
        std::cout << "node of edge: " << i + 1 << ", node:" << node_of_edge[i] << "\n";
    }

    std::vector<std::vector<int> > bridge_pillars;
    std::vector<std::vector<int> > river_connections;
    std::vector<bool> connection_is_shore;
    
    for(int i = 0; i < node_of_edge.size(); i++) {
        //std::cout << "edge: " << i+1 << ", at node: " << node_of_edge[i] << ", with adjacent: " << edges[i].adjacent << "\n";
        
        std::vector<int> connection = {node_of_edge[i], node_of_edge[edges[i].adjacent-1]};
        
        connection_is_shore.push_back(edges[i].is_shore);

        node_connections.push_back(connection);

        connection.push_back(edges[i].face_id);
        if(edges[i].is_shore) {
            bridge_pillars.push_back(connection);
        } else {
            river_connections.push_back(connection);
        }
        //connection_is_shore.
        
        std::cout << "Connection from " << connection[0] << " to " << connection[1] << "\n";
    }
    
    std::cout << "n vertices: " << num << "\n";
    std::cout << "n half edges: " << edges.size() << "\n";
    std::cout << "n faces: " << edges.size()/4 << "\n";
    
    //nlohmann::json j;
    
    //j["connections"] = node_connections;
    
    //std::cout << j;

    // make a for vertex_per_face

    return std::make_tuple(node_connections, vertex_per_face, distance_of_node, distance_of_face, bridge_pillars, river_connections, connection_is_shore);
}

std::vector< std::tuple<int, int, bool> > MeanderMap::convert_to_arcs() {
    //nlohmann::json j;
    std::vector< std::tuple<int, int, bool> > arcs;
    Edge current_edge;
    for (int i = 0, endi = edges.size(); i < endi; ++i) {
        current_edge = edges[i];
        Edge adjacent_edge = adjacent(current_edge);
        int length = abs(current_edge.face_id - adjacent_edge.face_id);
        if (current_edge.id % 4 == 2)
        {
            // lower half
            arcs.push_back(std::make_tuple(current_edge.face_id, adjacent_edge.face_id, false));
        } else if (current_edge.id % 4 == 0) {
            // upper half
            arcs.push_back(std::make_tuple(current_edge.face_id, adjacent_edge.face_id, true));
        }
    }
    return arcs;
}

/*
 *
 *
 * MeanderMonteCarlo
 * 
 * 
 */
// random functions below
Edge MeanderMonteCarlo::random_edge_from(const std::vector<Edge> & edges) {
    size_t size = edges.size();
    int index = random.rand_int(0, size-1);
    assert(index < size);
    return edges[index];
}

// function to select any random edge
Edge MeanderMonteCarlo::random_edge() {
    return random_edge_from(map.edges);
}

// the next edge of non-shore edge should ALWAYS be a shore edge.
Edge MeanderMonteCarlo::random_shore_edge() {
    struct Edge edge = random_edge();
    if(edge.is_shore == false) {
        edge = next(edge, map.edges);
    }
    assert(edge.is_shore == true);
    return edge;
}

bool MeanderMonteCarlo::accept(double probability) {
    double rand = random.uniform();
    return (rand <= probability);
}

void MeanderMonteCarlo::setup() {
    map.generate();
    permutation.from(map.edges);
}

unsigned int MeanderMonteCarlo::get_seed() {
    return this->seed;
}

int MeanderMonteCarlo::k_components() {
    return permutation.k_components();
}

// from Barkley, J., & Budd, T. (2019). Precision measurements of Hausdorff dimensions in two-dimensional quantum gravity. Classical and Quantum Gravity, 36(24). https://doi.org/10.1088/1361-6382/ab4f21
// Face/triangle distance, DUAL DISTANCE
std::vector<uint64_t> MeanderMonteCarlo::measure_dist() {
	std::vector<uint64_t> data;
	std::vector<int> edgeList;
	std::vector<int> tmpEdgeList;
    std::vector<bool> visited(map.num_edges(), false);

    edgeList.push_back(random_edge().id);
    for (unsigned dist = 0; !edgeList.empty(); ++dist) {
        tmpEdgeList.clear();
        uint64_t trisAtDist = 0;
        for (auto edgeId : edgeList) {
            Edge edge = map.get_edge(edgeId);
            trisAtDist += (uint64_t)!visited[edge.id-1];
            while (!visited[edge.id-1]) {
                tmpEdgeList.push_back(map.adjacent(edge).id);
                visited[edge.id-1] = true;
                edge = map.next(edge);
            }
        }
        data.push_back(trisAtDist);

        std::swap(edgeList, tmpEdgeList);
        //edgeList = std::move(tmpEdgeList);
    }
		
	return data;
}

std::vector<uint64_t> MeanderMonteCarlo::measure_dist_edges() {
	std::vector<uint64_t> data;
	std::vector<int> edgeList;
	std::vector<int> tmpEdgeList;
    std::vector<bool> visited(map.num_edges(), false);

    edgeList.push_back(random_edge().id);
    for (unsigned dist = 0; !edgeList.empty(); ++dist) {
        tmpEdgeList.clear();
        uint64_t edgesAtDist = 0;
        for (auto edgeId : edgeList) {
            Edge edge = map.get_edge(edgeId);
            edgesAtDist += (uint64_t)!visited[edge.id-1];
            while (!visited[edge.id-1]) {
                for (auto nearby = map.cw(edge); nearby != edge; nearby = map.cw(nearby)) {
                    tmpEdgeList.push_back(nearby.id);
                }
                visited[edge.id-1] = true;
                edge = map.adjacent(edge);
            }
        }
        data.push_back(edgesAtDist);
        std::swap(edgeList, tmpEdgeList);
        //edgeList = std::move(tmpEdgeList);
    }
		
	return data;
}



std::vector<std::vector<int>> MeanderMonteCarlo::get_paths() {
    return get_all_paths(map.edges);
}

std::vector<std::vector<int>> MeanderMonteCarlo::get_paths_from_permutation() {
    return permutation.get_norm_paths();
}



std::vector<uint64_t> MeanderMonteCarlo::stringsusceptibility_full() {
    std::vector<bool> visited(this->map.num_edges(), false);

    std::vector<uint64_t> sizes;

	for (int i = 0; i < this->map.num_edges(); i++)
    {
        while (!visited[i]) {
            Edge edge = this->map.get_edge(i+1);
            Edge adjacent = this->map.adjacent(edge);

            std::vector<int> out = map.edges_around_vertex_excluding_initial_cw(adjacent);
            std::vector<int> in = map.edges_around_vertex_excluding_initial_acw(adjacent);

            

            std::sort(out.begin(), out.end());
            std::sort(in.begin(), in.end());

            std::vector<int> common;
        
            std::set_intersection(out.begin(), out.end(),
                                in.begin(), in.end(),
                                std::back_inserter(common));

            // if (common.size() == 0) {
            //     visited[i] = true;
            //     continue;
            // }

            // common.push_back(i+1);

            // std::cout << "common: ";
            // for (auto c: common) {
            //     std::cout << c << ", ";
            // }
            // std::cout << "\n";

            // std::vector<std::tuple<int, int> > babyuniversebounds;
            // int i = 0;
            // while(true) {
            //     if (common.size() > 1)
            //     {
            //         int start_edge = common.back();
            //         visited[start_edge-1] = true; 
            //         common.erase(common.end());

            //         for (auto c: common) {
            //             //babyuniversebounds.push_back(std::make_pair(start_edge, c));
            //             std::cout << "baby universe: " << start_edge << ", " << c << "\n";
            //         }
            //     } else {
            //         break;
            //     }
            // }
            

            for (auto c: common)
            {
                //visited[c-1] = true;
                if (!visited[c-1])
                {
                    Edge other = this->map.get_edge(c);

                    uint64_t baby_size = (uint64_t)this->num_faces_in(edge, other);
                    if (baby_size != -1)
                    {
                        sizes.push_back(baby_size);
                    }
                }
                //std::cout << "found baby of size: " << baby_size <<", from: " << edge.face_id << ", to: " << other.face_id << ", edge.id: " << edge.id << ", other.id: " << other.id << "\n";
            }
            
            visited[i] = true;
                        

        }
    }
    return sizes;
}

int MeanderMonteCarlo::num_faces_in(Edge initial, Edge common) {
    Edge common_edge = common;
    Edge adjacent = this->map.adjacent(initial);

    //int size = map.find_faces_in_area({common[i], initial.id});
    
    int num_faces = this->num_faces();

    // we need to check to what direction the closed loop is in the meander system for that check the adjacent edge, and check whether its face is greater or smaller, this way we have a measure for left or right over the quadrangulations, using this we can determine whether we need to compensate for going over the 128 faces (because we count faces from 1 to 128)
    int fast_size = 0;

    // if(initial.face_id == 1 && adjacent.face_id == num_faces) {
        
    // }

    // if(initial.face_id == num_faces && adjacent.face_id == 1) {
        
    // }

    if ((adjacent.face_id < initial.face_id || (initial.face_id == 1 && adjacent.face_id == num_faces)) && !(initial.face_id == num_faces && adjacent.face_id == 1))
    {
        // we can say adjacent is left of initial
        // so we must always count up to N and sometimes over N
        
        // in this case we expect common_edge to be left of common_edge_adjacent
        //std::cout << "expect true (1) =" << (common_edge.face_id < this->map.adjacent(common_edge).face_id) << "\n";

        // exception for 

        if (initial.face_id < common_edge.face_id)
        {
            // e.g. 8 faces case => 3..5 -> so size = 3 faces, where 3 = initial, and 5 = common
            fast_size = common_edge.face_id-initial.face_id+1;
            // std::cout << "CASE1\n";
        } else {
            // e.g. 8 faces case => ..3 5.. -> so size = 7, where 5 = initial, and 3 = common
            fast_size = num_faces-(initial.face_id-common_edge.face_id-1);
            //return -1;
                //std::cout << "CASE2\n";
        }
    } else {
        // counting from right to left
        //return -1;
        if (initial.face_id > common_edge.face_id) {
            // e.g. 8 faces case => 3..5 -> so size = 3 faces, where 3 = common, and 5 = initial
            fast_size = initial.face_id-common_edge.face_id+1;
                //std::cout << "CASE3\n";
        } else {
            // e.g. 8 faces case => ..3 5.. -> so size = 3 faces, where 5 = common, and 3 = initial
            fast_size = num_faces-(common_edge.face_id-initial.face_id-1);
            //return -1;
            // std::cout << "CASE4\n";
        }
        
    }
    
    //std::cout << "len: " << fast_size << " and according to " << size << "\n";
    
    if (fast_size == num_faces) {
        std::cout << "MMMMH\n";
        std::cout << adjacent.face_id << " " << initial.face_id << " " << common_edge.face_id;
        exit(-1);
    }

    // if (fast_size != size)
    // {
    //     std::cout << adjacent.face_id << " " << initial.face_id << " " << common_edge.face_id;
    //     exit(-1);
    // }
    

    return fast_size;
}

// Measuring string susceptibility by counting faces
int MeanderMonteCarlo::quad_stringsusceptibility_rand(Edge initial) {
    Edge adjacent = this->map.adjacent(initial);

    
	
	std::vector<int> out = map.edges_around_vertex_excluding_initial_cw(adjacent);
	std::vector<int> in = map.edges_around_vertex_excluding_initial_acw(adjacent);

    

    std::sort(out.begin(), out.end());
    std::sort(in.begin(), in.end());

	std::vector<int> common;
 
    std::set_intersection(out.begin(), out.end(),
                          in.begin(), in.end(),
                          std::back_inserter(common));
	
	int n_common = common.size();

    //std::cout << "finding commons: " << n_common << "\n";
	
	if (n_common != 0) {
		int i = (rand() % n_common);

        Edge common_edge = this->map.get_edge(common[i]);

	    //int size = map.find_faces_in_area({common[i], initial.id});
        
        int num_faces = this->num_faces();

        // we need to check to what direction the closed loop is in the meander system for that check the adjacent edge, and check whether its face is greater or smaller, this way we have a measure for left or right over the quadrangulations, using this we can determine whether we need to compensate for going over the 128 faces (because we count faces from 1 to 128)
        int fast_size = 0;

        // if(initial.face_id == 1 && adjacent.face_id == num_faces) {
            
        // }

        // if(initial.face_id == num_faces && adjacent.face_id == 1) {
            
        // }

        if ((adjacent.face_id < initial.face_id || (initial.face_id == 1 && adjacent.face_id == num_faces)) && !(initial.face_id == num_faces && adjacent.face_id == 1))
        {
            // we can say adjacent is left of initial
            // so we must always count up to N and sometimes over N
            
            // in this case we expect common_edge to be left of common_edge_adjacent
            //std::cout << "expect true (1) =" << (common_edge.face_id < this->map.adjacent(common_edge).face_id) << "\n";

            // exception for 

            if (initial.face_id < common_edge.face_id)
            {
                // e.g. 8 faces case => 3..5 -> so size = 3 faces, where 3 = initial, and 5 = common
                fast_size = common_edge.face_id-initial.face_id+1;
               // std::cout << "CASE1\n";
            } else {
                // e.g. 8 faces case => ..3 5.. -> so size = 7, where 5 = initial, and 3 = common
                fast_size = num_faces-(initial.face_id-common_edge.face_id-1);
                 //std::cout << "CASE2\n";
            }
        } else {
            // counting from right to left

            if (initial.face_id > common_edge.face_id) {
                // e.g. 8 faces case => 3..5 -> so size = 3 faces, where 3 = common, and 5 = initial
                fast_size = initial.face_id-common_edge.face_id+1;
                 //std::cout << "CASE3\n";
            } else {
                // e.g. 8 faces case => ..3 5.. -> so size = 3 faces, where 5 = common, and 3 = initial
                fast_size = num_faces-(common_edge.face_id-initial.face_id-1);
                // std::cout << "CASE4\n";
            }
            
        }
        
        //std::cout << "len: " << fast_size << " and according to " << size << "\n";
        
        if (fast_size == num_faces) {
            std::cout << "MMMMH\n";
            std::cout << adjacent.face_id << " " << initial.face_id << " " << common_edge.face_id;
            exit(-1);
        }

        // if (fast_size != size)
        // {
        //     std::cout << adjacent.face_id << " " << initial.face_id << " " << common_edge.face_id;
        //     exit(-1);
        // }
        

		return fast_size;
        //return size;
	}
	return -1;
}


// // Measuring string susceptibility by counting faces
// int MeanderMonteCarlo::quad_stringsusceptibility_rand(Edge initial) {
//     Edge adjacent = this->map.adjacent(initial);
	
// 	std::vector<int> out = map.edges_around_vertex_excluding_initial_cw(adjacent);
// 	std::vector<int> in = map.edges_around_vertex_excluding_initial_acw(adjacent);

//     //std::cout << "out: " << out.size() << " in: " << in.size() << "\n";

//     // std::cout << "out: ";
//     // for (auto i: out)
//     // {
//     //     std::cout << i << " ";
//     // }
//     // std::cout << "\n";

//     //  std::cout << "in: ";
//     // for (auto i: in)
//     // {
//     //     std::cout << i << " ";
//     // }
//     // std::cout << "\n";

//     std::sort(out.begin(), out.end());
//     std::sort(in.begin(), in.end());

// 	std::vector<int> common;
 
//     std::set_intersection(out.begin(), out.end(),
//                           in.begin(), in.end(),
//                           std::back_inserter(common));
	
// 	int n_common = common.size();

//     //std::cout << "finding commons: " << n_common << "\n";
	
// 	if (n_common != 0) {
// 		int i = (rand() % n_common);

//         Edge common_edge = this->map.get_edge(common[i]);

// 	    //int size = map.find_faces_in_area({common[i], initial.id});
        
//         int num_faces = this->num_faces();

//         // we need to check to what direction the closed loop is in the meander system for that check the adjacent edge, and check whether its face is greater or smaller, this way we have a measure for left or right over the quadrangulations, using this we can determine whether we need to compensate for going over the 128 faces (because we count faces from 1 to 128)
//         int fast_size = 0;

//         // if(initial.face_id == 1 && adjacent.face_id == num_faces) {
            
//         // }

//         // if(initial.face_id == num_faces && adjacent.face_id == 1) {
            
//         // }

//         if ((adjacent.face_id < initial.face_id || (initial.face_id == 1 && adjacent.face_id == num_faces)) && !(initial.face_id == num_faces && adjacent.face_id == 1))
//         {
//             // we can say adjacent is left of initial
//             // so we must always count up to N and sometimes over N
            
//             // in this case we expect common_edge to be left of common_edge_adjacent
//             //std::cout << "expect true (1) =" << (common_edge.face_id < this->map.adjacent(common_edge).face_id) << "\n";

//             // exception for 

//             if (initial.face_id < common_edge.face_id)
//             {
//                 // e.g. 8 faces case => 3..5 -> so size = 3 faces, where 3 = initial, and 5 = common
//                 fast_size = common_edge.face_id-initial.face_id+1;
//                // std::cout << "CASE1\n";
//             } else {
//                 // e.g. 8 faces case => ..3 5.. -> so size = 7, where 5 = initial, and 3 = common
//                 fast_size = num_faces-(initial.face_id-common_edge.face_id-1);
//                  //std::cout << "CASE2\n";
//             }
//         } else {
//             // counting from right to left

//             if (initial.face_id > common_edge.face_id) {
//                 // e.g. 8 faces case => 3..5 -> so size = 3 faces, where 3 = common, and 5 = initial
//                 fast_size = initial.face_id-common_edge.face_id+1;
//                  //std::cout << "CASE3\n";
//             } else {
//                 // e.g. 8 faces case => ..3 5.. -> so size = 3 faces, where 5 = common, and 3 = initial
//                 fast_size = num_faces-(common_edge.face_id-initial.face_id-1);
//                 // std::cout << "CASE4\n";
//             }
            
//         }
        
//         //std::cout << "len: " << fast_size << " and according to " << size << "\n";
        
//         if (fast_size == num_faces) {
//             std::cout << "MMMMH\n";
//             std::cout << adjacent.face_id << " " << initial.face_id << " " << common_edge.face_id;
//             exit(-1);
//         }

//         // if (fast_size != size)
//         // {
//         //     std::cout << adjacent.face_id << " " << initial.face_id << " " << common_edge.face_id;
//         //     exit(-1);
//         // }
        

// 		return fast_size;
//         //return size;
// 	}
// 	return -1;
// }

// Measuring string susceptibility by counting faces
int MeanderMonteCarlo::quad_stringsusceptibility_rand() {
	
    Edge initial = this->random_edge();
    //std::cout << "Initial: " << initial << "\n";
    return quad_stringsusceptibility_rand(initial);
}


// sizes
int MeanderMonteCarlo::num_faces() {
    return 2*meander_of_order_n();
}

int MeanderMonteCarlo::num_half_edges() {
    return 4*num_faces();
}

