//
//  main.cpp
//  MeanderFlip
//
//  Created by Rutger Berns on 09/10/2021.
//

#include <iostream>
#include <fstream>
#include <set>
#include "meandergenerator.hpp"
#include "meanderflip.hpp"
#include "json.hpp"

void debug_print_all_paths(const std::vector<std::vector<int> > & paths) {
    for (auto& path : paths) {
            std::cout << "path of size: " << path.size() << " ";
            for (int i: path)
                std::cout << i << "->";
            std::cout << "\n";
        }
}

int main(int argc, const char * argv[]) {
    std::cout << "Hello, World!\n";

    int N = 10; // 2n
    
    struct MeanderSystem generated_meander_system = generate_glued_map(N);

    std::vector<Edge> edges = generated_meander_system.edges;

    std::vector<std::vector<int> > paths = get_all_paths(edges);

    debug_print_all_paths(paths);
    debug_pretty_print_paths(paths, N);


    std::srand(1008);

    std::cout << "Find random shore edge:" << "\n";
    
    int max_edges = N/2;

    int iter = 0;
    int accepted = 0;

    std::vector<int> n_paths;

    std::vector<std::vector<std::vector<int> > > pathss;

    int it = std::min(N*100, 10000);

    for(int i = 0; i < it; i++) {
        
        while(flipv2(edges, max_edges) == false){
            iter++;
            std::cout << "iteration: " << iter << ", accepted: " << accepted << ", rate: " << (float)accepted/(float)iter << "\n";
        }
        accepted++;

        

        //std::cout << "All good\n";
        std::vector<std::vector<int> > paths = get_all_paths(edges);

        pathss.push_back(paths);

        n_paths.push_back(paths.size());

        //debug_print_all_paths(paths);
        debug_pretty_print_paths(paths, N);
    
    }


    // remove all duplicates from pathss
    std::sort( pathss.begin(), pathss.end() );
    pathss.erase( std::unique( pathss.begin(), pathss.end() ), pathss.end() );
    // done

    std::cout << "found " << pathss.size() << " unique meanders\n";
    std::vector<int> n_meanders_of_k(N/2, 0);
    for(auto paths: pathss) {
        n_meanders_of_k[paths.size()-1] += 1;
        debug_pretty_print_paths(paths, N);
    }
    for(int i = 0; i < n_meanders_of_k.size(); i ++) {
        std::cout << "M_n^(k) = M_" << N/2  << "^(" << i+1 << ") with frequency # = " << n_meanders_of_k[i] << "\n";
    }


    std::ofstream myfile;

    nlohmann::json j;

    j["n_paths"] = n_paths;

    j["pathss"] = pathss;


    std::cout << j;

    myfile.open ("../data.json");
    myfile << j;
    myfile.close();

    return 0;
}
