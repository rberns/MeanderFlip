% !TeX root = ../thesis.tex
\clearpage

\section{Hausdorff dimension}
\label{section:hausresults}

The Hausdorff dimension describes the dimension of a geometry, and it was first
introduced in section \ref{section:hausdorffdimension}. In Figure
\ref{theory:fig:plotofhowqbehavesversuscritical} the conjectured dependence of
$d_H$ on the weight $q$ was plotted\footnote{The Hausdorff dimension is
calculated using the conjectured Ding and Gwynne formula which depends on  the
Liouville coupling constant $\gamma$.}. In the following, numerical evidence
will be presented to support this conjecture. The Hausdorff complements the
stringsusceptibility as an observable because it is computationally less
expensive to measure for a lower $q \in [0,2]$ where the string susceptibility
$\gamma_\mathrm{s}$ is easier to measure for a higher $q \in [0,2]$. The string
susceptibility is therefore measured for $q \in [1,2]$. The Hausdorff dimension
will be used to study $q \in [0,1]$.

\subsection{Setup}

The Hausdorff dimension has been measured for two values, $q = 0.2$ and $q = 1$.
The Hausdorff dimension at $q = 1$ allows to check the overall validity of the
simulations and compare to other Hausdorff dimension measurements like in
\cite{Barkley_2019} by \citeauthor{Barkley_2019}. The simulations take about
$1/q$ longer when below $q < 1$, therefore $q = 0.2$ was chosen as a compromise
between the computational power required and approaching the meanders with just
one road for which $q \rightarrow 0$.


Measuring the Hausdorff dimension is performed by measuring the distance profile
of geometries produced by the Markov chain Monte Carlo simulation. The distance
profile is the probability $\rho(r)$ to find an edge or face at a distance $r$.
One can measure distance traversing the edges which is called the graph distance
or the distance traversing the faces which is called the dual graph distance.
The two types of distances are visualised in Figure
\ref{results:plot:dualvsgraphdistance}. Both graph and dual graph distances have
been measured. 

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.33\linewidth]{results/assets/dualvsgraphdistance.pdf}
    \caption{The dual graph distance traverses faces and is displayed in
    \textcolor{rblinkcolor}{blue}. The graph distance traverses edges and is
    displayed in \textcolor{rbprimarycolor}{red}.}
    \label{results:plot:dualvsgraphdistance}
\end{figure}


The distance profiles are taken from a certain randomly picked origin face or
edge which results in very fluctuant distance profiles. This can be reduced by
measuring the distance profile from multiple random points in the geometry. The
sample rate, $n_\text{samples}$, defines how many times the distance profile is
measured. Here the sample rate is put to $n_\text{samples} = \frac{n}{16}$
which translates to approximately $0.2 \% - 80\%$ CPU-time on measuring for
respectively $n = 128$ and $n = 131072$ for $q = 1$. Spending $80 \%$ CPU-time
measuring is not optimal, but for all the other sizes the percentage is below
$50 \%$.

% The observable is the sum of $n_\text{samples}$ distance profiles. The
% observable is then measured $N$ times in batches of size $m$. The batching size
% is $m = 8$ and every file contains $N_b = 2$ number of batches. There will be
% $N_f = 8$ files per seed. The $N_f = 8$ files form one $\textbf{run}$. One
% \textbf{run} corresponds to one Markov chain, one random seed, $8 \times 2
% \times 8 = 128$ measurements. The time between measurements is set at $1024\
% \mathrm{sweeps}$. 

For $q = 1$, a total of $11520$ independent measurements were taken at an
interval of $512\ \mathrm{sweeps}$. The interval of $512\ \mathrm{sweeps}$ was
determined by doing a binning analysis and determining the minimal bin size $m$
for independent measurements. The $11520$ independent measurements consist of
$720$ Markov chains with different seeds. The $11520$ independent measurements
are split up into $10$ batches with $1152$ measurements. In every batch the
statistical error of the distance profile is calculated using the standard
error. The maximum measured system size is $n = 131072$. The simulations are worth $1.15$ years of CPU-time for $q = 1$.

For $q = 0.2$, a total $2304$ measurements were taken at a rate of $2048 \
\mathrm{sweeps}$, also determined by performing a binning analysis. The $2304$
measurements consisted of $144$ Markov chains. The maximum measured system size
is $n = 32768$. The total dataset is divided up into $9$ batches and is worth
$22.3$ days of CPU-time.

% and .... for $q =
% 0.2$. 
% The final
% Hausdorff dimension is then calculated by averaging over the Hausdorff dimension
% determined per run. The error on the Hausdorff dimension is determined by the
% Jackknife-method. 

% \begin{figure}[htb!]
%     \centering
%     \includegraphics[]{results/assets/distanceprofile.pdf}
%     \caption{Distance profiles of different system sizes}
%     \label{results:plot:comparisonoffittingmethods}
% \end{figure}

% \begin{figure}
%     \centering
% \begin{tikzpicture}
%     \matrix (M) [matrix of nodes,
%         nodes={minimum height = 7mm, minimum width = 2cm, outer sep=0, anchor=center, draw, fill=rbrblueb!50},
%         column 1/.style={nodes={draw=none, fill=none}, minimum width = 4cm},
%         row sep=1mm, column sep=-\pgflinewidth, nodes in empty cells,
%         e/.style={fill=rbrblueb!50}
%       ]
%       {
%         File 1 & $m$ & $m$ & $m$ & $m$ & $m$ \\
%         File 2 & $m$ & $m$ & $m$ & $m$ & $m$ \\
%         \vdots \\
%         File $N_f$ & $m$ & $m$ & $m$ & $m$ & $m$ \\
%       };
%       \draw (M-1-2.north west) ++(0,2mm) coordinate (LT) edge[|<->|, >= latex] node[above]{Number of batches, $N_b$} (LT-|M-1-6.north east);
%       \draw ([xshift=2mm]M-4-6.south east) coordinate (LT) edge[|<->|, >= latex] node[anchor=west]{Number of files, $N_f$} ([xshift=2mm]M-1-6.north east);
%   \end{tikzpicture}
%   \caption{One \textbf{run} for measuring the Hausdorff dimension}
%   \label{results:fig:setupdrawing}
% \end{figure}






\subsection{Finite-size scaling}
\label{section:finitesizescaling}

Finite-size scaling can be used to estimate critical exponents. Finite-size
scaling measures the scaling of $r$ as a function of $N$ by rescaling the
distance profile \cite{BuddCourse2021}. Here finite-size scaling will be used to
extract the Hausdorff dimension from the distance profiles.
The distance profile of a system of size $n$ is denoted by $\rho_n(r)$. The
distance profiles of size $n$ can be rescaled as

\begin{equation}
    \lim_{n \rightarrow \infty} n^{1/d_H} \rho_n (x n^{1/d_H}) = \rho(x)
\end{equation}

which is assumed to converge to $\rho(x)$ for $n \rightarrow \infty$ and $x > 0$
\cite{Barkley_2019}. In other words, the distance profiles should overlap which
they do for the correct Hausdorff dimension $d_H$.

The ideal way to perform this finite-size scaling and collapse the distance
profiles is described by Barkley and Budd in \cite{Barkley_2019}. Collapsing the
distances profiles is performed by fitting distance profiles $\rho_n(r)$ of
different sizes $n$ to the largest distance profile $\rho_{n_0}(r)$ of size
$n_0$. This corresponds to taking the function $x \rightarrow k_n
\rho_n(k_{n}^{-1} x )$ and fitting it to the function $x \rightarrow
\rho_{n_0}(x)$. The tails of the distance profiles are cut off by $\rho_n(r)
\geq 1/5 \max{\rho_n(r)}$ because they are influenced by discretization effects
\cite{Barkley_2019}.

Practically speaking, the distance profile $\rho_{n_0}(r)$ are interpolated
linearly. The datasets of the distance profiles consist of $\mathbf{x^\star} =
(x_1, x_2, \dots, x_n)$, and $\mathbf{\rho}_n = (\rho_n(x_1), \rho_n(x_2), \dots,
\rho_n(x_n))$. The datasets describe the function $x^\star \rightarrow
\rho_n(x^\star)$. For the collapse the $\mathbf{x^\star}$ will be rescaled and the
values of the new $\mathbf{x}$ are then given by: $x_i k_n^{-1} = x_i^\star$
which yields $x_i = x_i^\star k_n$. So $\mathbf{x}$ will be equivalent to
changing the datasets to $\mathbf{x} = x_1 k_n, x_2 k_n, \dots, x_n k_n$. The
distance profiles will be rescaled to $\mathbf{\rho}_n^\prime =
(k_n\rho_n^\prime(x_1), k_n\rho_n^\prime(x_2), \dots, k_n\rho_n^\prime(x_n))$. Also,
the statistical error $S_{\rho_{n}}(r)$ of the distance profiles is taken into
account for the collapsing. 

To improve the accuracy of the collapse, a shift $s \in \mathbb{R}$ can be
introduced by
\begin{equation}
    \lim_{n \rightarrow \infty} n^{1/d_H} \rho_n (x n^{1/d_H} -s ) = \rho(x)
\end{equation}
which will still yield correct finite-size scaling results \cite{Barkley_2019}.
Firstly, the shift $s_n$ must be determined which can be done by taking the
function $x \rightarrow k_n \rho_n(k_{n}^{-1} (x+s_n)-s_n )$ and fitting it to
the function $x \rightarrow \rho_{n_0}(x)$. The shift $s$ can be determined by
the weighted average of the fitted values of $s_n$ \cite{Barkley_2019}. Using
the shift $s$, $k_n$ can be determined by fitting $x \rightarrow k_n
\rho_n(k_{n}^{-1} (x+s)-s)$ to $x \rightarrow \rho_{n_0}(x)$ \cite{Barkley_2019,
Budd2022}. 

An estimate for the Hausdorff dimension is given by \cite{Barkley_2019}

\begin{equation}
    d_H (n) = \frac{\log{(n_0/n)}}{(k_n/k_{n_0})}
    \label{results:haus:converge}
\end{equation}


The Hausdorff dimension can be more accurately determined by fitting $k_n$ to
the following ansatz

\begin{equation}
    k_n \approx \left(\frac{n}{n_0}\right)^{-\frac{1}{d_H}} \left(a + b \left(\frac{n}{n_0}\right)^{-\delta}\right)
    \label{results:eq:determingdhprecise}
\end{equation}
where $a$ and $b$ are fit parameters with $a \approx 1$, $|b| \ll 0$ and $\delta >
0$ \cite{Budd2022,Barkley_2019}.

\subsection{Results}
\label{results:hausdorff}

In Figure \ref{results:plot:hausdorfffit}, the results of measuring the
Hausdorff dimension for $q = 1$ \textbf{without} shift $s$ are plotted as a
function of the system size $n$. Equation \eqref{results:haus:converge} is used
to calculate $d_H(n)$ in Figure \ref{results:plot:hausdorfffit} from the fitted
$k_n$. The errors are determined by jackknife by using $10$ batches. The
predicted Hausdorff dimension by the Ding and Gwynne formula (see Equation
\eqref{eq:theory:dinggwynnehaus}) is equal to $d_H = 3.58$ and drawn with the
dashed line in Figure \ref{results:plot:hausdorfffit}. The measured Hausdorff
dimension by dual graph distance and graph distance approach the predicted value
as the system size $n$ increases. It is however clear that a larger system size
is required to find out to what value the estimates approach. By fitting
Equation \eqref{results:eq:determingdhprecise}, the Hausdorff dimension was
further determined to be for the $d_H = 3.331 \pm 0.016$ dual graph distance and
$d_H = 3.290 \pm 0.011$ for the graph distance. The raw fit results can be found
in Table \ref{appendix:table:dualgraphfitwithout} for the dual graph distance
and in Table \ref{appendix:table:graphfitwithout} for the graph distance. In
Figure \ref{results:plot:collapsewithouts}, the collapsed distance profiles of the graph and
dual graph distance are plotted for the fitted Hausdorff dimensions.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/hausdorfffit.pdf}
    \caption{Measuring the Hausdorff dimension $d_H$ at $q = 1$. The expected Hausdorff dimension is $d_H = 3.58$ by Ding and Gwynne.}
    \label{results:plot:hausdorfffit}
\end{figure}

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/finitesizescaledversionwithoutsgraphdualgraph.pdf}
    \begin{subfigure}[b]{0.55\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents1.pdf}
    \caption{}\label{fig:results:dualfinitewithout}
    \end{subfigure}
    \begin{subfigure}[b]{0.43\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents2.pdf}
    \caption{}\label{fig:results:graphfinitewithout}
    \end{subfigure}
    \caption{Finite-size scaling of the dual graph distance and graph distance
    with Hausdorff dimensions determined from fitting with Equation
    \eqref{results:eq:determingdhprecise}. The used Hausdorff dimensions are
    $d_H = 3.331 \pm 0.016$ for the dual graph distance and $d_H = 3.290 \pm
    0.011$ for the graph distance.}
    \label{results:plot:collapsewithouts}
\end{figure}
 
Introducing the shift $s$ changes the results significantly. The determined
values for the shift $s$ at $q = 1$ are $s = 3.297$ for the dual graph distance
and $s = 5.229$ for the graph distance. Fitting using the shift $s$ yields the results in
Figure \ref{results:plot:hausdorffresultsforq1} where again the errors have been
determined using jackknife and the Hausdorff dimension is calculated using
Equation \eqref{results:haus:converge}. 

The results in Figure \ref{results:plot:hausdorffresultsforq1} are more accurate
than without the shift and even coincide with the predicted Hausdorff dimension
by the Ding and Gwynne formula (see Equation \eqref{eq:theory:dinggwynnehaus}). The Watabiki formula for
the Hausdorff dimension is also plotted. The Watabiki formula is an older
conjectured relation between the Liouville coupling constant $\gamma$ and the
Hausdorff dimension that was contradicted by numerical simulations in
\cite{Barkley_2019}. The dual graph distance and graph distance seem to favour
the Ding and Gwynne formula for larger system sizes, but the statistical error
is too large to make any hard conclusions. 

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/hausdorfffitq1.pdf}
    \caption{Plot of the Hausdorff dimension at $q = 1$ calculated with Equation
    \eqref{results:haus:converge} for the finite-size scaling with shift $s$.
    The statistical errors are calculated by jackknifing $10$ batches.}
    \label{results:plot:hausdorffresultsforq1}
\end{figure}

Fitting using the ansatz Equation \eqref{results:eq:determingdhprecise} yields
for the dual graph distance $d_H = 3.5598 \pm 0.0028$ and for the graph distance
$d_H = 3.540 \pm 0.006$. The statistical error on these values is determined by
jackknifing and fits with a statistical error larger than $1$ on the Hausdorff
dimension were removed. The removed fits also do not respect the $a \approx 1$,
$|b| \ll 0$ of the ansatz. The full fit information can be found in Table
\ref{appendix:table:dualgraphfit} for the dual graph distance and in Table
\ref{appendix:table:graphfit} for the graph distance. The collapsed distance
profiles for the measured Hausdorff dimensions can be found in Figure
\ref{results:plot:collapsewith}. Lastly, the values determined using these fits
are in the range of $3.54 - 3.56$ which is lower than the conjectured values of
the Hausdorff dimension of $d_H = 3.58$. By looking at Figure
\ref{results:plot:hausdorffresultsforq1} however, it seems reasonable that when
the simulations are extended to larger system sizes the Hausdorff dimension will
converge to the conjectured value at $q = 1$.
 

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/finitesizescaledversionwithshift.pdf}
    \begin{subfigure}[b]{0.55\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents1.pdf}
    \caption{}\label{fig:results:dualfinitewith}
    \end{subfigure}
    \begin{subfigure}[b]{0.43\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents2.pdf}
    \caption{}\label{fig:results:graphfinitewith}
    \end{subfigure}
    \caption{Finite-size scaling of the dual graph distance and graph distance
    with Hausdorff dimensions determined from fitting with
    \eqref{results:eq:determingdhprecise}. The used Hausdorff dimensions are
    $d_H = 3.5598 \pm 0.0028$ for the dual graph distance and $d_H = 3.540 \pm
    0.006$ for the graph distance.}
    \label{results:plot:collapsewith}
\end{figure}

\clearpage
  
%The data is cut-off at 1/5 of the peak of the histogram.
For $q = 0.2$, the results are displayed in Figure
\ref{results:plot:hausdorffresultsforq02}. The maximum system size used for $q =
0.2$ is $n_0 = 32768$ in comparison to $n = 131072$ for $q = 1$. The statistical
errors on have been determined by jackknifing. The fit results of Equation
\eqref{results:eq:determingdhprecise}, for the Hausdorff dimension are $d_H =
3.3394 \pm 0.0024$ for the dual graph distance and $d_H = 3.355 \pm 0.019$ for
the graph distance. This is again lower than the expected Hausdorff dimension of
$d_H \approx 3.40$, but considering Figure
\ref{results:plot:hausdorffresultsforq02} the estimates for the Hausdorff
dimension could certainly approach the conjectured value of $d_H \approx 3.40$
for larger system sizes.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/hausdorfffitq02.pdf}
    \caption{Plot of the Hausdorff dimension at $q = 0.2$ calculated with Equation
    \eqref{results:haus:converge} for the finite-size scaling with shift $s$.
    The statistical errors are calculated by jackknifing $9$ batches.}
    \label{results:plot:hausdorffresultsforq02}
\end{figure}

In Figure \ref{results:plot:summary}, a summary is given of all the results of the
Hausdorff and string susceptibility measurements.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/meanderscriticalexponentscomp.pdf}
    \caption{Summary of the results of the Hausdorff dimension $d_H$ from
    section \ref{section:hausresults} and string susceptibility
    $\gamma_\mathrm{s}$ from section \ref{section:results:stringsus}. The lines
    indicate the conjectures values of the Hausdorff dimension and string
    susceptibility.}
    \label{results:plot:summary}
\end{figure}


% \begin{figure}[htb!]
%     \centering
%     \includegraphics[]{results/assets/finitesizescaledversion.pdf}
%     \caption{Rescaled $q = 1$ with $d_H = 3.56$}
%     \label{results:plot:finitefull}
% \end{figure}


% \begin{figure}[htb!]
%     \centering
%     \includegraphics[]{results/assets/finitesizescaledversionselection.pdf}
%     \caption{Rescaled $q = 1$ with $d_H = 3.56$}
%     \label{results:plot:finiteselect}
% \end{figure}



\newpage