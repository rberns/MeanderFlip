#include <stdio.h>
#include <iostream>
#include <cairo/cairo.h>
#include "json.hpp"
#include "planarmap+json.hpp"
#include "meandergenerator.hpp"

int main() {
    printf("Hello world!\n");

    int N = 20;

    struct MeanderSystem generated_meander_system = generate_glued_map(N);

    std::vector<Edge> edges = generated_meander_system.edges;

    

    cairo_surface_t *surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 250*N, 250*(N+1));
    cairo_t *cr = cairo_create (surface);

    cairo_select_font_face (cr, "serif", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size (cr, 32.0);
    cairo_set_source_rgb (cr, 0.0, 0.0, 1.0);
    cairo_move_to (cr, 10.0, 50.0);
    cairo_show_text (cr, "Hello, world");

    for (size_t i = 0; i < N; i++)
    {
        cairo_move_to (cr, i*250, N*125);
        cairo_rectangle (cr, i*250, N*125, 250, 250);
        cairo_set_source_rgb (cr, 1, 0, 0);
        cairo_set_line_width (cr, 2);
        cairo_stroke (cr);
    }

    // cairo_rel_curve_to (cr, -0.25, -0.125, -0.25, 0.125, -0.5, 0);
    
    std::vector<bool> visited_edges(edges.size(), false);
    
    std::vector<std::vector<int> > paths;

    Edge current_edge;
    
    int curr_face = 1;

    for (int i = 0, endi = edges.size(); i < endi; ++i) {
        current_edge = edges[i];
        Edge adjacent_edge = adjacent(current_edge, edges);
        int length = abs(current_edge.face_id - adjacent_edge.face_id);
        if (current_edge.id % 4 == 2)
        {
            // lower half
            cairo_new_path(cr);
            //cairo_move_to (cr, ((current_edge.face_id-1)*125 + (adjacent_edge.face_id-1)*125)+125, N*125+250);
            cairo_arc(cr, ((current_edge.face_id-1)*125 + (adjacent_edge.face_id-1)*125)+125, N*125+250, length*125, 0, M_PI);
        } else if (current_edge.id % 4 == 0) {
            // upper half
            cairo_new_path(cr);
            //cairo_move_to (cr, ((current_edge.face_id-1)*125 + (adjacent_edge.face_id-1)*125)+125, N*125);
            cairo_arc(cr, ((current_edge.face_id-1)*125 + (adjacent_edge.face_id-1)*125)+125, N*125, length*125, M_PI, 2*M_PI);
        }
        cairo_set_source_rgb (cr, 0, 0, 0);
        cairo_set_line_width (cr, 10);
        cairo_stroke (cr);

        cairo_select_font_face (cr, "sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
        cairo_set_font_size (cr, 32.0);
        cairo_set_source_rgb (cr, 0.0, 0.0, 1.0);
        if (current_edge.id % 4 == 2)
        {
            cairo_move_to (cr, (current_edge.face_id-1) * 250 + 125, N*125+250-40);
        } else if (current_edge.id % 4 == 0) {
            cairo_move_to (cr, (current_edge.face_id-1) * 250 + 125, N*125+40);
        }
        auto text = std::to_string(current_edge.id);
        cairo_show_text (cr, text.c_str());
        
    }   

    cairo_destroy (cr);
    cairo_surface_write_to_png (surface, "hello.png");
    cairo_surface_destroy (surface);
    return 0;

}