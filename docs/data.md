# Notes

## Files

The data is stored in the simdata/ directory per value of q and n. The data files have the following nameconvention with sim_*

The size and accuracy testing file start with diagnostics_sim_*

## Determining error of string susceptibility data

$ \text{error} = \frac{1}{\sqrt{N}}\sqrt{\frac{1}{N} \sum_{i} \xi^2 - \left(\frac{1}{N} \sum_{i} \xi\right)^2 {}} $

## Determining the error of histogram data

From Werner Krauth on statistics.

A histogram consisting of $N$ bins can be modelled as $N$ Bernoulli variables. If we perform $M$ measurements, the total will be given as follows:

$\xi_k = \xi_{k,1} + \xi_{k,2} + \cdot \cdot \cdot + \xi_{k,M}$ 

It is important to note that $\xi_{k,i} = 1$ when $k = k$ and  $\xi_{k,i} = 0$ when $k \neq k$.

The variance is then given by 

$\text{Var}(\xi_k) = \theta_k (1-\theta_k)$

or 

$\text{Var}(\xi_k) = \left\langle \xi_k^2 \right\rangle - \left\langle \xi_k \right\rangle^2
$

So 

$\sigma_k = \sqrt{\text{Var}(\xi_k)} = \sqrt{\left\langle \xi_k^2 \right\rangle - \left\langle \xi_k \right\rangle^2}$

For the experiment, we add multiple independent histograms. Each with a value $p_i$ and $\sigma_i$. 

or average





in Bernoulli case only 0 or 1, so Add them together by a weighted average: $\sigma_k = \sqrt{\text{Var}(\xi_k)} = \sqrt{\left\langle \xi_k \right\rangle - \left\langle \xi_k \right\rangle^2}$
