//
//  planarmap.hpp
//  MeanderFlip
//
//  Created by Rutger Berns on 11/10/2021.
//

#ifndef planarmap_hpp
#define planarmap_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <iostream>
#include <cassert>


struct Edge
{
    int id, next, previous, adjacent;
    bool is_shore;
    int face_id;
    bool is_end;

    bool operator==(const Edge& a) const {
      return  a.id == id && 
              a.next == next &&
              a.previous == previous && 
              a.adjacent == adjacent &&
              a.is_shore == is_shore && 
              a.face_id == face_id;
    }

    bool operator!=(const Edge& a) const {
      return  a.id != id && 
              a.next != next &&
              a.previous != previous && 
              a.adjacent != adjacent &&
              a.is_shore != is_shore && 
              a.face_id != face_id;
    }
};



typedef std::vector<Edge> Face;

void debug_face_printer(Face face);

//void convert_to_node_graph(const std::vector<Edge> & edges);

std::ostream& operator<<(std::ostream& os, const Edge& edge);

std::ostream& operator<<(std::ostream& os, const Face& face);




#endif /* planarmap_hpp */

