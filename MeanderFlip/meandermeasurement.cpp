#include "meandermeasurement.hpp"

MeanderMeasurement::MeanderMeasurement(MeanderRunner * runner_)
{
    runner = runner_;
}

void MeanderMeasurement::measure() {
    measure_stopwatch.resume();
    measure_();
    measure_stopwatch.pause();
}

void MeanderMeasurement::setup(nlohmann::json jsondata) {
    rate = jsondata["rate"];
    id = jsondata["id"];
    setup_(jsondata["setup_data"]);
}

nlohmann::json MeanderMeasurement::json() {
    nlohmann::json j;// = json_();
    j["t_measuring"] = measure_stopwatch.nanoseconds();
    j["rate"] = rate;
    j["id"] = id;
    j.merge_patch(json_());
    //std::cout << "t_measuring\n";
    return j;
}

MeanderStringSusceptibility::MeanderStringSusceptibility(MeanderRunner * runner_): MeanderMeasurement(runner_)
{
    histogram_size = runner_->simulation.meander_of_order_n();
    hists.push_back(Histogram(histogram_size));
}


/*
 * 
 * setup_data should be formatted as JSON, e.g.
 * {"n_samples": 2}
 * n_samples is old property
 */
void MeanderStringSusceptibility::setup_(nlohmann::json setup_data) {
    batching_size = setup_data["batching_size"];
}

// void MeanderStringSusceptibility::measure_string_susceptibility() {
//     std::vector<uint64_t> data;

//     for(int i = 0; i < n_samples; i++) {
//         int n = runner->simulation.quad_stringsusceptibility_rand();
//         if (n != -1) {
//             data.push_back(n);
//         }
//     }

//     int n_faces = runner->simulation.num_faces();

//     Histogram newHist(data, n_faces);

//     hist += newHist;
// }

void MeanderStringSusceptibility::measure_() {
    measure_string_susceptibility_full();
}

void MeanderStringSusceptibility::measure_string_susceptibility_full() {
    if (hists[batch_index].N == batching_size)
    {
        hists.push_back(Histogram(histogram_size));
        batch_index += 1;
    }
    
    std::vector<uint64_t> data;

    data = runner->simulation.stringsusceptibility_full();

    int n_faces = runner->simulation.meander_of_order_n();

    for (size_t i = 0; i < data.size(); i++)
    {
        data[i] = (uint64_t)(data[i]/2);
    }
    

    Histogram newHist(data, n_faces);

    hists[batch_index] += newHist;
}

// std::vector<std::tuple<uint64_t, uint64_t, uint64_t> > MeanderStringSusceptibility::data() {
//     std::vector<std::tuple<uint64_t, uint64_t, uint64_t> > out;
//     for(size_t i = 0; i < hist.size; i++) {
//         if (hist.hist[i] != 0)
//         {
//             out.push_back(std::make_tuple(i+1, hist.hist[i], hist.hist_squared[i]));
//         }
//     }
//     return out;
// }

std::vector<std::tuple<std::vector<uint64_t>, std::vector<uint64_t> > > MeanderStringSusceptibility::data() {
    std::vector<std::tuple<std::vector<uint64_t>, std::vector<uint64_t> > > out;
    for(auto hist : hists) {
        out.push_back(std::make_tuple(hist.hist, hist.hist_squared));
    }
    return out;
}

void MeanderStringSusceptibility::empty() {
    batch_index = 0;
    //size_t size = hist.size;
    hists.clear();
    hists.push_back(Histogram(histogram_size));
}

void MeanderStringSusceptibility::reset() {
    empty();
}

std::vector<uint64_t> MeanderStringSusceptibility::N() {
    std::vector<uint64_t> sizes;
    for(auto hist : hists) {
        sizes.push_back(hist.N);
    }
    return sizes;
}

nlohmann::json MeanderStringSusceptibility::json_() {
    nlohmann::json j;

    nlohmann::json j_nested;

    j["measurement_type"] = 2;
    j["data"] = data();
    j_nested["n_independent"] = N();
    //std::cout << j << "\n";
    j_nested["batching_size"] = batching_size;
    //j_nested["n_samples"] = n_samples;
    j["metadata"] = j_nested;
    //std::cout << j << "\n";

    return j;
}

/*
 * 
 * MeanderDistance: MeanderMeasurement
 * 
 * Measures the distribution of distances on geometry
 * 
 *
 */

MeanderDistance::MeanderDistance(MeanderRunner * runner_): MeanderMeasurement(runner_)
{
    hists.push_back(Histogram(histogram_size));
}

void MeanderDistance::measure_() {
    measure_distance();
}

void MeanderDistance::setup_(nlohmann::json setup_data) {
    n_samples = setup_data["n_samples"];
    batching_size = setup_data["batching_size"];
    setup_data.value("dist_type", "dual");
    if (setup_data["dist_type"] == "dual")
    {
        is_dual = true;
    } else {
        is_dual = false;
    }
    
}

void MeanderDistance::measure_distance() {
    if (hists[batch_index].N == batching_size)
    {
        hists.push_back(Histogram(histogram_size));
        batch_index += 1;
    }
    
    std::vector<uint64_t> data;

    data = runner->measure_dist(n_samples, is_dual);
    

    Histogram newHist(data);

    hists[batch_index] += newHist;

    //std::cout << "batch_index " << batch_index << "\n";

    // std::vector<uint64_t> data;
    // std::cout << "WARNING WHEN USING THIS";
    // data = runner->measure_dist(n_samples);

    // Histogram newHist(data);

    // hist += newHist;
}

// std::vector<std::tuple<uint64_t, uint64_t, uint64_t> > MeanderDistance::data() {
//     std::vector<std::tuple<uint64_t, uint64_t, uint64_t> > out;
//     for(size_t i = 0; i < hist.size; i++) {
//         if (hist.hist[i] != 0)
//         {
//             out.push_back(std::make_tuple(i+1, hist.hist[i], hist.hist_squared[i]));
//         }
//     }
//     return out;
// }

std::vector<std::tuple<std::vector<uint64_t>, std::vector<uint64_t> > > MeanderDistance::data() {
    std::vector<std::tuple<std::vector<uint64_t>, std::vector<uint64_t> > > out;
    size_t out_len = 1;
    for(auto hist : hists) {
        out_len = std::max(out_len, hist.size);
    }
    for(auto hist : hists) {
        hist.resize(out_len);
        out.push_back(std::make_tuple(hist.hist, hist.hist_squared));
    }
    return out;
}

std::vector<uint64_t> MeanderDistance::N() {
    std::vector<uint64_t> sizes;
    for(auto hist : hists) {
        sizes.push_back(hist.N);
    }
    return sizes;
}

void MeanderDistance::empty() {
    batch_index = 0;
    hists.clear();
    hists.push_back(Histogram(histogram_size));
}

nlohmann::json MeanderDistance::json_() {
    nlohmann::json j;


    nlohmann::json j_nested;

    j["measurement_type"] = "4";
    j["data"] = data();
    j_nested["n_independent"] = N();
    j_nested["batching_size"] = batching_size;
    j_nested["n_samples"] = n_samples;

    
    //std::cout << j << "\n";
    
    j["metadata"] = j_nested;
    return j;
}

void MeanderDistance::reset() {
    empty();
}


MeanderComponents::MeanderComponents(MeanderRunner * runner_): MeanderMeasurement(runner_)
{
    
}

void MeanderComponents::measure_() {
    k_components.push_back(runner->simulation.k_components());
}

void MeanderComponents::setup_(nlohmann::json setup_data) {
}

nlohmann::json MeanderComponents::json_() {
    nlohmann::json j;

    nlohmann::json j_nested;

    
    j["measurement_type"] = 3;
    j["data"] = k_components;

    j_nested["length"] = k_components.size();

    j["metadata"] = j_nested;

    return j;
}

void MeanderComponents::reset() {
    k_components.clear();
}


// MeanderComponents::MeanderComponents(MeanderRunner * runner_): MeanderMeasurement(runner_)
// {
    
// }

// void MeanderComponents::measure_() {
//     k_components.push_back(runner->simulation.k_components());
// }

// void MeanderComponents::setup_(nlohmann::json setup_data) {
// }

// nlohmann::json MeanderComponents::json_() {
//     nlohmann::json j;

//     nlohmann::json j_nested;

//     j["measurement_type"] = "2";
//     j["data"] = k_components;
//     j["metadata"] = j_nested;
// }

// void MeanderComponents::reset() {
//     k_components.clear();
// }