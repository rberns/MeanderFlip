
import matplotlib.pyplot as plt
import numpy as np
import json
import pandas as pd

def get_data(path):
    data = None
    N = None

    with open(path) as file_:
        data_ = json.loads(file_.read())
        N = data_["n"]
        data = data_["data"]
    
    return [data, N]

def get_conv(path):
    [data, N] = get_data(path)
    
    Mnk = np.array([item['Mnk'] for item in data])

    M = Mnk/Mnk.sum(axis=1, keepdims=True)
    
    return [M, N, Mnk.sum(axis=1, keepdims=True)]

def get_time(path):
    [data, N] = get_data(path)

    time = np.array([item['time'] for item in data])
    iterations = np.array([item['iterations'] for item in data])

    return [time, iterations]

def plot_conv(path):
    M = get_conv(path)

    plt.imshow(M.T)
    plt.show()


def plot_deviation(file, flat):
    M, N, M_sum = get_conv(file)
    df = pd.read_csv("datafrompaper/Meanderfoldingarchtab1.csv")

    x = np.arange(1, N+1)
    y_analytic = (df[str(N)]/df[str(N)].sum())[:N]


    y_sim = M[0]

    print("file: {}, size: {} {} {}".format(file, x.size, y_analytic.size, y_sim.size))

    fig, ax1 = plt.subplots()

    # plt.title("size: {}".format(N))
    ax1.plot(x, y_analytic, label="y_analytic")
    ax1.plot(x, y_sim,  label="y_sim")

    #plt.yscale('log')
    ax1.set_yscale('log')
    ax1.legend()

    ax2 = ax1.twinx()

    ax2.set_title("size: {}, percentage: {}%".format(N, M_sum/df[str(N)].sum()*100))
    #plt.title("size: {}, percentage: {}%".format(N, M_sum/df[str(N)].sum()*100))
    # plt.yscale('log')
    if flat: ax2.plot(x, y_analytic-y_sim, label="sim-analytic", color='r')
    if not flat: ax2.plot(x, y_sim/y_analytic, label="sim/analytic", color='r')
    #ax2.set_yscale('log')

    ax2.legend()
