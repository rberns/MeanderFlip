//
//  meandergenerator.hpp
//  MeanderGenerator
//
//  Created by Rutger Berns on 17/10/2021.
//

#ifndef meandergenerator_hpp
#define meandergenerator_hpp

#include "planarmap.hpp"
#include "helpers.hpp"
#include "meanderpermutation.hpp"

struct MeanderSystem
{
    std::vector<Edge> edges;
    std::vector<int> face_of_edge;
    std::vector<Face> faces;
    int n_shore_ends;
};

Face generate_bridge_segment(int degree, int start_id, int face_id);
MeanderSystem generate_glued_map(int n_bridges);


#endif /* meandergenerator_hpp */