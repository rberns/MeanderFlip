//
//  meander.hpp
//  MeanderFlip
//
//  Created by Rutger Berns on 5/12/2021.
//

#ifndef meander_hpp
#define meander_hpp

#include <iostream>

#define DEBUG false // this
#define DEBUGPERM false
#define DEEPDEBUGPERM false

#define LOGDEBUG false

#define INEFFICIENTINFOPRINT false // this
#define INFOPRINT false // this
typedef float Floatingpoint; 
#define SEED 1234;

#define Precondition(x) (_Precondition(x, #x));

inline void _Precondition(bool condition, const char * str) {
    std::cout << str << " is " << condition << "\n";
    if(!condition) {
        exit(1);
    }
}

#if(LOGDEBUG)
#define LOG_DEBUG(x) (std::cout << "[" << __FUNCTION__ << "] " << x << "\n");
#else
#define LOG_DEBUG(x);
#endif

#endif /* meander_hpp */