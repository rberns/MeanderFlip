#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2/catch.hpp>
#include <numeric>

#include "meanderrunner.hpp"

TEST_CASE("Test creation of MeanderMap") {
    MeanderMap map(4);
    map.generate();
}


const uint64_t mnk[12][12] ={{1,0,0,0,0,0,0,0,0,0,0,0},
                            {2,2,0,0,0,0,0,0,0,0,0,0},
                            {8,12,5,0,0,0,0,0,0,0,0,0},
                            {42,84,56,14,0,0,0,0,0,0,0,0},
                            {262,640,580,240,42,0,0,0,0,0,0,0},
                            {1828,5236,5894,3344,990,132,0,0,0,0,0,0},
                            {13820,45164,60312,42840,17472,4004,429,0,0,0,0,0},
                            {110954,406012,624240,529104,271240,85904,16016,1430,0,0,0,0},
                            {933458,3772008,6540510,6413784,3935238,1569984,405552,63648,4862,0,0,0},
                            {8152860,35994184,69323910,76980880,54787208,26200468,8536890,1860480,251940,16796,0,0},
                            {73424650,351173328,742518832,919032664,742366152,412348728,161172704,44346456,8356656,994840,58786,0},
                            {678390116,3490681428,8028001566,10941339452,9871243896,6230748192,2830421952,934582000,222516030,36936988,3922512,208012}};




TEST_CASE("Distribution n = 6") {
    int n = 6;
    int seed = rand();
    double q = 1.0;
    int iterations = 10000000;

    MeanderRunner runner(n, seed, q);

    runner.setup();

    runner.iterate_and_mnk(iterations);

    std::vector<uint64_t> n_meanders_of_k_paper(mnk[n-1], mnk[n-1]+12);

    uint64_t sum = std::accumulate(runner.n_meanders_of_k.begin(), runner.n_meanders_of_k.end(), 0);

    CHECK(sum == iterations);

    uint64_t sum_paper = std::accumulate(n_meanders_of_k_paper.begin(), n_meanders_of_k_paper.end(), 0);

    std::vector<double> deviation(n, 0);

    for (size_t i = 0; i < n; i++)
    {   // first rescale to size of n_meanders_of_k_paper, than divide.
        deviation[i] = ((double)runner.n_meanders_of_k[i]/(double)sum) / ((double)n_meanders_of_k_paper[i]/(double)sum_paper);
        //CAPTURE((double)runner.n_meanders_of_k[i]/(double)sum);
        //CAPTURE(((double)n_meanders_of_k_paper[i]/(double)sum_paper));
    }
    
    CAPTURE(runner.n_meanders_of_k);


    CAPTURE(deviation);

    CHECK(false);
}

TEST_CASE("Distribution n = 12") {
    int n = 12;
    int seed = rand();
    double q = 1.0;
    int iterations = 10000000;

    MeanderRunner runner(n, seed, q);

    runner.setup();

    runner.iterate_and_mnk(iterations);

    std::vector<uint64_t> n_meanders_of_k_paper(mnk[n-1], mnk[n-1]+12);

    uint64_t sum = std::accumulate(runner.n_meanders_of_k.begin(), runner.n_meanders_of_k.end(), (uint64_t)0);

    CHECK(sum == iterations);

    uint64_t sum_paper = std::accumulate(n_meanders_of_k_paper.begin(), n_meanders_of_k_paper.end(), (uint64_t)0);

    std::vector<double> deviation(n, 0);
    std::vector<double> deviation_error(n, 0);

    for (size_t i = 0; i < n; i++)
    {   // first rescale to size of n_meanders_of_k_paper, than divide.
        deviation[i] = ((double)runner.n_meanders_of_k[i]/(double)sum) / ((double)n_meanders_of_k_paper[i]/(double)sum_paper);
        double p = ((double)runner.n_meanders_of_k[i]/(double)sum);
        deviation_error[i] = (p*(1-p) / sqrt((double)sum)  )/ ((double)n_meanders_of_k_paper[i]/(double)sum_paper);
        //CAPTURE((double)runner.n_meanders_of_k[i]/(double)sum);
        //CAPTURE(((double)n_meanders_of_k_paper[i]/(double)sum_paper));
    }
    
    CAPTURE(runner.n_meanders_of_k);


    CAPTURE(deviation);
    CAPTURE(deviation_error);

    CHECK(false);
}

// testing generation performance
TEST_CASE("Perfomance of meander creation") {
    BENCHMARK("Meander generate 4") {
        MeanderMap map(4);
        return map.generate();
    };

    BENCHMARK("Meander generate 10") {
        MeanderMap map(10);
        return map.generate();  
    };

    BENCHMARK("Meander generate 100") {
        MeanderMap map(100);
        return map.generate();
    };

    BENCHMARK("Meander generate 1000") {
        MeanderMap map(1000);
        return map.generate();
    };

    BENCHMARK("Meander generate 10000") {
        MeanderMap map(10000);
        return map.generate();
    };
}

TEST_CASE("Creation of MeanderMonteCarlo") {
    int n = 100;
    int seed = 1094894;
    double q = 1.0;
    MeanderMonteCarlo simulation(n, seed, q);
    simulation.setup();
}

TEST_CASE("Meander runner") {
    int n = 100;
    int seed = 1094894;
    double q = 1.0;

    MeanderRunner runner(n, seed, q);

    runner.setup();

    runner.iterate(10000);

    CAPTURE(runner.simulation.measure_dist());
}


TEST_CASE("Flip performance") {
    int n = 100;
    int seed = 1094894;
    double q = 1.0;
    
    

    MeanderMonteCarlo simulation1 = MeanderMonteCarlo(100, seed, q);
    simulation1.setup();

    BENCHMARK_ADVANCED("flips n = 100")(Catch::Benchmark::Chronometer meter) {
        
        meter.measure([&simulation1] {
            for (size_t i = 0; i < 10000; i++)
            {
                simulation1.flip();
            }
            CAPTURE(simulation1.k_components());
            return simulation1.k_components();
        });
    };

    MeanderMonteCarlo simulation2 = MeanderMonteCarlo(1000, seed, q);
    simulation2.setup();

    BENCHMARK_ADVANCED("flips n = 1000")(Catch::Benchmark::Chronometer meter) {
        meter.measure([&simulation2] {
            for (size_t i = 0; i < 10000; i++)
            {
                simulation2.flip();
            }
            CAPTURE(simulation2.k_components());
            return simulation2.k_components();
        });
    };

    MeanderMonteCarlo simulation3 = MeanderMonteCarlo(10000, seed, q);
    simulation3.setup();

    BENCHMARK_ADVANCED("flips n = 10000")(Catch::Benchmark::Chronometer meter) {
        meter.measure([&simulation3] {
            for (size_t i = 0; i < 10000; i++)
            {
                simulation3.flip();
            }
            CAPTURE(simulation3.k_components());
            return simulation3.k_components();
        });
    };

    MeanderMonteCarlo simulation4 = MeanderMonteCarlo(100000, seed, q);
    simulation4.setup();

    BENCHMARK_ADVANCED("flips n = 100000")(Catch::Benchmark::Chronometer meter) {
        meter.measure([&simulation4] {
            for (size_t i = 0; i < 10000; i++)
            {
                simulation4.flip();
            }
            CAPTURE(simulation4.k_components());
            return simulation4.k_components();
        });
    };

    MeanderMonteCarlo simulation5 = MeanderMonteCarlo(1000000, seed, q);
    simulation5.setup();

    BENCHMARK_ADVANCED("flips n = 1000000")(Catch::Benchmark::Chronometer meter) {
        
        meter.measure([&simulation5] {
            for (size_t i = 0; i < 10000; i++)
            {
                simulation5.flip();
            }
            CAPTURE(simulation5.k_components());
            return simulation5.k_components();
        });
    };

    // MeanderMonteCarlo simulation6 = MeanderMonteCarlo(10000000, seed, q);
    // simulation6.setup();

    // BENCHMARK_ADVANCED("flips n = 10000000")(Catch::Benchmark::Chronometer meter) {
        
    //     meter.measure([&simulation6] {
    //         for (size_t i = 0; i < 10000; i++)
    //         {
    //             simulation6.flip();
    //         }
    //         CAPTURE(simulation6.k_components());
    //         return simulation6.k_components();
    //     });
    // };
}