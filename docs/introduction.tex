% !TeX root = thesis.tex

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.5\textwidth]{pictures/PisaFancy.png}
    \caption{Google Earth image of the river Arno in Pisa.}
    \label{theory:meanders:meanderimage}
\end{figure}


A meander is typically a river that zigzags through some landscape like in
Figure \ref{theory:meanders:meanderimage}. Meanders are also a combinatorial
problem or statistical system. A question one might ask: how many unique loops
can you make using the four bridges in Figure
\ref{theory:meanders:meanderimage}? You are only allowed to pass every bridge
once. This seemingly simple question can only be solved by counting all the
possible combinations. It has not (yet) been solved exactly
\cite{DiFrancescoExactAsym2000}. That is fascinating for a simple problem like
this. The loop and the river in Figure \ref{theory:meanders:meanderimage} are
drawn more structured in Figure \ref{theory:pic:arno}.

\begin{figure}[htb!]
    \centering
\begin{tikzpicture}
    \draw (0,0) [thick, color=rbdarkgray]-- (4,0);
    %\draw[dotted] (0,-2) grid (4,2);
    \node (node1) at (0.5,0) {};
    \node (node2) at (1.5,0) {};
    \node (node3) at (2.5,0) {};
    \node (node4) at (3.5,0) {};

    \draw[meanderroad] (0.5,0) arc (180:0:1.5);
    \draw[meanderroad] (1.5,0) arc (180:0:0.5);
    \draw[meanderroad] (0.5,0) arc (-180:0:0.5);
    \draw[meanderroad] (2.5,0) arc (-180:0:0.5);
    \draw[bridgenode] (node1) circle (0.05);
    \draw[bridgenode] (node2) circle (0.05);
    \draw[bridgenode] (node3) circle (0.05);
    \draw[bridgenode] (node4) circle (0.05);
\end{tikzpicture}
\caption{Example of a meander of order 2, which has 4 bridges. The horizontal line is the river and the loop is the road.}
\label{theory:pic:arno}
\end{figure}


Meanders as a toy model of two-dimensional quantum gravity is quite an abstract
title. In the 90s Francesco et al. conjectured that Meanders are a model of
two-dimensional quantum gravity (2DQG). How can one see this intuitively?
Quantum gravity aims to unify quantum mechanics and general relativity. General
relativity is a theory of geometry and quantum mechanics is a theory of
probability and integrals over all possibilities. Analytically the combination
of these theories is quite complex. So one way of studying the combination of
them is by random geometry which is discrete and easier to study. Taking
quadrangles as a building block and glueing them together based on a meander
gives a geometry. Taking the continuum limit of that surface is conjectured to be
2DQG. The continuum limit is taking the size of the building blocks to zero and
taking the number of building blocks to infinity. One can also say,
the continuum limit of random geometry decorated by meander is conjectured to be
2DQG. 2DQG is a toy model of quantum gravity that allows for studying quantum
gravity in a more simple manner. But why would physics want to have a theory of
quantum gravity?

In the 17th century, Isaac Newton transformed the way we think of gravity
through his theory of Newtonian gravity. Newtonian gravity gave accurate
predictions of the orbits of planets. At the beginning of the 19th century,
Maxwell and Lorentz unified electricity and magnetism into Electromagnetism. In
the 20th century, Albert Einstein was intrigued by the principle of relativity,
which means that physical laws are the same in any inertial frame. Einstein
thought that it was no coincidence to find the principle of relativity in both
electromagnetism and classical mechanics. Based on the Michelson-Morley
experiment, which proved that there is no ether, Einstein came up with the
theory of special relativity \cite{griffiths2014introduction}. Later, he
generalised special relativity into general relativity by also considering
accelerating inertial frames. General relativity predicts phenomena like the
bending of light around the sun and gravitational waves \cite{Levan2020}. The
big difference is that special relativity and general relativity treat the
3-space dimensions on the same footing as the time dimension.

In the 20th century, another advancement was made in the field of quantum
mechanics. Quantum mechanics is about the microscopic interactions between
particles. A major takeaway is the wave-particle duality: particles can be
considered as waves or particles. Applying quantum mechanics to fields led to
the quantum field theories known as the standard model, which describes the
electromagnetic, strong and weak interaction, as well as matter \cite{Budd2012}.

We know that matter described by the standard model also curves spacetime,
therefore one would like to describe general relativity and quantum mechanics in
a common framework, called quantum gravity (QG) \cite{Klitgaard2022}. There are
however more reasons for wanting quantum gravity. In the singularity of a black
hole, general relativity breaks down \cite{Kiefer2005} which is an example that
the current theoretical knowledge is lacking.

Setting up quantum gravity turns out to be quite hard analytically. One of the
issues is, that gravity is perturbatively non-renormalisable. Perturbatively
non-renormalisable means that there is a maximum energy up to which a
perturbative theory is valid \cite{Klitgaard2022}. So in the ideal case, you
would want to research quantum gravity non-perturbatively. Studying quantum
gravity non-perturbatively can be done using dynamical triangulations for 2D,
and causal dynamical triangulations (CDT) for 4D. Dynamical triangulations (DT)
are a form of random geometry. Dynamical triangulations are originally inspired
by how QCD is studied. QCD can be studied using Monte Carlo simulations after a
lattice regularization which is discretizing the problem onto a finite lattice
\cite{Budd2012}. When studying quantum gravity using DT or CDT, the problem is
not only put on a finite lattice but also the lattice itself becomes variable.

2DQG is the continuum limit of random geometry and more precisely the continuum
limit of discrete random surfaces of the 2-sphere. 2DQG aims to extend Feynman's
path integral to surfaces \cite{Garban2012}. These random surfaces have fractal
properties. Matter can be introduced to 2DQG by coupling it to a certain
critical statistical system. Meanders are such a critical statistical system.
Therefore, meanders can be seen as matter coupled to 2DQG. One of the main
things studied in 2DQG is how the critical exponents of 2DQG react to coupling
to matter. A critical exponent is an observable that describes for example the
dimension of a surface. 

As mentioned earlier the meanders can be described as a statistical system
coupled to 2DQG. In the 90s \citeauthor{DiFrancescoNumCheck2000} conjectured the
critical exponents of the statistical system coupled to 2DQG. This thesis will
be about checking the predicted critical exponents using numerical simulations
of discrete random surfaces decorated with meanders. So far, previous research
has only studied the predictions of the conjecture for small systems, using a
developed flip move for Markov chain Monte Carlo simulations larger systems can
be studied. 

The conjectured critical exponents also make predictions about the asymptotic of
the question asked in the beginning: how many unique loops can you make using
$n$ bridges? This is why it can also be said that meanders are governed by matter
coupled to 2DQG. The numerical simulations in this thesis will therefore also
somewhat contribute to the combinatorial meander problem.

In chapter \ref{section:theoreticalbackground}, the theoretical background for
understanding meanders and 2DQG is described. Also, the conjectures of
\citeauthor{DiFrancescoNumCheck2000} are described there in detail. In chapter
\ref{section:methods}, the methods for studying meanders as a statistical system
coupled to 2DQG are described, like the Markov chain Monte Carlo techniques. The
research question is introduced more formally in chapter
\ref{section:researchquestion}, in chapter \ref{section:results} the results are
presented and lastly in chapter \ref{section:conclusionsandoutlook} the results
are concluded and discussed.


% These toy models learn about how the random surfaces behave. This again allows
% studying how one should perform a path integral over random metrics which is the
% goal of quantum gravity.

% Research into
% 2DQG is useful to learn about quantum gravity. Quantum gravity tries to extend
% Feynman's path integral to surfaces \cite{Garban2012}. For that one must for
% learn about: what is a random surface of the 2-sphere \cite{Garban2012}? Matter
% can be introduced to 2DQG by coupling it to a certain critical statistical
% system.


% 

% \subsection{Quantum gravity is it worth looking for? Why do we want to research this?}

% \begin{itemize}
%     \item Previous research showed that the predictions made about semi-meanders
%     are wrong. There might be more problems.
%     \item We can learn something about 2D quantum gravity
%     \item We can learn something about the phase transition that meanders are
%     conjectured to show.
%     \item We can learn about Quantum gravity.
%     \item We can make a geometry of almost any universality class.
%     \item Two-dimensional quantum gravity is a good system to study the fractal dimension (system to benchmark computationally \cite{Barkley_2019}).
%     \item The random discrete surfaces originate when one tries to solve the
%     path integral of metrics on a surface which can be done by a lattice
%     discretization \cite{Barkley_2019}. And contrary to lattice QCD the lattice
%     itself will be variable instead of a square lattice.
% \end{itemize}



% A possible scenario in non-perturbative quantum gravity is
% that of Weinberg's asymptotic safety, which is a conjecture that gravity would
% have an ultraviolet-fixed point \cite{Klitgaard2022}. 

% 2DQG is the continuum limit of random surfaces of the 2-sphere. Matter can be
% introduced to 2DQG by coupling it to a certain critical statistical system.
% Meanders can be described as such a critical statistical system. Therefore
% meanders can be seen as matter fields coupled to 2D-quantum gravity.

% Firstly, section \ref{section:quantumgravity} addresses a few further details
% about two-dimensional quantum gravity.  Section \ref{section:graphsandplanarmaps}
% introduces graphs, maps and surfaces. 

% Two-dimensional quantum gravity has an analytically
% solved interpretation called Liouville quantum gravity (LQG). 


% Studying a statistical model on a random lattice is studying the model in its
% quantum gravity form \cite{Garban2012}.
