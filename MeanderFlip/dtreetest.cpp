#include <iostream>
//#include "meanderpermutation.hpp"
#include "dtree/type.h"
#include "dtree/tree.h"
#include "dtree/tree-inl.h"
#include "dtree/seq.h"
#include "dtree/seq-inl.h"


//#include "link_cut.hpp"


// typedef dtree::WithAggr<dtree::Min<int>,
//                              dtree::WithValue<dtree::Add<int>,
//                                               dtree::Begin<> > > C;

// dtree::Count<int>



typedef dtree::WithAggr<dtree::Count<int>,dtree::Begin<>> C;

typedef dtree::WithValue<dtree::Nop<int>, C> V;

typedef dtree::WithAggr<dtree::Min<int>, V> M;

typedef dtree::WithReverse<M> R;




typedef dtree::EndSeq<R> dNode;

void print_seq(dNode * n) {
    dNode * node = dtree::Root(n);

    while (node != 0)
    {
        std::cout << V::Value(node) << " -> ";
        node = dtree::Leftward(node);
    }
    std::cout << "\n";
}

int main() {
    std::cout << "Hallo\n";
    
    for (int i = 0; i < 100; i++) {
        
        dNode * seq = new dNode[8];

        std::cout << "sameseq: "<< dtree::SameSeq(&seq[0], &seq[1]) << "\n";

        std::cout << "huh " << C::AggrSeq(&seq[0]) << "\n";
        
        for (size_t i = 0; i < 8; i++)
        {
            V::SetValue(&seq[i], i+1);
        }
        
        dNode * tracker;
        tracker = dtree::LinkRightOf(&seq[0], &seq[1]);
        print_seq(&seq[0]);
        tracker = dtree::LinkRightOf(&seq[1], &seq[2]);
        R::ReverseSeq(&seq[0]);
        print_seq(&seq[0]);
        tracker = dtree::LinkRightOf(&seq[2], &seq[3]);
        R::ReverseSeq(&seq[0]);
        print_seq(&seq[0]);
        tracker = dtree::LinkRightOf(&seq[3], &seq[4]);
        print_seq(&seq[0]);
        tracker = dtree::LinkRightOf(&seq[4], &seq[5]);
        R::ReverseSeq(&seq[0]);
        print_seq(&seq[0]);
        
        R::ReverseSeq(&seq[0]);

        std::cout << "Root node?? " << V::Value(tracker) << "\n";

        tracker = &seq[5];

        std::cout << "dtree::Root: " << dtree::Root(tracker) << "\n";
        std::cout << "dtree::Left: " << dtree::Left(tracker) << "\n";
        std::cout << "dtree::Right: " << dtree::Right(tracker) << "\n";

        std::cout << "sameseq: "<< dtree::SameSeq(&seq[0], &seq[1]) << "\n";
        
        R::ReverseSeq(&seq[0]);
        
        std::cout << "huh " << C::AggrSeq(&seq[0]) << "\n";
        std::cout << "huh " << C::AggrSeq(&seq[1]) << "\n";

        std::cout << "huh " << C::AggrSeq(&seq[4]) << "\n";
        
        R::ReverseSeq(&seq[0]);

        std::cout << "left" << dtree::Dirward(dtree::kRight, &seq[5]) << "\n";
        //std::cout << "left" << dtree::Leftward(&seq[3]) << "\n";
        std::cout << "left" << dtree::Root(&seq[2]) << "\n";
        std::cout << "left22" << &seq[2] << "\n";
        std::cout << "left22" << &seq[5] << "\n";

        std::cout << "left" << dtree::Root(&seq[1]) << "\n";
        std::cout << "left22" << &seq[0] << "\n";
        std::cout << "left22" << &seq[1] << "\n";

        dNode * node = dtree::Root(&seq[0]);

        while (node != 0)
        {
            std::cout << "node " << V::Value(node) << "\n";
            std::cout << "min " << M::AggrSeq(node) << "\n";
            node = dtree::Leftward(node);
        }

        std::cout << "min " << M::AggrSeq(&seq[7]) << "\n";

        std::cout << "before_rootnode " << V::Value(dtree::Root(&seq[0])) << "\n";
        std::cout << "Left " << V::Value(dtree::Left(&seq[2])) << "\n";
        std::cout << "Right " << V::Value(dtree::Right(&seq[2])) << "\n";

        R::ReverseSeq(&seq[0]);

        std::cout << "after_rootnode " << V::Value(dtree::Root(&seq[0])) << "\n";

        std::cout << "Left " << V::Value(dtree::Left(&seq[2])) << "\n";
        std::cout << "Right " << V::Value(dtree::Right(&seq[2])) << "\n";

        

        node = dtree::Root(&seq[0]);

        while (node != 0)
        {
            std::cout << "node " << V::Value(node) << "\n";
            node = dtree::Leftward(node);
        }
        

        //std::cout << "huh " << C::AggrAnc(&seq[0]) << "\n";

        // LinkCut seq = LinkCut(5);

        // seq.link(0,1);
        // seq.link(1,2);
        
        // //seq.link(3,4);
        // seq.link(2,3);

        // //treeprint(&seq.x[3], 2);
        // int root = seq.root(3);
        // int root2 = seq.root(2);
        // int root1 = seq.root(1);
        // //int deep = seq.deepest_depth(3);


        // // std::cout << "fact: " << seq.x[4].sz << "\n";
        // // std::cout << "seq: " << seq.root(1) << "\n";
        
        // // std::cout << "seq4: " << seq.root(4) << "\n";
        // std::cout << "len: " << seq.depth(seq.root(1)) << "\n";
        // // std::cout << "len: " << seq.deepest_depth(2) << "\n";


    }
    
    
    
}
