//
//  meanderplotter.cpp
//  MeanderFlip
//
//  Created by Rutger Berns on 18/12/2021.
//
#include "meanderplotter.hpp"

void plot(const std::vector<Edge> & edges, int N, std::string filename, std::map<int, std::string> * highlight_edges) {

    int base = 100;
    int double_base = 2*base;
    
    cairo_surface_t *surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, double_base*N, double_base*(N+1));
    //cairo_surface_t *surface = cairo_svg_surface_create (filename.c_str(), double_base*N, double_base*(N+1));
    cairo_t *cr = cairo_create (surface);

    // cairo_select_font_face (cr, "serif", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    // cairo_set_font_size (cr, 32.0);
    // cairo_set_source_rgb (cr, 0.0, 0.0, 1.0);
    // cairo_move_to (cr, 10.0, 50.0);
    // cairo_show_text (cr, "Hello, world");

    for (size_t i = 0; i < N; i++)
    {
        cairo_move_to (cr, i*double_base, N*base);
        cairo_rectangle (cr, i*double_base, N*base, double_base, double_base);
        cairo_set_source_rgb (cr, 52.0/255.0, 143.0/255.0, 167.0/255.0);
        cairo_set_line_width (cr, 10);
        cairo_stroke (cr);
    }

    // cairo_rel_curve_to (cr, -0.25, -0.base, -0.25, 0.base, -0.5, 0);
    
    std::vector<bool> visited_edges(edges.size(), false);
    
    std::vector<std::vector<int> > paths;

    Edge current_edge;
    
    int curr_face = 1;

    // if (highlight_edges)
    // {
    //     for (int i = 0, endi = highlight_edges->size(); i < endi; ++i) {
    //         current_edge = edges[i];
    //         if (current_edge.id % 4 == 2)
    //         {
                
    //         } else if (current_edge.id % 4 == 0) {

    //         }
    //     }
    // }
    

    for (int i = 0, endi = edges.size(); i < endi; ++i) {
        current_edge = edges[i];
        Edge adjacent_edge = adjacent(current_edge, edges);
        int length = abs(current_edge.face_id - adjacent_edge.face_id);
        if (current_edge.id % 4 == 2)
        {
            // lower half
            cairo_new_path(cr);
            //cairo_move_to (cr, ((current_edge.face_id-1)*base + (adjacent_edge.face_id-1)*base)+base, N*base+double_base);
            cairo_arc(cr, ((current_edge.face_id-1)*base + (adjacent_edge.face_id-1)*base)+base, N*base+double_base, length*base, 0, M_PI);
        } else if (current_edge.id % 4 == 0) {
            // upper half
            cairo_new_path(cr);
            //cairo_move_to (cr, ((current_edge.face_id-1)*base + (adjacent_edge.face_id-1)*base)+base, N*base);
            cairo_arc(cr, ((current_edge.face_id-1)*base + (adjacent_edge.face_id-1)*base)+base, N*base, length*base, M_PI, 2*M_PI);
        }
        cairo_set_source_rgb (cr, 65.0/255.0, 61.0/255.0, 123.0/255.0);
        cairo_set_line_width (cr, 10);
        cairo_stroke (cr);

        cairo_select_font_face (cr, "sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
        cairo_set_font_size (cr, 32.0);
        cairo_set_source_rgb (cr, 0.0, 0.0, 1.0);
        if (current_edge.id % 4 == 2)
        {
            cairo_move_to (cr, (current_edge.face_id-1) * double_base + base, N*base+double_base-40);
        } else if (current_edge.id % 4 == 0) {
            cairo_move_to (cr, (current_edge.face_id-1) * double_base + base, N*base+40);
        }
        auto text = std::to_string(current_edge.id);
        if (highlight_edges && highlight_edges->count(current_edge.id) == 1)
        {
            text += highlight_edges->at(current_edge.id);
        }
        
        cairo_show_text (cr, text.c_str());
        
    }   

    cairo_destroy (cr);
    cairo_surface_write_to_png (surface, filename.c_str());
    cairo_surface_destroy (surface);
}