q = 2.0
n = 128

# size_data = {1.0: [256, 512, 1024, 2048],
#              2.0: [256, 512, 1024], # , 4096, 8192
#              4.0: []}

#@jit(nopython=True)
def sample_autocovariance(x, tmax, steps, rate):
    '''Compute the autocorrelation of the time series x for t = 0,1,...,tmax-1.'''
    x_shifted = x - np.mean(x)
    t = np.linspace(0, tmax/rate, num=steps, dtype=int)
    return t*rate, np.array([np.dot(x_shifted[:len(x)-t_], x_shifted[t_:])/len(x) for t_ in t])

#@jit(nopython=True)
def find_correlation_time(t, autocov):
    '''Return the index of the first entry that is smaller than autocov[0]/e.'''
    return t[autocov < np.exp(-1)*autocov[0]][0]

size_data = {1.0: [256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072],
             2.0: [128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536]}



#fig, axs = plt.subplots(1)

#axs = axs.flat

n_list = []
q_list = []
corr_list = []
#corrfit_list = []


def corrfit(x, t):
    return np.exp(-x/t)

for q in size_data.keys():
    for n in size_data[q]:
        corrtimes = []
        corrtimesfit = []

        for data in SimDataIterator(q=q, n=n, name="compcorrelation"):

            continuous_data = data["measurements"][0]["data"]#[slice:]
            #print("n:", n, "continuous_data (length)", len(continuous_data))
            
            #start_pos = 200000
            #continuous_data = continuous_data[start_pos:1]
            #print(continuous_data)
            rate = data["measurements"][0]["rate"]
            tmax = sweeps(1.5, n=n)
            t, autocov = sample_autocovariance(continuous_data, tmax, steps=100, rate=rate)
            corrtime = find_correlation_time(t, autocov)

            n_list.append(n)
            q_list.append(q)
#
            corr_list.append(corrtime)
            
    

      

d = {'n': n_list, 'q': q_list, 'corr': corr_list}
df = pd.DataFrame(data=d)

outputdf = df.groupby(['q', 'n']).agg(['mean', 'sem', 'count'])["corr"]#.loc[(1.0, 256)]["mean"]

outputdf.to_pickle('simdata/compcorrelation/report.pkl')

outputdf