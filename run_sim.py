import os
import sys
from datetime import datetime


n = int(sys.argv[1]) # powers of two
i = int(sys.argv[2]) # powers of i

date = datetime.now()

os.system("./build/meander_main -n {} -i {} -f 'simdata/q1/n{}/{}{:02g}{:02g}-{:02g}{:02g}{:02g}.json' ".format(2**n, 2**i, 2**n, date.year, date.month, date.day, date.hour, date.minute, date.second))
