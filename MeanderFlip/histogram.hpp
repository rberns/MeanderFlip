//
//  Histogram.hpp
//  Histogram
//
//  Created by Rutger Berns on 12/02/2021.
//

#ifndef Histogram_hpp
#define Histogram_hpp

#include <iostream>
#include <vector>
#include <tuple>

struct Histogram
{
    // susceptibility data
    std::vector<uint64_t> hist;
    std::vector<uint64_t> hist_squared;

    size_t size;

    uint64_t N;

    Histogram(size_t size);
    ~Histogram();

    /// initialize from list of numbers
    Histogram(std::vector<uint64_t> list, size_t size);
    /// initialize from histogram
    Histogram(std::vector<uint64_t> list);
    std::vector<std::tuple<uint64_t, uint64_t, uint64_t> > to_table();

    void resize(size_t new_size);

    Histogram &operator +=(Histogram const &other) {
        if (size < other.size) {
			hist.resize(other.size, 0);
            hist_squared.resize(other.size, 0);
            size = other.size;
		}
		for (size_t i = 0; i < other.size; ++i) {
			hist[i] += other.hist[i];
            hist_squared[i] += other.hist_squared[i];
		}
        N += other.N;
        return *this;
    }

    uint64_t sum();
};




#endif