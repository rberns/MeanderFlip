#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=1000M
#SBATCH --time=6-23:59:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 12 -w cn113

cd ~/MeanderFlip/
python3 MeanderFlipRunnerDistance2.py -q 1 1 1 -s 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 -p 12 -c 8 -f 16 -o distancemeasure
