from default_analysis_funcs import *
import matplotlib.pyplot as plt
import sys
import os
import pickle

sims, max_iter = pickle.load(open('latestsim.pkl', 'rb'))
# plot_deviation("data_N22q1s1000000gen.json")
# plot_deviation("data_N22q1s100000gen.json")
# plot_deviation("data_N24q1s1000000gen.json")
# plot_deviation("data_N24q1s100000gen.json")

id_ = sys.argv[1]

flat_print = False

if len(sys.argv) > 2 and sys.argv[2] == '-d':
    flat_print = True

for sim in sims:
    sim += id_ + ".json"
    print("sim at iter: {}".format(max_iter))
    plot_deviation(sim, flat_print)

plt.show()
# plt.show()