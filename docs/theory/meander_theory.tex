% !TEX root = ../thesis.tex


\section{Meanders}
\label{section:meanders}

Meanders can be regarded as a road and a river that cross each other at $2n$
points (bridges) or more generally speaking: two crossing self-avoiding loops.
Meanders have several applications like the combinatorics of folding polymer
chains \cite{DiFrancescoExactAsym2000}.

For the number of meander configurations no exact formula has been found so far.
However, in the 90s Di Francesco et al. conjectured that meanders are governed
by the gravitational version of a two-dimensional conformal field theory with a
central charge of -4. The critical exponents that can be extracted from this
conjecture, allows to approximate the number of meander configurations at large
system size $n$.

\subsection{Meanders as a combinatorial problem}
A meander is a closed self-avoiding road that intersects an infinite line
(river) $2n$ times \cite{DiFrancescoExactAsym2000}. 
\begin{figure}[htb!]
    \centering
\begin{tikzpicture}
    \draw (0,0) [thick]-- (4,0);
    %\draw[dotted] (0,-2) grid (4,2);
    \node (node1) at (0.5,0) {};
    \node (node2) at (1.5,0) {};
    \node (node3) at (2.5,0) {};
    \node (node4) at (3.5,0) {};

    \draw[meanderroad] (0.5,0) arc (180:0:1.5);
    \draw[meanderroad] (1.5,0) arc (180:0:0.5);
    \draw[meanderroad] (0.5,0) arc (-180:0:0.5);
    \draw[meanderroad] (2.5,0) arc (-180:0:0.5);
    \draw[bridgenode] (node1) circle (0.05);
    \draw[bridgenode] (node2) circle (0.05);
    \draw[bridgenode] (node3) circle (0.05);
    \draw[bridgenode] (node4) circle (0.05);
\end{tikzpicture}
\caption{Example of a meander of order 2, which has 4 bridges.}
\label{theory:pic:meanderorder2}
\end{figure}

$M_n$ denotes the number of unique meanders (inequivalent meanders) with a fixed
number of bridges. Next to meanders there are meander systems that allow for
more than one closed-road. A meander system is defined by having one infinite
river and multiple self-avoiding closed roads. $M_n^k$ denotes the number of
inequivalent meander systems of order $n$ with $k$ non-intersecting closed
loops. Meanders and meander systems are considered inequivalent when there is no
smooth way of deforming them into each other without changing the order of the
bridges \cite{DiFrancescoMeanderFoldingArchStat1997}.

Meander systems are easier to work with than pure meanders, since some of their
properties can be calculated analytically using combinatorics. Later, some of
these analytic properties will be used to check simulation results. Meanders
with just one road is such a specific case, that the name meander will often be
used for the more general meander systems. 


\begin{figure}[htb!]
    \centering

    \begin{tikzpicture}
        \draw (0,0) [thick]-- (4,0);
        %\draw[dotted] (0,-2) grid (4,2);
        \node (node1) at (0.5,0) {};
        \node (node2) at (1.5,0) {};
        \node (node3) at (2.5,0) {};
        \node (node4) at (3.5,0) {};
    
        \draw[meanderroad] (0.5,0) arc (180:0:1.5);
        \draw[meanderroad] (0.5,0) arc (-180:0:1.5);
        \draw[meanderroad] (1.5,0) arc (180:0:0.5);
        \draw[meanderroad] (1.5,0) arc (-180:0:0.5);
        
        \draw[bridgenode] (node1) circle (0.05);
        \draw[bridgenode] (node2) circle (0.05);
        \draw[bridgenode] (node3) circle (0.05);
        \draw[bridgenode] (node4) circle (0.05);
    
        \draw (4.5,0) [thick]-- (8.5,0);
        %\draw[dotted] (0,-2) grid (4,2);
        \node (node1b) at (5.0,0) {};
        \node (node2b) at (6.0,0) {};
        \node (node3b) at (7.0,0) {};
        \node (node4b) at (8.0,0) {};
    
        \draw[meanderroad] (node1b) arc (180:0:0.5);
        \draw[meanderroad] (node1b) arc (-180:0:0.5);
    
        \draw[meanderroad] (node3b) arc (180:0:0.5);
        \draw[meanderroad] (node3b) arc (-180:0:0.5);
        %\draw[color=blue] (0.5,0) arc (-180:0:0.5);
        %\draw[color=blue] (2.5,0) arc (-180:0:0.5);
        \draw[bridgenode] (node1b) circle (0.05);
        \draw[bridgenode] (node2b) circle (0.05);
        \draw[bridgenode] (node3b) circle (0.05);
        \draw[bridgenode] (node4b) circle (0.05);
    
    \end{tikzpicture}
    
    
\caption{Meander that is symmetric with respect to the river (horizontal line). One can calculate $M^{(2)}_2 = c_2 = 2$ by Equation \eqref{theory:meanders:Mnn}.}
\label{theory:pic:symmetricmeander}
\end{figure}

Some meander systems can be seen as arches that can be reflected in the river, see
Figure \ref{theory:pic:symmetricmeander}. For these types of configurations the
number of meander systems is given by 

\begin{equation}
    M^{(n)}_n = c_n = \frac{1}{n+1}\binom{2n}{n}
    \label{theory:meanders:Mnn}
\end{equation}
where $c_n$ is $n^\text{th}$ Catalan number \cite{DiFrancescoMeanderFoldingArchStat1997}.

The total number of inequivalent meander systems can also be calculated taking the
product of two arch configuration of size $n$:

\begin{equation}
    \sum_{k=1}^n {M^{(k)}_n} = (c_n)^2
    \label{theory:meanders:Mnsum}
\end{equation}

since the number of arch configuration is counted by the Catalan numbers.



% \subsubsection{Hausdorff dimension}




% \subsection{Connection to Quantum gravity}
% \subsubsection{Fully packed loop models}


% \begin{tikzpicture}
%     \draw (0,0) -- (4,0);
%     \draw[dotted] (0,-2) grid (4,2);
%     \draw[color=blue] (0.5,0) arc (180:0:1.5);
%     \draw (1.5,0) arc (180:0:0.5);
%     \draw (0.5,0) arc (-180:0:0.5);
%     \draw (2.5,0) arc (-180:0:0.5);
%     \draw (2,1) circle (0.1);
% \end{tikzpicture}
    


% % Meanders are a quite simple model. It's described by a road passing a river. If the road passes the river $2n$ times we speak of meander of order $n$.

% % It is a self-avoiding loop.

% % The common questions for meanders is finding how many configurations can you make for a certain amount of bridges. This is relevant for example for polymer chain folding of long molecules [1]. Another example would be the number of ways to fold a piece of paper [1].



% % Finding the exact amount of configurations is the challenging part. So far no exact formula for the amount of configurations has been found [2]. 

% % One approach one can take is enumerating all possible configurations. The total amount of configurations becomes very huge very quick and therefore this has only been done for up to $n = 30$ [3,4]. In other words for up to 60 bridges.

% % 60 bridges is not a lot.

% % So how would you get passed this problem?

% % The first step was to put it into a loop model.

% % meander numbers scale asymptotically as Mn ∼ CR2n/nα,where R is a connectivity constant and α a configuration exponent.

% % First of all you need a m
% % odel.

% % You give every closed segment (loop) a weight for example $n_1$ [2]
% % 2D quantum gravity
% % Why would you want to study 2D quantum gravity? 2D quantum gravity can be seen as a laboratory, where conceptual questions about quantum gravity can be studied [5].
% % \cite{DiFrancesco1996}

