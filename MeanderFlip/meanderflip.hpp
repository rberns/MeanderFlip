//
//  meanderflip.hpp
//  MeanderFlip
//
//  Created by Rutger Berns on 17/10/2021.
//

#ifndef meanderflip_hpp
#define meanderflip_hpp

#include "planarmap.hpp"
#include "helpers.hpp"
#include "meanderpermutation.hpp"
#include "puremeanderflip.hpp"
#include "meander.hpp"

struct FlipInfo {
    Edge first;
    Edge cw;
    Edge afirst;
    Edge acw;
    int v_initial;
    int v_final;
    int k_initial;
    int k_final;
    int n_initial_to_suggestion;
    int n_suggestion_to_initial;
    int n_neighbors_initial_edge;
    int n_neighbors_suggestion_edge;
    bool accepted = false;
    double acceptance_probability;
};

std::vector<Edge> flip(std::vector<Edge> & edges, Edge first_edge, Edge second_edge);
void flip_permutation(std::vector<Edge> & edges, Edge first_edge, Edge second_edge);
//bool flipv(std::vector<Edge> & edges, MeanderPermutation *permutation, double q, int * N_ends, int N, FlipInfo * info = 0, void (*preflipcontrol)(FlipInfo*) = 0);

#endif /* meanderflip_hpp */