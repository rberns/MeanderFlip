#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=100M
#SBATCH --time=2:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=rutger.berns@ru.nl
#SBATCH -N 1 -n 2 -w cn113

cd ~/MeanderFlip/
python3 MeanderFlipRunnerAutoCorr2.py -q 2 -s 256 -d 64 -p 1 -f 1000 -o thermalizationtime