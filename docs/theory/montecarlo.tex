% !TEX root = ../thesis.tex
\label{section:theory:montecarlo}

In order to study the conjectures by \citeauthor{DiFrancescoExactAsym2000} about
meanders, geometries decorated with meanders will be generated using Markov
Chain Monte Carlo Techniques. Markov chain Monte Carlo was introduced by
Nicholas Metropolis et al. in 1953 \cite{Berg2005}. The main goal of
Markov chain Monte Carlo techniques is sampling a certain probability
distribution $\pi(\mathbf{x})$. 

In Monte Carlo techniques there are actually two main directions, direct
sampling and Markov chain sampling. The latter will be used here. With the
former, direct sampling, one can for example calculate the area of a circle by
generating random coordinates in a square and checking whether the distance to
the origin is smaller than 1 \cite{Krauth2006}. An example is given in Figure
\ref{results:plot:montecarlodirectsampling} where using a $1000$ samples $\pi$
is approximated to be $3.16$.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{theory/assets/montecarlodirect.pdf}
    \caption{Direct Monte Carlo sampling with $1000$ samples estimates $\pi \approx 3.16$.}
    \label{results:plot:montecarlodirectsampling}
\end{figure}

Markov chain sampling will be used here because it doesn't sample all
configurations or states but only the states most relevant which makes it more
efficient than direct sampling \cite{Newman1999}. Markov chain Monte Carlo is
based on a Markov chain. The Markov chain can be represented by $X_1$, $X_2$,
$X_3$, $\dots \in \Gamma$, $\Gamma$ is a state space and $X_i$ can be seen as a
state in the Markov chain. The defining property of the Markov chain is the
Markov property which states that the probability of the next configuration,
$X_{i + 1}$, only depends on the preceding configuration, $X_i$
\cite{Krauth2006, BuddCourse2021}. A nice way of saying this: "The future
depends on the past only through the present" by \citeauthor{Joseph2019}
\cite{Joseph2019}.

For a Markov chain to have the desired stationary distribution $\pi
(\mathbf{x})$ there are two requirements: ergodicity and detailed balance.
\textbf{Ergodicity} says that all possible configurations have to be reachable
in the Markov chain if the Markov chain is run for sufficiently long
\cite{Newman1999}. \textbf{Detailed balance} says that the probability current
between two states $\nu$ and $\mu$ should be equal. The detailed balance
condition is given by

\begin{equation}
    \pi (\nu) P(\nu \rightarrow \mu) = \pi (\mu) P(\mu \rightarrow \nu)
    \label{theory:montecarlo:detailedbalance}
\end{equation}
where $P$ is the transition probability. Using the transition probability, one
can define a \textbf{stationary distribution} as the distribution that satisfies

\begin{equation}
    \pi = \mathbf{P} \cdot \pi 
\end{equation}

where $\mathbf{P}$ is the Markov matrix or transition matrix that encodes the
transition probabilities between the different states \cite{Joseph2019}. In
other words, the distribution $\pi(\textbf{x})$ is in equilibrium when it is an
eigenstate of the Markov matrix.

The goal is to use these Markov chains to calculate useful properties. For that
one can define an observable $f: \Gamma \rightarrow \mathbb{R}$. So the function
$f$ translates the sample $X_i$ into a real number. 


\subsubsection{Metropolis-Hastings algorithm}

The Markov chain converges to the correct distribution $\pi (\mathbf{x})$ when
detailed balance and ergodicity are satisfied. One needs to find an algorithm to
generate a new configuration $X_{i+1}$ out of $X_i$ that satisfies detailed
balance and ergodicity. The Metropolis-Hastings algorithm is suitable for this.
The Metropolis-Hastings algorithm consists of two steps: proposing and
accepting, visualised in Figure \ref{fig:theory:metropolis}. First the
"proposer" generates a new configuration $\nu$ from the original configuration
$\mu$ with probability $g(\mu \rightarrow \nu)$. The new configuration $\nu$ is
then rejected or accepted based on a certain acceptance probability $A(\mu
\rightarrow \nu)$. So $g(\mu \rightarrow \nu)$ quantifies the preference or bias
that the algorithm has to generate a configuration $\nu$ from $\mu$. The
acceptance probability $A(\mu \rightarrow \nu)$ fixes the probabilities in order
to satisfy detailed balance. Also, one should make sure that the "proposer"
satisfies ergodicity.

\begin{figure}[htb!]
    \centering
    \begin{tikzpicture}[node distance = 3cm]
    
    \node (generator) [lblock, align=center, fill=rbprimarycolor!30] {Proposer \\ $g(\mu \rightarrow \nu)$};
    
    \node (acceptor) [lblock, align=center, right of=generator, xshift=1cm, fill=rbprimarycolor!30] {Acceptor \\ $A(\mu \rightarrow \nu)$};
    
    \begin{scope}[on background layer]
    \node (test) [rectangle, minimum width=8cm, minimum height=3cm, align=center, draw=black, fill=rbrblueb!50, shift=($(acceptor)!0.5!(generator)$)] {};
    \end{scope}
    
    \draw [arrow] (generator) -- node[anchor=south] {$\nu$} ([xshift=-1cm]acceptor);
    
    \draw [arrow] (acceptor) -- node[anchor=south, xshift=0.3cm] {$\nu$} ([xshift=1cm]acceptor.east);
    \draw [arrow] ([xshift=-1cm]generator.west) -- node[anchor=south, xshift=-0.3cm] {$\mu$} (generator);
    
    \end{tikzpicture}
    \caption{Algorithm}
    \label{fig:theory:metropolis}
    \end{figure}

But what should one choose as the acceptance probability $A(\mu \rightarrow
\nu)$? The first step is to split the transition probability $P(\mu \rightarrow
\nu)$ (from Equation \eqref{theory:montecarlo:detailedbalance}) up into $P(\mu
\rightarrow \nu) = g(\mu \rightarrow \nu) A(\mu \rightarrow \nu)$. Now one can
calculate the ratio of the transition probabilities as

\begin{equation}
    \label{MCMC:eq:detailedbalanceandacceptance}
    \frac{P(\mu \rightarrow \nu)}{P(\nu \rightarrow \mu)} = \frac{g(\mu \rightarrow \nu)A(\mu \rightarrow \nu)}{g(\nu \rightarrow \mu)A(\nu \rightarrow \mu)}, \frac{\pi(\nu)}{\pi(\mu)} = \frac{g(\mu \rightarrow \nu)A(\mu \rightarrow \nu)}{g(\nu \rightarrow \mu)A(\nu \rightarrow \mu)}
\end{equation}
where the detailed balance condition (Equation
\eqref{theory:montecarlo:detailedbalance}) is used to go from the transition
probability to the ratio of the probability distribution. Rewriting Equation \eqref{MCMC:eq:detailedbalanceandacceptance} gives
\begin{equation}
    \frac{A(\mu \rightarrow \nu)}{A(\nu \rightarrow \mu)} = \frac{\pi(\nu)g(\nu \rightarrow \mu)}{\pi(\mu)g(\mu \rightarrow \nu)}
    \label{eq:monte:metro:final}
\end{equation}

To satisfy Equation \eqref{eq:monte:metro:final} the acceptance probability can be chosen as
\begin{equation}
    \label{appmcmc:eq:acceptanceprobabilityfirst}
    A(\mu \rightarrow \nu) = \text{min}\left(1, \frac{\pi (\nu) g(\nu \rightarrow \mu)}{\pi (\mu) g(\mu \rightarrow \nu)}\right)
\end{equation}
which makes sure detailed balance is satisfied. 




\newpage

\subsection{The flip algorithm}
\label{section:theflipalgorithm}

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{.40\linewidth}
    \includegraphics[page=2, width=\linewidth]{theory/assets/demonstrationofflip.pdf}
    \caption{$n = 3$}\label{theory:montecarlo:fig:firstsetup}
    \end{subfigure}
    \begin{subfigure}[b]{.40\linewidth}
    \includegraphics[page=3, width=\linewidth]{theory/assets/demonstrationofflip.pdf}
    \caption{$n = 3$}\label{theory:montecarlo:fig:resultsetup}
    \end{subfigure}
    
    \begin{subfigure}[b]{.40\linewidth}
    \includegraphics[page=4, width=\linewidth]{theory/assets/demonstrationofflip.pdf}
    \caption{$n = 3$}\label{theory:montecarlo:fig:firstglued}
    \end{subfigure}
    \begin{subfigure}[b]{.40\linewidth}
    \includegraphics[page=1, width=\linewidth]{theory/assets/demonstrationofflip.pdf}
    \caption{$n = 3$}\label{theory:montecarlo:fig:resultglued}
    \end{subfigure}
    
    
    \caption{Sketch of the algorithm to propose new meander configurations. (c),
    (d) is the map that one gets when (a), (b) are embedded on a flat plane. The
    algorithm transforms (a) into (b) and (c) into (d). The
    \textcolor{rbrblued}{blue dashed line} represents the road and how the
    quadrangles should be glued together. The river flows perpendicular to the
    \textcolor{rblinkcolor}{blue} edges. The \textcolor{rbprimarycolor}{red}
    outlined lined vertex indicates the relevant vertex for the flip and $v$ is
    the number of shore edges connected a red-lined node. The
    \textcolor{rbprimarycolor}{red} and \textcolor{rbdarkgray}{dark grey} are
    the shore edges. The shore edges are perpendicular to the road (they can
    also be seen as the 'bridge pillars'). In (a), }
    
    
    
    \label{theory:montecarlo:flipdemonstration}
    
    
    
    \end{figure}

Here the algorithm to propose a new quadrangulation $X_{i+1}$, decorated with a
meander system, from the original configuration $X_i$ will be introduced. In
Figure \ref{theory:montecarlo:fig:firstsetup}, the flip move can be regarded as
cutting the two arcs at the locations indicated with the
\textcolor{rbprimarycolor}{red} crosses. Then in Figure
\ref{theory:montecarlo:fig:resultsetup}, the \textcolor{rbprimarycolor}{red}
shore edges are glued back together but switched. The computer understands the
quadrangulations as a combinatorial object described by cycles of the next and
adjacent edges. For finding the exact proposal algorithm, one should look at
Figures \ref{theory:montecarlo:fig:firstglued} and
\ref{theory:montecarlo:fig:resultglued}. The interpretation of the move in
Figures \ref{theory:montecarlo:fig:firstglued} and
\ref{theory:montecarlo:fig:resultglued} is described in the following.

In Figure \ref{theory:montecarlo:fig:firstglued}, a shore half-edge $i$ is
selected by randomly sampling one of all the shore half-edges. Shore edges are
indicated by the \textcolor{rbprimarycolor}{red} and \textcolor{rbdarkgray}{dark
grey} lines in Figure \ref{theory:montecarlo:flipdemonstration}. The shore edges
are perpendicular to the roads and roads are indicated by the
\textcolor{rbrblued}{blue} dashed line. There are a total of $4n$ shore
half-edges and therefore the shore half-edge with index $i$ is selected with a
probability $\frac{1}{4n}$. In Figure \ref{theory:montecarlo:fig:firstglued},
the dotted \textcolor{rbprimarycolor}{red} half-edge pointing to the
\textcolor{rbprimarycolor}{red} outlined vertex is the initially selected shore
half-edge. From the other half-edges pointing to the
\textcolor{rbprimarycolor}{red} outlined vertex, a new shore edge $j$ is
randomly selected. In Figure \ref{theory:montecarlo:fig:firstglued}, the
non-dotted \textcolor{rbprimarycolor}{red} half-edge is the newly selected shore
edge. There are $v-1$ \textbf{other} shore half-edges pointing to the vertex and
thus the probability for picking a shore half-edge $j$ is $\frac{1}{v-1}$. The
amount of shore half-edges pointing to the  \textcolor{rbprimarycolor}{red}
outlined vertex in Figure \ref{theory:montecarlo:fig:firstglued} is equal to $v
= 3$.

The procedure to generate configuration $\nu$ from $\mu$ is done by un-glueing
the initially selected shore half-edge and the new selected shore half-edge from
their adjacent edges and then re-glueing to the adjacent edges switched. In
Figure \ref{theory:montecarlo:fig:resultglued}, the resulting new configuration
$\nu$ is drawn. The un-glueing and re-glueing is demonstrated in Figure
\ref{theory:plot:diamond} where $i$ is the initially select half-shore edge,
$a(i)$ is the adjacent half-edge of $i$, $j$ is the new half-shore edge
and $a(j)$ the adjacent half-edge of $j$. The glueing changes from $i
\leftrightarrow a(i)$, $j \leftrightarrow a(j)$ to $i \leftrightarrow a(j)$, $j
\leftrightarrow a(i)$, where $a(i)$ and $a(j)$ refer to the old glueing. 

\begin{figure}[htb!]
    \centering
    \includegraphics[]{theory/assets/flipdiamond.pdf}
    \caption{Detailed drawing what happens around the flip vertex indicated by
    the \textcolor{rbprimarycolor}{red} outlined vertex. The drawing is a part
    of Figure \ref{theory:montecarlo:fig:firstglued} and Figure
    \ref{theory:montecarlo:fig:resultglued} with the same meaning to colors and
    dotted edges. On the left is the initial state $\mu$, in the middle the
    un-glued state and on the right the re-glued new state $\nu$.}
    \label{theory:plot:diamond}
\end{figure}

The probability for selecting a certain new configuration $\nu$ is $g(\mu
\rightarrow \nu) = \frac{1}{4n} \frac{1}{v_\mu-1}$ where $v$ is the amount of
shore half-edges pointing to the \textcolor{rbprimarycolor}{red} outlined
vertex. The probability for going from state $\nu$ to state $\mu$ is similarly
given by $g(\nu \rightarrow \mu) = \frac{1}{4n}\frac{1}{v_\nu-1}$. 

With the introduced information about the proposing algorithm, the acceptance
probability can be derived. The Metropolis-Hastings acceptance probability from
equation \eqref{appmcmc:eq:acceptanceprobabilityfirst} can be used to sample
based on the probability distribution $\pi(x)$ \cite{BuddCourse2021}. The
partition function for the meander systems from section
\ref{theory:fullypackedloopmodel} is given by

\begin{equation}
    Z = \sum_{configurations} q^k
\end{equation}
and the probability distribution is then given by

\begin{equation}
    \label{meanders:eq:probabilitydistribution}
    \pi(\textbf{x}) = \frac{1}{Z} q^{k(\textbf{x})}
\end{equation}
where $k$ is the number of components and $q$ is the weight of the roads.
Substituting $g$ and $\pi$ into Equation
\eqref{appmcmc:eq:acceptanceprobabilityfirst} gives

\begin{equation}
    \label{meanders:eq:acceptanceprobability}
    A(\mu \rightarrow \nu) = \text{min}\left(1, q^{k_\nu-k_\mu} \cdot \frac{v_\mu-1}{v_\nu-1}\right)
\end{equation}
which is the acceptance probability to accept or reject a proposed
configuration. 

The algorithm proposed in this section allows generating Markov
chains of these meander configurations where $q$, the weight assigned to the
roads, is variable.