#include "meanderpermutation.hpp"

void MeanderPermutation::reverse(Node * node) {
    remove_root(node);
    R::ReverseSeq(node);
    add_root(node);
}

MeanderPermutation::MeanderPermutation(int n_bridges): connections_(NULL)
{
    this->n_bridges = n_bridges;
    //bridges.reserve(n_bridges);
    Node* old_node = connections_;
  // avoids a double delete[] if new[] throws
    connections_ = NULL;
    delete[] old_node;

    this->connections_ = new Node[n_bridges*2];
    for (int i = 0; i < n_bridges; i++)
    {
        this->translator[&connections_[i]] = i+1;
        add_node(i+1);

        assert(i+1 == id(&connections_[i]));
    }   
}

MeanderPermutation::~MeanderPermutation()
{
    delete[] this->connections_;
}

Node * MeanderPermutation::bridge_node(int bridge) {
    //std::cout << "bridge id: " << bridge-1 << "\n";
    Node * node = &connections_[bridge-1];
    assert(node);
    return node;
}

// Figure out on what side of from_bridge you can find to_bridge.
// -1 for non of the options;
DirectionalCutter MeanderPermutation::direction(Node * from_node, Node * to_node) {
    Node * Leftward = dtree::Leftward(from_node);
    Node * Rightward = dtree::Rightward(from_node);

    if(Leftward == to_node) {
        return {dtree::kLeft, true};
    } else if (Rightward == to_node) {
        return {dtree::kRight, true};
    } else {
        // the permutation is cyclic so the to_node should be on the other side
        if (Leftward == 0) {
            assert(dtree::Right(from_node) == to_node);
            return {dtree::kLeft, false};
        } else if (Rightward == 0) {
            assert(dtree::Left(from_node) == to_node);
            return {dtree::kRight, false};
        }
        
        // this shouldn't happen;
        assert(0);
        return {-1, false};
    }
}


int MeanderPermutation::shiftcut(int from_bridge, int to_bridge) {
    Node * from_node = bridge_node(from_bridge);
    Node * to_node = bridge_node(to_bridge);

    return this->shiftcut(from_node, to_node);
}

int MeanderPermutation::shiftcut(Node * from_node, Node * to_node) {
    // Remove root of original sequence from the roots set. The nodes from_node and to_node should be in the same sequence.

    Node * result;

    LOG_DEBUG("Before");
    PRINTDEBUG();

    remove_root(from_node);

    

    DirectionalCutter direction = this->direction(from_node, to_node);

    // The sequences are periodic. Therefore we want to cut it in a such a way that the open ends come at the leftmost and rightmost side. 
    LOG_DEBUG("Direction fetched and it needs to " << (direction.should_cut ? "cut" : "not cut") << " between " << id(from_node) << " and " << id(to_node));
    
    if (direction.direction == dtree::kLeft && direction.should_cut) {
        // <----to from--->
        result = dtree::CutLeftOf(from_node);
        assert(result != 0);
        // <----to> <from--->
        Node * right_node = dtree::Right(from_node);
        Node * left_node = dtree::Left(to_node);
        // <left_node----to> <from---right_node>
        // goal: <from---right_node> <left_node----to> 
        result = dtree::LinkRightOf(left_node, right_node);
        assert(right_node == result); // left_node will be on the rightmost side of right_node. The function should return rightmost of right_node
        // result: <from---right_node left_node----to> 

    } else if (direction.direction == dtree::kRight && direction.should_cut) {
        // <----from to---->
        result = dtree::CutRightOf(from_node);
        assert(result != 0);
        // <----from> <to---->
        Node * right_node = dtree::Right(to_node);
        Node * left_node = dtree::Left(from_node);
        // <left_node----from> <to---right_node>
        // goal: <to---right_node> <left_node----from> 
        result = dtree::LinkRightOf(left_node, right_node);
        assert(right_node == result); // left_node will be on the rightmost side of right_node. The function should return rightmost of right_node
        // result: <to---right_node left_node----from> 
    }

    add_root(from_node);
    add_root(to_node);

    LOG_DEBUG("After");
    PRINTDEBUG();

    return direction.direction;
}

int MeanderPermutation::disconnect(int from_bridge, int to_bridge) {
    Node * from_node = bridge_node(from_bridge);
    Node * to_node = bridge_node(to_bridge);

    return this->disconnect(from_node, to_node);
}

int MeanderPermutation::disconnect(Node * from_node, Node * to_node) {
    // Remove root of original sequence from the roots set. The nodes from_node and to_node should be in the same sequence.
    Node * result;

    remove_root(to_node);

    DirectionalCutter direction = this->direction(from_node, to_node);
    
    if (direction.direction == dtree::kLeft && direction.should_cut) {
        result = dtree::CutLeftOf(from_node);
        assert(result != 0);
    } else if (direction.direction == dtree::kRight && direction.should_cut) {
        result = dtree::CutRightOf(from_node);
        assert(result != 0);
    }
    
    add_root(from_node);
    add_root(to_node);

    return direction.direction;
}

void MeanderPermutation::flip(int first, int cw, int afirst, int acw) {
    LOG_DEBUG("Flip");
    PRINTDEBUG();

    this->last_flip = FlipMap {first, cw, afirst, acw};

    Node * first_node = bridge_node(first);
    Node * cw_node = bridge_node(cw);
    Node * afirst_node = bridge_node(afirst);
    Node * acw_node = bridge_node(acw);

    int first_direction = this->shiftcut(first_node, afirst_node);

    int cw_direction = this->disconnect(cw_node, acw_node);

    #if(DEBUGPERM)
    std::cout << "first_direction: " << first_direction << ", f: " << first << ", af: " << afirst << "\n";
    this->print();
    #endif
    
    if (first_direction == dtree::kLeft)
    {
        // ---afirst> <first----
        if (cw_direction == dtree::kLeft) {
            // cw_direction == dtree::kLeft should be read as left of cw is acw.
            // acw> <cw

            // perform acw> <first
            simple_connect(acw_node, first_node);
            // perform afirst> <cw
            simple_connect(afirst_node, cw_node);
            #if(DEEPDEBUGPERM)
            std::cout << "afirst> <first, acw> <cw\n";
            #endif
            
        } else if (cw_direction == dtree::kRight) {
            // cw> <acw

            // perform reverse(<acw) = acw>
            reverse(acw_node);
            // perform acw> <first
            simple_connect(acw_node, first_node);
            // perform reverse(cw>) = <cw
            reverse(cw_node);
            // perform afirst > <cw
            simple_connect(afirst_node, cw_node);
            #if(DEEPDEBUGPERM)
            std::cout << "afirst> <first, cw> <acw\n";
            #endif
        }
    } else if (first_direction == dtree::kRight) {
        // first> <afirst
        if (cw_direction == dtree::kLeft) {
            // acw> <cw

            // perform reverse(acw>) = <acw
            reverse(acw_node);
            // perform first> <acw
            simple_connect(first_node, acw_node);
            // perform reverse(<cw) = cw>
            reverse(acw_node);
            // perform cw> <afirst
            simple_connect(cw_node, afirst_node);
            #if(DEEPDEBUGPERM)
            std::cout << "first> <afirst, acw> <cw\n";
            #endif

        } else if (cw_direction == dtree::kRight) {
            // cw> <acw

            // perform first> <acw
            simple_connect(first_node, acw_node);
            // perform afirst> <cw
            simple_connect(afirst_node, cw_node);
            #if(DEEPDEBUGPERM)
            std::cout << "first> <afirst, cw> <acw\n";
            #endif
        }
    }    
    
}

void MeanderPermutation::simple_connect(int left_bridge, int right_bridge) {
    #warning Can be optimized since root will always the on the left most side?? Not true, but from_node should be closer to root right?

    Node * left_node = bridge_node(left_bridge);
    Node * right_node = bridge_node(right_bridge);

    this->simple_connect(left_node, right_node);
}

void MeanderPermutation::simple_connect(Node * left_node, Node * right_node) {
    LOG_DEBUG("");
    PRINTDEBUG();

    remove_root(left_node);
    remove_root(right_node);

    Node * rightmost_of_left;

    if(!dtree::SameSeq(left_node, right_node)) {
        rightmost_of_left = dtree::LinkRightOf(right_node, left_node); // link the right_node right of left_node
        assert(rightmost_of_left != 0);
    }

    add_root(right_node);
}

void MeanderPermutation::from(const std::vector<Edge> & edges) {
    LOG_DEBUG("");
    std::vector<bool> visited_faces(edges.size()/4, false);
    
    std::vector<std::vector<int> > paths;

    Edge current_edge;
    
    int path_id = 0;

    for (int i = 0, endi = edges.size(); i < endi; ++i) {
        current_edge = edges[i];
        if (!visited_faces[current_edge.face_id-1]) {
            paths.push_back(std::vector<int>());
            int first_face = 0;
            while (true) {
                visited_faces[current_edge.face_id-1] = true;
                
                if (first_face == 0) first_face = current_edge.face_id;
                

                
                paths[path_id].push_back(current_edge.face_id);
                #warning This assumes that current edge is always non-shore, which is probably always true.
                
                
                while (current_edge.is_shore == false)
                {
                    current_edge = next(current_edge, edges);
                }

                Edge adjacent_of_current_edge = adjacent(current_edge, edges);

                if(adjacent_of_current_edge.face_id == first_face) break;



                //std::cout << "connecting " << current_edge.face_id << " -> " << adjacent_of_current_edge.face_id << "\n";
                simple_connect(current_edge.face_id, adjacent_of_current_edge.face_id);

                current_edge = next(adjacent_of_current_edge, edges);                
            }
            path_id++;
        }
    }
    LOG_DEBUG("Done!");
    PRINTDEBUG();
}

std::vector<int> MeanderPermutation::get_sizes() {
    std::cout << "k = " << roots.size() << "\n";
    std::vector<int> sizes;
    int depth = 0;
    for (auto root : roots)
    {
        depth = C::AggrSeq(bridge_node(root));
        sizes.push_back(depth);

        #if(INFOPRINT)
        std::cout << "n_comps = " << depth << "\n";
        #endif
    }

    return sizes;
}

void MeanderPermutation::print() {
    for (auto root : roots)
    {
        Node * node = bridge_node(root);
        std::cout << "R ";
        while (node != 0)
        {
            std::cout << id(node) << " <- ";
            node = dtree::Leftward(node);
        }
        std::cout << " L\n";
    }
}

void MeanderPermutation::print(Node * node_) { 
    Node * node = dtree::Right(node_);
    std::cout << "R ";
    while (node != 0)
    {
        std::cout << id(node) << " <- ";
        node = dtree::Leftward(node);
    }
    std::cout << " L\n";
    
}

std::vector<int> MeanderPermutation::get_path(int node_) {
    Node * node = bridge_node(node_);

    std::vector<int> path;

    while (node != 0)
    {
        int value = id(node);
        path.push_back(value);
        node = dtree::Leftward(node);
    }

    return path;
}


std::vector<std::vector<int> > MeanderPermutation::get_paths() {
    std::vector<std::vector<int> > paths;
    for (auto root : roots)
    {
        Node * node = bridge_node(root);

        std::vector<int> path;

        while (node != 0)
        {
            int value = id(node);
            path.push_back(value);
            node = dtree::Leftward(node);
        }

        paths.push_back(path);
    }
    return paths;
}

std::vector<std::vector<int> > MeanderPermutation::get_norm_paths() {
    std::vector<std::vector<int> > paths = this->get_paths();
    for (int i = 0; i < paths.size(); i++) {
        int minIndex = std::min_element(paths[i].begin(), paths[i].end()) - paths[i].begin();
        std::vector<int> path;
        int path_len = paths[i].size();
        for (int j = 0; j < path_len; j++)
        {
            int k = (j + minIndex) % path_len; 
            path.push_back(paths[i][k]);
        }
        paths[i] = path;
    }    
    return paths;
}

void MeanderPermutation::undo() {
    this->flip(last_flip.first, last_flip.cw, last_flip.acw, last_flip.afirst);
}

int MeanderPermutation::k_components() {
    return roots.size();
}

void MeanderPermutation::add_node(int id) {
    roots.insert(id);
}

void MeanderPermutation::remove_node(int id) {
    roots.erase(id);
}

void MeanderPermutation::add_node(Node * node) {
    add_node(id(node));
}

void MeanderPermutation::remove_node(Node * node) {
    remove_node(id(node));
}

void MeanderPermutation::add_root(Node * node) {
    add_node(dtree::Right(node));
}

void MeanderPermutation::remove_root(Node * node) {
    remove_node(dtree::Right(node));
}

int MeanderPermutation::id(Node * node) {
    return translator[node];
}

Node * MeanderPermutation::get(int id) {
    return bridge_node(id);
}
