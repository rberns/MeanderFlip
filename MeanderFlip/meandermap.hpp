//
//  meandermap.hpp
//  MeanderMap
//
//  Created by Rutger Berns on 30/12/2021.
//

#ifndef meandermap_hpp
#define meandermap_hpp

#include "meandergenerator.hpp"
#include "meanderpermutation.hpp"
#include "meanderflip.hpp"
#include <utility>

class MeanderMap {
private:
  std::vector<Edge> edges;

  // order of the meander
  int n;
public:
  // n is the order of the meander.
  MeanderMap(int n);

  // get edges
  const std::vector<Edge> & get_edges();
  
  // setup the structure
  void generate();

  // edge from id
  Edge get_edge(int id);

  // general operators
  Edge adjacent(Edge edge);
  Edge next(Edge edge);
  Edge cw(Edge edge);
  Edge acw(Edge edge);

  // global operators
  int find_faces_in_area(std::vector<int> boundary);

  // function for finding other edges around a vertex
  std::vector<int> edges_around_vertex_excluding_initial_cw(Edge initial);
  std::vector<int> edges_around_vertex_excluding_initial_acw(Edge initial);

  // stats
  int num_edges();

  // debug functions
  void print_edges();
  std::tuple<std::vector<std::vector<int> >, std::vector<std::vector<int> >, std::vector<int>, std::vector<int>, std::vector<std::vector<int> >, std::vector<std::vector<int> >, std::vector<bool> > node_graph();
  std::vector< std::tuple<int, int, bool> > convert_to_arcs();

  // friend class
  friend class MeanderMonteCarlo;
};



class MeanderMonteCarlo {
private:
  // parameters
  double q;

  //settings
  unsigned int seed;
  
  // structure
  //MeanderMap map;
  MeanderPermutation permutation;

  // random generator
  RandomGenerator random;
  
  // random functions
  Edge random_edge();
  Edge random_shore_edge();
  Edge random_edge_from(const std::vector<Edge> & edges);
  bool accept(double probability);

public:
  class MeanderMonteCarloDelegate {
  public:
    virtual void afterflip(MeanderMonteCarlo * simulation, const FlipInfo& info, uint64_t iteration) = 0;
  };

  // delegate
  MeanderMonteCarloDelegate * delegate = 0;

  // n is the order of the meander, seed: random seed, q: partition function parameter.
  MeanderMonteCarlo(int n, int seed, double q);

  // setup
  void setup();

  // flip move
  bool flip(bool dump = false, uint64_t iteration = 0);

  // information
  int meander_of_order_n();
  unsigned int get_seed();
  int k_components();
  double get_q();

  // size
  int num_half_edges();
  int num_faces();

  // measurements
  std::vector<uint64_t> measure_dist();
  std::vector<uint64_t> measure_dist_edges();

  
  // permutation/paths O(n)
  std::vector<std::vector<int>> get_paths();

  // permutation (always updated)
  std::vector<std::vector<int>> get_paths_from_permutation();

  // string susceptibility functions
  std::vector<uint64_t> stringsusceptibility_full();

  int quad_stringsusceptibility_rand();
  int quad_stringsusceptibility_rand(Edge initial);

  int num_faces_in(Edge intial, Edge common);

  // current meander
  void current_state(); // O(n)

  #warning(move back)
  MeanderMap map;
};

#endif