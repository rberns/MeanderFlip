from pathlib import Path
import os
import subprocess
import sys
import multiprocessing
import argparse
import random
import math

def iterations_generator(n, q):
    base = pow(2, int(math.ceil(math.log2(q*256))))
    return n*base

def run_sim(q, n, configurations, iterations, dump=False, susceptibility=False, n_measurements_per_file=None):
   Path("simdata/q{:.4f}/n{}".format(q, n)).mkdir(parents=True, exist_ok=True)
   cmd = "./build/meander_main -q {} -n {} -i {} -c {} --seed {} {} {} {}".format(q, n, iterations, configurations, random.randint(0, 2**30), "-d" if dump else "", "-s" if susceptibility else "", "-f {}".format(n_measurements_per_file) if n_measurements_per_file else "")
   return subprocess.call(cmd, shell=True)

def run_diagnostics(n, q):
    return run_sim(q=q, n=n, configurations=1, iterations=64*iterations_generator(n, q), dump=True) # n*256*int(q)
    

def run(conf):
    n, q, i, susceptibility, n_measurements_per_file = conf
    return run_sim(q=q, n=n, configurations=i, iterations=iterations_generator(n, q), susceptibility=susceptibility, n_measurements_per_file=n_measurements_per_file) # relaxation time is at least 64 times n

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Generates Meander Sytems')
    parser.add_argument("--diagnostics", default=False, action="store_true")
    parser.add_argument('-q', type=float, help='q weight')
    parser.add_argument('--sus', default=False, action="store_true")
    parser.add_argument('-s', type=int, help='Size (list of integers)', nargs="+")
    parser.add_argument('-i', type=int, default=200, help='Number of independent configurations')
    parser.add_argument('-p', type=int, default=4, help='Number of concurrent processes')
    parser.add_argument('-f', type=int, default=1000, help='Number measurements per file')
    args = parser.parse_args()

    if args.q is None:
        parser.error("No weight q specified!")
    if args.s is None:
        parser.error("No sizes s specified!")
    for size in args.s:
        if size % 2:
            parser.error("Not dividable by two! You sure?")

    if args.diagnostics:
        [run_diagnostics(n=size, q=args.q) for size in args.s]
    else:
        pool_obj = multiprocessing.Pool(processes=args.p)
        pool_obj.map(run, [(size, args.q, args.i, args.sus, args.f) for size in args.s])