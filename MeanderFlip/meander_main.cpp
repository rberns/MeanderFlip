//
//  meander_main.cpp
//  MeanderFlip
//
//  Created by Rutger Berns on 5/12/2021.
//
// used https://github.com/pboettch/cxx_argp
#include <iostream>
#include <fstream>
#include <random>
#include <set>
#include "meandergenerator.hpp"
#include "meanderflip.hpp"
#include "json.hpp"
#include "meanderpermutation.hpp"
#include "popl/popl.hpp"
#include <chrono>
#include "planarmap+json.hpp"
//#include "meanderplotter.hpp"
#include <iostream>
#include <fstream>
#include "meanderrunner.hpp"
#include <iomanip>
#include <ctime>
#include "json.hpp"
#include "meandermeasurement.hpp"
#include "stopwatch.hpp"
#include "fileio.hpp"


std::string curr_date() {
    std::stringstream stream;
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    stream << std::put_time(&tm, "%Y%m%d%H%M%S");
    return stream.str();
}

// std::string file_name_generator(double q, int n, std::string id) {
    
//     std::stringstream stream;
//     stream << "simdata/q";
//     stream << std::fixed << std::setprecision(4) << q;
//     stream << "/n" << n;
//     stream << "/" << "sim_" << id << ".json";
    
//     return stream.str();
// }

std::string pre_generator(bool will_dump, bool will_measure_string_susceptibility) {
    if (will_dump)
    {
        return "diagnostics";
    } else {
        if(will_measure_string_susceptibility) {
            return "datastring";
        } else {
            return "data";
        }
    }
}


/*
 * 378231440
 * 377455115637

 * Old:

 * 376426758393
 * 373757564
 *
 * 
 * 
 */

//#define NUM_MEASUREMENTS 32
//#define NUM_MEASUREMENTS_STRING_SUSCEPTIBILITY 512




int main(int argc, const char * argv[]) {

    double q = 2.0; // q parameter
    int n = 8; // 2n bridges
    //int i = 10; // number of iterations
    int seed = 1234;
    uint64_t equilibration_time = 0;
    uint64_t max_number_of_measurements_per_packet = 8;
    int c = 2; // number of independent configurations
    bool will_dump = false;
    bool will_measure_string_susceptibility = false;
    bool will_measure_benford = false;
    bool will_measure_distance = false;
    //bool use_sweeps_as_unit = false;

    //std::string default_meas = "[{\"id\": 1, \"rate\": 100, \"type\": \"string_susceptibility\", \"setup_data\": {\"n_samples\": 1}}, {\"id\": 2, \"rate\": 4, \"type\": \"components\", \"setup_data\": {}}]";
    std::string default_meas = "[{\"id\": 5, \"rate\": 64, \"type\": \"distance\", \"setup_data\": {\"batching_size\": 1, \"n_samples\": 2}}]"; 

    std::string default_name = "default";
    //std::string file_name = "data.json";

    popl::OptionParser op("Allowed options");

    auto help_option   = op.add<popl::Switch>("h", "help", "produce help message");
    auto config_option   = op.add<popl::Value<std::string>>("", "config", "give ini config file with options");
	auto q_option   = op.add<popl::Value<double>>("q", "weight", "q parameter (weight)", q, &q);
    auto N_option   = op.add<popl::Value<int>>("n", "meanderoforder", "meander of order n, which results in 2n bridges", n, &n);
    //auto ns_option   = op.add<popl::Value<int>>("i", "relaxtime", "relaxtion time (number of iterations)", i, &i);
    auto s_option = op.add<popl::Value<int>>("c", "independentconfigurations", "number of runs", c, &c);
    auto seed_option   = op.add<popl::Value<int>>("", "seed", "random seed", seed, &seed);
    auto dump_option = op.add<popl::Switch>("d", "dump", "dump edges", &will_dump);
    auto e_option = op.add<popl::Value<uint64_t>>("e", "equilibrationtime", "equilibration time (number of samples before the start of measuring)", equilibration_time, &equilibration_time);
    
    auto name_option = op.add<popl::Value<std::string>>("o", "name", "output name", default_name);
    //auto measure_benford = op.add<popl::Switch>("", "benford", "this will measure average lengths of rivers", &will_measure_benford);
    //auto measure_distribution = op.add<popl::Switch>("", "distribution", "measure the distance profile of the configuration", &will_measure_distance);
    //auto string_sus_option = op.add<popl::Switch>("s", "will_measure_string_susceptibility", "measure string susceptibility", &will_measure_string_susceptibility);
    auto packing_option = op.add<popl::Value<uint64_t>>("f", "max_number_of_measurements_per_packet", "number of independent configurations per packet", max_number_of_measurements_per_packet, &max_number_of_measurements_per_packet);
    auto measurements_option = op.add<popl::Value<std::string>>("m", "measurements", "give JSON objects to describe what measurements should take place. [{\"id\": 1, \"rate\": 100, \"type\": \"string_susceptibility\", \"setup_data\": {\"n_samples\": 1}}, ...]", default_meas);
    //auto use_sweep_unit_option = op.add<popl::Switch>("", "sweeps", "uses sweeps as unit", &use_sweeps_as_unit);

	op.parse(argc, argv);

    if (help_option->is_set()) {
	    std::cout << op << "\n";
        exit(0);
    }

    if (config_option->is_set()) {
        std::cout << "Reading config at: " << config_option->value() << "\n";
        op.parse(config_option->value());
    }

    std::cout << "q: " << q << "\n";
    std::cout << "n: " << n << ", so 2n bridges: " << 2*n << "\n";
    std::cout << "e (equilibration time in flips): " << equilibration_time << "\n";
    //std::cout << "iterations (should be more than relaxation time): " << i << "\n";
    //std::cout << "use_sweeps_as_unit: " << use_sweeps_as_unit << "\n";
    //int scale = use_sweeps_as_unit ? n : 1;

    std::cout << "o (output name): " << name_option->value() << "\n";
    std::cout << "c (independent configurations): " << c << "\n";
    std::cout << "seed: " << seed << "\n";
    std::cout << "will_dump: " << will_dump << "\n";
    std::cout << "will_measure_distance: " << will_measure_distance << "\n";
    std::cout << "will_measure_benford: " << will_measure_benford << "\n";
    std::cout << "will_measure_string_susceptibility: " << will_measure_string_susceptibility << "\n";
    std::cout << "max number of independent configurations per packet: " << max_number_of_measurements_per_packet << "\n";
    std::cout << "measurements_option: " << measurements_option->value() << "\n";
    
    std::mt19937 generator (seed);

    auto measurement_config = nlohmann::json::parse(measurements_option->value());

    auto measurements = std::vector<MeanderMeasurement*>();

    MeanderRunner runner(n, seed, q);

    runner.setup();

    Fileio file = Fileio(q, n, seed, c, name_option->value());
    file.initialize_dir();

   

    for (auto object: measurement_config)
    {
        std::string type = object["type"];
        MeanderMeasurement * measurement;
        if (type.compare("string_susceptibility") == 0) {
            measurement = new MeanderStringSusceptibility(&runner);
            
        } else if (type.compare("components") == 0) {
            measurement = new MeanderComponents(&runner);
        } else if (type.compare("distance") == 0) {
            measurement = new MeanderDistance(&runner);
        } else  {
            std::cout << "could not find the type of measurement";
            continue;
        }
        measurement->setup(object);
        measurements.push_back(measurement);
    }

    uint64_t num_edges = 2*((uint64_t)n)*4;

    std::ofstream myfile;

    std::cout << "Iterating:\n";
    
    auto measurement_rates = std::vector<uint64_t>();

    for (auto measurement: measurements) {
        measurement_rates.push_back(measurement->rate);
    }

    auto min_rate = *std::min_element(std::begin(measurement_rates), std::end(measurement_rates));
    auto max_rate = *std::max_element(std::begin(measurement_rates), std::end(measurement_rates));

    std::cout << "min_rate: " << min_rate << "\n";
    std::cout << "max_rate: " << max_rate << "\n";

    // the max_rate will determine the number of independent measurements

    Stopwatch flip_stopwatch;

    if (equilibration_time != 0)
    {
        flip_stopwatch.resume();
        runner.iterate(equilibration_time);
        flip_stopwatch.pause();
    }

    runner.reset();
    
    
    for (size_t j = 0; j < c; j++)
    {
        std::cout << "#run " << j+1 << "/" << c << "\n";

        


        while (true)
        {
            flip_stopwatch.resume();
            runner.iterate(min_rate);
            flip_stopwatch.pause();

            

            for (auto measurement: measurements) {
                if (runner.iter % measurement->rate == 0)
                {
                    //std::cout << "at: " << runner.iter << "\n";
                    measurement->measure();
                }
            }

            if(runner.iter >= max_rate * max_number_of_measurements_per_packet) {
                break;
            }
        }
        


        //file_name = file_name_generator(q, n, "data_" + curr_date() + "_" + std::to_string(seed) + "_" + std::to_string(j+1) + "_" + std::to_string(c));

        nlohmann::json json = runner.json_report();
        
        json["date"] = curr_date();
        json["run"] = j;
        json["totalrun"] = c;
        json["time_flipping"] = flip_stopwatch.nanoseconds();

        for (auto measurement: measurements) {
            json["measurements"].push_back(measurement->json());
            measurement->reset();
        }
        

        runner.reset();
        flip_stopwatch.reset();

        file.write(json, j+1);
    }

    while (!measurements.empty())
    {
        delete measurements.back();
        measurements.pop_back();
    }
    
}