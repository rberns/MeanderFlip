from ast import With
import sys

sys.path.append(sys.path[0] + '/../..')

print(sys.path)
import pickle
from meanderutils import *

data_per_size = dict()

q = 1.0

size_data = {1.0: [128, 256, 512, 1024, 2048]}

for n in size_data[q]:
    for data in SimDataIterator(1.0, n=n, name="componentscorrelationtime"):
        print(n)
        data_per_size[n] = pd.Series(data["measurements"][0]["data"])

#df = pd.DataFrame()


# for size, data in data_per_size.items():
#     print(size)
#     print(data)
#     df[str(size)] = data
    

#print(df)

#df.to_pickle("simdata/componentscorrelationtime/q{:.4f}/data.pkl".format(q))

with open("simdata/componentscorrelationtime/q{:.4f}/data.pkl".format(q), 'wb') as f:
    pickle.dump(data_per_size, f) 