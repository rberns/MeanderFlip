% !TeX root = ../thesis.tex


% We know that Quantum effects become significant below the Planck scale.
\clearpage

\section{Two-dimensional quantum gravity}

\label{section:2dquantumgravity}

Two-dimensional quantum gravity can be defined via two routes: via the scaling
limit of discrete random geometries or via Liouville quantum gravity
\cite{Barkley_2019}. The two routes both try to solve performing the path
integral over metrics on a surface. Two-dimensional quantum gravity as the
scaling limit of discrete random geometries originates from doing a lattice
discretization to solve the path-integral \cite{Barkley_2019}. In the lattice
discretization also the lattice itself is variable which is why planar maps are
used. Liouville quantum gravity can be seen as an analytic interpretation of
two-dimensional quantum gravity, and it is conjectured to be the scaling limit
of discrete random geometries \cite{Ding2021}.

Two-dimensional quantum gravity without matter is called 'pure gravity' which is
a universality class with certain critical exponents. Matter can be introduced
in two-dimensional quantum gravity by dressing/coupling the geometry to a
critical statistical system with a certain central charge \cite{Barkley_2019}.
The coupling changes the critical exponents and thus allows for discovering
other universality classes. \textbf{Note that the statistical system is the
matter field, so the matter is quite abstract \cite{Budd2022}.}

Liouville quantum gravity is characterized by its coupling constant $\gamma$.
The 'pure gravity' universality class corresponds to the coupling constant
$\gamma = \sqrt{8/3}$ \cite{Ding2021,Barkley_2019}. (Undecorated) uniform
quadrangulations in the continuum limit are an example of 'pure gravity'. Adding
matter fields changes the coupling constant, $\gamma$, of Liouville quantum
gravity. 

Two-dimensional quantum gravity will be coupled to meanders. Specifically,
quadrangulations will be decorated by meanders which will change the
universality class of the system. Critical exponents can then be measured to
find out the universality class of those systems. These critical exponents which
are observables will be described in the following section. Also, the KPZ formula
will be introduced which describes the critical exponents when a statistical
system with a certain central charge is coupled to 2DQG.

Actually, the quadrangulations will be built based on meander configurations. It
is a common thing in 2DQG to build discrete surfaces based on a statistical
system. In \cite{Budd2022} by \citeauthor{Budd2022}, random surfaces are for
example built by knitting trees which also allows creating random surfaces of a
certain universality class.

\subsection{Observables}
The critical exponents are observables of 2DQG. Two critical exponents that will
be considered are the Hausdorff dimension and the string susceptibility. The
Hausdorff dimension describes how geodesic distances grow on a surface and the
string susceptibility describes the roughness or spikiness of a geometry.

\input{theory/observables.tex}

\clearpage

\subsection{Relation string susceptibility, Hausdorff dimension and central charge}
\label{section:criticalrelationskpz}
% "It is conjectured that the geometry of random surfaces coupled to a statistical
% system is described by Liouville quantum gravity" \cite{Barkley_2019}.

As mentioned earlier, the critical exponents of two-dimensional quantum gravity
change when coupled to a critical statistical system with a certain central
charge $c$. Here the relations between the central charge, Liouville coupling
constant $\gamma$, string susceptibility and Hausdorff dimension will be given.

Central to studying the critical behaviour of statistical systems is the KPZ
formula founded in the 80s by Knizhnik, Polyakov and Zamolodchikov
\cite{Garban2012}. The KPZ formula tells how the critical exponents of the
original statistical system on a flat, regular (Euclidean) system are related to
the critical exponents of the statistical system coupled to 2DQG
\cite{Garban2012}. Coupling the statistical system to 2DQG can be
seen as studying the statistical system on a random planar lattice
\cite{Garban2012}. The KPZ formula is a widely used application of Liouville
Quantum Gravity \cite{Ding2021}. It is however a conjecture and not rigorously
proven \cite{Garban2012}. More details about the KPZ formula can be read in
\cite{Garban2012} by Christophe Garban.

Using the KPZ formula, the string susceptibility $\gamma_\mathrm{s}$ can be
related to the central charge of the statistical system by

\begin{equation}
    \gamma_\mathrm{s} = \frac{c-1 -\sqrt{(c-1)(c-25)}}{12}
    \label{theory:observables:eq:stringcentralcharge2}
\end{equation}
which is valid for $c \in (-\infty, 1]$ \cite{Barkley_2019}.


The coupling constant $\gamma \in (0, 2]$ of Liouville quantum gravity is
related to the central charge by
\begin{equation}
    c = 25 - 6 \left(\frac{2}{\gamma}+\frac{\gamma}{2}\right)^2
    \label{eq:theory:kpzstringsus}
\end{equation}
where again $c \in (-\infty, 1]$ \cite{Barkley_2019}. The string susceptibility
$\gamma_\mathrm{s}$ is related to the Liouville coupling constant $\gamma$ by

\begin{equation}
    \gamma_\mathrm{s} = 1-\frac{4}{\gamma^2}
    \label{eq:theory:translategammastring}
\end{equation}

One of the challenges in the field is obtaining a relation between the Liouville
coupling constant $\gamma$ and the Hausdorff dimension. In \cite{Barkley_2019}
\citeauthor{Barkley_2019} did numerical research into the relation. Numerical
evidence and earlier derived bounds supports 
\begin{equation}
    d_H = 2 + \frac{\gamma^2}{2} + \frac{\gamma}{\sqrt{6}}
    \label{eq:theory:dinggwynnehaus}
\end{equation}
proposed by Ding and Gwynne in \cite{Ding_2019}. Liouville quantum gravity only
 describes the coupling of a statistical system to random geometry for $c \in
 (-\infty, 1]$ \cite{Barkley_2019} instead of the older Watabiki formula
 \cite{Barkley_2019}. There is the $c = 1$-barrier beyond which the random
 geometries degenerate into branched polymers, like in Figures
 \ref{theory:universality:geometryq20n100},
 \ref{theory:universality:geometryq20n500} and
 \ref{theory:universality:geometryq20n1000}. This branched polymer phase (shaded
 in light blue in Figure \ref{theory:fig:stringsusandhausdorff}) for $c > 1$ is
 characterized by Hausdorff dimension $d_H = 2$ and string susceptibility
 $\gamma_\mathrm{s} = \frac{1}{2}$ \cite{BuddCourseGeometry2017}. It is
 interesting to note that $d_H = 2$ would suggest flat geometry, although
 branched polymers are not flat geometry \cite{Ambj_rn_2000}. The string
 susceptibility $\gamma_\mathrm{s}$ and the Hausdorff dimension $d_H$ are
 plotted as a function of the central charge $c$ in Figure
 \ref{theory:fig:stringsusandhausdorff}.

For $c =0$, there is the 'pure gravity' universality class with the Liouville
coupling constant $\gamma = \sqrt{8/3}$, $\gamma_\mathrm{s} = -\frac{1}{2}$ and
$d_H = 4$ \cite{Ding2021,Barkley_2019}. A Hausdorff dimension of $4$ is quite
high, usual surfaces have a fractal dimension of 2, so 'pure gravity' is very
fractal \cite{Sheffield2020}. $c = -2$ corresponds to the universality class of
quadrangulations decorated by a spanning tree \cite{Barkley_2019}.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{theory/assets/observablesasfuncofc.pdf}
    
    
    
    \caption{The string susceptibility $\gamma_\mathrm{s}$ and the Hausdorff
    dimension $d_H$ as a function of the central charge $c$ of the coupled
    statistical system are plotted. The highlighted \textcolor{rbprimarycolor}{red} dots
    indicate at $c = 0$ 'pure gravity' and at $c = -2$ the spanning tree
    decorated quadrangulations universality class. The branched polymer phase,
    $c > 1$, is shaded in light blue. For the Hausdorff dimension equations
    \eqref{eq:theory:dinggwynnehaus}, \eqref{eq:theory:translategammastring} and
    \eqref{eq:theory:kpzstringsus} were used and for the string susceptibility
    equation \eqref{eq:theory:kpzstringsus}.}
    \label{theory:fig:stringsusandhausdorff}
\end{figure}

% TODO
% TODO

Being able to sample large and independent random surfaces of a certain
universality class can help to collect numerical data on the relation between
the Hausdorff dimension and the Liouville coupling constant $\gamma$. Meanders
are a statistical system that could possibly to collect numerical data of that
relation.

% Meanders are a statistical system that can be coupled onto a random surface.
% Francesco et al. conjectured what the critical exponents of the random geometry
% should be when coupled to meanders.

% In General Relativity, one has the Einstein equation given by

% \begin{equation}
%     R_{\mu \nu} - \frac{1}{2} R \, g_{\mu \nu} = 8 \pi \, G \, T_{\mu \nu}
%     \label{theory:2qm:einsteineqn}
% \end{equation}

% , where $g_{\mu \nu}$ is the metric, $T_{\mu \nu}$ is the stress-energy tensor and
% $G$ is the gravitational constant\cite{Carroll2003}.

% In 2D $R_{\mu \nu} = \frac{1}{2} Rg_{\mu \nu}$ which makes the equation
% \eqref{theory:2qm:einsteineqn} trivial \cite{Ertl2001}.

% This makes that any metric $g_{\mu \nu}$ is valid and thus every geometry is
% valid. 

% % TODO ASK TIMOTHY WHETHER IT IS CORRECT IN THIS WAY, AND DOESNT THIS SUGGEST
% % THERE IS NO MATTER IN 2D quantum gravity





% \subsubsection{Path integral}

% We start at the spacetime path integral, which is a path integral over all
% possible histories of a spacetime and this history is described by the
% Lorentzian Metric and in other words it describes the geometry of spacetime.
% This path integral is a hard to calculate object. So we wish to change it using
% a Wick rotation to a euclidean path integral. And in this case you integrate
% over a Riemannian metric. Some factors get a large action and some get a small
% action which will be suppressed. This is better than the case of the Lorentzian
% metric because there are no more terms that have to destructively interfere.

% From this integration over a Riemannian metric, the infinite degrees of freedom
% are replaced by something piecewise linear which is a structure that you could
% build out of triangulations.

% \textbf{Add equations and a bit more clarity.}