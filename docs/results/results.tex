% !TEX root = ../thesis.tex



\section{Number of components}

To study the predicted phase transition, the number of components $k$ is studied
as a function of the order of the meander system $n$ and the weight $q$. The
number of components $k$ is the number of roads in a meander system. Also, the
Binder cumulant is calculated to further investigate the predicted phase
transition.

\subsection{Preliminaries}
\subsubsection{Thermalization time}




\begin{figure}[htb!]
    \centering
    \begin{tikzpicture}[node distance = 2cm]
    \node (denscalc) [lblock, align=center, minimum height=3cm] {Sample \\ density \\ calculator};
    \node (mcmcsampler) [lblock, left color=rbprimarycolor, right color=rbrbluee, align=center, right of=denscalc, xshift=3cm] {{\sffamily \color{white} \textbf{MCMC}} \\ {\sffamily \color{white} \textbf{sampler}}};

    %\node (gfpl) [block, right of=fpl, xshift=3cm] {$\mathrm{GFPL}^2 (q_1, q_2)$-model};
    %\node (multim) [block, right of=gfpl, xshift=3cm, yshift=1cm] {Multimeanders};
    %\node (mea) [block, right of=gfpl, xshift=3cm, yshift=-1cm] {Meanders};
    
    \draw [arrow, yshift=1cm] (-3,0) -- node[anchor=south] {$\rho_\mathrm{samples}$} ([yshift=1cm]denscalc.west);
    \draw [arrow, yshift=0] (-3,0) -- node[anchor=south, ] {$n_\mathrm{sweeps}$} ([yshift=0]denscalc.west);
    \draw [arrow, yshift=-1cm] (-3,0) -- node[anchor=south] {$n$} ([yshift=-1cm]denscalc.west);


    \draw [arrow] (denscalc) -- node[anchor=south] {$k$ (rate)} (mcmcsampler.west);
    \draw [arrow] (denscalc) -- node[anchor=north] {$n_\mathrm{measurements}$} (mcmcsampler.west);



    \node (components) [block, align=center, right of=mcmcsampler, xshift=3cm] {$k$ components};
    \draw [arrow] (mcmcsampler) -- node[anchor=south] {$X_i, t$} (components);


    %\draw [arrow, yshift=-0.5cm] (-3,0) -- node[anchor=south] {$k$ (rate)} ([yshift=-0.5cm]mcmcsampler.west);
    %\draw [arrow, yshift=0.5cm] (-3,0) -- node[anchor=south] {} ([yshift=0.5cm]mcmcsampler.west);
    %\draw [arrow] (gfpl) -- node[anchor=south] {$q_1 \rightarrow 0$} (multim);
    %\draw [arrow] (multim) -- node[anchor=west] {$q_2 \rightarrow 0$} (mea);
    
    
    \end{tikzpicture}
    \caption{Setup for measuring thermalization time}
    \label{fig:setup:thermalizationtimesetup}
    \end{figure}

As described in Figure \ref{fig:setup:thermalizationtimesetup}, the Monte Carlo
sampler requires a measuring rate $k$ at which the Markov chain will be measured
and the total number of measurements $n_\mathrm{measurements}$ that need to be
performed. The goal is to only save to the disk what is needed. It will be
assumed that the order of magnitude of the thermalization time will be around
$10 - 100\ \mathrm{sweeps}$. It is therefore also assumed that the
thermalization time scales linearly with the system size.

For the following measurements, the sample density, was chosen to be
$\rho_\mathrm{samples} = 64$ samples per sweep. The number of values for a 1000
$\mathrm{sweep}$ run is therefore limited to a maximum of $64.000$ samples
independent of the system size $n$. Now the thermalization time can be
determined using Figure \ref{results:plot:thermalizationtime}. Based on Figure
\ref{results:plot:thermalizationtime}, a very safe thermalization time is chosen
of $512\ \mathrm{sweeps}$. The assumption that the thermalization time grows
linearly with the system size is also true based on Figure
\ref{results:plot:thermalizationtime}. The following safe thermalization time
given by
\begin{equation}
    \tau_{\mathrm{eq}} = 512 q\approx 512 \cdot \exp{(\mathrm{abs}(\log{(q)}))}
\end{equation}
which is chosen by also taking into account the thermalization graphs for
different values of $q$.



\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/thermalizationtime.pdf}
    \caption{Markov chain of the number of components $k$. The measurements are
    taken at $q = 1$ for different system sizes $n$. \textit{A sweep corresponds
    to $2n$ flips.}. Note that not all data is visible in this plot. The
    sampling rate is $\frac{1}{64}\ \mathrm{sweeps}$.}
    \label{results:plot:thermalizationtime}
\end{figure}

Unfortunately, the thermalization cannot be compared to an analytic average
number of components for meanders, which would be given by

\begin{equation}
    \langle k \rangle = \sum_k k \cdot \frac{M^{(k)}_n}{M_n}
    \label{results:expectationvalue:kcomponents}
\end{equation}
since there is no analytic formula for $M^{(k)}_n$. Next up the autocorrelation
of the Markov chain should be calculated. This allows to estimate a measuring
rate for sampling uncorrelated/independent samples.

\subsubsection{Autocorrelation time}
\label{section:autocorr}

In Figure \ref{results:plot:autocorrelationtime}, the autocorrelation time is
plotted determined by the $\tau_{int}$ method, see section
\ref{theory:montecarlodataanalysis}.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/autocorrelationtime.pdf}
    \caption{Measurements of the autocorrelation time $\tau$ in
    $\mathrm{sweeps}$ for different system sizes as a function of the weight
    $q$. The autocorrelation time is fitted using the
    $\tau_\mathrm{int}$-method.}
    \label{results:plot:autocorrelationtime}
\end{figure}

Now all the variables to measure the number of components systematically are
collected.


\clearpage

\subsection{Error analysis}

The goal is to estimate the error with an accuracy of $20-30\%$ of the correct
error. For the $k$-components simulation, six methods were used to estimate the
standard error (see Table \ref{tab:results:errorestimates}). The dataset
consists of measurements of the number of components, $k$, every $2 \tau$
autocorrelation times. The methods for estimating the error were introduced in
section \ref{section:sub:erroranalysis}. The first method "Standard mean error"
refers to simply using the standard deviation and dividing by the square root of
the number of data points, the autocorrelation is then neglected. The second
method uses the autocorrelation and Equation
\eqref{theory:eq:Montecarlo:erroranalysiswithautocorr} to correct the
statistical error of the first method. The third, fourth, fifth and sixth
methods are binning, jackknife, bootstrap and stationary bootstrap respectively.
They are described in section \ref{section:sub:erroranalysis}.

Error estimates of the different methods are displayed in Table
\ref{tab:results:errorestimates}. One can observe that the "Standard mean error"
estimate equals the "Jackknife" estimate. The standard error of the
"Autocorrelation", "Binning", and "Stationary Bootstrap" are similar.
The "Bootstrap" estimate is somewhere in the middle with respect to the other
error estimates.

\begin{table}[htb!]
    \centering
    \caption{Error estimates, $S_{\bar{k}}$, of the different methods introduced
    in section \ref{section:sub:erroranalysis}. The observable is
    $k$-components of a meander system. The measurements are taken at $2 \tau$ autocorrelation times
    and the $N = 131072$ measurements and the Markov chain consists of $N = 2.7
    \cdot 10^8$ flips. The measuring rate, $r = 2\tau = 2048$.}
\begin{tabular}{|c|c|}
    \hline
    \bfseries Method    & $S_{\bar{k}}$ \\
    \hline
    \bfseries Standard mean error & 0.037 \\
    \bfseries Autocorrelation & 0.052 \\
    \bfseries Binning $(m = 1024)$ & 0.050 \\
    \bfseries Jackknife & 0.037 \\
    \bfseries Bootstrap & 0.039 \\
    \bfseries Stationary bootstrap $(p = 0.005)$ & 0.045 \\
    \hline
    \end{tabular}
    \label{tab:results:errorestimates}
\end{table}

For binning, a plot is made in Figure \ref{fig:results:erroranalysis:binning}
of the standard error $S_{\bar{k}}$ versus the bin size $m$. In the plot, one
can see that the error flattens from around a bin size of $m = 1024$. The bin
size should therefore be at least $m = 1024$. Note that the measurements are
taken at $2\tau$ autocorrelation times, thus having $2048$ autocorrelation times
worth of data yields reliable binning estimates. $2048$ autocorrelation times
for $q = 1$ are equivalent to $1024\ \mathrm{sweeps}$.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/binningresults.pdf}
    \caption{Example of binning analysis with the standard error $S_{\bar{k}}$
    of the number of components $k$ versus the bin size $m$. Parameters: system
    size $n = 1024$ and weight $q = 1.0$. The total length of the Markov chain
    is $N = 131072$. A bin size of $1$ corresponds to measurements taken every
    $2 \tau$ autocorrelation times with autocorrelation times from section
    \ref{section:autocorr}.}
    \label{fig:results:erroranalysis:binning}
\end{figure}

The error estimated using the standard mean error and jackknife doesn't take
into account the correlation of the data, contrary to binning and the
autocorrelation method. Autocorrelation and binning give the most accurate
results and Figure \ref{fig:results:erroranalysis:binning} allows to check the
hypothesis that the data is correlated, but if the bins are large enough the
data becomes uncorrelated and the statistical error stops increasing.

For the stationary bootstrap, the data is resampled at sequences of about $1/p$
which is typically chosen on the order of the autocorrelation time. For this
dataset, which is sampled at every $2 \tau$, one should choose $p = 1$ which is
the regular bootstrap estimate. By trail and error $p = 0.005$ or $1/p = 200$
was chosen since this best resembled the errors found with the autocorrelation
and binning methods. It must be noted that $1/p = 200$ has the same order of
magnitude as the start of the flattening for the binning analysis which will
most likely explain the better results. 

In conclusion, the valid methods are: binning with a bin size of $m = 1024$,
stationary bootstrap and autocorrelation. The stationary bootstrap will
however not be used anywhere for two reasons: estimating an extra parameter $p$
adds unwanted complexity and the randomness involved in the resampling makes the
error non-deterministic.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/autocorrvscomp.pdf}
    \caption{On the x-axis one can find the number of measurements and the
    number of bins between these two quantities is a constant factor of $m =
    1024$ which is bin size of for binning error estimate. The y-axis is the
    ratio between the error by the binning divided by the error by
    autocorrelation. The different lines correspond to different experiments of
    the $k$-components. The lines are made from a combination of $q = 1.0, 2.0,
    2.5, 3.0$ and $n = 256, 512, 1024, 2048, 4096, 8192$ totalling $24$ lines.}
    \label{fig:results:convergenceofthebinningerrortoauto}
\end{figure}


\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/autocorrvscompv2.pdf}
    \caption{The standard deviation, $\sigma$, versus the number of measurements every $2\tau$. Measurements taken of system with $n = 8192$ and $q = 1.0$. Stationary Bootstrap with $p = 0.005$ and binning with bin size $m = 1024$. The upper x-axis indicates the $N_b$ number of bins for the "Binning" analysis.}
    \label{fig:results:binsizevsbinningautobootstrap}
\end{figure}

Now the question arises for binning: what is the minimum amount of data required
to get an accurate error? In Figure
\ref{fig:results:convergenceofthebinningerrortoauto}, a comparison between the
binning and autocorrelation method was made. The different lines indicate
different experiments with different system size $n$ and weight $q$. The y-axis
is the ratio between the error by the binning, $S_\mathrm{binning}$ divided by
the error by autocorrelation, $S_\mathrm{autocorrelation}$. On the x-axis, the
dataset is increased, while keeping the bin size ($m = 1024$) the same. The most
important observation is that all the lines converge to $1$ when the number of
bins/the amount of data in the data set is increased, thus the autocorrelation
and binning method give consistent results for a large enough dataset. The
convergence of the lines in Figure
\ref{fig:results:convergenceofthebinningerrortoauto} is also quite symmetric.
The same plot for $m = 512$ would show convergence to a value below 1 because
the error would then be underestimated by binning.

In Figure \ref{fig:results:binsizevsbinningautobootstrap}, the standard
deviation is calculated by $\sigma = S_{\bar{k}} \cdot \sqrt{N}$. The $\sqrt{N}$
correction makes the plot independent of the number of data points, but it will
allow to study the fluctuations of the error when the number of data points
increases. In Figure \ref{fig:results:binsizevsbinningautobootstrap}, one can
clearly see that independent of the number of measurements the autocorrelation
gives very consistent results. Binning is more fluctuative but it converges to
the autocorrelation for larger data sets. The stationary bootstrap is also
reasonable, but does not converge as much as "Binning". One could however
conclude that the autocorrelation method produces accurate errors even with
small datasets.

Figure \ref{fig:results:howmanybinsyouneed} shows the amount of error estimates
within the $30 \%$-goal. The x-axis is the size of dataset and the y-axis
$S_\mathrm{binning}$ divided by $S_\mathrm{autocorrelation}$. Figure
\ref{fig:results:howmanybinsyouneed} measures the deviation of the binning error
from the autocorrelation error, so it is assumed that the autocorrelation error
is the best error which is a reasonable assumption based on Figure
\ref{fig:results:binsizevsbinningautobootstrap}. The experiment was done for
$24$ Markov chains with sizes $n = 256, 512, 1024, 2048, 4096, 8192$ and weights
$q = 1.0, 2.0, 2.5, 3.0$. The amount of calculated binning errors that reach the
$30\%$-goal are $84\%$ for $8 - 16$ bins, $92\%$ for $16 - 32$ bins,   $95\%$
for $32 - 64$ bins, $100\%$ for $64 - 128$ bins, $100\%$ for $128 - 256$ bins.
So at least 64 bins are required for accurate binning errors when performing
measurements every $2 \tau$, i.e. at least $65536$ measurements at a rate of $2
\tau$.

The conclusion of this analysis: autocorrelation is a very powerful tool to
estimate errors and the errors are consistent with results from binning. The
autocorrelation method yields more consistent results also for smaller
datasets, thus reducing the computational power needed for simulations. If
autocorrelation is not applicable in a situation, one could resort to
binning.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/autocorrvsbinscompfancyplot.pdf}
    \caption{2D-histogram of the graphs in figure
    \ref{fig:results:convergenceofthebinningerrortoauto}. The histogram is
    normalized over the y-axis such that one can see the percentage error
    estimates within a certain factor of the autocorrelation estimate per $N_b$
    number of bins. The percentage of errors with less than $30\%$ deviation
    with respect to the error estimate using autocorrelation is given by  $84\%$
    for $8 - 16$ bins, $92\%$ for $16 - 32$ bins,   $95\%$ for $32 - 64$ bins,
    $100\%$ for $64 - 128$ bins,  $100\%$ for $128 - 256$ bins. }
    \label{fig:results:howmanybinsyouneed}
\end{figure}

% \begin{figure}[htb!]
%     \centering
% \begin{tabular}{|c|c|c|c|c|c|}
%     \hline
%     \bfseries Jackknife & \bfseries Bootstrap & \bfseries Stationary bootstrap ($p = 0.005$) & \bfseries Binning ($m = 1024$) & \bfseries Autocorrelation \\
%     \hline
%     0.037 & 0.036 & 0.049 & 0.050 & 0.052 \\
%     \hline
% \end{tabular}
% \end{figure}








% \paragraph{Autocorrelation}

% - Why does a Markov chain have autocorrelation
% - Autocorrelation function
% - Fitting methods for determining autocorrelation time
% - Error estimates by using the autocorrelation



% \paragraph{Conclusion}

% In the measurements of the number of components as a function of $q$ there are
% no discontinuities which suggests a higher order phase transition.

% % The system is under a constant phase transistion

% We can explain this by saying that the observable $k$ the number of components
% is a reasonably local property.

% The number of loops in one baby universe can change dramatically without
% influencing the rest of the data.

 

% For measuring the stringsusceptibility we could expect there to a function
% $N_1(q)$ and a function $N_2(q)$ that show the dependence of the location of the
% phase transition as a function of $q$. 

% \begin{equation}
%     n^{(-\gamma_s-2)} \cdot \left(1+\left(\frac{n}{N_1(q)}\right)^{-\delta_1} + \left(\frac{n}{N_2(q)}\right)^{-\delta_2}\right)
% \end{equation}

\clearpage

\input{results/components.tex}

\newpage
 



\input{results/stringsusceptibility.tex}

\input{results/hausdorff.tex}
