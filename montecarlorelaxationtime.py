import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json

path = "test.json"

class SimFile(object):
    def __init__(self, file_name):
        self.file_name = file_name
      
    def __enter__(self):
        self.file = open(self.file_name)
        return json.load(self.file)
  
    def __exit__(self, exc_type, exc_value, exc_tb):
        self.file.close()

with SimFile(path) as data:
    k = data["k_by_iterations"]
    plt.plot(k)
    plt.show()
