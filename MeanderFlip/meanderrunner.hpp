//
//  meanderrunner.hpp
//  MeanderFlip
// 
//  Created by Rutger Berns on 29/12/2021.
//

#ifndef meanderrunner_hpp
#define meanderrunner_hpp

#include "meandermap.hpp"
#include <functional>
#include "json.hpp"

class MeanderRunner: MeanderMonteCarlo::MeanderMonteCarloDelegate
{
private:
    // settings
    bool dump = false;

    // delegate implementation
    void afterflip(MeanderMonteCarlo * simulation, const FlipInfo& info, uint64_t iteration);
public:
    MeanderRunner(int n, int seed, double q);
    // meanders of order n
    void setup();
    void iterate();
    void iterate(uint64_t iterations);
    void iterate_and_mnk(uint64_t iterations);

    // debug report
    void report();
    void reset(); // resets data and diagnostics
    void current_state();

    // data report
    nlohmann::json json_report();

    // Hausdorff dimension
    std::vector<uint64_t> measure_dist(int n_samples, bool is_dual=true);
    
    // data
    std::vector<uint64_t> n_meanders_of_k;
    std::vector<int> k_components;

    // diagonistics
    uint64_t iter = 0;
    uint64_t accepted = 0;

    // simulation
    MeanderMonteCarlo simulation;
};


#endif