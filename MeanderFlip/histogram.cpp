#include "histogram.hpp"

Histogram::Histogram(size_t size_): hist(size_, 0), hist_squared(size_, 0)
{
    size = size_;
    N = 0;
}

Histogram::Histogram(std::vector<uint64_t> list, size_t size_): hist(size_, 0), hist_squared(size_, 0)
{
    size = size_;
    N = 1;
    for(auto out: list) {
		hist[((uint64_t)out)-1]++;
	}

    for (size_t i = 0; i < size; i++)
    {
        hist_squared[i] = hist[i]*hist[i];
    }
}

Histogram::Histogram(std::vector<uint64_t> list): hist_squared(list.size(), 0)
{
    size = list.size();
    N = 1;
    hist = list;

    for (size_t i = 0; i < size; i++)
    {
        hist_squared[i] = hist[i]*hist[i];
    }
}

Histogram::~Histogram()
{
    
}

// void Histogram::from_list(std::vector<uint64_t> list) {
        
// }

uint64_t Histogram::sum() {
    uint64_t sum = 0;
    for (uint64_t e: hist)
    {
        sum += e;
    }
    return sum;
}

void Histogram::resize(size_t new_size) {
    size = new_size;
    hist.resize(new_size, 0);
    hist_squared.resize(new_size, 0);
}