% !TeX root = ../thesis.tex

\clearpage

\section{String susceptibility}
\label{section:results:stringsus}

The string susceptibility was introduced in section
\ref{section:observables:stringsus}. The string susceptibility can be extracted
from the size distribution of baby universes. A large amount of data is required
because the asymptotic (i.e. large minimum neck baby universes) of the
distribution contains the most valuable information. These large baby universes
in the asymptotic are however very rare. The distribution of baby universes was
measured for different values of the weight $q$ and were fitted to obtain the
string susceptibility as a function of $q$. Histograms of the sizes of baby
universes found in the Markov chain were collected to find the size distribution
of baby universes. The measurement interval was taken at an interval of $128 \
\tau (n,q)$ autocorrelation times, which was estimated using binning for $1 < q
< 10$. The total dataset consists of $1.85 \ \mathrm{years}$ of CPU-time. The
number of independent measurements per $q$ and system size $n$ are given in
section \ref{section:practicalinfoonstringsus}. In the following, the measured
results at $q = 1$ are first compared to the analytic results. Secondly, the
numerical measurements are used to measure the string susceptibility as a
function of $q$ using an earlier derived fit function based on the distribution
of baby universes around $q = 1$. Thirdly, the results of alternative fitting
methods will be discussed. Lastly, the string susceptibility as a function of
the weight $q$ allows to study the phase transition conjectured at $q = 2$.

% The asymptotic corresponds to large baby universes, i.e. baby universes of
% about $N/2$ faces where $N$ is the total amount of faces and these baby
% universes have a very low probability. 

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/stringsusceptibilitycomptoana.pdf}
    \begin{subfigure}[b]{0.55\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents1.pdf}
    \caption{}\label{fig:results:string128}
    \end{subfigure}
    \begin{subfigure}[b]{0.43\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents2.pdf}
    \caption{}\label{fig:results:string512}
    \end{subfigure}

    \caption{Plot of $p$, the probability for a meander baby universe of size $n$. The plots are made for (a) meanders of order $N = 128$ and (b) meanders of order $N = 512$.}
    \label{results:plot:distributionofp}
\end{figure}

For $q = 1$, an analytic formula for the probability $p$ of a baby universe of
size $n$ was derived as Equation \eqref{theory:stringsus:probability} in section
\ref{section:observables:stringsus}. In Figure
\ref{results:plot:distributionofp}, $p$ is plotted as a function of $n(1-n/N)$
for two sizes $N = 128$ and $N = 512$ where $n$ is the order of the
\textbf{meander baby universe} and $N$ is the order of the meander. Small baby
universes correspond to small $n(1-n/N)$, larger baby universes correspond to
large $n(1-n/N)$. The largest possible meander baby universe has order $n =
N/2$. Figure \ref{results:stringsus:fancyplotanal}, shows that large baby
universes are very rare and unfortunately the probability for finding them
decreases cubically with the order of the meander $N$.

In Figure \ref{results:stringsus:fancyplotanal}, the analytic probability is
compared to the measured data, by dividing $p_{measured} / p_{analytic}$. The
x-axis is transformed as $n(1-n/N)$.  The probability for the small baby
universe coincides accurately. For the larger sizes the data is noisy but
centered around $1$. The numerically measured size distribution of baby
universes at $q = 1$ therefore matches the analytic results.

% for example a maximum size baby universe of size $n = N/2$
% for a meander of order $N$, will be transformed to $N/4$.


\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{\linewidth}
    \centering
    \includegraphics[]{results/assets/analyticcompn128.pdf}
    \caption{$N = 128$, $n_\text{measurements} = 8.874456 \cdot 10^6$, $\text{CPU-time} \approx 217.2\ \mathrm{hrs}$.}\label{results:analyticcomp:plot:128}
    \end{subfigure}
    
    \begin{subfigure}[b]{\linewidth}
    \centering
    \includegraphics[]{results/assets/analyticcompn512.pdf}
    \caption{$N = 512$, $n_\text{measurements} = 3.54696 \cdot 10^6$, $\text{CPU-time} \approx 465.3\ \mathrm{hrs}$}\label{results:analyticcomp:plot:512}
    \end{subfigure}
    

    \caption{Comparison of measured probability to analytic probability to find
    a minimum neck baby universe (mimbu) of size/order $n$. The analytic probability is given
    by Equation \eqref{theory:stringsus:probability}.}
    
    
    
    \label{results:stringsus:fancyplotanal}
    
    
    
    \end{figure}


For the other values of the weight $q$, there is no analytic formula to compare
the numerical results to. The string susceptibility can however be measured and
there are several ways to do that which will be introduced in the following.

\subsection{The approximation fitting method}
\label{section:approximationfittingmethod}

In section \ref{section:predictionofstringsus}, an approximation of the
probability for a meander baby universe of size $n$ at $q = 1$ was turned into a
fit function for the string susceptibility $\gamma_\mathrm{s}$, see Equation
\eqref{theory:observables:eq:fitfunction}. The string susceptibilities obtained
using this fitting method are plotted in Figure
\ref{results:plot:stringsusfitspureapprox}. The results at $q = 1$ are
excellent. Assuming that the values of different system sizes $n$ can be
combined using a weighted average gives $\gamma_s = -0.99284 \pm 0.00022$ which
is very close to the correct value $\gamma_\mathrm{s} = -1$. This accuracy is
not unexpected because the fit function is based on the analytic results for $q
= 1$. The slight deviation that still exists can be attributed to systematic
errors of the fitting procedure. The approximation itself introduces a
systematic error since making an approximation loses information. For $1< q < 2$
in Figure \ref{theory:observables:eq:fitfunction}, the results clearly deviate
from the conjectured relation, how can this be explained?

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/pureapproximationfit.pdf}
    \caption{Fitted results of the string susceptibility $\gamma_\mathrm{s}$ as
    a function of the weight $q$ using the fit function given by Equation
    \eqref{theory:observables:eq:fitfunction}.}
    \label{results:plot:stringsusfitspureapprox}
\end{figure}

In order to find out, the probability for a meander baby universe of size $n$ is
fitted to numerical data to extract the string susceptibility for two different
values of $q$. The numerical data and fits for $q = 1$ and $q = 2$ for system
size $n = 512$ are plotted in Figures \ref{fig:results:fittingq1} and
\ref{fig:results:fittingq2} respectively. The $q = 1$ fit yields
$\gamma_\mathrm{s} = -0.99197 \pm 0.00031$ which is similar to the expected
$\gamma_s = -1.0$. For $q = 2$, the fit yields $\gamma_\mathrm{s} = -0.5872 \pm
0.0008$ which is quite different from the conjectured $\gamma_s = -0.7676$. More
precisely, for $q = 1$ the deviation is only about $0.008$ where for $q = 2$ the
deviation is $0.1804$. The fitted string susceptibility $\gamma_\mathrm{s}$ at
$q = 2$ clearly does not match with the conjecture. In Figure
\ref{fig:results:fittingq2}, it is however visible that the fitted function for
$q = 2$ does not fit the data well. The asymptotic of the fit is slightly above
the asymptotic of the numerical data in Figure \ref{fig:results:fittingq2}. Compare that to $q = 1$
in Figure \ref{fig:results:fittingq1} where the fit is exactly centered in the
asymptotic of the data. Thus fitting based on the approximated fit function at
$q = 1$ might not be correct for different values of $q$.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/fittingthepureapprox512.pdf}
    \begin{subfigure}[b]{0.55\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents1.pdf}
    \caption{}\label{fig:results:fittingq1}
    \end{subfigure}
    \begin{subfigure}[b]{0.43\linewidth}
    %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents2.pdf}
    \caption{}\label{fig:results:fittingq2}
    \end{subfigure}

    \caption{Fitting of Equation \eqref{theory:observables:eq:fitfunction} to
    $p$ versus $n(1-n/N)$ to extract the string susceptibility
    $\gamma_\mathrm{s}$. The system size $N = 512$. (a) corresponds to $q = 1$
    and (b) corresponds to $q = 2$.}
    \label{results:plot:stringsusdeviationfitting}
\end{figure} 


At $q = 1$ the analytic formula for the
probability of a meander baby universe of size $n$ was expanded and turned into
a fit function given by Equation \eqref{theory:observables:eq:fitfunction} in
section \ref{section:predictionofstringsus}.

\subsection{Linear fitting}
\label{section:linearfitting}
In \cite{Ambjorn1993} by \citeauthor{Ambjorn1993}, two methods are introduced to
fit the probability for a baby universe of size $n$ to find the string
susceptibility: a linear fit and a fit using a more sophisticated fit function.
From the logarithmic $p$ versus $(n(1-n/N))$ plots (like Figure
\ref{results:plot:stringsusdeviationfitting}) it is clear that the asymptotic is
a straight line which will be fitted using the linear fitting method. Linear
fitting itself is however inaccurate because only the asymptotic is straight,
and therefore a cutoff needs to be introduced. In \cite{Ambjorn1993}, they
introduce a cutoff such that only baby universes of size $n \in [n_c,N/2]$ are
used for fitting where $n_c$ is the cutoff and $N/2$ is the maximum size baby
universe. 

More formally the linear method works by fitting $y = ax + b$ to the numerical
data where $y = \ln{p}$ and $x = \ln{n(1-n/N)}$ where $a$ and $b$ are fit
parameters. The slope $a$ can be related to the string susceptibility using
$\gamma_s = a + 2$, which can be derived using Equation
\eqref{theory:observables:eq:distributionofbabyuniverses}.

The second fitting method introduced by \citeauthor{Ambjorn1993} in
\cite{Ambjorn1993} is by fitting the following function to the numerical data:

\begin{equation}
    \ln{(p \cdot n)} = a + (\gamma_\mathrm{s} -2) \ln{\left(n\left(1-\frac{n}{N}\right)\right)} + b/n
    \label{results:eq:ambjornfit}
\end{equation}
where $a$ and $b$ are fit parameters. This fit function will be called the
"baby" fit\footnote{The name "baby" fit originates from the name of the article
"Baby universes in 2d quantum gravity" \cite{Ambjorn1993} by
\citeauthor{Ambjorn1993}.}.

In Figure \ref{results:stringsus:fittingcomparisoncutoff}, the linear, baby and
the approximation fit have been plotted as a function of the cutoff $n_c$. The
approximation-fit works very well for $q = 1$ (Figure
\ref{results:fittingcutoff:plot:512q1}) but again fails for $q = 2$ (Figure
\ref{results:fittingcutoff:plot:512q2}) even with cutoff. The linear and baby
fit work well for both $q$ and the linear fit converges a bit faster than the
baby fit. The baby fit will therefore not be further used. For the linear and
approximation fit the statistical error of the numerical data is taken into
account. For the baby universe, the statistical error is not taken into account
because in that case it tends to only fit well on areas with a low statistical
error. 

The linear fit approaches the string susceptibility for increasing cutoff $n_c$.
The final string susceptibility can therefore be extracted by fitting the
following ansatz to the results of the linear fit:
\begin{equation}
    \gamma_\mathrm{s} (n_c) = \gamma_\mathrm{s} - c e^{-d n_c}
    \label{eq:results:ansatzfitting}
\end{equation}
where $c$ and $d$ are fit parameters and $\gamma_\mathrm{s}$ is the final string
susceptibility. This exponential is plotted in Figure
\ref{results:fittingcutoff:plot:fitted} and yields a string susceptibility
$\gamma_\mathrm{s} = -0.9638 \pm 0.0019$. This method of obtaining the string
susceptibility has a higher systematic error than the approximation fitting
method in section \ref{section:approximationfittingmethod}, but it should work
more accurately and reliable on all values of $1 < q < 10$. In the following,
this method will be referred to as the linear+exponential method.

In Figures \ref{results:fittingcutoff:plot:512q1} and
\ref{results:fittingcutoff:plot:512q2}, another fitting method was used called
Random sample consensus RANSAC. RANSAC is able to get a better convergence than
the linear and baby fit. It works roughly by randomly sampling what data is
considered for the fit and choosing the best least-squares fit \cite{Foley1981,
sklearndocs}. The sampling removes outliers from the data. This method does
however not take into account the statistical error of the data and will
therefore not be used, but it might be interesting for future use.

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[b]{\linewidth}
    \centering
    \includegraphics[]{results/assets/stringsusfittingversuscutoff1.pdf}
    \caption{$N = 512$, $q = 1$}\label{results:fittingcutoff:plot:512q1}
    \end{subfigure}
    
    \begin{subfigure}[b]{\linewidth}
    \centering
    \includegraphics[]{results/assets/stringsusfittingversuscutoff2.pdf}
    \caption{$N = 512$, $q = 2$}\label{results:fittingcutoff:plot:512q2}
    \end{subfigure}
    %\centering
    \begin{subfigure}[b]{\linewidth}
        \centering
        \includegraphics[]{results/assets/fittedlinearsus512.pdf}
        \caption{$N = 512$, $q = 1$}\label{results:fittingcutoff:plot:fitted}
        \end{subfigure}
    
    
    \caption{Comparison of the different fitting methods in (a) and (b) for respectively $q = 1$ and conjectured $q = 0.7676$. In (c), an exponential is fitted to the linear fit results as a function of the cutoff $n_c$. Note that the error bars in (a), (b) and (c) are determined by the covariance matrix and are often smaller than the markers.}
    
    
    
    \label{results:stringsus:fittingcomparisoncutoff}
    
    
    
    \end{figure}

    \clearpage

\subsection{Results}

In Figure \ref{results:plot:stringspecialfit}, the string susceptibility is
plotted as a function of $q$ for different system sizes $n$. The
linear+exponential method introduced in section \ref{section:linearfitting} is
used for the fitting with a maximum cutoff $n_c = 80$. The results in Figure
\ref{results:plot:stringspecialfit} show a better correspondence to the
conjectured relation between the string susceptibility and the weight $q$, than
the results for the approximation fit in Figure
\ref{results:plot:stringsusfitspureapprox}.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/specialfit.pdf}
    \caption{Fitted results of the string susceptibility $\gamma_\mathrm{s}$ as
    a function of the weight $q$ using linear+exponential fitting and a maximal $n_c = 80$.
    Errors are determined by the covariance matrix of the least-squares fit and
    smaller than the markers.}
    \label{results:plot:stringspecialfit}
\end{figure}

At $q \approx 8.5$, the larger system sizes go from having a lower string
susceptibility $\gamma_\mathrm{s}$ to having a higher string susceptibility with
respect to the smaller system sizes. This location could be identified as a
special point, $q_c \approx 8.5$. The region of the point $q_c$ is cropped in
Figure \ref{results:plot:stringspecialfitzoomed}, where it is more clear that
all the sizes exactly switch around when comparing $q = 7.0$ and $q = 9.0$.

It turns out that doing a linear fit with a set cutoff can also yield consistent
results. The results for linear fitting with the cutoff set to $30$ is given in
Figure \ref{results:plot:stringlinear}. The region around the point $q_c$ is
again cropped in Figure \ref{results:plot:stringlinearzoomed}.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/specialfitzoomed.pdf}
    \caption{Fitted results of the string susceptibility $\gamma_\mathrm{s}$ as
    a function of the weight $q$ using linear+exponential fitting method with a
    maximal $n_c = 80$. Errors are determined by the covariance matrix of the
    least-squares fit and smaller than the markers.}
    \label{results:plot:stringspecialfitzoomed}
\end{figure}



\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/linearfitcutof30.pdf}
    \caption{Fitted results of the string susceptibility $\gamma_\mathrm{s}$ as
    a function of the weight $q$ using linear fitting with a cutoff of $30$. Errors
    are determined by the covariance matrix of the least-squares fit and smaller
    than the markers.}
    \label{results:plot:stringlinear}
\end{figure}

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/linearfitcutof30zoomed.pdf}
    \caption{Fitted results of the string susceptibility $\gamma_\mathrm{s}$ as
    a function of the weight $q$ using linear fitting with a cutoff of
    $30$. Errors are determined by the covariance matrix of the least-squares
    fit and smaller than the markers.}
    \label{results:plot:stringlinearzoomed}
\end{figure}

% \paragraph{Linear fitting with exponential correction (linear + exp)}

% The numerically measured $\ln{p}$ in Figure \ref{fig:results:linexpfit} deviates
% from the analytic $\ln{p}$. In Figure \ref{fig:results:linexpfitfx}, the
% difference between the numerical and analytic $\ln{p}$ is plotted which looks
% like an exponential. Let's assume that the correct fitting function has a
% correction $f(x)$, so that $y = ax + b + f(x)$. Fitting an exponential to $f(x)$
% works good which can be seen in Figure \ref{fig:results:linexpfitfx}.


% \begin{figure}[htb!]
%     \centering
%     \includegraphics[]{results/assets/stringsusdeviation512.pdf}
%     \begin{subfigure}[b]{0.55\linewidth}
%     %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents1.pdf}
%     \caption{}\label{fig:results:linexpfit}
%     \end{subfigure}
%     \begin{subfigure}[b]{0.43\linewidth}
%     %\includegraphics[height=5cm]{theory/assets/meanderscriticalexponents2.pdf}
%     \caption{}\label{fig:results:linexpfitfx}
%     \end{subfigure}

%     \caption{Plot of $f(x) = y - ax - b$, $n = 512$, $q = 1$. Fitting the string susceptibility gives $\gamma_\mathrm{s} = -1.022$.}
%     \label{results:plot:stringsusdeviation}
% \end{figure} 

% The linear + exponential fit leads to the following fit function:
% \begin{equation}
%     \ln{p} = (\gamma_\mathrm{s} - 2)x + b + c e^{(d x)}
% \end{equation}
% where $\gamma_\mathrm{s}$ is the string susceptibility and $b$, $c$, $d$ are fit
% parameters and $x = \ln{(n(1-n/N))}$. Fitting this fit function to $n = 512$ in
% Figure \ref{results:plot:stringsusdeviation} yields $\gamma_\mathrm{s} = -1.023$
% which is not too far from the analytically predicted value $\gamma_\mathrm{s} =
% -1$

% This method works good for small system
% sizes with an asymptotic with a low statistical error. For larger system sizes
% and higher statistical error in the asymptotic the fit function tends to
% overfit.



% In figure \ref{results:plot:comparisonoffittingmethods}, the fitting methods are
% compared for a system of $n = 512$. The expansion method gives inconsistent
% results with a low systematic error. The linear method gives consistent results
% but with a large systematic error. This is also clear from
% \ref{results:plot:stringsusfitsexpansion} and
% \ref{results:plot:stringsusfitslinear}.

% \begin{figure}[htb!]
%     \centering
%     \includegraphics[]{results/assets/stringsusfittingcompn512.pdf}
%     \caption{Results of fitting system $n = 512$ at $q = 1$, analytic value for string susceptibility would be $\gamma_s = -1$. Error bars are based on the covariance matrix.}
%     \label{results:plot:comparisonoffittingmethods}
% \end{figure}


% \begin{figure}[htb!]
%     \centering
%     \includegraphics[]{results/assets/stringsusexpansion.pdf}
%     \caption{Plot of fits of string susceptibility using the expansion fit. The conjectured relation between $\gamma_s$ and $q$ is also drawn. Error bars based on the covariance matrix.}
%     \label{results:plot:stringsusfitsexpansion}
% \end{figure}

% \begin{figure}[htb!]
%     \centering
%     \includegraphics[]{results/assets/stringsuslinear.pdf}
%     \caption{Plot of fits of string susceptibility using linear fit. The conjectured relation between $\gamma_s$ and $q$ is also drawn. Error bars based on the covariance matrix.}
%     \label{results:plot:stringsusfitslinear}
% \end{figure}

% \newpage
% \newpage
% \clearpage


% \subsection{Measuring string susceptibility}

% Measuring interval 128 sweeps, and take 10000 measurements per file.

% \subsection{Measuring Hausdorff dimension}

The correspondence of the conjectured relation between the string susceptibility
and the weight $q$ is plotted in Figures \ref{results:plot:stringspecialfit} and
\ref{results:plot:stringlinear} by the \textcolor{rblinkcolor}{blue} line. For
$1 \leq q \leq 2$, the numerical string susceptibility follows a similar shape
to the conjectured string susceptibility. Approaching $q = 2$, the measure
string susceptibility starts to deviate slightly. For $q > 2$, the numerical
string susceptibility clearly deviates, the kink is not visible. How can this
result be interpreted? The conjectures that meanders are governed by 2DQG is
made in the continuum limit. Here, the conjecture is studied using discrete
geometries which can cause discretization effects explaining the continuous
transition from $q = 2$ to the branched polymer phase. It must however be noted
that extrapolating $q \rightarrow \infty$ seems to approach the string
susceptibility $\gamma_\mathrm{s} = 1/2$ which corresponds to the branched
polymer phase.

This kind of smooth transition from 2DQG coupled to a CFT with $c < 1$ to
branched polymers is also commonly observed in literature, for example in
\cite{Thorleifsson1998} where they measure a phase transition from $c = 1$ to
branched polymers and from $c = 0$ to branched polymers. For the meanders a
phase between $c = -1$ and branched polymers is studied. It is however difficult
to call it a real phase transition because for $q < 2$ the system is
continuously changing phase.

Also, an explanation needs to be given for the larger system sizes having a
larger string susceptibility with respect to the smaller system sizes for $q$
higher than $q_c \approx 8.5$. The naive explanation would that for $q < q_c$,
the fits approaches the string susceptibility of the phase at $q = 2$, and for
$q > q_c$ one could say that the fits approach the string susceptibility
$\gamma_\mathrm{s}$ of the branched polymer phase $\gamma_\mathrm{s} = 1/2$. The
reality is however probably more complex. One could make a very rough
approximation of the probability $p$ for a meander baby universe of size $n$ by

\begin{equation}
    p \sim n^{\gamma_\mathrm{s}-2} \left(1 + \left(\frac{n}{N_1(q)}\right)^{-\delta_1}\right)
    \label{eq:veryapprox}
\end{equation}
where $n$ refers to the total system size and $N_1(q)$ is a system size
dependent on $q$. For $n < N_1(q)$, $\delta_1$ will be added to the string
susceptibility resulting in an effective string susceptibility
$\gamma_\mathrm{s}^{\text{effective}} \approx \gamma_\mathrm{s} + \delta_1$. For
$n > N_1(q)$, the string susceptibility will be equal to
$\gamma_\mathrm{s}^{\text{effective}} \approx \gamma_\mathrm{s}$. The assumption
in Equation \eqref{eq:veryapprox} says that the string susceptibility should
only increase or decrease at a set $q$ when the system size is varied. To
explain the results seen around $q_c \approx 8.5$ higher order corrections are
required.


\clearpage
 
In \cite{DiFrancescoNumCheck2000}, \citeauthor{DiFrancescoNumCheck2000} report
that their numerical results get worse when approaching $q = 2$ from below. They
measured a string susceptibility higher than expected. In Figure
\ref{results:plot:convergenceatq2}, the results for the string susceptibility at
$q = 2$ are plotted using the linear fitting with cutoff $n_c = 30$ and
linear+exponential method with $n_c = 80$. Here like in
\cite{DiFrancescoNumCheck2000}, the string susceptibility is higher than the
conjectured value. The values for the string susceptibility $\gamma_\mathrm{s}$
seem to approach the conjectured value for larger system sizes $n$. The
linear fitting method has a systematic error that can be reduced by increasing
the cutoff, however for increasing the cutoff more numerical data is required.

\begin{figure}[htb!]
    \centering
    \includegraphics[]{results/assets/convergenceatq2.pdf}
    \caption{Fitted results of the string susceptibility $\gamma_\mathrm{s}$ at
    $q = 2$ using linear fitting with cutoff $n_c = 30$ and
    linear+exponential method with $n_c = 80$. The errors are determined using the least
    squares covariance matrix.}
    \label{results:plot:convergenceatq2}
\end{figure}

