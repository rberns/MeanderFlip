% !TeX root = ../thesis.tex

\clearpage

\section{Predicting the string susceptibility for multi-meanders}
\label{section:predictionofstringsus}

\begin{figure}[hbt!]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
         \includegraphics[page=2,width=\textwidth]{theory/assets/babyuniversefortriangulationmeanders.pdf}
         \caption{}
         \label{fig:theory:babyuniversemeandersketch}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
         \includegraphics[page=1,width=\textwidth]{theory/assets/babyuniversefortriangulationmeanders.pdf}
         \caption{}
         \label{fig:theory:embeddedbabyuniversemeandersketch}
    \end{subfigure}
    \caption{(a) Sketch of how quadrangles can be glued into a geometry using a
    meander configuration as the glue rule. The baby universe/mimbu is indicated
    in \textcolor{rbprimarycolor}{red}. (b) 2D representation of the planar map
    of the geometry. Here it is clear that the faces painted
    \textcolor{rbprimarycolor}{red} are only connected to the rest of the
    geometry through a minimum neck of size $2$. The minimum neck is indicated
    by the \textcolor{rbprimarycolor}{red} edges. The root edge is indicated by
    the arrow and the root face is the face right of the root edge.}
    \label{fig:theory:observables:babyuniversesmeanders}
\end{figure}

In section \ref{section:observables:stringsus}, the string susceptibility was
introduced which measures the roughness of a surface. A \textbf{mimbu}
corresponded to a minimum neck baby universe, i.e. a piece of the geometry only
connected through a minimum neck of size $2$. For meanders the equivalent of a
mimbu is an area of the meander that is only connected/contained via the river
to the rest of the structure and has no roads leaving. It is a meander system
that you could take out and replace with another meander system without changing
the rest of the structure. For clarity, such a 'meander baby universe' is drawn
in Figure \ref{fig:theory:observables:babyuniversesmeanders}. It is clear in
Figure \ref{fig:theory:observables:babyuniversesmeanders} that the
\textcolor{rbprimarycolor}{red} baby universe is connected only through $2$
edges which is the minimum neck. For $q = 1$, the total amount of meander
configurations of size $n$ is given by Equation \eqref{theory:meanders:Mnsum}.
This allows to calculate the probability for finding a 'meander baby universe'
of size $n$ and ultimately make predictions about the string susceptibility at
$q = 1$. The probability for a 'meander baby universe' of size $n$ defined as
$p$ is given by:

\begin{equation}
    p = \frac{2 (c_n)^2 (c_{N-n})^2}{(c_n)^2}
    \label{theory:stringsus:probability}
\end{equation}
where $c_n$ are the Catalan numbers. It should be noted that $N$ and $n$ in this
case correspond to the order of the meander. This means that the number of faces
of the geometry is always even, because there is one face per bridge and a
meander of order $N$ has $2N$ bridges. The mimbu's therefore also need to
consist of an even number of faces, because it must be possible 'to take a mimbu
out'. There will however still be $2N$ possible starting positions for the
'meander baby universe' of order $n$ which explains the factor 2 in the
numerator. 

% Note that the equation is almost equivalent to
% \eqref{theory:observables:eq:distributionofbabyuniverses} \footnote{Note that
% meanders always have even faces.}.

From the probability defined in Equation \eqref{theory:stringsus:probability},
one can try to find the asymptotic behavior and then compare it to Equation
\eqref{theory:observables:eq:distributionofbabyuniverses} to find the analytic
string susceptibility. 


In order to expand the Equation \eqref{theory:stringsus:probability}, the
asymptotic approximation of the Catalan numbers is used
\begin{equation}
    c_n = \frac{4^n}{\sqrt{\pi}n^{3/2}}\left(1+O\left(\frac{1}{n}\right)\right)
\end{equation}
which when substituted into Equation \eqref{theory:stringsus:probability} gives

\begin{equation} \label{theory:observables:meanderp}
\begin{split}
p & = \frac{2}{\pi} \frac{N^3}{n^3(N-n)^3} \frac{\left(1+O\left(\frac{1}{n}\right)\right)^2 \left(1+O\left(\frac{1}{N-n}\right)\right)^2}{\left(1+O\left(\frac{1}{N}\right)\right)^2} \\
    & = \frac{2}{\pi} (n \cdot (1-n/N))^{-3} \frac{\left(1+O\left(\frac{1}{n}\right)\right)^2 \left(1+O\left(\frac{1}{N-n}\right)\right)^2}{\left(1+O\left(\frac{1}{N}\right)\right)^2} \\
\end{split}
\end{equation}

Comparing the $(n \cdot (1-n/N))^{-3}$ term in Equation
\eqref{theory:observables:meanderp} to $(n \cdot (1- n/N))^{\gamma-2}$ in
equation \eqref{theory:observables:eq:distributionofbabyuniverses} describing
the probability of finding a baby universe of size $n$, gives for the string
susceptibility a value of $\gamma = -1$ for $q = 1$, which corresponds to the
critical exponents found in section \ref{theory:criticalexponentsofmeanders}. 

Equation \eqref{theory:observables:meanderp} can be used to fit the string
susceptibility by substituting $-3$ by $\gamma_\mathrm{s}-2$ which gives

\begin{equation}
    p = \frac{2}{\pi} (n \cdot (1-n/N))^{\gamma_\mathrm{s}-2} \frac{\left(1-c(n)^{-d}\right)^2 \left(1-c(N-n)^{-d}\right)^2}{\left(1-c(N)^{-d}\right)^2} 
    \label{theory:observables:eq:fitfunction}
\end{equation}

% If one wants to measure the string susceptibility in numerical simulations, it
% will be easier to work with $\ln{p}$, which is given by

% \begin{equation}
%     \begin{split}
%         \ln{p} = \left. \ln{2/\pi} -3 \ln{\left(\frac{n(N-n)}{N}\right)} + \ln{\left(1+O\left(\frac{1}{n}\right)\right)^2} + \ln{\left(1+O\left(\frac{1}{N-n}\right)\right)^2} \right. \\ \left.- \ln{\left(1+O\left(\frac{1}{N}\right)\right)^2} \right. \\
%         \ln{p} = \left. \ln{2/\pi} -3 \ln{\left(\frac{n(N-n)}{N}\right)} + 2\ln{\left(1+O\left(\frac{1}{n}\right)\right)} + 2\ln{\left(1+O\left(\frac{1}{N-n}\right)\right)} \right. \\ \left.- 2\ln{\left(1+O\left(\frac{1}{N}\right)\right)} \right.
%     \end{split}
%     \label{theory:observables:lnp}
% \end{equation}

% % is relevant for fitting purposes. Equation
% % \eqref{theory:observables:lnp} demonstrates that in the asymptotic of $\ln{p}$
% % versus $\ln{n(1-n/N)}$ linear fitting is possible.

% %\begin{equation} \label{theory:observables:lnp}
% % \begin{multiline}
% % \ln{p} = \ln{2/\pi} -3 \ln{\left(\frac{n(N-n)}{N}\right)} + \ln{\left(1+O\left(\frac{1}{n}\right)\right)^2} + \ln{\left(1+O\left(\frac{1}{N-n}\right)\right)^2} \\ - \ln{\left(1+O\left(\frac{1}{N}\right)\right)^2} \\
% % \ln{p}= \ln{2/\pi} & -3 \ln{\left(\frac{n(N-n)}{N}\right)} + 2\ln{\left(1+O\left(\frac{1}{n}\right)\right)} + 2\ln{\left(1+O\left(\frac{1}{N-n}\right)\right)} \\ - 2\ln{\left(1+O\left(\frac{1}{N}\right)\right)}
% % \end{multiline}
% %\end{equation}

% Equation \eqref{theory:observables:lnp} can be turned into a method to fit the
% string susceptibility by substituting $-3$ by $\gamma-2$ and assuming that for
% different universality classes the correction terms will be roughly the same.
% One could take the following function for fitting:
% %\begin{equation}
% \begin{multline}
%     \ln{p} = a + \ln{2/\pi} +(\gamma-2) \ln{\left(\frac{n(N-n)}{N}\right)} + 2\ln{\left(1-c(n)^{-d}\right)} + 2\ln{\left(1-c(N-n)^{-d}\right)} \\ - 2\ln{\left(1-c(N)^{-d}\right)}
%     \label{theory:observables:eq:fitfunction}
% \end{multline}
%\label{theory:observables:eq:fitfunction}
%\end{equation}

where also $O\left(\frac{1}{n}\right)$ is substituted by $-cn^{-d}$ and $c$ and
$d$ will be fit parameters determined by fitting. The signs in front of the free
parameter have been determined by comparing the approximation (Equation
\eqref{theory:observables:eq:fitfunction}) to the analytic result and setting
the free parameters such that for $q = 1$, $c > 0$, $d > 0$ one gets the best
fit. The approximation can be seen in Figure
\ref{theory:observables:stringsusfittingplot} where $\gamma = -1, c = 1$ and $d
= 1$. 

\begin{figure}[htb!]
    \centering
    \includegraphics[]{theory/assets/lnplnn.pdf}
    \caption{Plot of $p$ versus $n\left(1-\frac{n}{N}\right)$ on a logarithmic
    scale. The measured data in the plot is taken from a meander system of $n =
    128$ and $q = 1$. The analytic graph is Equation
    \eqref{theory:stringsus:probability} with exact values for the Catalan
    numbers $c_n$. The approximation is based on Equation
    \eqref{theory:observables:eq:fitfunction} with $\gamma = -1, c = 1, d = 1$.
    The probability for mimbu of size $n$ is based on the approximation from
    Equation \eqref{theory:stringsus:probability}.}
    \label{theory:observables:stringsusfittingplot}
\end{figure}

From Figure \ref{theory:observables:stringsusfittingplot}, it is clear that the
probability for a mimbu of size $n$ approaches the analytic probability for
meanders with a mimbu in the asymptotic. Furthermore, the same goes for the
approximation of the analytic result.

% \paragraph{Relation of string susceptibility and the central charge}

% In equation \eqref{theory:observables:eq:stringcentralcharge} the relation
% between the string susceptibility and the central charge is given
% \cite{Barkley_2019}. 

% \begin{equation}
%     \gamma_\mathrm{s} = \frac{c-1 -\sqrt{(c-1)(c-25)}}{12}
%     \label{theory:observables:eq:stringcentralcharge}
% \end{equation}
% where $c$ is the central charge of the coupled system and $\gamma_s$ the string
% susceptibility. Equation \eqref{theory:observables:eq:stringcentralcharge} is
% valid for $c \in (-\infty, 1]$. The relation can be derived from the KPZ
% formula \cite{Barkley_2019}.