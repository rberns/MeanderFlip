#include "link_cut.hpp"

void update(Node *x)
{
    x->sz = 1;
    if (x->l)
        x->sz += x->l->sz;
    if (x->r)
        x->sz += x->r->sz;
    //added
    if (x->vir)
        x->sz += x->vir;
    //end added
}

// void pushup(Node *x)
// {
//     x->sz = (x->l)->sz + (x->r)->sz + x->vir + 1;
// }

void rotr(Node *x)
{
    Node *y, *z;
    y = x->p, z = y->p;
    if ((y->l = x->r))
        y->l->p = y;
    x->r = y, y->p = x;
    if ((x->p = z))
    {
        if (y == z->l)
            z->l = x;
        else
            z->r = x;
    }
    x->pp = y->pp;
    y->pp = 0;
    update(y);
}

void rotl(Node *x)
{
    Node *y, *z;
    y = x->p, z = y->p;
    if ((y->r = x->l))
        y->r->p = y;
    x->l = y, y->p = x;
    if ((x->p = z))
    {
        if (y == z->l)
            z->l = x;
        else
            z->r = x;
    }
    x->pp = y->pp;
    y->pp = 0;
    update(y);
}

void splay(Node *x)
{
    Node *y, *z;
    while (x->p)
    {
        y = x->p;
        if (y->p == 0)
        {
            if (x == y->l)
                rotr(x);
            else
                rotl(x);
        }
        else
        {
            z = y->p;
            if (y == z->l)
            {
                if (x == y->l)
                    rotr(y), rotr(x);
                else
                    rotl(x), rotr(x);
            }
            else
            {
                if (x == y->r)
                    rotl(y), rotl(x);
                else
                    rotr(x), rotl(x);
            }
        }
    }
    update(x);
}

Node *access(Node *x)
{
    splay(x);
    if (x->r)
    {
        x->r->pp = x;
        x->r->p = 0;
        x->r = 0;
        update(x);
    }

    Node *last = x;
    while (x->pp)
    {
        Node *y = x->pp;
        last = y;
        splay(y);
        //added
        y->vir -= x->sz;
        //end added
        if (y->r)
        {
            y->r->pp = y;
            y->r->p = 0;
            //added
            y->vir += y->r->sz;
            //end added
        }
        
        
        
        
        y->r = x;
        x->p = y;
        x->pp = 0;
        update(y);
        splay(x);
    }
    return last;
}

Node *root(Node *x)
{
    access(x);
    while (x->l)
        x = x->l;
    splay(x);
    return x;
}

int cut(Node *x)
{
    Node * cutted_node;
    access(x);
    cutted_node = x->l;
    x->l->p = 0;
    x->l = 0;
    update(x);
    return cutted_node->label;
}

void link(Node *x, Node *y)
{
    access(x);
    access(y);
    x->l = y;
    y->p = x;
    //added
    y->vir += x->sz;
    //end added
    update(x);
}

Node *lca(Node *x, Node *y)
{
    access(x);
    return access(y);
}

int depth(Node *x)
{
    access(x);
    return x->sz - 1;
}

int deepest_depth(Node *x)
{
    access(x);
    //while (x->p) x = x->p;
    return x->sz - 1;
}

LinkCut::LinkCut(int n)
{
    x = new Node[n];
    for (int i = 0; i < n; i++)
    {
        x[i].label = i;
        update(&x[i]);
    }
}

LinkCut::~LinkCut()
{
    delete[] x;
}

void LinkCut::link(int u, int v)
{
    ::link(&x[u], &x[v]);
}

int LinkCut::cut(int u)
{
    return ::cut(&x[u]);
}

int LinkCut::root(int u)
{
    return ::root(&x[u])->label;
}

int LinkCut::depth(int u)
{
    return ::depth(&x[u]);
}

int LinkCut::deepest_depth(int u)
{
    return ::deepest_depth(&x[u]);
}

int LinkCut::lca(int u, int v)
{
    return ::lca(&x[u], &x[v])->label;
}

