import pandas as pd
import numpy as np
import json
from os.path import getctime
import glob
from regex import L
import scipy.special

from pathlib2 import Path

class SimFile(object):
    def __init__(self, file_name):
        self.file_name = file_name
      
    def __enter__(self):
        self.file = open(self.file_name)
        return json.load(self.file)
  
    def __exit__(self, exc_type, exc_value, exc_tb):
        self.file.close()

def getsims(q, n, latest=False, pre="sim_data"):
    #print("simdata/q{:.4f}/n{}/sim_*".format(q, n))
    if not latest:
        return glob.glob("simdata/q{:.4f}/n{}/{}_*".format(q, n, pre))
    else:
        return max(glob.glob("simdata/q{:.4f}/n{}/{}_*".format(q, n, pre)), key=getctime)

def getanalysis(q, n):
    return glob.glob("simdata/q{:.4f}/n{}/dist.pkl".format(q, n))


class SimData(object):
    def __init__(self, q, n, susceptibility=False):
        self.q = q
        self.n = n
        self.susceptibility = susceptibility
      
    def __enter__(self):
        self.files = []
        data = []
        for sim_path in getsims(self.q, self.n, pre="sim_datastring" if self.susceptibility else "sim_data"):
            file = open(sim_path)
            self.files.append(file)
            data.append(json.load(file))

        return data
  
    def __exit__(self, exc_type, exc_value, exc_tb):
        for file in self.files:
            file.close()

class SimDataIterator(object):
    def __init__(self, q, n, susceptibility=False):
        self.q = q
        self.n = n
        self.susceptibility = susceptibility
        self.paths = getsims(self.q, self.n, pre="sim_datastring" if self.susceptibility else "sim_data")

    def __iter__(self):
        return self

    def __next__(self):
        if len(self.paths) == 0:
            raise StopIteration
        path = self.paths.pop(0)
        print("left =", len(path))
        with open(path) as file:
            data = json.load(file)

        return data



class SimDiagnostics(object):
    def __init__(self, q, n):
        self.q = q
        self.n = n
      
    def __enter__(self):
        self.files = []
        data = []
        for sim_path in getsims(self.q, self.n, pre="sim_diagnostics"):
            file = open(sim_path)
            self.files.append(file)
            data.append(json.load(file))

        return data
  
    def __exit__(self, exc_type, exc_value, exc_tb):
        for file in self.files:
            file.close()

class SimAnalysis(object):
    def __init__(self, q, n):
        self.q = q
        self.n = n
        self.file_name = getanalysis(q, n)[0]
      
    def __enter__(self):
        return pd.read_pickle(self.file_name)
  
    def __exit__(self, exc_type, exc_value, exc_tb):
        pass


def conjectured_hausdorff(c):
    if c <= 1:
        return 2*(np.sqrt(25-c)+np.sqrt(49-c)) / (np.sqrt(25-c)+np.sqrt(1-c))
    else:
        return 2

def string_susceptibility(c):
    return (c-1-np.sqrt((c-1)*(c-25)))/12

def central_charge(q):
    e = (1.0/np.pi)*np.arccos(q/2.0)
    
    return 1.0-(6.0*(e**2.0))/(1.0-e)

def catalan(n):
    #return (1/(n+1))*scipy.special.binom(2*n, n)
    return (1/(n+1))*scipy.special.comb(2*n, n, exact=False)

def p_analytic(x, N):
    #b = np.arange(n+1)

    return (catalan(x)*catalan(N-x)/catalan(N))**2

def histogram_to_coordinates(hist, hist_sq):
    vals = []
    for i, (val, val_sq) in enumerate(zip(hist, hist_sq)):
        if val != 0:
            vals.append(np.array([(i+1), val, val_sq]))
    return np.array(vals)

def string_hist(q, n):
    
    N = 2*n
    arr = np.zeros(N)
    arr_sq = np.zeros(N)
    num_measurements = []
    n_indep = 0
    n_accepted = 0
    n_iterations = 0
    n_avg_autocorrelationtime_ = 0
    t_flipping = 0
    t_measuring = 0

    for measurement in SimDataIterator(q=q, n=n, susceptibility=True):
        num_measurements.append(measurement["num_measurements"])
        n_indep += measurement["N"]
        n_accepted += measurement["accepted"]
        n_iterations += measurement["iter"]
        n_avg_autocorrelationtime_ += measurement["iterations"]
        t_flipping += measurement["time_flipping"]
        t_measuring += measurement["time_measuring"]

        for [x, y, y_sq] in measurement["stringhist"]:
            arr[int(x-1)] += y
            arr_sq[int(x-1)] += y_sq

    n_files = len(num_measurements)

    n_avg_autocorrelationtime = n_avg_autocorrelationtime_ / n_files
    n_avg_autocorrelationtime_sweeps = n_avg_autocorrelationtime / (2*n)

    hist = histogram_to_coordinates(arr, arr_sq).T

    x = hist[0]
    y = hist[1]
    y_sq = hist[2]

    d = {'x': hist[0], 'y': hist[1], 'y_sq': hist[2]}
    df = pd.DataFrame(data=d)

    y_err, y_std = calc_error(y, y_sq, n_indep)

    df["n"] = y / n_indep
    df["n_err"] = y_err
    df["n_std"] = y_std

    total_time_hr = (t_flipping + t_measuring) / (1_000_000_000 * 3600)

    measuring_percentage = (t_measuring / t_flipping) * 100

    time_per_sweep = total_time_hr / n_indep * 3600

    acceptance_rate = n_accepted / n_iterations * 100

    time_per_flip_us = ((t_flipping) / n_iterations) / 1000

    return [df, {"n_indep": n_indep, "n_accepted": n_accepted, "n_iterations": n_iterations, "n_avg_autocorr_sweeps": n_avg_autocorrelationtime_sweeps, "t_flipping": t_flipping, "t_measuring": t_measuring, "n": n, "q": q, "n_faces": 2*n, "total_time (hr)": total_time_hr, "measuring_percentage (%)": measuring_percentage, "time_per_sweep (s)": time_per_sweep, "time_per_flip (us)": time_per_flip_us, "acceptance_rate (%)": acceptance_rate}]

def calc_error(arr, arr_sq, N):
    return [1/(np.sqrt(N))*np.sqrt(arr_sq/N - ((1/N)*arr)**2), np.sqrt(arr_sq/N-((1/N)*arr)**2)]
