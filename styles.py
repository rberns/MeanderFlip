import matplotlib.pyplot as plt
from colors import *
import seaborn as sns

sns.set_context("paper")
sns.set_theme()
sns.set_style("ticks")
sns.set_palette(sns.color_palette("RdBu_r"))

plt.rcParams.update({'axes.linewidth': 1.0,
 'grid.linewidth': 0.8,
 'lines.linewidth': 0.8,
 'lines.markersize': 4.8,
 'patch.linewidth': 0.8,
 'xtick.major.width': 1.0,
 'ytick.major.width': 1.0,
 'xtick.minor.width': 0.8,
 'ytick.minor.width': 0.8,
 'xtick.major.size': 4.8,
 'ytick.major.size': 4.8,
 'xtick.minor.size': 3.2,
 'ytick.minor.size': 3.2,
 'font.size': 9,
 'axes.labelsize': 9,
 'axes.titlesize': 9,
 'xtick.labelsize': 7,
 'ytick.labelsize': 7,
 'legend.fontsize': 8,
 'legend.title_fontsize': 9.6})

#palette = sns.color_palette("tab10", as_cmap=True)
