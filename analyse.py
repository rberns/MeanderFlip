# %%

import matplotlib.pyplot as plt
import numpy as np
import json
import pandas as pd

# %%

data = None
pathss = None

with open('data.json') as file_:
    data_ = json.loads(file_.read())
    data = data_["n_paths"]
    pathss = np.array(data_["pathss"])
    


#print(data)

# %%

plt.hist(data, bins=np.arange(50))
# %%

#pairs_even = np.split(pathss, len(pathss)/2)
#pairs_odd = np.split(pathss[1:len(pathss)-1], (len(pathss)-2)/2)

#hash_pairs = [np.array2string(pair) for pair in pairs_odd]
#hash_pairs += [np.array2string(pair) for pair in pairs_even]

#print(pairs_even, pairs_odd)

items = [str(e) for e in pathss]

df = pd.DataFrame(data={'pairs': items})

labels, levels = pd.factorize(df['pairs'])
# %%

labels


# %%
